# Rename this file to `sistr-config.py` and fill out with correct info
SECRET_KEY = 'super secret key'

# MAIL_SERVER = 'smtp.gmail.com'
MAIL_SERVER = ''
MAIL_PORT = 587
MAIL_USE_TLS = True
MAIL_USE_SSL = False
MAIL_USERNAME = 'someone@email.com'
MAIL_PASSWORD = 'email_password'
MAIL_DEFAULT_SENDER = 'someone@email.com'
MAIL_MAX_EMAILS = None

PUBLIC_SISTR_USER_NAME = 'sistr'

ADMIN_EMAILS = [
	'someone@email.com'
]

MIST_MARKERS_FILES = {'MLST': 'mist_assays/MLST.markers',
                      'wgMLST_330': 'mist_assays/wgMLST_330.markers',
                      'rMLST': 'mist_assays/rMLST.markers', }

MIST_ALLELES_DIR = 'mist_assays/alleles'

QUAST_BIN_PATH = '/home/peter/bioinf/quast-2.3/quast.py'

MASH_BIN_PATH = '/home/peter/bin/mash'

TEMP_DIR = '/tmp'
CACHE_TYPE = 'redis'
CACHE_KEY_PREFIX = 'SISTR'

DATABASE_URI = 'postgresql://sistr:sistr_password@localhost:5432/sistr_db'
TEST_DATABASE_URI = 'postgresql://sistr:sistr_password@localhost:5432/sistr_db'

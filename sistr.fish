#!/usr/bin/env fish
#
# Usage:
# $ cd /path/to/sistr_backend
# $ . sistr.fish [f|flaskrun] [c|celery]
# 
# Example:
# Activate virtualenv and export necessary envars for sistr_backend
# $ . sistr.fish
# 
# Start Python Flask server
# $ . sistr.fish f
# 
# Start Leiningen Figwheel for Clojurescript dev
# $ . sistr.fish l
# 
# Start Celery task queue
# $ . sistr.fish c
# 
# Start Celery beat periodic task queue
# $ . sistr.fish b
# 
# Run tests
# $ . sistr.fish t
# 

source .venv/bin/activate.fish
echo "Virtualenv activated!"
set CURR_DIR (pwd)
set -x PYTHONPATH $CURR_DIR
set -x SISTR_APP_SETTINGS "$CURR_DIR/sistr-config.py"
echo "Env vars PYTHONPATH and SISTR_APP_SETTINGS exported!"
echo "PYTHONPATH=$PYTHONPATH"
echo "SISTR_APP_SETTINGS=$SISTR_APP_SETTINGS"

for arg in $argv
	switch "$arg"
	case i ipython
		echo "Starting IPython session"
		ipython --config "$CURR_DIR/init_ipy.py"
	case f flaskrun
		echo "Starting Flask dev server"
		set -x FLASK_APP "$CURR_DIR/run.py"
		set -x FLASK_DEBUG 1
		flask run 
	case c celery 
		echo "Starting Celery"
		celery worker -A app.tasks 
	case b beat
		echo "Starting Celery Beat - Periodic Task Scheduler"
		celery beat -A app.tasks 
	case l figwheel
		echo "Starting lein figwheel for frontend dev"
		echo "NOTE: Start Flask app server at localhost:5000"
		echo "Open browser at localhost:3449 and localhost:5000"
		echo "figwheel will be active in both sessions"
		cd web/
		lein figwheel
	case t pytest
		echo "Running all backend tests"
		pytest -v tests/
	case *
		echo "Unknown arg specified"
	end
end

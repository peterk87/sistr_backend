## *Salmonella In Silico* Typing Resource (SISTR) backend Python Flask app 

The [*Salmonella In Silico* Typing Resource (SISTR)](http://lfz.corefacility.ca/sistr-app/) is a web application for the rapid *in silico* typing and *in silico* serovar prediction of *Salmonella enterica* isolates using whole-genome sequence (WGS) data (seems to work well for *S. bongori* as well).
In addition, it allows various metadata-driven comparative genomic and epidemiological analyses through the [SISTR web application](https://bitbucket.org/peterk87/sistr-app).

### Citation 

The *Salmonella In Silico* Typing Resource (SISTR): an open web-accessible tool for rapidly typing and subtyping draft *Salmonella* genome assemblies. Catherine Yoshida, Peter Kruczkiewicz, Chad R. Laing, Erika J. Lingohr, Victor P.J. Gannon, John H.E. Nash, Eduardo N. Taboada. PLoS ONE 11(1): e0147101. doi: 10.1371/journal.pone.0147101

Paper Link: http://journals.plos.org/plosone/article?id=10.1371/journal.pone.0147101

BibTeX:

```
@article{Yoshida2016,
  doi = {10.1371/journal.pone.0147101},
  url = {http://dx.doi.org/10.1371/journal.pone.0147101},
  year  = {2016},
  month = {jan},
  publisher = {Public Library of Science ({PLoS})},
  volume = {11},
  number = {1},
  pages = {e0147101},
  author = {Catherine E. Yoshida and Peter Kruczkiewicz and Chad R. Laing and Erika J. Lingohr and Victor P. J. Gannon and John H. E. Nash and Eduardo N. Taboada},
  editor = {Michael Hensel},
  title = {The Salmonella In Silico Typing Resource ({SISTR}): An Open Web-Accessible Tool for Rapidly Typing and Subtyping Draft Salmonella Genome Assemblies},
  journal = {{PLOS} {ONE}}
}
```

### Quick Tech Overview

SISTR consists of a [Python (v2.7+)](https://www.python.org/) server application and [PostgreSQL (v9.4+)](http://www.postgresql.org/) database and a [ClojureScript (v0.0-3xxx)](https://github.com/clojure/clojurescript) web application.
The server app is implemented in Python using the [Flask web micro web framework (v0.10.x)](http://flask.pocoo.org/) and communicates with the PostgreSQL database using [SQLAlchemy (v1.x.x)](http://www.sqlalchemy.org/).
The server app exposes a REST API through which the user facing [SISTR web application](https://bitbucket.org/peterk87/sistr-app) sends and receives data.
It is also possible for other applications to send data to and from the server app through the REST API.
A [Celery distributed task queue (v3.1.x)](https://celery.readthedocs.org/en/latest/index.html) is used for asynchronously running tasks such as in silico analyses on user uploaded genomes.

The source code for SISTR-backend is available on Bitbucket at:

https://bitbucket.org/peterk87/sistr_backend

### Documentation

The documentation for the SISTR backend Python Flask app can be found in `docs/` and can be compiled with [Sphinx](http://www.sphinx-doc.org/en/stable/) or you can visit

https://lfz.corefacility.ca/sistr-backend-docs/


### Issues

If you encounter any problems or have any questions feel free to create an issue anonymously or not to let us know so we can address it!


### License

Copyright © 2016 Public Health Agency of Canada

Distributed under the GNU Public License version 3.0

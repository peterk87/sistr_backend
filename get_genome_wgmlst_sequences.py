#!.venv/bin/python

__author__ = 'piotr'
from app import Base
from sqlalchemy.orm import sessionmaker, scoped_session
from sqlalchemy.engine import create_engine
from sqlalchemy import and_
from app.models import MistMarkerResult, MistMarker, Genome, MistAllele, MistTest

import os

engine = create_engine('postgresql://sistr:sistr_password@localhost:5433/sistr_test_db', use_native_hstore=True)

# Base.metadata.drop_all(bind=engine)
Base.metadata.create_all(engine)

session = scoped_session(sessionmaker(bind=engine))

g = session.query(Genome).first()
print g.mist_marker_results.count()

fasta_outdir = 'wgMLST_330-genome_allele_sequences'
try:
    os.mkdir(fasta_outdir)
except:
    import shutil

    shutil.rmtree(fasta_outdir)
    os.mkdir(fasta_outdir)

query_stmt = session.query(Genome.name, MistMarker.name, MistAllele.seq) \
    .join(Genome.mist_marker_results) \
    .join(MistTest, MistTest.id == MistMarkerResult.test_id) \
    .join(MistMarker, MistMarker.id == MistMarkerResult.marker_id) \
    .join(MistAllele,
          and_(MistMarkerResult.marker_id == MistAllele.marker_id, MistAllele.name == MistMarkerResult.result)) \
    .filter(MistTest.name == 'wgMLST_330') \
    .order_by(MistMarker.name)

marker_genome_seq = {}
for genome, marker, allele_seq in query_stmt.all():
    with open(os.path.join(fasta_outdir, marker + '.fasta'), 'a') as f:
        f.write('>{0}\n{1}\n'.format(genome, allele_seq))





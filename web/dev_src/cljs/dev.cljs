(ns sistr-app.dev
  (:require
    [devtools.core :as devtools]
    [figwheel.client :as fw]
    [sistr-app.core]
    ))

(enable-console-print!)

(devtools/install!)
(js/console.log (range 200))

(fw/start
  {:websocket-url "ws://localhost:3449/figwheel-ws"}
  )

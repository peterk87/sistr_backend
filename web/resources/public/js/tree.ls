
# _ = require 'prelude-ls'

export class PhyloTree

  get_plot_dim: ->
    w = @options.width 
      or d3.select @container_el_id .style \width 
      or d3.select @container_el_id .attr \width
    h = @options.height 
      or d3.select @container_el_id .style \height
      or d3.select @container_el_id .attr \height
    w = parseInt w
    h = parseInt h
    @_width := w
    @_height := h

  get_tree: ->
    @dist_sum_max := 0
    d3.layout.cluster!
        .size [ @_height, @_width ]
        # .sort -> if it.children then it.children.length else -1
        .children -> it.children
        .separation (a, b) ~> 
          gs = @options.genome_groups
          (gs[a.name].length + 1) + (gs[b.name].length + 1)

  (@container_el_id, @tree_data, @options) ->
    @container := d3.select @container_el_id
    @container$ := document.getElementById @container.attr \id
    @get_plot_dim!
    @margin := [20,20,100,20]
    @_height := @_height - @margin.1 - @margin.3
    @_width := @_width - @margin.0 - @margin.2
    @_tree := @get_tree!
    @selected := []

    @_diagonal := @rightAngleDiagonal!

    @_nodes := @_tree @tree_data
    @xscale := @scaleBranchLengths @_nodes, @_width
    leaf_dists = @_nodes.map -> unless it.children then it.rootDist else null 
    
    
    min_leaf_dist = d3.min leaf_dists
    max_leaf_dist = d3.max leaf_dists
    
    @_svg := d3.select @container_el_id
           .append "svg:svg"
           .attr \width @_width + @margin.0 + @margin.2
           .attr \height @_height + @margin.1 + @margin.3
    @_vis := @_svg
           .append "svg:g"
           .attr \transform "translate(20, 20)"
    derp_dists = @_nodes.map -> unless it.children then it.branch_length else null
    genome_group_sizes = [@options.genome_groups[k].length + 1 for k of @options.genome_groups]

    max_derp = d3.max  genome_group_sizes
    dist_sum = 2 * d3.sum genome_group_sizes
    @r_y_scale := d3.scale.linear!
      .domain [1, max_derp]
      .range [(1 / dist_sum * @_height), (max_derp / dist_sum * @_height)]
    @yscale := d3.scale.linear!
      .domain [0, @_height]
      .range [0, @_height]
    @zoom := d3.behavior.zoom!
      .x @xscale
      .y @yscale
      .on \zoom !~> @zoomed!
    @_svg.call @zoom

    @_link = @_vis
      .selectAll "path.link" 
      .data @_tree.links @_nodes
        .enter!
        .append "svg:path"
        .attr \class \link
        .attr \d @_diagonal
        .attr \fill \none
        .attr \stroke \black
        .attr \stroke-width \1px

    @_node = @_vis.selectAll "g.node"
      .data @_nodes
      .enter!
      .append "svg:g"
      .attr "class", (it) ->
        if it.children
          if it.depth is 0
            "root node"
          else
            "inner node"
        else
          "leaf node"
      .attr "transform", ~> "translate(#{@xscale(it.rootDist)},#{@yscale(it.x)})"

    @pie := d3.layout.pie!
      .sort null
      .value ->
        it.value

    @arc := d3.svg.arc!
      .outerRadius ~>
        group_size = @options.genome_groups[it.data.name].length + 1
        @r_y_scale group_size
      .innerRadius 0

    unless options.skipLabels
      @leaves := @_vis.selectAll "g.leaf.node" 

      @leaf-pies := @leaves.append "svg:g"
        .attr \class \leaf-pie
        .attr \transform ~> "translate(#{5 + @r_y_scale @options.genome_groups[it.name].length + 1},0)"
        # .attr \cx ~> @r_y_scale @options.genome_groups[it.name].length + 1
        # .attr \fill \black
        # .attr \r ~> @r_y_scale @options.genome_groups[it.name].length + 1
        # .style \fill-opacity 0.2
        .on \click (d) !~> 
          console.log "leaf-pies click", d
          d.selected = !d.selected 
          selected = []
          @leaf-pies.classed \selected, -> 
            if it.selected
              selected.push it.name
            it.selected
          @selected := selected
          evt = new Event \change
          @container$.dispatchEvent evt
    

  zoomed: !->
    @_node
      .attr "transform", ~> 
        "translate(#{@xscale(it.rootDist)},#{@yscale(it.x)})"
    @arc := d3.svg.arc!
      .outerRadius ~>
        group_size = @options.genome_groups[it.data.name].length + 1
        @zoom.scale! * (@r_y_scale group_size)
      .innerRadius 0
    @leaf-pies
      .attr \transform ~> 
        "translate(#{5 + @zoom.scale! * (@r_y_scale @options.genome_groups[it.name].length + 1)},0)"
    @leaf-pies.selectAll \path
      .attr \d @arc
    # @_vis.selectAll "g.leaf.node"
    #   .selectAll "circle"
    #   .attr \cx ~> @zoom.scale! * @r_y_scale @options.genome_groups[it.name].length + 1 
    #   .attr \r ~> @zoom.scale! * @r_y_scale @options.genome_groups[it.name].length + 1
    @_link.attr \d @_diagonal
    

  
  # Since this is just based on the angle of the right triangle formed
  # by the coordinate and the origin, each quad will have different 
  # offsets
  scaleBranchLengths: (nodes, w) ->
    
    # Visit all nodes and adjust y pos width distance metric
    visitPreOrder = (root, callback) !->
      callback root
      if root.children
        for child in root.children by -1
          visitPreOrder child, callback
      

    visitPreOrder nodes[0], (node) !->
      node.rootDist = ((if node.parent then node.parent.rootDist else 0)) + (node.branch_length or 0)   

    rootDists = nodes.map -> it.rootDist
    
    yscale = d3.scale.linear!
      .domain [0, d3.max(rootDists)]
      .range [0, w]
    
    yscale

  rightAngleDiagonal: ->
    diagonal = (it) ~>
      source = it.source
      target = it.target
      pathData = [
        source
        {
          x: target.x
          rootDist: source.rootDist
        }
        target
      ]
      pathData = pathData.map projection
      p = path pathData
      return p
    projection = ~> [ @xscale(it.rootDist), @yscale(it.x) ]

    path = -> "M#{it.0} #{it.1} #{it.2}"
    diagonal

  highlight_leaves: (leaves) !->
    return unless leaves?

    @leaves.classed \table-selected, -> 
      leaves.indexOf it.name != -1

  clear_selection: !->
    @selected := []
    @leaf-pies.classed \selected, -> 
      it.selected = false
      false
    evt = new Event \change
    @container$.dispatchEvent evt


  get_pie_node_data: (d, category='source_type') ~>
    strains = [d.name] ++ @options.genome_groups[d.name]
    category_types = []
    for strain in strains
      strain_metadata = @options.md[strain]
      unless strain_metadata?
        console.log 'genome', strain, strain_metadata
      else
        category_types.push strain_metadata[category]

    leftovers = {'leftovers':0}
    type_counts = _.countBy category_types
    for type, count of type_counts
      if type of @group_colour_dict
        leftovers[type] = count
      else
        leftovers['leftovers'] = leftovers['leftovers'] + count
    @pie [{value:count, type:type, name:d.name} for type, count of leftovers]

  get_group_colour: (group) ->
    if group of @group_colour_dict
      return @group_colour_dict[group]
    '#ddd'

  update_highlighted_groups: (category, groups, colours) !->
    @update_legend category, groups, colours
    @group_colour_dict := {}
    for d, i in groups
      @group_colour_dict[d] = colours[i]

    leaf_paths = @leaf-pies.selectAll \path
      .data (~> @get_pie_node_data it, category), ( -> it.data.type)

    leaf_paths.enter!
      .append \path
      .attr \d @arc
      .attr \fill ~>
        @get_group_colour it.data.type

    leaf_paths
      .attr \d @arc
      .attr \fill ~>
        @get_group_colour it.data.type

    leaf_paths.exit!remove!


    # @_node_elements.selectAll \text .remove!

    # node_text = @_node_elements.append \text
    #   .attr \class \node-group-number
    #   .attr \dy ".71em"
    #   .attr \text-anchor \middle
    #   .attr \font-size ~>
    #     font_size = 6 + Math.pow @data.grouped_nodes[it.id].length, @size_power
    #     "#{font_size}px"
    #   .attr \font-family \sans-serif
    #   .attr \transform ~>
    #     font_size = 6 + Math.pow @data.grouped_nodes[it.id].length, @size_power
    #     "translate(0,#{-font_size/3})"
    #   .text ~> 
    #     1 + @data.grouped_nodes[it.id].length

    # @node_text := node_text
    
    # @node_text.style \opacity if @_show_node_sizes then 1 else 0

  update_legend: (title, groups, colours) !->
    if groups is null or colours is null or groups.length is 0 or colours.length is 0
      # remove legend if there are no groups highlighted
      @_svg.selectAll \.legend
        .transition!
        .style \fill-opacity 0
        .remove!
      @_svg.selectAll \.legend-title 
        .remove!
    else #if groups.length == colours.length
      # update legend with new group data and colours
      legend_data = []
      
      /*
      Check if single group specified
      */
      if typeof groups is \string
        datum =
          group: groups
          colour: colours
        legend_data.push datum
      else
        /*
        If more than one group is highlighted
        */
        for i til groups.length
          datum =
            group: groups[i]
            colour: colours[i]
          legend_data.push datum

      /*
      Remove the old legend
      */
      @_svg.selectAll \.legend .remove!

      /*
      Create new legend
      */
      legend = @_svg
        .append \g
        .attr \class \legend

      legend_background = legend.append \rect
        .attr \fill \white
        .attr \fill-opacity 0.8
        .style \stroke \lightgray
        .style \stroke-width \2px
      


      legend_items = legend.selectAll \.legend-item
        .data legend_data, -> it.group

      legend_items = legend_items.enter!append \g
        .attr \class \legend-item
        .attr \transform (d, i) -> 
          "translate(0,#{(i+1) * 20 + 10})"

      /*
      Create rect elements with group colours
      */
      legend_items.append \rect 
        .attr \x 0
        .attr \width 18
        .attr \height 18
        .style \fill -> it.colour
      
      /*
      Create text elements with group names
      */
      legend_items.append \text
        .attr \x 22
        .attr \y 9
        .attr \dy ".35em"
        .style \text-anchor \start
        .text -> it.group

      /*
      Update the legend title
      */
      legend.selectAll \.legend-title .remove!
      legend.append \text
        .attr \class \legend-title
        .attr \x 22
        .attr \y 20
        .attr \font-weight \bold
        .text title

      /*
      Get the width of the legend g element
      */
      legend_dim = legend[0][0].getBBox!
      
      legend_background
        .attr \x -5
        .attr \width legend_dim.width + 10
        .attr \height legend_dim.height + 20

      /*
      Translate the legend group over to the right side of the plot area
      */
      legend.attr \transform, "translate(#{@_width - legend_dim.width - 10 + @margin.2}, 10)"
(ns ^:figwheel-load sistr-utils.utils
  (:require-macros [cljs.core.async.macros :refer [go go-loop]])
  (:require
    [cljsjs.d3]
    [goog.events :as events]
    [reagent.core :refer [atom]]
    [cljs.core.async :refer [put! chan <!]]
    [inflections.core :refer [plural]]
    [ajax.core :refer [GET POST PUT]]))

;==================
; nREPL in Cursive
;==================
(comment
  (do
    (use 'figwheel-sidecar.repl-api)
    (cljs-repl)))

;================================
; URL Query String manipulation
;================================

(defn parse-query-string
  "Parse URL query string into map with keywordized keys"
  []
  (let [url (.-location js/document #_"?token=whatever&user=someone&api-url=http://localhost:8000/api/")]
    (->> (-> url
             (clojure.string/split #"\?")
             last
             (clojure.string/split #"\&"))
         (map #(clojure.string/split % #"="))
         (map (fn [[k v]] [(keyword k) v]))
         (into {}))))

(defn get-user-from-query-string
  "Get the SISTR app user from the URL query string"
  [default-user]
  (if-let [[_ user] (re-matches #".*\?.*user=(\w+)" (str (.-location js/document)))]
    user
    default-user))

(defn set-user-query-string!
  "Update the user's URL to include a query string with user=`sistr-user`"
  [user]
  (js/history.replaceState nil "" (str "?user=" user)))

(defn clear-query-string! []
  (js/history.replaceState nil "" "?"))

;======================
; DOM helper functions
;======================
(defn by-id [el] (.getElementById js/document el))

(defn listen
  "Listen for event `type` on DOM element `el`"
  [el type]
  (let [out (chan)]
    (events/listen el type
                   (fn [e] (put! out e)))
    out))

;==============
; Math helpers
;==============
(defn round
  "Round a number, `n`, to `places` number of decimal places"
  ([n] (js/Math.round n))
  ([n places]
   (let [p (js/Math.pow 10 places)]
     (/ (js/Math.round (* p n)) p))))

(defn sci-not
  "Scientific notation (e.g. 0.000135 -> 1.35e-4)"
  [n places]
  (let [small? (< n 1)
        op (if small? * /)]
    (loop [x n
           step 0]
      (cond
        (zero? x) (str x)
        (and small? (zero? (js/Math.floor x))) (recur (op x 10)
                                                      (inc step))
        (and (not small?) (> x 10)) (recur (op x 10)
                                           (inc step))
        :else (str (round x places) "e" (when small? "-") step)))))

(defn clamp [min v max]
  (cond
    (< v min) min
    (> v max) max
    :else v))

;===========================
; Generic utility functions
;===========================
(defn keyword->str [x] (->> x str rest (apply str)))

(defn all-true? [v] (every? true? v))

(defn not-empty? [x] (not (empty? x)))

(defn in?
  "true if seq contains elm"
  [seq elm]
  (some #(= elm %) seq))

(defn sorted-freqs [xs]
  (let [m (->> xs (frequencies))]
    (into (sorted-map-by #(* -1 (compare [(get m %1) %1] [(get m %2) %2]))) m)))

(defn pluralize
  "Pluralize a word `w` based on a quanitity `q` using inflections.core plural"
  [w n]
  (if (= 1 n) w (plural w)))


;=================================
; Collection manipulation
;=================================
(defn transpose
  "Transpose a vector of vectors"
  [x]
  (apply mapv vector x))

(defn map-lists->list-maps
  "Transform a map of lists to a list of maps."
  [x]
  (let [ks (->> x keys)
        vs (->> x vals)
        t-vs (transpose vs)]
    (map #(zipmap ks %) t-vs)))

(defn remove-map-nil-vals
  "Remove nil values from a hashmap"
  [coll]
  (map #(into {} (filter (complement (comp nil? val)) %)) coll))

(defn list-maps->map-lists
  "Transform a list of maps to a map of lists"
  [x]
  (let [ks (->> x first keys)
        vs (->> x (map vals))
        t-vs (transpose vs)]
    (zipmap ks t-vs)))

(defn remove-forbidden-keys
  "Return collection (e.g. hashmap) without certain keys"
  [forbidden-keys coll]
  (remove (fn [[k _]]
            (some #(= k %) forbidden-keys))
          coll))

(defn remove-keys-by-regex
  "Return collection without keys matching a regex"
  [regex coll]
  (remove (fn [[k _]]
            (re-matches
              (if (regexp? regex) regex (re-pattern regex))
              (keyword->str k)))
          coll))

(defn regex-rename-keys
  "Return collection with renamed keys based on regex"
  [regex coll]
  (map (fn [[k v]]
         (let [m (re-matches
                   (if (regexp? regex) regex (re-pattern regex))
                   (keyword->str k))]
           (if m
             [(keyword (last m)) v]
             [k v])))
       coll))

;===============================
; LocalStorage
;===============================
(defn store [k obj]
  (.setItem js/localStorage k (js/JSON.stringify (clj->js obj))))

(defn keywordify [m]
  (cond
    (map? m) (into {} (for [[k v] m] [(keyword k) (keywordify v)]))
    (coll? m) (vec (map keywordify m))
    :else m))

(defn fetch [k default]
  (let [item (.getItem js/localStorage k)]
    (if item
      (-> (.getItem js/localStorage k)
          (or (js-obj))
          (js/JSON.parse)
          (js->clj)
          (keywordify))
      default)))


;===================
; Logging functions
;===================
(defn log-http-error
  "Log HTTP GET/PUT/POST errors"
  [http-errors key resp-error & {:keys [message uri http-method]
                                 :or   {message     "Error occurred while retrieving data from server."
                                        uri         ""
                                        http-method "GET"}}]
  (swap! http-errors
         assoc
         (keyword key)
         {:uri         uri
          :http-method http-method
          :message     message
          :response    resp-error
          :timestamp   (js/Date.)
          :error       (get-in resp-error [:response :error])
          :status      (:status resp-error)}))

(defn log-http
  "Log HTTP GET/PUT/POST requests"
  [http-log key resp & {:keys [message uri http-method]
                        :or   {message     ""
                               uri         ""
                               http-method "GET"}}]
  (swap! http-log
         conj
         {:key         (keyword key)
          :uri         uri
          :http-method http-method
          :message     message
          :response    resp
          :timestamp   (js/Date.)
          :error       (get-in resp [:response :error])
          :status      (:status resp)}))

;==========================
; Data retrieval and saving
;==========================

(defn map->csv-text
  "Create a CSV encoded URI for some `data` with some specified `headers`.
  `data` must be a ISeq of maps with keys `headers`.
  Return the CSV encoded URI for user download."
  ([data] (map->csv-text data (->> data first keys sort)))
  ([data headers]
   (let [header-string (->> headers
                            (map keyword->str)
                            (map #(str \" % \"))
                            (interpose ",")
                            (apply str))
         data-string (->> data
                          (map (fn [x]
                                 (map #(get x %) headers)))
                          (map (fn [l]
                                 (->> l
                                      (map #(if (nil? %) "" %))
                                      (map #(str \" % \"))
                                      (interpose ",")
                                      (apply str)
                                      )))
                          (interpose "\n")
                          (apply str))]
     (str header-string "\n" data-string))))

(defn text->blob->obj-url [text & {:keys [type]
                                   :or   {type "text/csv;charset=utf-8"}}]
  (let [blob (new js/Blob (clj->js [text]))]
    (js/window.URL.createObjectURL blob (clj->js {"type" type}))))

(defn get-base-uri
  "Get the user's base URI for retrieving data from backend server."
  [api-uri user]
  (str api-uri "user/" user))

(defn create-temp-user
  "Create a new temporary user with a randomly generated name."
  [api-uri user]
  (let [c (chan)]
    (POST (get-base-uri api-uri user)
          {:response-format :json
           :keywords?       true
           :handler         #(put! c (:name %))
           :error-handler   #(put! c {:error %})})
    c))

(defn create-registered-user
  [api-uri username password & {:keys [email]}]
  (let [c (chan)]
    (POST (get-base-uri api-uri username)
          {:response-format :json
           :format          :json
           :keywords?       true
           :handler         #(put! c %)
           :error-handler   #(put! c {:error %})
           :params          {:password password
                             :email    email}})
    c))

(defn auth-headers
  ([token] (auth-headers token "x"))
  ([user password] {"Authorization" (str "Basic " (js/btoa (str user ":" password)))}))

(defn get-auth-token
  ([uri token] (get-auth-token uri token "x"))
  ([uri user password]
   (let [c (chan)]
     (GET uri {:handler         #(put! c (:token %))
               :error-handler   #(put! c {:error %})
               :response-format :json
               :keywords?       true
               :headers         (auth-headers user password)})
     c)))


(defn get-uri->chan
  "Send a GET request to a URI and receive the success/error response to a
  cljs.core.async channel.
  Return the channel."
  [uri & {:keys [headers params keywords?]
          :or   {headers   {}
                 params    {}
                 keywords? true}}]
  (let [c (chan)]
    (GET uri {:handler         #(put! c (if (nil? %) {} %))
              :error-handler   #(put! c {:error %})
              :response-format :json
              :params          params
              :keywords?       keywords?
              :headers         headers})
    c))

(defn put->chan
  [uri & {:keys [data headers keywords?]
          :or   {data      {}
                 headers   {}
                 keywords? true}}]
  (let [c (chan)]
    (PUT uri {:handler         #(put! c (if (nil? %) {} %))
              :error-handler   #(put! c {:error %})
              :format          :json
              :response-format :json
              :keywords?       keywords?
              :headers         headers
              :params          data})
    c))

(defn trigger-download!
  "Trigger download of some data string by creating an <a> HTML element,
  assigning the data as a js/Blob URLObject and triggering a click of that <a>
  element. <a> is removed afterwards"
  [data filename & {:keys [type]
                    :or    {type "text/csv;charset=utf-8"}}]
  (let [a (.createElement js/document "a")
        body (aget (.getElementsByTagName js/document "body") 0)]
    (aset a "href" (text->blob->obj-url data))
    (aset a "download" filename)
    (.appendChild body a)
    (.click a)
    (.removeChild body a)))

(defn get-user-selections!
  [base-uri user-name selections-atom log-atom & {:keys [headers] :or {headers {}}}]
  (go (let [uri (str base-uri user-name "/selections")
            resp (<! (get-uri->chan uri
                                    :headers headers))
            error-resp (:error resp)]
        (if error-resp
          (do
            (log-http log-atom
                      :selections error-resp
                      :message "Could not retrieve user genome selections"
                      :uri uri)
            )
          (do
            (log-http log-atom
                      :selections resp
                      :message "Retrieved user genome selections"
                      :uri uri)
            (reset! selections-atom resp))))))

(defn put-user-selections!
  [base-uri user-name selections log-atom & {:keys [headers] :or {headers {}}}]
  (go (let [uri (str base-uri user-name "/selections")
            resp (<! (put->chan uri
                                :data {:selections selections}
                                :headers headers))
            error-resp (:error resp)]
        (if error-resp
          (do
            (log-http log-atom
                      :selections error-resp
                      :message "Could not save user genome selections"
                      :uri uri
                      :http-method "PUT"))
          (do
            (log-http log-atom
                      :selections resp
                      :message "Saved user genome selections"
                      :uri uri
                      :http-method "PUT")
            ))
        )))

;======================================
; Color Schemes and Palettes
;======================================
; `color-brewer` contains color specifications and designs developed by Cynthia Brewer (http://colorbrewer.org/).
; JavaScript specs as packaged in the D3 library (d3js.org). Please see license at http://colorbrewer.org/export/LICENSE.txt
(def color-brewer
  {:Accent      ["#7fc97f" "#beaed4" "#fdc086" "#ffff99" "#386cb0" "#f0027f" "#bf5b17" "#666666"]
   :Dark2       ["#1b9e77" "#d95f02" "#7570b3" "#e7298a" "#66a61e" "#e6ab02" "#a6761d" "#666666"]
   :Paired      ["#a6cee3" "#1f78b4" "#b2df8a" "#33a02c" "#fb9a99" "#e31a1c" "#fdbf6f" "#ff7f00" "#cab2d6" "#6a3d9a" "#ffff99" "#b15928"]
   :Pastel1     ["#fbb4ae" "#b3cde3" "#ccebc5" "#decbe4" "#fed9a6" "#ffffcc" "#e5d8bd" "#fddaec" "#f2f2f2"]
   :Pastel2     ["#b3e2cd" "#fdcdac" "#cbd5e8" "#f4cae4" "#e6f5c9" "#fff2ae" "#f1e2cc" "#cccccc"]
   :Set1        ["#e41a1c" "#377eb8" "#4daf4a" "#984ea3" "#ff7f00" "#ffff33" "#a65628" "#f781bf" "#999999"]
   :Set2        ["#66c2a5" "#fc8d62" "#8da0cb" "#e78ac3" "#a6d854" "#ffd92f" "#e5c494" "#b3b3b3"]
   :Set3        ["#8dd3c7" "#ffffb3" "#bebada" "#fb8072" "#80b1d3" "#fdb462" "#b3de69" "#fccde5" "#d9d9d9" "#bc80bd" "#ccebc5" "#ffed6f"]
   :category20  (-> (js/d3.scale.category20) (.range) (vec))
   :category20b (-> (js/d3.scale.category20b) (.range) (vec))
   :category20c (-> (js/d3.scale.category20c) (.range) (vec))})

(defn qual-colours
  "Fetch appropriate qualitative colour scheme for ``n``."
  [n]
  (cond
    (>= n 21) (let [ordered-palettes (distinct (concat
                                                 [:category20 :Set1 :Set3 :Paired]
                                                 (keys color-brewer)))
                    all-colors (flatten
                                 (map #(get color-brewer %) ordered-palettes))]
                (fetch :qual-colours-21 all-colors))
    (>= n 13) (fetch :qual-colours-20 (:category20 color-brewer))
    (>= n 10) (fetch :qual-colours-12 (:Paired color-brewer))
    (>= n 1) (fetch :qual-colours-9 (:Set1 color-brewer))))

(defn cgmlst-matching-color-scale [n]
  (let [n-matching (clj->js [0 100 200 250 275 300 310 315 320 325 330])
        colours (clj->js ["#a50026" "#d73027" "#f46d43" "#fdae61" "#fee08b" "#ffffbf" "#d9ef8b" "#a6d96a" "#66bd63" "#1a9850" "#006837"])
        color-scale (->
                      (js/d3.scale.linear)
                      (.domain n-matching)
                      (.range colours))]
    (color-scale n)))

(defn hex->rgb-vec [c]
  (let [hex-colour (clojure.string/replace c #"[^0-9a-zA-Z]" "")
        colour-int (js/parseInt hex-colour 16)
        r (-> colour-int (bit-shift-right 16) (bit-and 255))
        g (-> colour-int (bit-shift-right 8) (bit-and 255))
        b (-> colour-int (bit-and 255))]
    [r g b]))

(defn color-plate [color]
  [:div.color-plate {:style {:display          "inline-block"
                             :height           20
                             :width            20
                             :background-color color
                             :border           "black 1px solid"}}])

(defn color-palette-row [palette-name colours]
  [:div.row.color-palette-row
   {:on-click #(prn "clicked color-palette-row" palette-name colours)}
   [:div.col-md-1
    [:p (keyword->str palette-name)]
    ]
   [:div.col-md-8
    [:div.color-palette
     (for [c colours]
       ^{:key [palette-name c]}
       [color-plate c]
       )]]])

(defn color-palettes []
  (let [palette-names (->> color-brewer
                           keys
                           sort)]
    [:div.container
     (for [palette-name palette-names]
       ^{:key palette-name}
       (color-palette-row palette-name (get color-brewer palette-name)))]))
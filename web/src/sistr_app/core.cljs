(ns ^:figwheel-always sistr-app.core
  (:require-macros [cljs.core.async.macros :refer [go go-loop]])
  (:require
    [cljsjs.react]
    [cljsjs.jquery]
    [cljsjs.d3]
    [cljsjs.selectize]
    [cljsjs.dimple]
    [cljsjs.leaflet]
    [cljsjs.bootstrap]
    [cljs.core.async :refer [put! chan <! timeout]]
    [sistr-utils.utils :as utils]
    [sistr-data.data :as data]
    [reagent.core :as reagent :refer [atom]]
    [sistr-app.pages.temporal :as temporal-epi-page]
    [sistr-app.pages.about :as about-page]
    [sistr-app.pages.geomap :as geomap-page]
    [sistr-app.pages.genomes :as genomes-page]
    [sistr-app.pages.upload :as upload-page]
    [sistr-app.pages.mst :as mst-page]
    [sistr-app.pages.results :as results-page]
    [sistr-app.pages.tree :as tree-page]
    [sistr-app.pages.user :as user-page]
    [sistr-app.pages.login :as login-page]))

(enable-console-print!)

(defn label-genome-count [public? bs-style]
  (when-not (empty? @data/genome-name-metadata)
    (let [guest? (= "sistr" @data/sistr-user)
          n-total-genomes (count @data/genome-name-metadata)
          n-public-genomes (if guest?
                             n-total-genomes
                             (->> @data/genome-name-metadata
                                  vals
                                  (filter #(= "sistr" (get % :user_uploader)))
                                  count))
          n-user-genomes (- n-total-genomes n-public-genomes)
          ]
      (when (or (and public? (> n-public-genomes 0)) (and (not public?) (> n-user-genomes 0)))
        [:span.label.genome-count-label {:class (str "label-" bs-style)} (if public?
                                                                           (str n-public-genomes " public")
                                                                           (str n-user-genomes " user"))]))))

(defn labels-genome-counts []
  [:div.label-group
   [label-genome-count true "default"]
   [label-genome-count false "primary"]])

(defn uploading? []
  (when (> @data/upload-countdown 0)
    [:div.label-group
     [:span.label.genome-count-label
      {:class "label-primary"}
      [:i.fa.fa-spinner.fa-spin]
      (str " " @data/upload-countdown)]]))

(defn log-page []
  [:table.table.table-striped
   [:thead
    [:tr
     [:th "Time"]
     [:th "key"]
     [:th "Message"]
     [:th "HTTP"]]]
   [:tbody
    (map-indexed
      (fn [i {:keys [key uri http-method timestamp message status error]}]
        ^{:key i}
        [:tr
         [:td (str timestamp)]
         [:td (utils/keyword->str key)]
         [:td (when error [:strong.text-danger
                           (when status [:span.label.label-danger "HTTP " status]) " " error " "]) message]
         [:td [:strong http-method] " " [:em uri]]])
      (->> @data/app-log (sort-by :timestamp) reverse)
      )]])

(defn sistr-app []
  [:div
   [:nav {:class "navbar navbar-default navbar-fixed-top"
          :role  "navigation"}
    [:div.container-fluid
     [:div.navbar-header
      [:button {:type          "button"
                :class         "navbar-toggle collapsed"
                :data-toggle   "collapse"
                :data-target   "#sistr-navbar-collapse"
                :aria-expanded false
                :aria-controls "navbar"}
       [:span.sr-only "Toggle navigation"]
       [:span.icon-bar] [:span.icon-bar] [:span.icon-bar]]
      [:a.navbar-brand {:href        "#about-page"
                        :data-toggle "tab"
                        :on-click    #(reset! data/active-page :about-page)}
       "SISTR"]
      ]
     [:div {:class "collapse navbar-collapse"
            :id    "sistr-navbar-collapse"}
      [:ul {:class "nav navbar-nav"}
       [:li
        {:class (if (= @data/active-page :upload-page) "active" "")}
        [:a {:href        "#upload-page"
             :data-toggle "tab"
             :on-click    #(reset! data/active-page :upload-page)
             }
         [:i.fa.fa-upload] " Upload"]]
       (when (> @data/upload-countdown 0)
         [:li [:div {:style {:margin-left   0
                             :margin-right  0
                             :margin-bottom 0
                             :margin-top    0}}
               [uploading?]]]
         )
       (when (data/logged-in? @data/sistr-user @data/sistr-user-token)
         [:li
          {:class (if (= @data/active-page :user-genome-page) "active" "")}
          [:a {:href        "#user-genome-page"
               :data-toggle "tab"
               :on-click    #(do
                              (reset! data/active-page :user-genome-page)
                              (reset! user-page/show-table? true)
                              (user-page/refresh-table!))}
           [:i.fa.fa-user-secret] " Your Genomes"]])
       [:li
        {:class (if (= @data/active-page :genomes-table-page) "active" "")}
        [:a {:href        "#genomes-table-page"
             :data-toggle "tab"
             :on-click    #(do
                            (reset! data/active-page :genomes-table-page)
                            (reset! genomes-page/show-table? true)
                            (genomes-page/refresh-table!))}
         [:i.fa.fa-table] " Genomes"
         ]]
       (when-not (empty? @data/genome-name-metadata)
         [:li [:div {:style {:margin-left   0
                             :margin-right  0
                             :margin-bottom 0
                             :margin-top    0}}
               [labels-genome-counts]]]
         )
       [:li
        {:class (if (= @data/active-page :results-page) "active" "")}
        [:a {:href        "#results-page"
             :data-toggle "tab"
             :on-click    #(do
                            (reset! data/active-page :results-page)
                            (reset! results-page/show-table? true)
                            (results-page/refresh-table!))}
         [:i.fa.fa-search] " Serovar Predictions"]]
       [:li.dropdown {:class (condp = @data/active-page
                               :tree-page "active"
                               :mst-page "active"
                               "")}
        [:a.dropdown-toggle {:href          "#"
                             :data-toggle   "dropdown"
                             :role          "button"
                             :aria-expanded "false"
                             }
         [:i.fa.fa-tree] " Phylogeny " [:span.caret]]
        [:ul.dropdown-menu {:role "menu"}
         [:li
          {:class (if (= @data/active-page :tree-page) "active" "")}
          [:a {:href        "#tree-page"
               :data-toggle "tab"
               :on-click    #(do
                              (reset! data/active-page :tree-page)
                              (reset! tree-page/show-tree? true)
                              (tree-page/set-phylocanvas-metadata!))}
           [:i.fa.fa-tree] " Dendrogram"]]
         [:li
          {:class (if (= @data/active-page :mst-page) "active" "")}
          [:a {:href        "#mst-page"
               :data-toggle "tab"
               ;; when the user goes to the MST tab, make sure everything looks alright
               ;; - resize the width of the MST plot to fill the parent element
               ;; - update the group highlighting and legend
               :on-click    #(do
                              (reset! data/active-page :mst-page)
                              (reset! mst-page/show-mst? true)
                              (mst-page/mst-refresh!))}
           [:i.fa.fa-pie-chart] " Minimum spanning tree (MST)"]]
         ]
        ]
       [:li.dropdown {:class (condp = @data/active-page
                               :map-page "active"
                               :epi-page "active"
                               "")}
        [:a.dropdown-toggle {:href          "#"
                             :data-toggle   "dropdown"
                             :role          "button"
                             :aria-expanded "false"}
         [:i.fa.fa-stethoscope] " Epi " [:span.caret]]
        [:ul.dropdown-menu {:role "menu"}
         [:li
          {:class (if (= @data/active-page :map-page) "active" "")}
          [:a {:href        "#map-page"
               :data-toggle "tab"
               :on-click    #(do
                              (reset! data/active-page :map-page)
                              (reset! geomap-page/show-map? true))}
           [:i.fa.fa-globe] " Geographical Distribution"]]
         [:li
          {:class (if (= @data/active-page :epi-page) "active" "")}
          [:a {:href        "#epi-page"
               :data-toggle "tab"
               :on-click    #(do
                              (reset! data/active-page :epi-page)
                              (reset! temporal-epi-page/show-plot? true))}
           [:i.fa.fa-clock-o] " Temporal Distribution"]]
         ]
        ]
       ]
      [:ul.nav.navbar-nav.navbar-right
       (if (data/logged-in? @data/sistr-user @data/sistr-user-token)
         [:li
          {:class (if (= @data/active-page :login-page) "active" "")}
          [:a {:href        "#login-page"
               :data-toggle "tab"
               :on-click    #(do
                              (reset! data/active-page :login-page))}
           [:i.fa.fa-user] " " [:strong @data/sistr-user]]]
         [:li
          {:class (if (= @data/active-page :login-page) "active" "")}
          [:a {:href        "#login-page"
               :data-toggle "tab"
               :on-click    #(reset! data/active-page :login-page)}
           [:i.fa.fa-sign-in] [:strong " Login"]]]
         )
       [:li.dropdown {:class (condp = @data/active-page
                               :log-page "active"
                               :about-page "active"
                               "")}
        [:a.dropdown-toggle {:href          "#"
                             :data-toggle   "dropdown"
                             :role          "button"
                             :aria-expanded "false"}
         [:i.fa.fa-gear.fa-lg]]
        [:ul.dropdown-menu {:role "menu"}
         [:li
          {:class (if (= @data/active-page :about-page) "active" "")}
          [:a {:href        "#about-page"
               :data-toggle "tab"
               :on-click    #(reset! data/active-page :about-page)}
           [:i.fa.fa.fa-smile-o] " About SISTR"]]
         [:li
          {:class (if (= @data/active-page :log-page) "active" "")}
          [:a {:href        "#log-page"
               :data-toggle "tab"
               :on-click    #(reset! data/active-page :log-page)}
           [:i.fa.fa-info-circle] " Log"]]
         (when (data/logged-in? @data/sistr-user @data/sistr-user-token)
           [:li
            [:a
             {:href     "#"
              :on-click #(data/logout!)}
             [:i.fa.fa-sign-out] " Logout"]])]
        ]
       ]
      ]]]
   ;; Tab content
   [:div.tab-content
    ;; About/Home/Info page
    [:div.tab-pane.sistr-page {:id "about-page"
                               ;:class "active"
                               }
     [about-page/about-page]]
    ; User login page/account management page
    [:div.tab-pane.sistr-page {:id    "login-page"
                               ;:class "active"
                               }
     [login-page/user-page]
     ]
    [:div.tab-pane.sistr-page {:id "user-genome-page"
                               ;:class "active"
                               }
     [user-page/user-genomes]
     ]
    ;; Genome Upload tab
    [:div.tab-pane.sistr-page {:id "upload-page"
                               :class "active"
                               }
     [upload-page/genome-upload-page]
     ]
    [:div.tab-pane.sistr-page {:id "genomes-table-page"
                               ;:class "active"
                               }
     [genomes-page/genomes-page]
     ]
    ;; Phylogenetic Tree page
    [:div.tab-pane.sistr-page {:id "tree-page"
                               ;:class "active"
                               }
     [tree-page/tree-page]
     ]
    ;; MST tab
    [:div.tab-pane.sistr-page {:id "mst-page"
                               ;:class "active"
                               }
     [mst-page/mst-page]
     ]
    ;; Genome metadata Overview
    [:div.tab-pane.sistr-page {:id "epi-page"
                               ;:class "active"
                               }
     [temporal-epi-page/temporal-epi-page]
     ]
    ;; Serovar prediction results
    [:div.tab-pane.sistr-page {:id "results-page"
                               ;:class "active"
                               }
     [results-page/in-silico-results-page]]
    ;; Map page
    [:div.tab-pane.sistr-page {:id "map-page"
                               ;:class "active"
                               }
     [geomap-page/map-page]]
    [:div.tab-pane.sistr-page {:id "log-page"
                               ;:class "active"
                               }
     [log-page]]
    ]])

(defn render-app []
  (reagent/render-component [sistr-app]
                            (utils/by-id "app")
                            ))

;; get initial app data
(if (empty? @data/genome-name-metadata)
  (data/get-all-data-async!))

;; Render SISTR app on app div node
(render-app)

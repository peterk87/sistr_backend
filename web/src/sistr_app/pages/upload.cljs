(ns ^:figwheel-load sistr-app.pages.upload
  (:require-macros [cljs.core.async.macros :refer [go go-loop]])
  (:require
    [cljs.core.async :refer [put! chan <! timeout]]
    [reagent.core :refer [atom]]
    [sistr-data.data :as data]
    [sistr-utils.utils :as utils]
    [ajax.core :refer [POST GET]]
    [sistr-components.alerts :as alerts]
    [sistr-components.bootstrap :refer [progress-bar well-collapsible panel]]
    [sistr-app.pages.about :as about]))

(def task-recheck-timeout 2000)
(def queue-poll-timeout 30000)

(defonce queue-status (atom nil))
(defonce genome-results (atom {}))
(defonce genome-relatives (atom {}))
(defonce genome-serovar-prediction (atom {}))
(defonce genome-status (atom {}))
(defonce genome-mash-refseq (atom {}))
(defonce genome-sistrcmd (atom {}))
(defonce n-genomes-uploading (atom 0))
(defonce genome-upload-errors (atom []))
(defonce uploaded-genome-table-data (atom {}))
(defonce files-to-upload (atom {}))
(defonce current-upload (atom {:progress 0
                               :filename nil}))

(defn post-error-handler! [{:keys [status status-text]}]
  (swap! genome-upload-errors conj {:time   (js/Date)
                                    :status status
                                    :msg    status-text}))

(defn xhr-post-genome-fasta->chan
  "Create an XHR POST request of `form-data` containing a genome fasta file to a
  `uri` and receive success and error responses to a cljs.core.async channel.
  Success is HTTP 201 (resource created); failure is anything else.
  Return the channel."
  [uri form-data &
   {:keys [timeout headers on-progress!]
    :or   {timeout      600000
           headers      {}
           on-progress! nil}}]
  (let [c (chan)
        xhr (new js/XMLHttpRequest)
        xhr-upload (-> xhr .-upload)]
    (when on-progress!
      (.addEventListener (if xhr-upload xhr-upload xhr) "progress" on-progress!))
    (.addEventListener xhr "load"
                       (fn [e]
                         (let [target (.-target e)
                               status (.-status target)
                               resp (utils/keywordify (js->clj (js/JSON.parse (.-responseText target))))]
                           (put! c (if (= status 201)
                                     resp
                                     {:error resp})))))
    (.open xhr "post" uri true)
    (doseq [[k v] headers]
      (.setRequestHeader xhr k v))
    (.send xhr form-data)
    c))

(defn task-status->chan
  "Get the current status of a list of genome analysis tasks."
  [task_id]
  (let [c (chan)]
    (GET (str data/api-uri "task")
         {:params          {:task_id task_id}
          :response-format :json
          :keywords?       true
          :handler         #(put! c %)})
    c))

(defn clear-completed-tasks! []
  (reset! genome-status (into {} (remove (fn [[_ task] _]
                                           (get task :all-done?)) @genome-status))))

(defn genome-analysis-progress []
  (let [show-done? (atom true)]
    (when-not (empty? @genome-status)
      (fn []
        [panel
         "analysis-progress-table"
         [:h4 [:i.fa.fa-tachometer] " Uploaded genome analysis progress"]
         "default"
         false
         [:div
          (when-not (empty? @genome-status)
            [:div.form-inline.pull-right
             [:div.form-group
              [:button.btn.btn-default {:on-click clear-completed-tasks!}
               [:i.fa.fa-close] " Clear completed"]
              [:div.checkbox
               [:label
                [:input {:type      "checkbox"
                         :checked   @show-done?
                         :on-change #(swap! show-done? not)}]
                "Show completed?"]]

              ]
             ])
          [:table.table.table-striped
           [:thead
            [:tr
             [:th {:style {:width "200px"}}
              "Genome"]
             [:th {:style {:width "50px"}}
              "Progress"]
             [:th "Current Task"]
             [:th "Serovar"]
             [:th {:title "Does the genome look like Salmonella by Mash MinHash genomic distance estimation calculation to NCBI 55k RefSeq genomes? See below for more detailed Mash results."
                   :style {:cursor "help"}}
              "Salmonella?"]
             ]]
           [:tbody
            (let [running-tasks (if @show-done?
                                  (vals @genome-status)
                                  (remove #(get % :all-done?) (vals @genome-status)))
                  mash-results @genome-mash-refseq
                  sistrcmd-results @genome-sistrcmd]
              (for [x running-tasks]
                (let [all-done? (get x :all-done?)
                      genome (get x :genome)
                      status (get x :status)
                      row-class (cond
                                  (= status "FAILURE") "danger"
                                  all-done? "success"
                                  :else "")
                      progress (* 100 (get x :progress))
                      active (= row-class "")
                      task (get x :task)
                      salmonella? (get-in mash-results [genome :mash :is_salmonella])
                      mash-genome (some-> mash-results
                                          (get-in [genome :mash :match_id])
                                          (clojure.string/split #"-")
                                          last
                                          (clojure.string/replace ".fna" ""))
                      serovar (get-in sistrcmd-results [genome :serovar])]
                  ^{:key genome}
                  [:tr
                   [:td genome]
                   [:td [progress-bar progress row-class active]]
                   [:td (if (empty? task) "Done!" task)]
                   [:td (when-not (nil? serovar)
                          serovar)]
                   [:td (when-not (nil? salmonella?)
                          (if salmonella?
                            [:span [:i.fa.fa-check.text-success] (str " Looks like '" mash-genome "'")]
                            [:span [:i.fa.fa-ban.text-danger] (str " Uh oh... this looks like '" mash-genome "'")]))]
                   ])))]]]
         ]))))

(defn download-uploaded-genome-results-table []
  (when-not (empty? @uploaded-genome-table-data)
    (do
      ;; get the selected genomes from the MST node selection
      (let [data (->> @uploaded-genome-table-data vals)
            ;; get all metadata headers
            header-kws (->> data
                            first
                            keys
                            sort)
            ordered-header-kws (distinct (concat [:genome
                                                  :serovar_prediction
                                                  :serovar_antigen_prediction
                                                  :Serogroup_prediction
                                                  :H1_prediction
                                                  :H2_prediction
                                                  :cgMLST_serovar_prediction
                                                  :cgMLST_serovar_count_predictions
                                                  :MLST_ST_in_silico
                                                  :MLST_serovar_prediction
                                                  :MLST_serovar_count_predictions
                                                  :closest-public-genome-name
                                                  :closest-public-genome-alleles-matching
                                                  :closest-public-genome-serovar
                                                  :closest-public-genome-mlst-st] header-kws))
            csv-data (utils/map->csv-text data ordered-header-kws)
            url-obj (utils/text->blob->obj-url csv-data)
            n-genomes (count @uploaded-genome-table-data)
            ]
        #_(prn "ordered-header-kws" ordered-header-kws)
        ;; It appears necessary to deref selected-tree-genomes in order to properly reactively update
        ;; this component with the proper CSV data
        (deref uploaded-genome-table-data)
        [:div.form-inline.pull-right

         [:a.btn.btn-default {:href     url-obj
                              :download (str "uploaded-" n-genomes "_" (utils/pluralize "genome" n-genomes) "-results_summary_table.csv")}
          [:i.fa.fa-download.fa-lg.pull-left]
          " Download Table (CSV) "
          [:span.label.label-default (str n-genomes " " (utils/pluralize "genome" n-genomes))]
          ]]))))

(defn uploaded-genome-results-table-row [result]
  (let [genome (get result :genome)
        test-results (get result :test_results)
        serovar-prediction (get @genome-serovar-prediction (keyword (str genome)))
        {:keys [serovar_prediction
                subspecies
                serovar_antigen_prediction
                Serogroup_prediction
                H1_prediction
                H2_prediction
                cgMLST_serovar_prediction
                cgMLST_serovar_count_predictions
                cgMLST_cluster_level
                cgMLST_cluster_number
                MLST_serovar_prediction
                MLST_serovar_count_predictions
                ]} serovar-prediction
        mlst-st (get-in test-results [:MLST :ST])
        marker-results (get result :marker_results)
        wgmlst-results (vals (get marker-results :wgMLST_330))
        allele-count (count wgmlst-results)
        partial-alleles-count (->> wgmlst-results (map :is_contig_truncated) (filter true?) count)
        missing-alleles-count (->> wgmlst-results (map :is_missing) (filter true?) count)
        complete-alleles-count (- allele-count partial-alleles-count missing-alleles-count)
        row-class (cond
                    (> missing-alleles-count 0) "danger"
                    (> partial-alleles-count 0) "warning"
                    :else "")
        relative-data (get @genome-relatives (keyword (str genome)))
        closest-relative-data (get relative-data :closest_relative)
        closest-relative-sim-count (get closest-relative-data :n_matching_alleles)
        closest-relative-name (get closest-relative-data :genome)
        closest-relative-md (get @data/genome-name-metadata (str closest-relative-name))
        max-td-len 40
        ]
    (swap! uploaded-genome-table-data assoc genome {
                                                    :genome                                 genome
                                                    :subspecies                             subspecies
                                                    :serovar_prediction                     serovar_prediction
                                                    :serovar_antigen_prediction             serovar_antigen_prediction
                                                    :Serogroup_prediction                   Serogroup_prediction
                                                    :H1_prediction                          H1_prediction
                                                    :H2_prediction                          H2_prediction
                                                    :cgMLST_serovar_prediction              cgMLST_serovar_prediction
                                                    :cgMLST_serovar_count_predictions       cgMLST_serovar_count_predictions
                                                    :cgMLST_cluster_number                  cgMLST_cluster_number
                                                    :cgMLST_cluster_level                   cgMLST_cluster_level
                                                    :MLST_ST_in_silico                      mlst-st
                                                    :MLST_serovar_prediction                MLST_serovar_prediction
                                                    :MLST_serovar_count_predictions         MLST_serovar_count_predictions
                                                    :closest-public-genome-name             closest-relative-name
                                                    :closest-public-genome-alleles-matching closest-relative-sim-count
                                                    :closest-public-genome-serovar          (get closest-relative-md :serovar)
                                                    :closest-public-genome-mlst-st          (get closest-relative-md :MLST:ST)
                                                    :wgMLST_330:complete-alleles            complete-alleles-count
                                                    :wgMLST_330:partial-alleles             partial-alleles-count
                                                    :wgMLST_330:missing-alleles             missing-alleles-count})
    ^{:key genome}
    [:tr {:class row-class}
     [:td genome]
     [:td subspecies]

     [:td {:title serovar_prediction}
      (if (< (count serovar_prediction) max-td-len)
        serovar_prediction
        (str (apply str (take max-td-len serovar_prediction)) "..."))]
     [:td (str cgMLST_serovar_prediction
               " (dist=" (utils/round cgMLST_cluster_level 2) "; "
               (js/Math.floor (* (- 1 cgMLST_cluster_level) allele-count)) "/"
               allele-count ")")]
     [:td {:title serovar_antigen_prediction}
      (if (< (count serovar_antigen_prediction) max-td-len)
        serovar_antigen_prediction
        (str (apply str (take max-td-len serovar_antigen_prediction)) "..."))]
     [:td Serogroup_prediction]
     [:td H1_prediction]
     [:td H2_prediction]

     [:td MLST_serovar_prediction]
     [:td mlst-st]

     ;[:td closest-relative-name]
     ;(let [bg-color-hex (utils/cgmlst-matching-color-scale closest-relative-sim-count)
     ;      [r g b] (utils/hex->rgb-vec bg-color-hex)
     ;      alpha 0.3
     ;      rgba-color (str "rgba(" r "," g "," b "," alpha ")")
     ;      ]
     ;  [:td.td-number {:style {:background-color rgba-color}} closest-relative-sim-count])
     ;[:td.td-number (get closest-relative-md :MLST:ST)]
     ;[:td (get closest-relative-md :serovar)]

     [:td.td-number (str complete-alleles-count)]
     [:td.td-number (str partial-alleles-count)]
     [:td.td-number (str missing-alleles-count)]
     ]
    ))

(defn results-table []
  [:table.table.table-striped
   [:thead
    [:tr
     [:th {:rowSpan 2} "Genome"]
     [:th {:rowSpan 2} "Subspecies"]
     [:th {:title   "In silico serovar predictions are provided via cgMLST+antigen, O/H-antigen, cgMLST similarity to trusted genomes and MLST ST. Hopefully with the results from these different methods, you can get a fairly good idea of what the serovars of your genomes are."
           :colSpan 8
           :style   {:cursor "help"}} "Serovar Prediction"]
     [:th {:title   "The number of complete vs missing vs partial cgMLST 330 alleles can be a helpful measure for assessing the quality of your Salmonella genomes. Your Salmonella genomes should have complete copies of all 330 cgMLST loci or at least very few missing or partial alleles."
           :colSpan 3
           :style   {:cursor "help"}}
      "cgMLST 330 Allele Stats"]
     ]
    [:tr
     [:th {:title "Serovar prediction by antigen determination with cgMLST refinement"
           :style {:cursor "help"}}
      "Serovar (overall)"]
     [:th {:title "Serovar prediction by cgMLST similarity to closest related curated public genome."
           :style {:cursor "help"}}
      "Serovar (cgMLST)"]
     [:th {:title "Serovar by O, H1 and H2 antigen determination."
           :style {:cursor "help"}}
      "Serovar (antigen)"]
     [:th {:title "Serogroup prediction from wzx and wzy gene allele determination."
           :style {:cursor "help"}}
      "Serogroup"]
     [:th {:title "H1 antigen prediction from fliC gene allele determination."
           :style {:cursor "help"}}
      "H1"]
     [:th {:title "H2 antigen prediction from fljB gene allele determination."
           :style {:cursor "help"}}
      "H2"]
     [:th {:title "Serovar prediction by MLST using MLST to serovar info from Enterobase (courtesy of Nabil Ali-Khan and Mark Achtman from the University of Warwick)"
           :style {:cursor "help"}}
      "Serovar (MLST)"]
     [:th {:title "Multiple locus sequence typing (MLST) sequence type (ST) using the University of Warwick MLST scheme for Salmonella enterica (http://mlst.warwick.ac.uk/mlst/dbs/Senterica)"
           :style {:cursor "help"}}
      "MLST ST"]

     [:th {:title "Number of complete cgMLST loci found (Max=330)"
           :style {:cursor "help"}}
      "Complete"]
     [:th {:title "Number of cgMLST loci found to be truncated by the end of a contig (Max=330). If any loci are found truncated, genome result will be highlighted yellow."
           :style {:cursor "help"}}
      "Partial"]
     [:th {:title "Number of cgMLST loci missing (Max=330). If any loci are found missing, genome result will be highlighted red."
           :style {:cursor "help"}}
      "Missing"]
     ]]
   [:tbody
    (let [results (->> @genome-results
                       vals
                       (sort-by :genome))]
      (for [result results]
        ^{:key result}
        [uploaded-genome-results-table-row result]))]])

(defn results-panel []
  (when-not (empty? @genome-results)
    [panel
     "uploaded-genome-results-table"
     [:h4 [:i.fa.fa-table] " Uploaded genome analysis results"]
     "default"
     false
     [:div
      [download-uploaded-genome-results-table]
      [results-table]]
     ]))

(defn help-info []
  [:div
   [:p "Select one or more genomes to upload to the SISTR server for analysis."]
   [:p "Analysis progress will be shown in the top right table in the main panel."]
   [:p "A summary of results will be shown in the bottom right table in the
    main panel as analysis is completed for your uploaded genomes."]])

(defn genome-uploading-indicator
  "Show an alert indicating that a genome is being uploaded to the server."
  []
  (when (:filename @current-upload)
    (let [{:keys [filename progress]} @current-upload]
      [:div.alert.alert-info
       [:h5 "Uploading... " filename]
       [:p "Progress " (utils/round progress 1) "%"]
       [progress-bar progress "primary" true]])))

(defn mash-results-table []
  @genome-mash-refseq
  (when-not (empty? @genome-mash-refseq)
    (let [mash-results @genome-mash-refseq
          genomes (sort (keys mash-results))]
      [:table.table.table-condensed.table-striped
       [:thead
        [:tr
         [:th "Your Genome"]
         [:th "Strain"]
         [:th "Serovar"]
         [:th "Subspecies"]
         [:th.td-number "Mash Distance"]
         [:th.td-number "Matching"]
         [:th "Taxonomy ID"]
         [:th "BioProject"]
         [:th "BioSample"]
         [:th "Assembly Accession"]
         [:th "Plasmid"]]]
       [:tbody
        (map-indexed
          (fn [idx genome]
            (let [vs (get-in mash-results [genome :mash])
                  {:keys [assembly_accession
                          bioproject
                          biosample
                          distance
                          matching
                          plasmid
                          serovar
                          strain
                          subspecies
                          taxid
                          match_id
                          is_salmonella]} vs
                  salmonella? is_salmonella
                  ]
              ^{:key idx}
              [:tr {:class (if (not salmonella?) "danger" "")}
               [:td
                (when-not salmonella?
                  [:span [:i.fa.fa-exclamation-triangle.text-danger] [:span " "]])
                genome]
               [:td [:a {:target "_blank"
                         :href   (str "https://www.ncbi.nlm.nih.gov/nuccore/?term=" strain)}
                     (if (nil? strain) match_id strain)]]
               [:td serovar]
               [:td subspecies]
               [:td (utils/sci-not distance 2)]
               [:td matching]
               [:td [:a {:target "_blank"
                         :href   (str "https://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?id=" taxid)}
                     taxid]]
               [:td [:a {:target "_blank"
                         :href   (str "https://www.ncbi.nlm.nih.gov/bioproject/?term=" bioproject)}
                     bioproject]]
               [:td [:a {:target "_blank"
                         :href   (str "https://www.ncbi.nlm.nih.gov/biosample/?term=" biosample)}
                     biosample]]
               [:td [:a {:target "_blank"
                         :href   (str "https://www.ncbi.nlm.nih.gov/assembly/?term=" assembly_accession)}
                     assembly_accession]]
               [:td plasmid]
               ]
              ))
          genomes)]])))

(defn mash-info-alert []
  [alerts/alert-dismissable "info"
   ^{:key 0}
   [:p (str "Mash is used to closest RefSeq match of your uploaded genome. "
            "The Mash result can be used to inform you whether there may be potential contamination issues or sequencing error with your \"Salmonella\" genomes."
            "In some cases the Mash result can provide insight into the subspecies and serovar of your genome.")]])

(defn mash-results-panel []
  (let [mash-results (->> @genome-mash-refseq
                          (remove nil?))]
    (when-not (empty? mash-results)
      [panel
       "mash-results"
       [:h4 [:i.fa.fa-table] " Mash RefSeq results"]
       "default"
       false
       [:div
        [mash-info-alert]
        [mash-results-table]]
       ])))

(defn rmlst-results-table []
  (let [genomes (sort (keys @genome-results))
        test-results (->> genomes (map #(get @genome-results %)) (map :test_results) (map :rMLST))
        m (zipmap genomes test-results)]
    [:table.table.table-condensed.table-striped
     [:thead
      [:th "Genome"]
      [:th "Genus"]
      [:th "Species"]
      #_[:th "Other Designation(s)"]
      [:th.td-number "% Matching alleles to an rMLST profile"]]
     [:tbody
      (map-indexed
        (fn [i g]
          (let [vs (get m g)
                {:keys [genus species matching_alleles other_designation]} vs
                salmonella? (or (= genus "Salmonella") (= species "Salmonella enterica"))]
            (when vs
              ^{:key i}
              [:tr {:class (if (not salmonella?) "danger" "")}
               [:td
                (when-not salmonella?
                  [:span [:i.fa.fa-exclamation-triangle.text-danger] [:span " "]])
                (utils/keyword->str g)]
               [:td genus]
               [:td species]
               #_[:td other_designation]
               [:td (-> matching_alleles js/parseFloat (* 100) (utils/round 1)) "%"]
               ])
            ))
        genomes)]]))

(defn rmlst-info-alert []
  [alerts/alert-dismissable "info"
   ^{:key 0}
   [:p (str "rMLST is used to determine the genus/species of your uploaded genome. "
            "The rMLST determined genus should be 'Salmonella' and the expected species should be 'Salmonella enterica'. "
            "There may be issues with your \"Salmonella\" genomes if this is not the case.")]])

(defn rmlst-results-panel []
  (let [rmlst-results (->> @genome-results
                           (map :test_results)
                           (map :rMLST)
                           (remove nil?))]
    (when-not (empty? rmlst-results)
      [panel
       "rmlst-results"
       [:h4 [:i.fa.fa-table] " rMLST results"]
       "default"
       false
       [:div
        [rmlst-info-alert]
        [rmlst-results-table]]
       ])))

(defn check-genome-status!
  "While there are uncompleted tasks for a genome, get a status update for that
  genome. If all tasks are done, then get the MIST results for that genome so
  it can be reported to the user."
  [task]
  (let [task-id (:task task)
        genome (:genome task)]
    (js/console.log "Check genome status"
                    (clj->js task))
    (go-loop []
             (let [task-data (first (<! (task-status->chan task-id)))
                   status (:status task-data)
                   task-info (:info task-data)

                   failed? (= status "FAILURE")
                   done? (= status "SUCCESS")
                   progress (cond
                              (= status "PENDING") 0.0
                              done? 1.0
                              (not (nil? (:progress task-info))) (/ (:progress task-info) 100)
                              :else 0.0
                              )
                   task-current (cond
                                  (nil? task-info) "Enqueuing..."
                                  done? "Genome succesfully analyzed!"
                                  (nil? (:desc task-info)) "???"
                                  (not (nil? (:desc task-info))) (:desc task-info)
                                  :else "ERROR!")
                   task-mash (:mash task-info)
                   task-sistrcmd (:sistrcmd task-info)
                   ]
               (when-not (nil? task-mash)
                 (swap! genome-mash-refseq assoc genome {:genome genome
                                                         :mash   task-mash}))
               (when-not (nil? task-sistrcmd)
                 ;TODO: swap genome serovar prediction from task sistrcmd key value
                 (swap! genome-sistrcmd assoc genome {:genome genome
                                                      :sistrcmd task-sistrcmd}))
               (swap! genome-status assoc (keyword genome) {:genome    genome
                                                            :all-done? done?
                                                            :progress  progress
                                                            :task      task-current
                                                            :mash      task-mash
                                                            :sistrcmd task-sistrcmd
                                                            :status    status})
               ; when genome analysis is done then get:
               ; - MIST results (cgMLST, MLST, SGSA, etc)
               ; - closest trusted/curated public genome
               ; - serovar prediction results
               (when done?
                 (let [auth-headers (utils/auth-headers @data/sistr-user-token)
                       base-uri (str data/api-uri "user/" @data/sistr-user "/genome/" genome)
                       genome-kw (keyword genome)
                       mist-results-uri (str base-uri "/mist_results")
                       new-genome-mist-results (<! (utils/get-uri->chan mist-results-uri
                                                                        :headers auth-headers))
                       closest-relative-uri (str base-uri "/wgmlst/closest_relative?curated_only=true")
                       new-genome-closest-relative (<! (utils/get-uri->chan closest-relative-uri
                                                                            :headers auth-headers))
                       serovar-prediction-uri (str base-uri "/serovar_prediction")
                       serovar-prediction-results (<! (utils/get-uri->chan serovar-prediction-uri
                                                                           :headers auth-headers))]
                   (swap! genome-results
                          assoc
                          genome-kw
                          new-genome-mist-results)
                   (swap! genome-relatives
                          assoc
                          genome-kw
                          new-genome-closest-relative)
                   (swap! genome-serovar-prediction
                          assoc
                          genome-kw
                          serovar-prediction-results)
                   ))
               ; TODO: better method for determining if data should be re-fetched
               ; done or failed? then decrement data/upload-countdown
               ; At 0 the app state will be re-fetched from the server
               (when (or done? failed?)
                 (swap! data/upload-countdown dec))
               ; wait a bit before re-checking
               (<! (timeout task-recheck-timeout))
               (when (not (or done? failed?))
                 (recur))))))

(defn upload-progress! [e]
  (let [loaded (.-loaded e)
        total (.-total e)
        progress (* (/ loaded total) 100)]
    (swap! current-upload assoc :progress progress)))

(defn upload-genome-fasta!
  "Upload genome fasta file. Retry a specified number of times if an error is encountered."
  [genome-name form-data & {:keys [retry-countdown
                                   retry-timeout
                                   reanalyze]
                            :or   {retry-countdown 2
                                   retry-timeout   5000
                                   reanalyze       false}}]
  (let [c (chan)]
    (go
      (let [uri (str data/api-uri "user/" @data/sistr-user "/genome/" genome-name (if reanalyze "?reanalyze=true" ""))
            auth-headers (utils/auth-headers @data/sistr-user-token)

            _ (swap! current-upload assoc :filename genome-name)
            resp (<! (xhr-post-genome-fasta->chan uri form-data
                                                  :headers auth-headers
                                                  :on-progress! upload-progress!))
            _ (swap! current-upload assoc :filename nil)
            _ (swap! current-upload assoc :progress 0)
            error-resp (:error resp)]
        (if error-resp
          (do
            (post-error-handler! error-resp)
            (utils/log-http data/app-log :upload error-resp
                            :message (str "Could not upload genome '" genome-name "'! Retrying... " retry-countdown)
                            :uri uri
                            :http-method "POST")
            (when (> retry-countdown 0)
              (do
                (prn "Retrying upload in "
                     retry-timeout "ms for genome " genome-name
                     " :: retry-countdown=" retry-countdown)
                (<! (timeout retry-timeout))
                (upload-genome-fasta! genome-name form-data
                                      :retry-countdown (dec retry-countdown)
                                      :retry-timeout (* retry-timeout 2)))))
          (do
            (utils/log-http data/app-log :upload resp
                            :message (str "Uploaded genome '" genome-name "'! Polling analysis progress")
                            :uri uri
                            :http-method "POST")
            (check-genome-status! resp))))
      (put! c 1)
      )))

(add-watch files-to-upload :upload-change
           (fn [_ _ _ new-state]
             (go
               ;; Get the private user or create a new private user if no private user specified
               (when (and (nil? @data/sistr-user-token) (= @data/sistr-user data/public-sistr-user))
                 (reset! data/sistr-user (<! (utils/create-temp-user data/api-uri @data/sistr-user)))
                 ;; Change the URL to contain query string with new/current user name
                 (utils/set-user-query-string! @data/sistr-user)
                 )
               (let [filenames (-> new-state keys sort)
                     n-files (count new-state)]
                 (reset! data/upload-countdown n-files)
                 (reset! n-genomes-uploading n-files)
                 (doseq [filename filenames]
                   (let [file (get new-state filename)
                         ; strip the extension to get the genome name from the filename
                         genome-name (last (re-find #"(.*)\..*$" filename))
                         ; create JS FormData object with JS File object appended as "fasta"; API expects "fasta" to
                         ; contain a File object
                         form-data (doto
                                     (js/FormData.)
                                     (.append "fasta" file filename))
                         _ (js/console.info "formdata " form-data)]
                     ;; POST user genome upload and start checking analysis progress
                     ; TODO: need to block here or else ALL files will upload at the same time...
                     (<! (upload-genome-fasta! genome-name form-data))
                     ))))))

(defn upload-genomes-btn []
  [:span.btn.btn-primary.btn-file
   [:i.fa.fa-upload]
   " Upload FASTAs"
   [:input#files {:type      "file"
                  :name      "file"
                  :multiple  "multiple"
                  :on-change (fn [e]
                               (let [files (array-seq (-> e .-target .-files))
                                     filenames (map #(.-name %) files)]
                                 (reset! files-to-upload (zipmap filenames files))))
                  :style     {:padding-bottom 15}}]])

(defn show-files-to-upload []
  (let [n-files (count @files-to-upload)
        first-file (first (keys @files-to-upload))]
    (if (= n-files 1)
      [:p (str "Selected '" first-file "' for upload")]
      [:p (str "Selected " n-files " files for upload")])))

(defonce poll-queue-status!
         (let [uri (str data/api-uri "queue")]
           (go-loop []
                    (when (= @data/active-page :upload-page)
                      (let [resp (<! (utils/get-uri->chan uri))
                            err-resp (:error resp)]
                        (if err-resp
                          (do
                            (<! (timeout queue-poll-timeout)))
                          (do
                            (reset! queue-status (assoc resp :timestamp (js/Date.)))))))
                    (<! (timeout queue-poll-timeout))
                    (recur))))

(defn queue-status-table []
  (let [{:keys [tasks_queued runtime_stats timestamp]} @queue-status
        {:keys [std mean]} runtime_stats]
    [:table.table.table-condensed
     [:tbody
      [:tr [:td "Global tasks in queue"] [:td (str tasks_queued)]]
      [:tr
       [:td "Mean runtime (s)"]
       [:td (utils/round mean 1) "+/-" (utils/round std 1)]]
      [:tr
       [:td "As of"]
       [:td (str timestamp)]]
      ]]))

(defn queue-status-well []
  [well-collapsible "queue-status-info"
   [:span [:i.fa.fa-timer] " Queue status"]
   false
   [queue-status-table]
   ])

(defn get-csv-uri->chan
  "Send a GET request to a URI and receive the success/error response to a
  cljs.core.async channel.
  Return the channel."
  [uri & {:keys [headers params]
          :or   {headers {}
                 params  {}}}]
  (let [c (chan)]
    (GET uri {:handler         #(put! c (if (nil? %) {} %))
              :error-handler   #(put! c {:error %})
              :response-format :raw
              :params          params
              :headers         (merge headers
                                      {"Accept"       "text/csv"
                                       "Content-Type" "text/csv"})})
    c))

(def loading-csv-data (atom false))

(defn dl-cgmlst-csv []
  (go
    (reset! loading-csv-data true)
    (let [url (str data/api-uri "user/" @data/sistr-user "/genomes/cgmlst/csv")
          auth-headers (utils/auth-headers @data/sistr-user-token)
          resp (<! (get-csv-uri->chan url :headers auth-headers))
          err-resp (:error resp)]
      (if err-resp
        (swap! genome-upload-errors conj {:time   (js/Date)
                                          :status (:status err-resp)
                                          :msg    (str (:status-text err-resp)
                                                       " Could not load cgMLST profiles table data from server.")})
        (utils/trigger-download! resp "sistr-cgmlst.csv")))
    (reset! loading-csv-data false)))

(defn load-cgmlst-csv []
  (if @loading-csv-data
    [:button.btn.btn-primary.disabled
     [:i.fa.fa-spinner.fa-spin]
     " Loading cgMLST profiles from server..."]
    [:button.btn.btn-primary
     {:on-click dl-cgmlst-csv}
     "Download cgMLST profiles table (CSV)"]))

(def dragging? (atom false))
(def dropped? (atom false))

(def drop-events
  {:on-drag-over  (fn [e]
                    (.preventDefault e)
                    (reset! dragging? true))
   :on-drag-enter (fn [e]
                    (.preventDefault e)
                    (let [dt (.-dataTransfer e)]
                      (aset dt "effectAllowed" "copy")
                      (aset dt "dropEffect" "copy")
                      (reset! dragging? true)))
   :on-drop       (fn [e]
                    (.preventDefault e)
                    (let [dt (.-dataTransfer e)
                          files (array-seq (.-files dt))
                          filenames (map #(.-name %) files)]
                      (reset! files-to-upload (zipmap filenames files)))
                    (reset! dragging? false)
                    (reset! dropped? true))
   :on-drag-leave (fn [e]
                    (.preventDefault e)
                    (reset! dragging? false))})

(defn dragging-notification []
  (if @dropped?
    [:h1 [:i.fa.fa-thumbs-up] " Dropped files uploading!"]
    (if @dragging?
      [:h1 [:i.fa.fa-upload] " Drop files here to start upload and analysis!"]
      [:h1 [:i.fa.fa-copy] " Drag & Drop FASTA files here!"])))

(defn genome-upload-init-msg []
  [:div
   (merge drop-events
          {:style {:height "90vh"
                   :margin "5px 5px 5px 5px"
                   :background-color (if @dragging? "rgb(184, 218, 185)" "#d0e9ff")
                   :border (if @dragging? "5px dashed" "5px dashed")
                   :border-radius "10px"}})
   [:div {:style {:margin "0"
                  :position "absolute"
                  :top "35%"
                  :left "50%"
                  :margin-right "-50%"
                  :transform "translate(-50%, -50%)"}}
    [dragging-notification]
    [:h4 "Uploaded genome analysis progress and results will appear here"]
    [:p "Click on \"Upload\" in the left sidebar panel to begin uploading genome FASTA files for analysis"]
    [:p "After selecting files for upload, they will be uploaded to the SISTR server for analysis."]
    [:p "Genome analysis progress will be reported below."]
    [:p "As genome analysis is completed, a summary of results will appear here as well."]]])

(defn genome-upload-page []
  [:div {:style {:padding-left 300}}
   [:div.sidebar
    [:div.well.well-sm
     [well-collapsible "mst-help-info"
      [:span [:i.fa.fa-info-circle] " Help"]
      true
      [help-info]]
     [well-collapsible "serovar-variant-info-well"
      [:span [:i.fa.fa-exclamation-triangle] " Serovar variant prediction"]
      true
      [about/serovar-variant-warning-info]]
     [:div.alert.alert-danger
      [:h5 "Upload " [:strong "multiple "] [:em "Salmonella enterica"] " genome assemblies (FASTA format) for SISTR analysis."]
      [:h5 [:em "One genome per FASTA file!"]]]
     [upload-genomes-btn]
     [show-files-to-upload]
     [genome-uploading-indicator]
     ;[load-cgmlst-csv]
     [queue-status-well]
     ]]
   (when (empty? @genome-status)
     [genome-upload-init-msg])
   [:div.container-fluid
    [:div.row
     [:div.col-md-12
      [:div.row
       [:div.col-md-6
        [alerts/upload-errors-alerts genome-upload-errors]]]
      [genome-analysis-progress]
      [results-panel]
      [mash-results-panel]
      [rmlst-results-panel]]]]])

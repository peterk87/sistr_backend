(ns ^:figwheel-load sistr-app.pages.genomes
  (:require-macros [cljs.core.async.macros :refer [go go-loop]])
  (:require
    [reagent.core :refer [atom]]
    [cljs.core.async :refer [put! chan <! timeout]]
    [sistr-data.data :as data]
    [sistr-utils.utils :as utils]
    [sistr-components.bootstrap :refer [panel well-collapsible]]
    [sistr-components.alerts :refer [show-genomes-in-qviz alert-dismissable]]
    [sistr-components.slickgrid :as slick-grid :refer [slick-grid-table]]
    [sistr-components.filtered-selection :refer [dynamite-filtered-selection]]
    [sistr-components.dimple-charts :as dimple]
    [sistr-components.selection-manager :as selection]
    ))

(defonce show-table? (atom false))
(defonce table-grid (atom nil))
(defonce table-dataview (atom nil))
(defonce dynamite-obj (atom nil))

(defonce genome-metadata (atom {}))
(defonce filtered-genomes (atom []))

(add-watch data/genome-name-metadata :genomes-page
           (fn [key _ _ new-state]
             (prn "genome-metadata" key "reset! new value")
             (reset! genome-metadata new-state)))

(defn download-genome-metadata-table []
  (let [header-kws (->> @genome-metadata
                        vals
                        first
                        keys
                        sort)
        ordered-header-kws (distinct (concat @data/default-cols header-kws))
        csv-data (utils/map->csv-text (vals @genome-metadata) ordered-header-kws)
        obj-blob (utils/text->blob->obj-url csv-data)]
    [:a.btn.btn-default {:href     obj-blob
                         :download (str "SISTR-" (count @genome-metadata) "_genomes-table.csv")}
     [:i.fa.fa-download] " Download Table (CSV)"]))

(defn genome-table-help-info []
  [well-collapsible "genome-table-info-well"
   [:span [:i.fa.fa-info-circle] " Help"]
   true
   [:ul
    [:li "Ctrl/Shift-click to select genomes within table to display in visualizations"]
    [:li "Click and drag table columns to reorder"]
    [:li "Left-click column to sort by column"]
    [:li "Right-click column to select which columns to display in table"]]
   ])

(defn dataview-filter [item args]
  (let [search-string (aget args "searchString")]
    (if (empty? search-string)
      true
      (try
        (let [search-pattern (re-pattern search-string)
              search-fields (map utils/keyword->str [:genome
                                                     :serovar
                                                     :serovar_predicted])]
          (->> search-fields
               (map #(aget item %))
               (remove nil?)
               (map #(re-find search-pattern %))
               (remove nil?)
               empty?
               not))
        (catch js/Object _
          true)))))

(defn genome-metadata-table []
  [slick-grid-table
   "genome-metadata-table"
   genome-metadata
   :genome
   (map utils/keyword->str @data/default-cols)
   data/genome-selection
   table-grid
   table-dataview
   :filter-fn dataview-filter])

(defn input-box [label val]
  [:div.form-horizontal
   [:label.control-label label]
   [:input.form-control {:value     @val
                         :on-change #(reset! val (-> % .-target .-value))}]
   ])

(defonce search-string (atom ""))

(add-watch search-string :update
           (fn [_ _ _ v]
             (try
               (re-pattern v)
               (slick-grid/update-filter! table-grid table-dataview v)
               (catch js/Object e
                 (js/console.error "Search string invalid" e)))))

(defn filter-genomes []
  [input-box "Filter table by genome name and/or serovar (regex allowed)" search-string]
  )

(defn genome-metadata-table-panel []
  [panel
   "genome-metadata-table-panel"
   [:h4 [:i.fa.fa-table] " Genome Metadata"]
   "default"
   false
   [:div
    [filter-genomes]
    [genome-metadata-table]
    [download-genome-metadata-table]]])



(defn show-number-selected []
  (let [n-selected (count @data/genome-selection)
        n-genomes (count @data/genome-name-metadata)
        n-filtered (count @genome-metadata)]
    [:div.alert.alert-info
     (str n-selected " "
          (utils/pluralize "genome" n-selected)
          " selected of "
          n-genomes (utils/pluralize "genome" n-genomes)
          " (" n-filtered " filtered)")]))

(defn loading-genome-data []
  (when (empty? @data/genome-name-metadata)
    [:div.alert.alert-warning
     [:h1 "Loading genome data..." [:i.fa.fa-spinner.fa-spin.fa-lg]]]))

(defn get-filtered-genomes!
  ([]
   (let [gs (keys @data/genome-name-metadata)]
     (reset! filtered-genomes gs)
     (reset! genome-metadata @data/genome-name-metadata)))
  ([gs]
   (reset! filtered-genomes gs)
   (reset! genome-metadata (->> gs (map #(find @data/genome-name-metadata %)) (into {})))))

(defn filter-panel []
  (when-not (empty? @data/genome-name-metadata)
    [panel
     "genomes-filter-panel"
     [:h4 [:i.fa.fa-filter] " Filter Genomes Table"]
     "default"
     true
     (let [d (atom (utils/list-maps->map-lists (vals @data/genome-name-metadata)))]
       [dynamite-filtered-selection "dynamite-genomes"
        d
        ;data/genome-md-map-lists
        :genome
        dynamite-obj
        get-filtered-genomes!])
     ]))

(defn on-chart-item-click! [y x category-key series-key]
  (let [genomes (->> @genome-metadata
                     vals
                     (filter
                       (fn [m]
                         (= [y x]
                            [(get m (keyword category-key))
                             (get m (keyword series-key))])))
                     (map :genome))
        old-selection @data/genome-selection
        new-selection (distinct (concat genomes old-selection))]
    (if (= (count old-selection) (count new-selection))
      (reset! data/genome-selection (sort (remove (fn [x] (some #(= x %) genomes))
                                                  old-selection)))
      (reset! data/genome-selection (sort new-selection)))
    (slick-grid/update-selection
      @table-grid
      @table-dataview
      @data/genome-selection)))

(defn update-table-selection! [genomes]
  (slick-grid/update-selection @table-grid @table-dataview genomes))

(defn refresh-table! []
  (when-not (nil? @table-grid)
    (.resizeCanvas @table-grid)
    (update-table-selection! @data/genome-selection)))


(defn genomes-page []
  [:div.page-container
   [:div.sidebar
    {:id "genomes-page-controls"}
    [:div.well.well-sm
     [genome-table-help-info]
     [show-number-selected]
     [selection/genome-selection-manager "genome-metadata-selection-manager" #(update-table-selection! %)]
     ]]
   [:div.container-fluid
    [:div.row
     [:div.col-md-12
      [loading-genome-data]
      (when (and @show-table? @data/genome-name-metadata)
        [:div
         [filter-panel]
         [genome-metadata-table-panel]
         [dimple/horiz-bar-chart-panel
          "genome-metadata-overview"
          [:h4 [:i.fa.fa-bar-chart] " Metadata Overview"]
          genome-metadata
          data/categories
          :category-key :serovar
          :series-key :source_type
          :on-click-handler! on-chart-item-click!]])]]]])

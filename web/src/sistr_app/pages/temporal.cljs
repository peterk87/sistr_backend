(ns ^:figwheel-load sistr-app.pages.temporal
  (:require
    [reagent.core :as reagent :refer [atom]]
    [sistr-data.data :as data]
    [sistr-utils.utils :as utils]
    [sistr-components.alerts :refer [alert-dismissable]]
    [sistr-components.slickgrid :refer [slick-grid-table]]
    [sistr-components.selectize :refer [single-selectize multi-selectize]]
    [sistr-components.bootstrap :refer [slider]]))


(def show-plot? (atom false))
;; List of genomes filtered by date range
(defonce genomes (atom []))
(defonce category-group-items (atom []))
(defonce selected-genomes (atom #{}))
(defonce selected-metadata (atom nil))
(defonce epi-category (atom "serovar_predicted"))
(defonce epi-groups (atom []))
(defonce epi-group-colors (atom {}))
(defonce min-year (atom 1950))
(defonce max-year (atom 2015))
(defonce show-nil-items (atom true))
(defonce descending-order? (atom true))
(defonce selection-table-grid (atom nil))
(defonce selection-table-dataview (atom nil))

;; TODO: on clear selection, update selected bar chart items to no longer be selected
(defn clear-genome-selection! []
  (reset! selected-genomes #{})
  (reset! selected-metadata nil))

(defn get-freq-data [x kw]
  (->> x
       (group-by (keyword kw))
       (remove (fn [[k _] _] (nil? k)))
       (map (fn [[k v] _] {:label (str k) :value (count v) :data v})))
  )


(defn elm-in-coll? [elm coll]
  (some #(= % elm) coll))

(defn get-grouped-freq-data [data item-to-group-by kw]
  (let [grouped (->> data
                     (group-by (keyword item-to-group-by)))
        grouped-keys (keys grouped)
        grouped-values (vals grouped)
        grouped-freq-data (->> grouped-values
                               (map #(get-freq-data % kw)))]
    (zipmap grouped-keys grouped-freq-data)
    ))

(defn get-grouped-freq-data-in-groups [data item-to-group-by category groups]
  (let [xs (get-grouped-freq-data data item-to-group-by category)
        ks (keys xs)
        vs (vals xs)
        filtered-vs (map (fn [x] (filter #(elm-in-coll? (str (:label %)) groups) x)) vs)
        filtered-xs (zipmap ks filtered-vs)]
    (into {} (remove #(zero? (count (second %))) filtered-xs))
    ))

(defn category-change-handler! [e]
  (reset! epi-category (js->clj e))
  (reset! category-group-items (->> @genomes
                                    (map #(get @data/genome-name-metadata %))
                                    (filter #(not (nil? (:collection_year %))))
                                    (map (keyword @epi-category))
                                    )))



(defn category-select []
  (when-not (and (nil? @data/categories) (nil? @data/genome-name-metadata))
    (single-selectize
      "temporal-category"
      "Select category to highlight"
      data/categories
      epi-category
      category-change-handler!)))


(defn group-change-handler! [e]
  (reset! epi-groups (js->clj e))
  (reset! epi-group-colors (zipmap @epi-groups (utils/qual-colours (count @epi-groups)))))


(defn group-select []
  (when-not (nil? @category-group-items)
    (multi-selectize
      "temporal-groups"
      "Select groups to highlight"
      category-group-items
      epi-groups
      group-change-handler!)))


(defn epi-bar-item-click-handler! [d]
  (let [selected? (:selected? d)
        item-genomes (map :genome (:data d))]
    (reset! selected-genomes (if selected?
                               (apply conj @selected-genomes item-genomes)
                               (apply disj @selected-genomes item-genomes))))
  (reset! selected-metadata (map #(get @data/genome-name-metadata %) @selected-genomes))
  )

(defn bar-item [x-translate y-scale x-scale color-map i v g d click-handler!]
  [:g {:transform (str "translate(" x-translate "," (y-scale i) ")")
       :on-click  #(do
                    (swap! d assoc :selected? (not (:selected? @d)))
                    (click-handler! @d))
       :class     (str "bar-item" (when (:selected? @d) " bar-item-selected"))}
   [:rect {:width  (x-scale v)
           :height (.rangeBand y-scale)
           :fill   (get color-map g)
           :style  {:cursor "pointer"}}]
   [:text {:text-anchor "end"
           :font-size   9
           :dx          -5
           :dy          ".35em"
           :y           (/ (.rangeBand y-scale) 2)}
    g]
   [:text {:text-anchor "start"
           :font-size   9
           :x           (x-scale v)
           :dx          5
           :dy          ".35em"
           :y           (/ (.rangeBand y-scale) 2)
           }
    (if (zero? v) nil v)]
   ])

(defn epi-bar-chart []
  (when-not (and (empty? @epi-groups) (nil? @epi-category))

    (deref show-nil-items)
    (deref descending-order?)
    (let [data (get-grouped-freq-data-in-groups
                 (vals @data/genome-name-metadata)
                 "collection_year"
                 @epi-category
                 @epi-groups)
          values (->> data
                      vals
                      flatten
                      (map :value))
          data-keys (->> data
                         keys
                         (map int)
                         sort
                         ((fn [x]
                            (if @descending-order?
                              (reverse x)
                              x)))
                         (filter #(and (>= % @min-year)
                                       (<= % @max-year))))
          container-width (.width (js/jQuery "#bar-chart-container"))
          width (if (nil? container-width) 800 container-width)
          height (-> (count @epi-groups)
                     (* (count data-keys))
                     (* 10)
                     (+ (* 10 (count data-keys))))
          margin-year-label (->> data-keys
                                 (map str)
                                 (map count)
                                 (map #(* % 15))
                                 (apply max))
          label-length-multiplier 6
          ;; adjust the left margin according to the length of the longest label
          ;; string so that all labels are shown
          margin-left (->> data
                           vals
                           flatten
                           (map :label)
                           (map str)
                           (map count)
                           (map #(* % label-length-multiplier))
                           (apply max))
          margin-right 50
          margin-top 20
          margin-bottom 20
          w (- width margin-year-label margin-left margin-right)
          h (- height margin-top margin-bottom)
          scale-x (-> js/d3
                      .-scale
                      .linear
                      (.domain #js [0 (apply max values)])
                      (.range #js [0 w]))
          scale-y (-> js/d3
                      .-scale
                      .ordinal
                      (.domain (apply array (range (count data-keys))))
                      (.rangeRoundBands #js [0 h] 0.1))
          ]
      [:svg {:id    "bar-chart-derp"
             :style {:width  width
                     :height height}}
       [:g
        {:transform (str "translate(" margin-year-label "," margin-top ")")}
        (doall
          (map-indexed
            (fn [idx k]
              ^{:key idx}
              [:g.bar-year {:transform (str "translate(0," (scale-y idx) ")")}
               [:text {:text-anchor "end"
                       :dx          -5
                       :dy          ".35em"
                       :y           (/ (.rangeBand scale-y) 2)
                       :font-size   16
                       }
                k]
               (let [items-grouped (group-by :label (get data k))
                     items (->> (map #(get items-grouped %) @epi-groups) flatten (remove nil?))
                     item-count (if @show-nil-items (count @epi-groups) (count items))
                     scale-y-items (-> js/d3 .-scale .ordinal
                                       (.domain (apply array (range item-count)))
                                       (.rangeRoundBands #js [0 (.rangeBand scale-y)] 0.1))]
                 (if @show-nil-items
                   (doall
                     (map-indexed
                       (fn [i g]
                         (let [d (first (get items-grouped g))
                               v (if (nil? d) 0 (get d :value))]
                           ^{:key i}
                           [bar-item
                            margin-left
                            scale-y-items
                            scale-x
                            @epi-group-colors
                            i
                            v
                            g
                            (atom d)
                            epi-bar-item-click-handler!]
                           ))
                       @epi-groups))
                   (doall
                     (map-indexed
                       (fn [i d]
                         (let [g (:label d)
                               v (if (nil? d) 0 (get d :value))]
                           ^{:key i}
                           [bar-item
                            margin-left
                            scale-y-items
                            scale-x
                            @epi-group-colors
                            i
                            v
                            g
                            (atom d)
                            epi-bar-item-click-handler!]
                           ))
                       items)))
                 )])
            data-keys))]])))

(defn svg-legend [category groups group-colors]
  (when-not (empty? @groups)
    [:div
     (let [gs @groups
           category-str @category
           idx-groups-colours (map vector (range) gs)
           ]
       [:svg {:height (-> gs count inc (* 20) (+ 10))}
        [:text {:x     22
                :y     20
                :style {:font-weight "bold"}}
         category-str]
        [:g
         (doall
           (for [[idx group] idx-groups-colours]
             ^{:key [idx group]}
             [:g {:transform (str "translate(0," (-> idx inc (* 20) (+ 10)) ")")}
              [:text {:x     22
                      :y     9
                      :dy    ".35em"
                      :style {:text-anchor "start"}
                      }
               group]
              [:rect {:x      0
                      :width  18
                      :height 18
                      :fill   (get @group-colors group)}]]
             ))
         ]
        ])
     ]))

(defn set-filtered-genomes! []
  (let [genomes-grouped-by-year (->> @data/genome-name-metadata
                                     vals
                                     (group-by :collection_year))
        year-range (range @min-year @max-year)
        filtered-genomes (->> year-range
                              (map #(get genomes-grouped-by-year %))
                              flatten
                              (map :genome)
                              (remove nil?))]
    (reset! genomes filtered-genomes)))

(defn min-year-change-handler! [e]
  (reset! min-year (int (-> e .-target .-value)))
  (set-filtered-genomes!))

(defn max-year-change-handler! [e]
  (reset! max-year (int (-> e .-target .-value)))
  (set-filtered-genomes!))



(defn year-range-sliders [years-min years-max]
  [:div
   [:div
    "Min. Year: " [:b @min-year]
    [:p
     [:em.pull-left years-min]
     [:em.pull-right @max-year]
     [slider :value min-year :min years-min :max @max-year :change-handler! min-year-change-handler!]]]
   [:div
    "Max. Year: " [:b @max-year]
    [:p
     [:em.pull-left @min-year]
     [:em.pull-right years-max]
     [slider :value max-year :min @min-year :max years-max :change-handler! max-year-change-handler!]]]])

(defn epi-year-sliders-component []
  (when-not (nil? @data/genome-name-metadata)
    (let [years (remove nil? (map :collection_year (vals @data/genome-name-metadata)))
          years-min (apply min years)
          years-max (apply max years)]
      (reset! min-year years-min)
      (reset! max-year years-max)
      (reset! genomes (keys @data/genome-name-metadata))
      (.warn js/console "epi-year-sliders-component")
      [year-range-sliders years-min years-max])))


(defn checkbox-show-nil-items []
  [:div.checkbox
   [:label
    [:input {:name      "epi-show-nil-items"
             :id        "epi-show-nil-items"
             :type      "checkbox"
             :checked   @show-nil-items
             :on-change #(reset! show-nil-items (not @show-nil-items))}]
    "Show all items?"
    ]
   ]
  )

(defn checkbox-descending-order []
  [:div.checkbox
   [:label
    [:input {:type      "checkbox"
             :checked     @descending-order?
             :on-change #(reset! descending-order? (not @descending-order?))}]
    "Display years in descending order?"
    ]
   ]
  )

(defn show-n-filtered []
  (let [n-genomes-total (count @data/genome-name-metadata)
        n-genomes (count @genomes)]
    [:div.alert.alert-info
     (str "Filtering for " n-genomes " of " n-genomes-total
          (utils/pluralize "genome" n-genomes-total))]))

(defn bar-chart-panel []
  [:div.panel.panel-default
   [:div.panel-heading
    [:h4
     [:a {:data-toggle "collapse"
          :href        "#epi-bar-chart-panel"}
      [:i.fa.fa-bar-chart.fa-rotate-90]
      " Distribution of " [:b @epi-category]
      " from "
      [:b @min-year] "-"
      [:b @max-year]
      [:span.caret.pull-right]]]]
   [:div.panel-collapse.collapse.in {:id "epi-bar-chart-panel"}
    [:div.panel-body
     [:div.row
      [:div.col-md-11
       [:div#bar-chart-container
        [epi-bar-chart]]
       ]
      [:div.col-md-1
       [svg-legend epi-category epi-groups epi-group-colors]
       ]]]]])

(defn selection-table-header []
  (when-not (empty? @selected-genomes)
    (let [n-genomes (count @selected-metadata)
          genome-label (str n-genomes " " (utils/pluralize "genome" n-genomes))]
      [:div
       [:h3 (str "Temporal bar chart selection table for " genome-label)]
       ])))

(defn selection-table []
  (when-not (empty? @selected-metadata)
    [slick-grid-table
     "mst-selection-table"
     selected-metadata
     :genome
     (map utils/keyword->str @data/default-cols)
     (atom [])
     selection-table-grid
     selection-table-dataview]
    ))

(defn table-panel []
  (when-not (empty? @selected-genomes)
    [:div.panel.panel-default
     [:div.panel-heading
      [:h4
       [:a {:data-toggle "collapse"
            :href "#temporal-selection-table-panel"}
        [:i.fa.fa-table]
        " Selection table"
        [:span.caret.pull-right]]]]
     [:div.panel-collapse.collapse.in {:id "temporal-selection-table-panel"}
      [:div.panel-body
       [:div
        [selection-table-header]
        [selection-table]]]]
     ]
    )
  )

(defn clear-selection-button []
  (when-not (empty? @selected-genomes)
    [:button.btn.btn-danger
     {:on-click #(clear-genome-selection!)}
     [:i.fa.fa-ban]
     (let [n-genomes (count @selected-genomes)]
       (str " Clear selection (" n-genomes " " (utils/pluralize "genome" n-genomes) ")"))
     ]))

(defn temporal-epi-page []
  (when @show-plot?
    [:div.row
     [:div.col-md-2
      [:div.well.well-sm
       [:h4 "Filter genomes by date"]
       [epi-year-sliders-component]
       [show-n-filtered]
       [clear-selection-button]
       [category-select]
       [group-select]
       [checkbox-show-nil-items]
       [checkbox-descending-order]
       ]]
     [:div.col-md-9
      [bar-chart-panel]
      [table-panel]
      ]]))

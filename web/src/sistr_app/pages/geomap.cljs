(ns ^:figwheel-load sistr-app.pages.geomap
  (:require
    [cljsjs.leaflet]
    [reagent.core :as reagent :refer [atom]]
    [sistr-data.data :as data]
    [sistr-utils.utils :as utils]
    [sistr-components.alerts :refer [alert-dismissable]]
    [sistr-components.slickgrid :refer [slick-grid-table]]
    [sistr-components.selectize :refer [single-selectize multi-selectize]]
    [sistr-components.bootstrap :refer [slider panel well-collapsible]]
    [sistr-components.selection-manager :as selection]))

(defonce show-map? (atom false))
(defonce leaflet-obj (atom nil))
(defonce category (atom "serovar_predicted"))
(defonce groups (atom nil))
(defonce selection (atom #{}))
(defonce points (atom nil))
(defonce selected-metadata (atom nil))
(defonce category-group-items (atom []))
(defonce genome-metadata (atom nil))

(defonce view-history (atom {}))
(defonce table-grid (atom nil))
(defonce table-dataview (atom nil))


(defonce point-min-size (atom 5))
(defonce point-max-size (atom 35))
(defonce point-opacity (atom 70))


(add-watch data/genome-name-metadata :update-genome-metadata
           (fn [_ _ _ new-value]
             (prn "geomap genome-name-metadata changed!")
             (let [ks (keys @genome-metadata)
                   m (->> ks (map #(find new-value %)) (into {}))]
               (cond
                 (nil? new-value) (do
                                    (reset! genome-metadata nil))
                 (nil? @genome-metadata) (do
                                           (reset! genome-metadata new-value)
                                           (let [sorted-genomes (->> new-value keys sort)]
                                             (swap! view-history assoc sorted-genomes new-value))
                                           )
                 :else (do
                         (reset! genome-metadata m)
                         (swap! view-history assoc ks m))
                 ))
             ))

(defn set-category-group-items! []
  (reset! category-group-items (->> @genome-metadata
                                    vals
                                    (filter #(not (nil? (:source_country %))))
                                    (map (keyword @category))
                                    )))

(defn show-selected-genomes []
  [:div
   [:div
    (when-not (= @genome-metadata @data/genome-name-metadata)
      [:button.btn.btn-success
       {:on-click (fn [_]
                    (reset! genome-metadata @data/genome-name-metadata)
                    (let [sorted-genome-names (->> @data/genome-name-metadata keys sort)]
                      (let [md (get @view-history sorted-genome-names)]
                        (when (= md nil)
                          (swap! view-history assoc sorted-genome-names @genome-metadata))))
                    (set-category-group-items!))}
       "Show all"])]
   (when-not (empty? @data/genome-selection)
     (let [md (get @view-history @data/genome-selection)]
       (when (not= md @genome-metadata)
         [:button.btn.btn-danger
          {:on-click (fn [_]
                       (if (not= md nil)
                         (do
                           (reset! genome-metadata md)
                           (set-category-group-items!))
                         (do
                           (reset! genome-metadata (->> @data/genome-selection
                                                        (map #(find @data/genome-name-metadata %))
                                                        (into {})))
                           (swap! view-history assoc @data/genome-selection @genome-metadata)
                           (set-category-group-items!))))}
          (str "Show " (count @data/genome-selection) " genomes only")])))])

(defn map-category-change-handler! [e]
  #_(prn "map-category-change-handler!" e)
  (reset! category (js->clj e))
  (set-category-group-items!))


(defn map-category-select []
  (when-not (and (nil? @data/categories) (nil? @genome-metadata))
    (single-selectize
      "map-category"
      "Select category to highlight"
      data/categories
      category
      map-category-change-handler!)))

(defn map-group-change-handler! [e]
  (when-not (and (nil? @leaflet-obj) (nil? @genome-metadata))
    (reset! groups (map keyword (js->clj e)))
    ))

(defn map-group-select []
  (when-not (nil? @category-group-items)
    (multi-selectize
      "map-groups"
      "Select groups to highlight"
      category-group-items
      groups
      map-group-change-handler!)))

(defn map-group-highlight
  "Create 2 Selectize widgets for controlling what groups to highlight in the
  map plot from which category."
  []
  (deref data/categories)
  (deref genome-metadata)
  (deref leaflet-obj)
  [:div
   [map-category-select]
   [map-group-select]
   ]
  )

(defn map-container-component []
  (deref genome-metadata)
  (deref groups)
  (deref category)
  (deref point-min-size)
  (deref point-max-size)
  (deref point-opacity)
  [:div {:id    "leaflet-map"
         :style {:width  "100%"
                 :height 600}}])

(defn map-mounter! []
  (when-not (nil? @leaflet-obj)
    (.remove @leaflet-obj)
    )
  (let [m (-> (new js/L.map "leaflet-map" (clj->js {:fullscreenControl true}))
              (.setView #js [0 0] 2))]
    ;; Leaflet can use custom map tiling layers
    ;; There are a lot of free options to choose from:
    ;; http://leaflet-extras.github.io/leaflet-providers/preview/
    (-> (js/L.tileLayer
          "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
          (clj->js
            {:attribution "&copy; <a target=\"_blank\" href=\"https://www.openstreetmap.org/copyright\">OpenStreetMap</a>"
             :maxZoom     19}))
        (.addTo m))
    (._initPathRoot m)
    #_(aset js/window "leaflet_map_object" m)
    (reset! leaflet-obj m)))


(defn map-clear-selection []
  (reset! selection #{})
  (-> @points (.classed
                "selected"
                (fn [d]
                  (aset d "selected" false)
                  false))))


(defn scale-point [n & {:keys [min-size max-size] :or {min-size 5 max-size 35}}]
  (-> js/d3 .-scale .log (.range #js [min-size max-size]) (.domain #js [1 n])))

(defn lat-lng-grouped-metadata [md-map]
  (->> md-map
       vals
       (group-by (fn [{:keys [source_lat source_lng]}] [source_lat source_lng]))))

(defn latlng-frequencies [md-map]
  (->> (lat-lng-grouped-metadata md-map)
       (map (fn [[k v]] [k (count v)]))
       (remove (fn [[[lat lng] _]] (= [lat lng] [nil nil])))
       (into {})))

(defn set-selected-metadata! []
  (let [latlng-md-groups (lat-lng-grouped-metadata @genome-metadata)
        sel (flatten (map #(get latlng-md-groups %) @selection))]
    (reset! selected-metadata sel)))

(defn map-updater! []
  (when-not (and (empty? @groups) (nil? @genome-metadata))
    (let [m @leaflet-obj
          lat-lngs-md-groups (lat-lng-grouped-metadata @genome-metadata)
          lat-lng-freqs (latlng-frequencies @genome-metadata)
          point-scaler (scale-point (->> lat-lng-freqs vals (apply max))
                                    :min-size @point-min-size
                                    :max-size @point-max-size)
          map-data (map
                     (fn [[[lat lng] count]]
                       {:latlng     (new js/L.LatLng lat lng)
                        :source_lat lat
                        :source_lng lng
                        :count      count})
                     lat-lng-freqs)
          SOURCE_LAT "source_lat"
          SOURCE_LNG "source_lng"
          pie (-> js/d3 .-layout .pie (.sort nil) (.value #(aget % "value")))
          arc (-> js/d3 .-svg .arc
                  (.outerRadius (fn [d]
                                 (let [lat (-> d (aget "data") (aget SOURCE_LAT))
                                       lng (-> d (aget "data") (aget SOURCE_LNG))]
                                   (point-scaler (get lat-lng-freqs [lat lng])))))
                  (.innerRadius 0))
          svg (-> js/d3 (.select "#leaflet-map svg"))
          get-pie-data! (fn [lat lng category types]
                          (let [gs (get lat-lngs-md-groups [lat lng])
                                types-set (apply hash-set types)
                                colours (utils/qual-colours (count types))
                                type-colours (zipmap types colours)
                                freqs (->> gs
                                           (map category)
                                           frequencies)
                                in-groups (filter (fn [[k _] _] (contains? types-set (keyword (str k)))) freqs)
                                in-groups-map (map (fn [[k v] _] {:value      v
                                                                  :type       k
                                                                  :source_lat lat
                                                                  :source_lng lng
                                                                  :colour     (get type-colours (keyword (str k)))}) in-groups)
                                out-groups (remove (fn [[k _] _] (contains? types-set (keyword (str k)))) freqs)
                                sum-out-groups (reduce + (map last out-groups))]
                            (pie
                              (clj->js
                                (conj in-groups-map
                                      {:value      sum-out-groups
                                       :type       "LEFTOVERS"
                                       :source_lat lat
                                       :source_lng lng
                                       :colour     "white"})))))
          _ (-> svg (.select "g") .remove)
          g (-> svg (.append "g"))
          point-groups (-> g (.selectAll "point-group")
                           (.data (clj->js map-data))
                           .enter
                           (.append "g")
                           (.attr (clj->js {:class "point-group"})))
          point-paths (-> point-groups
                          (.selectAll "path")
                          (.data
                            #(do
                              (get-pie-data! (aget % SOURCE_LAT)
                                             (aget % SOURCE_LNG)
                                             (keyword @category)
                                             (vec @groups)))
                            #(aget (aget % "data") "type")))
          _ (-> point-paths
                .enter
                (.append "path")
                (.attr (clj->js {:d             arc
                                :fill           #(-> % (aget "data") (aget "colour"))
                                :fill-opacity   (* @point-opacity 0.01)
                                :data-toggle    "tooltip"
                                :data-placement "top"
                                :title          (fn [d]
                                                  (let [lat (-> d (aget "data") (aget SOURCE_LAT))
                                                        lng (-> d (aget "data") (aget SOURCE_LNG))
                                                        location (str lat "," lng)
                                                        n (-> d (aget "data") (aget "count"))]
                                                    (str location ": " n (utils/pluralize "genome" n))
                                                    ))})))
          _ (-> point-paths
                (.attr (clj->js {:d           arc
                                :fill         #(-> % (aget "data") (aget "colour"))
                                :fill-opacity (* @point-opacity 0.01)})))
          _ (-> point-paths .exit .remove)
          update! (fn []
                    (let [zoom-level (-> m .getZoom)
                          arc (-> js/d3 .-svg .arc
                                  (.outerRadius (fn [d]
                                                 (let [lat (-> d (aget "data") (aget SOURCE_LAT))
                                                       lng (-> d (aget "data") (aget SOURCE_LNG))]
                                                   (* (point-scaler (get lat-lng-freqs [lat lng])) (/ zoom-level 4))
                                                   )
                                                 ))
                                  (.innerRadius 0))]
                      (-> point-groups
                          (.attr
                            (clj->js {:transform (fn [d]
                                                   (let [latlng (aget d "latlng")
                                                         layer-point (-> m (.latLngToLayerPoint latlng))
                                                         x (.-x layer-point)
                                                         y (.-y layer-point)]
                                                     (str "translate(" x "," y ")")))})))
                      (-> point-paths
                          (.attr
                            (clj->js
                              {:d arc})))))]
      (-> point-groups
          (.on "click" (fn [d]
                        (aset d "selected" (not (aget d "selected")))
                        (let [lat (aget d SOURCE_LAT)
                              lng (aget d SOURCE_LNG)]
                          (reset! selection
                                  (if (aget d "selected")
                                    (conj @selection [lat lng])
                                    (disj @selection [lat lng])))
                          (set-selected-metadata!))
                         (-> point-groups
                             (.classed "selected" #(aget % "selected"))))))
      (reset! points point-groups)
      (-> m (.on "viewreset" update!))
      (update!))))


(defn map-legend []
  (when-not (empty? @groups)
    [:div.map-legend
     (let [groups @groups
           groups-colours (map (fn [x y] [x y]) groups (utils/qual-colours (count groups)))
           idx-groups-colours (map-indexed (fn [i x] [i x]) groups-colours)
           ]
       [:svg {:height (-> groups count inc (* 20) (+ 10))}
        [:text {:x     22
                :y     20
                :style {:font-weight "bold"}}
         @category]
        [:g
         (for [[idx [group colour]] idx-groups-colours]
           ^{:key [idx group colour]}
           [:g {:transform (str "translate(0," (-> idx inc (* 20) (+ 10)) ")")}
            [:text {:x     22
                    :y     9
                    :dy    ".35em"
                    :style {:text-anchor "start"}
                    }
             (utils/keyword->str group)]
            [:rect {:x      0
                    :width  18
                    :height 18
                    :fill   colour}]])]])]))

(defn map-refresh-btn []
  [:button.btn.btn-default
   {:on-click #(do
                (map-mounter!)
                (map-updater!))}
   [:i.fa.fa-refresh] " Refresh Map"])


(defn map-component []
  (reagent/create-class
    {:render               map-container-component
     :component-did-mount  map-mounter!
     :component-did-update map-updater!}))


(defn map-help-info []
  [:ul
   [:li "Select a category to highlight in the map"]
   [:li "You can highlight groups from the selected category in the nodes on the map"]
   [:li "Click on a node to select/deselect that node"]
   [:li "Genome metadata will be shown for selected nodes in a table below the map"]
   ])

(defn map-table-header []
  (let [n-selection (count @selection)
        n-genomes (count @selected-metadata)]
    [:h4 [:i.fa.fa-table] " Selection info for " (str n-selection) " " (utils/pluralize "location" n-selection) " and " (str n-genomes) " " (utils/pluralize "genome" n-genomes)]
    ))

(defn map-table-panel []
  (when-not (empty? @selection)
    [panel
     "map-sel-table-panel"
     [map-table-header]
     "default"
     false
     [slick-grid-table
      "map-table"
      selected-metadata
      :genome
      (map utils/keyword->str @data/default-cols)
      (atom [])
      table-grid table-dataview]]))


(defn download-map-table []
  (when-not (empty? @selection)
    ;; get the selected genomes from the MST node selection
    (let [n-genomes (count @selected-metadata)
          ;; get all metadata headers
          header-kws (->> @selected-metadata
                          first
                          keys
                          sort)
          ;; get CSV headers ordered by selected columns with the rest of the sorted columns appended
          ordered-header-kws (distinct (concat @data/default-cols header-kws))
          csv-data (utils/map->csv-text @selected-metadata ordered-header-kws)
          url-obj (utils/text->blob->obj-url csv-data)
          ]
      [:a.btn.btn-default {:href     url-obj
                           :download (str "subset-" n-genomes (utils/pluralize "genome" n-genomes) "-" (count @selection) "-table.csv")}
       [:i.fa.fa-download.fa-lg.pull-left]
       " Download Table (CSV)"
       [:br]
       (str n-genomes " selected " (utils/pluralize "genome" n-genomes))])))


(defn map-clear-selection-btn []
  (when-not (empty? @selection)
    [:button.btn.btn-danger
     {:on-click #(map-clear-selection)}
     [:i.fa.fa-ban] " Clear selection "
     (let [n (count @selection)
           label (str n " " (utils/pluralize "node" n))]
       [:span.badge label])]))

(defn map-show-number-of-genomes []
  (let [latlng-freqs (latlng-frequencies @genome-metadata)
        n-map-genomes (->> latlng-freqs vals (reduce +))
        n-total-genomes (count @genome-metadata)]
    [:div
     [:p
      [:strong (str "Showing " n-map-genomes " of " n-total-genomes " genomes in map")]]
     [:p
      [:em (str "Only " n-map-genomes " have location information")]]
     [:p (str "Showing data for " (count latlng-freqs) " unique locations")]
     ]
    ))


(defn point-min-size-change-handler! [e]
  (reset! point-min-size (int (-> e .-target .-value))))

(defn point-max-size-change-handler! [e]
  (reset! point-max-size (int (-> e .-target .-value))))

(defn point-size-sliders [min-size max-size]
  [:div
   [:div
    "Min map node size " [:b @point-min-size]
    [:p
     [:em.pull-left min-size]
     [:em.pull-right @point-max-size]
     [slider
      :value point-min-size
      :min min-size
      :max @point-max-size
      :change-handler! point-min-size-change-handler!
      ]]]
   [:div
    "Max map node size " [:b @point-max-size]
    [:p
     [:em.pull-left @point-min-size]
     [:em.pull-right max-size]
     [slider
      :value point-max-size
      :min @point-min-size
      :max max-size
      :change-handler! point-max-size-change-handler!]]]])

(defn point-opacity-slider []
  [:div
   "Map point opacity " [:b @point-opacity]
   [:p
    [:em.pull-left 0]
    [:em.pull-right 100]
    [slider
     :value point-opacity
     :min 0
     :max 100
     :change-handler! #(reset! point-opacity (int (-> % .-target .-value)))]]])

(defn map-plot-param-panel []
  [well-collapsible "map-plot-params-panel"
   [:span [:i.fa.fa-wrench] " Adjust plot"]
   true
   [:div
    [point-size-sliders 1 100]
    [point-opacity-slider]]
   ])

(defn view-history-item [genomes md]
  ^{:key genomes}
  [:tr {
        :style {:background-color (if (= md @genome-metadata)
                                    "rgba(0,100,255,0.2)"
                                    "transparent")
                :cursor           "pointer"}}
   (let [sel-hist-item (ffirst (filter (fn [[_ v]] (= genomes v)) @data/genome-selection-collection))]
     [:td
      {:on-click #(do
                   (reset! genome-metadata md))}
      (when-not (nil? sel-hist-item)
        [:strong (str "[" sel-hist-item "] - ")])
      (let [latlng-freqs (latlng-frequencies md)
            genomes-with-locations (->> latlng-freqs vals (reduce +))
            unique-locations (count latlng-freqs)]
        (str genomes-with-locations "/" (count md) " genomes; " unique-locations " locations"))])
   [:td.pull-right
    [:button.destroy {:type     "button"
                      :on-click #(swap! view-history dissoc genomes)}
     [:i.fa.fa-close.text-danger.fa-lg]]]])

(defn view-history-list []
  [:table.table.table-condensed
   [:tbody
    (map-indexed
      (fn [i [k v]]
        ^{:key i}
        [view-history-item k v])
      @view-history)]])

(defn view-manager-panel []
  [well-collapsible "map-view-manager"
   [:span [:i.fa.fa-globe] " View Manager"]
   false
   [:div
    [:strong "View history"]
    [view-history-list]
    [show-selected-genomes]
    [map-refresh-btn]]])


(defn map-page []
  (when @show-map?
    [:div {:style {:padding-left 300}}
     [:div.sidebar
      [:div.well.well-sm
       [well-collapsible "map-help-info"
        [:span [:i.fa.fa-info-circle] " Help"]
        true
        [map-help-info]]
       [map-show-number-of-genomes]
       [map-group-highlight]
       [map-plot-param-panel]
       [selection/genome-selection-manager "geomap-selection-manager" #(prn "geomap-selection changed")]
       [view-manager-panel]
       [map-clear-selection-btn]
       [download-map-table]
       ]]
     [:div.container-fluid
      [:div.row
       [:div.col-md-10
        [map-component]
        ]
       [:div.col-md-2
        [map-legend]]]
      [:div.row
       [:div.col-md-12
        [map-table-panel]]]]]))

(ns ^:figwheel-load sistr-app.pages.user
  (:require-macros [cljs.core.async.macros :refer [go go-loop]])
  (:require
    [cljsjs.leaflet]
    [cljs.core.async :refer [put! chan <! timeout]]
    [reagent.core :as reagent :refer [atom]]
    [clojure.string :refer [split join]]
    [sistr-data.data :as data]
    [sistr-utils.utils :as utils]
    [ajax.core :refer [POST GET PUT]]
    [sistr-components.bootstrap :refer [progress-bar well-collapsible panel]]
    [sistr-components.alerts :as alerts]
    [sistr-components.slickgrid :as slick-grid :refer [slick-grid-table]]
    [sistr-components.selectize :refer [single-selectize]]
    [sistr-utils.csv :refer [read-csv!]]
    [reagent-forms.core :refer [bind-fields]]))

;=======================
; User genome page data
;=======================
(defonce show-table? (atom false))
(defonce saving-to-server-info (atom nil))
(defonce table-grid (atom nil))
(defonce table-dataview (atom nil))
(defonce genome-metadata (atom nil))

; When the active page changes from the user genomes page to any other page then
; sync the user genome metadata changes with the data in the rest of the app
;TODO: better way to sync app data without incurring expensive changes
(add-watch data/active-page :user-genomes-changed?
           (fn [_ _ old-state new-state]
             (when (and (= :user-genome-page old-state)
                        (not= :user-genome-page new-state))
               (doseq [[genome md] @genome-metadata]
                 (do
                   (when (not= md (get @data/genome-name-metadata genome))
                     (do
                       #_(js/console.info "SOMETHING CHANGED FOR " genome "!"
                                          (clj->js md)
                                          (clj->js (get @data/genome-name-metadata genome)))
                       (swap! data/genome-name-metadata assoc genome md))))))))

(defonce selected-genome (atom #{}))

(defonce genome-metadata-history (atom []))
(defonce genome-metadata-forward-history (atom []))
(defonce selected-genome-md (atom nil))

(defonce leaflet-map-obj (atom nil))
(defonce leaflet-geocoder-obj (atom nil))
(defonce leaflet-marker-obj (atom nil))

; fields which the user cannot edit
(def uneditable-fields [:genome
                        :user_uploader
                        :serovar_prediction
                        :serovar_antigen_prediction
                        :Serogroup_prediction
                        :H1_prediction
                        :H2_prediction
                        :MLST_ST_in_silico
                        :curation_info])

;==============
; Util helpers
;==============
(defn padded-int [v]
  (str (if (< v 10) "0" "") v))

(defn metadata-typeahead-choices [key text]
  (filter #(-> % (.toLowerCase %) (.indexOf text) (> -1))
          (->> @data/genome-name-metadata
               vals
               (map (keyword key))
               distinct
               (remove nil?))))

;============
; UI helpers
;============
(defn row [label input]
  [:div.row
   [:div.col-md-2 [:label label]]
   [:div.col-md-4 input]])

(defn input-row [label type id]
  (row label [:input.form-control {:field type :id id}]))

(defn input [label type id]
  [:div
   [:div.col-md-2 [:label label]]
   [:div.col-md-4 [:input.form-control {:field type :id id}]]])

(defn set-init-selected-genome [zm-md]
  (when (empty? @selected-genome)
    (reset! selected-genome (into #{} [(->> zm-md keys first)]))))

(defn init-user-genome-metadata! []
  (let [md (get (->> @data/genome-name-metadata
                     vals
                     (group-by :user_uploader))
                @data/sistr-user)
        zm-md (zipmap (map :genome md) md)]
    (js/console.info " init-user-genome-metadata!")
    (reset! genome-metadata zm-md)
    (swap! genome-metadata-history assoc-in [0] zm-md)
    #_(set-init-selected-genome zm-md)))

(defn loading-genome-data []
  (if (empty? @data/genome-name-metadata)
    [:div.alert.alert-warning
     [:h1 "Loading genome data..." [:i.fa.fa-spinner.fa-spin.fa-lg]]]
    (do
      (init-user-genome-metadata!)
      [:div])))

(defonce search-string (atom ""))

(defn input-box [label val]
  [:div.form-horizontal
   [:label.control-label label]
   [:input.form-control
    {:value     @val
     :on-change #(reset! val (-> % .-target .-value))}]])

(defn filter-genomes []
  [input-box "Filter By Genome Name" search-string])

(defn genome-metadata-table []
  #_[t/Table filtered-genome-metadata data/default-cols :selection selected-genome]
  [slick-grid-table
   "user-genome-metadata-table"
   genome-metadata
   :genome
   (map utils/keyword->str @data/default-cols)
   selected-genome
   table-grid
   table-dataview])

(add-watch search-string :update-filter
           (fn [_ _ _ v]
             (slick-grid/update-filter! table-grid table-dataview v)))

(defn download-genome-metadata-table []
  (let [header-kws (->> @genome-metadata
                        vals
                        first
                        keys
                        sort)
        ordered-header-kws (distinct (concat @data/default-cols header-kws))
        csv-data (utils/map->csv-text (vals @genome-metadata) ordered-header-kws)
        obj-url (utils/text->blob->obj-url csv-data)]
    [:a.btn.btn-default {:href     obj-url
                         :download (str "SISTR-" (count @genome-metadata) "_genomes-table.csv")}
     [:i.fa.fa-download] " Download Table (CSV)"]))

(defn clear-genome-selection []
  [:button.btn.btn-danger
   {:on-click #(reset! selected-genome #{})}
   "Clear Selection"])

(defn re-analyze-selected-genomes-btn []
  (when-not (empty? @selected-genome)
    [:button.btn.btn-danger
     {:on-click (fn [_]
                  (let [genomes (apply vector @selected-genome)]
                    (doseq [genome genomes]
                      (do
                        (sistr-app.pages.upload/upload-genome-fasta! genome nil :reanalyze true)
                        (swap! data/upload-countdown inc)))
                    (js/alert (str "Rerunning SISTR analysis on: "
                                   (apply str (interpose "; " genomes))))))
      :title    "Redo SISTR analysis for selected genome(s)"}
     [:i.fa.fa-repeat] (str " Reanalyze " (count @selected-genome) " Selected Genome(s)")]))

(defn refetch-app-data-btn []
  [:button.btn.btn-warning
   {:on-click (fn [_]
                (data/get-all-data-async!))}
   "Re-fetch app data"])

(defn user-genome-table-panel []
  (when (and @show-table? (not (empty? @genome-metadata)))
    [panel
     "user-genome-panel"
     [:h4 [:i.fa.fa-table] " " @data/sistr-user "'s Genomes"]
     "default"
     false
     [:div
      #_[clear-genome-selection]
      [re-analyze-selected-genomes-btn]
      #_[refetch-app-data-btn]
      [filter-genomes]
      [genome-metadata-table]
      [download-genome-metadata-table]]
     ]))

(defn find-closest-country-region [lat lng]
  (let [geos (distinct (map (fn [{:keys [source_lat source_lng source_country source_region]}]
                              [source_lat source_lng source_country source_region])
                            (vals @data/genome-name-metadata)))]
    (->> geos (map (fn [[lat-geos lng-geos country region]]
                     (let [dlat (- lat-geos lat)
                           dlng (- lng-geos lng)
                           dlat2 (* dlat dlat)
                           dlng2 (* dlng dlng)]
                       {:distance (+ dlat2 dlng2)
                        :lat      lat
                        :lng      lng
                        :country  country
                        :region   region}
                       )))
         (sort-by :distance)
         first)))

(defn metadata-field-typeahead-input [id-key label]
  [:div
   [:div.col-md-2 [:label label]]
   [:div.col-md-4 [:div {:field           :typeahead
                         :id              (keyword id-key)
                         :data-source     (partial metadata-typeahead-choices (keyword id-key))
                         :input-class     "form-control"
                         :list-class      "typeahead-list"
                         :item-class      "typeahead-item"
                         :highlight-class "highlighted"
                         :clear-on-focus? false}]]])

(def form-template
  [:div
   [:h4 "Serovar and Subspecies Information"]
   [:div.row
    (metadata-field-typeahead-input :serovar "Curated Serovar")
    (metadata-field-typeahead-input :reported_serovar "Reported Serovar")]
   [:div.row
    (metadata-field-typeahead-input :subspecies "Subspecies")
    ]
   [:br]
   [:h4 [:i.fa.fa-stethoscope] " Source Information"]
   [:div.row
    (metadata-field-typeahead-input :source_type "Source Type")
    (metadata-field-typeahead-input :source_info "Source Info")]
   [:div.row
    (metadata-field-typeahead-input :host_common_name "Host species (common name)")
    (metadata-field-typeahead-input :host_latin_name "Host species (latin name)")]
   [:br]
   [:h4 [:i.fa.fa-calendar] " Temporal Information"]
   (row "Collection Date" [:div {:field       :datepicker
                                 :id          :collection_date
                                 :date-format "yyyy/mm/dd"
                                 :in-fn       #(when % (zipmap [:year :month :day]
                                                               (map int (split % #"/"))))
                                 :out-fn      #(when %
                                                (apply str
                                                       (interpose "/"
                                                                  [(:year %)
                                                                   (padded-int (:month %))
                                                                   (padded-int (:day %))])))
                                 :inline      false
                                 :auto-close  true}])
   [:br]
   [:h4 [:i.fa.fa-globe] " Location Information"]
   [:div.row
    (input "Source country" :text :source_country)
    (input "Source region" :text :source_region)]
   [:div.row
    (input "GPS Latitude" :numeric :source_lat)
    (input "GPS Longitude" :numeric :source_lng)]
   ])

(defn parse-collection-date-month
  [[id] _ {:keys [collection_date] :as document}]
  (when (and (some #{id} [:collection_date])
             (not (or (nil? collection_date) (empty? collection_date)))
             (re-matches #"[12]\d{3}/\d{1,2}/\d{1,2}" collection_date))
    (let [date-map (zipmap [:year :month :day] (map int (split collection_date #"/")))]
      (assoc document :collection_month (:month date-map))
      )))

(defn parse-collection-date-year
  [[id] _ {:keys [collection_date] :as document}]
  (when (and (some #{id} [:collection_date])
             (not (or (nil? collection_date) (empty? collection_date)))
             (re-matches #"[12]\d{3}/\d{1,2}/\d{1,2}" collection_date))
    (let [date-map (zipmap [:year :month :day] (map int (split collection_date #"/")))]
      (assoc document :collection_year (:year date-map))
      )))

(defn host-common-to-latin-name
  [[id] _ {:keys [host_common_name] :as document}]
  (when (and (some #{id} [:host_common_name])
             (or (not (nil? host_common_name))
                 (not (empty? host_common_name))))
    (let [common-latin (->> @data/genome-name-metadata
                            vals
                            (map (fn [{:keys [host_common_name host_latin_name]}]
                                   {host_common_name host_latin_name}))
                            (remove #(= {nil nil} %))
                            (into {}))
          latin-name-match (get common-latin host_common_name)]
      #_(prn "common to latin" host_common_name latin-name-match)
      (when latin-name-match
        (do
          #_(prn "latin-name-match not nil!" latin-name-match)
          (assoc document :host_latin_name latin-name-match))))))

(defn leaflet-marker-icon []
  (new js/L.icon (clj->js {:iconUrl       "js/lib/images/marker-icon.png"
                           :iconRetinaUrl "js/lib/images/marker-icon-2x.png"
                           :iconSize      [20 31]
                           :iconAnchor    [10 30]
                           :popupAnchor   [0 -24]
                           :shadowUrl     "js/lib/images/marker-shadow.png"
                           :shadowSize    [30 31]
                           :shadowAnchor  [10 30]
                           })))

(defn leaflet-map-component []
  (deref selected-genome-md)
  [:div {:id    "user-genome-leaflet-map"
         :style {:width  "100%"
                 :height 300}}])

(defn leaflet-map-updater! []
  (js/console.info "Updating leaflet map")
  (let [m @leaflet-map-obj
        marker @leaflet-marker-obj]
    (js/console.info "update leaflet map" marker)
    (let [{:keys [source_lat source_lng source_region source_country]} @selected-genome-md]
      (when marker
        (-> m (.removeLayer marker)))
      #_(prn "lat lng" lat lng)
      (if (or (nil? source_lat) (nil? source_lng))
        (do
          (reset! leaflet-marker-obj nil)
          )
        (let [icon (leaflet-marker-icon)
              new-marker (-> (js/L.marker #js [source_lat source_lng]
                                          (clj->js {:icon icon}))
                             (.addTo m))]
          (-> new-marker
              (.bindPopup (str "<p><em>Country</em>: <b>" source_country "</b></p>"
                               "<p><em>Location Info</em>: " source_region "</p>"
                               "<p><em>Lat/Lng</em>: " source_lat "," source_lng "</p>"))
              (.openPopup))
          #_(aset js/window "leaflet_marker" new-marker)
          (reset! leaflet-marker-obj new-marker)
          )
        ))
    ))

(defn leaflet-map-mounter! []
  (js/console.info "mounting leaflet map")
  (when-not (nil? @leaflet-map-obj)
    (.remove @leaflet-map-obj)
    )
  (let [m (-> (new js/L.map
                   "user-genome-leaflet-map"
                   (clj->js {:fullscreenControl true}))
              (.setView #js [25 0] 2))
        geocoder (-> (js/L.Control.geocoder (clj->js {:collapsed false}))
                     (.addTo m))]
    ;; Add markGeocode function to set marker to location of geocode results and save geocode results
    (aset geocoder "markGeocode" (fn [r]
                                   (js/console.info "markGeocode" r geocoder)
                                   (when @leaflet-marker-obj
                                     (-> m (.removeLayer @leaflet-marker-obj)))
                                   (js/console.info "Map" m)
                                   (let [marker (-> (js/L.marker (aget r "center")
                                                                 (clj->js {:icon (leaflet-marker-icon)}))
                                                    (.bindPopup (aget r "html"))
                                                    (.addTo m)
                                                    (.openPopup))
                                         _ (js/console.info "marker" marker)
                                         address (js->clj (-> r (aget "properties") (aget "address")) :keywordize-keys true)
                                         _ (js/console.info "address" address)
                                         region (apply str
                                                       (interpose
                                                         ", "
                                                         (remove nil? [(:city address)
                                                                       (:county address)
                                                                       (:state address)
                                                                       (:country address)])))
                                         _ (js/console.info "region" region)
                                         country (:country address)
                                         lat (-> r (aget "center") (aget "lat"))
                                         lng (-> r (aget "center") (aget "lng"))]

                                     #_(aset js/window "geocode_result" r)
                                     (swap! selected-genome-md assoc :source_lat lat)
                                     (swap! selected-genome-md assoc :source_lng lng)
                                     (swap! selected-genome-md assoc :source_country country)
                                     (swap! selected-genome-md assoc :source_region region)
                                     (reset! leaflet-marker-obj marker))))
    (reset! leaflet-geocoder-obj geocoder)
    #_(aset js/window "geocoder_obj" geocoder)
    ;; Leaflet can use custom map tiling layers
    ;; There are a lot of free options to choose from:
    ;; http://leaflet-extras.github.io/leaflet-providers/preview/
    (-> (js/L.tileLayer
          "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
          (clj->js
            {:attribution "&copy; <a target=\"_blank\" href=\"https://www.openstreetmap.org/copyright\">OpenStreetMap</a>"
             :maxZoom     19}))
        (.addTo m))
    (._initPathRoot m)
    (-> m (.on "click"
               (fn [e]
                 (let [lat (-> e (aget "latlng") (aget "lat"))
                       lng (-> e (aget "latlng") (aget "lng"))
                       geo-find (find-closest-country-region lat lng)]
                   #_(prn "closest geo match" geo-find)
                   (when @leaflet-marker-obj
                     (-> m (.removeLayer @leaflet-marker-obj)))
                   (let [marker (-> (js/L.marker #js [lat lng]
                                                 (clj->js {:icon (leaflet-marker-icon)}))
                                    (.addTo m))]
                     (reset! leaflet-marker-obj marker))
                   ;https://nominatim.openstreetmap.org/reverse?format=json&lat=5.0&lon=1.0&zoom=18&addressdetails=1
                   (go (let [rgeo-results (<! (utils/get-uri->chan
                                                (str "https://nominatim.openstreetmap.org/reverse?format=json&lat="
                                                     lat
                                                     "&lon="
                                                     lng
                                                     "&zoom=18&addressdetails=1"
                                                     )
                                                #(js/console.error "reverse geocoding error" %)))]
                         (if (nil? (:error rgeo-results))
                           (do
                             (let [rgeo-lat (:lat rgeo-results)
                                   rgeo-lng (:lon rgeo-results)
                                   rgeo-country (get-in rgeo-results [:address :country])
                                   rgeo-region (if (= rgeo-country (:display_name rgeo-results))
                                                 nil
                                                 (:display_name rgeo-results))]
                               (swap! selected-genome-md assoc :source_lat rgeo-lat)
                               (swap! selected-genome-md assoc :source_lng rgeo-lng)
                               (swap! selected-genome-md assoc :source_country rgeo-country)
                               (swap! selected-genome-md assoc :source_region rgeo-region)))
                           (do
                             (swap! selected-genome-md assoc :source_lat lat)
                             (swap! selected-genome-md assoc :source_lng lng)
                             (swap! selected-genome-md assoc :source_country (:country geo-find))
                             (swap! selected-genome-md assoc :source_region (:region geo-find))))))))))
    (reset! leaflet-map-obj m)
    (leaflet-map-updater!)))

(defn map-refresh-btn []
  [:button.btn.btn-default
   {:on-click #(do
                (leaflet-map-mounter!)
                (leaflet-map-updater!))}
   [:i.fa.fa-refresh] " Refresh map!"])

(defn leaflet-map []
  (reagent/create-class
    {:render               leaflet-map-component
     :component-did-mount  leaflet-map-mounter!
     :component-did-update leaflet-map-updater!}))

(defn freq-md-cgmlst
  [data value level & {:keys [md-key] :or {md-key :serovar_predicted}}]
  (let [cgmlst-groupings (->> data
                              (group-by (keyword level)))]
    (frequencies (map (keyword md-key) (get cgmlst-groupings value)))))

(defn antigenic-formula [predictions genome]
  (let [genome-predictions (->> predictions
                                (filter #(= (:genome %) genome))
                                first)
        {:keys [Serogroup_prediction H1_prediction H2_prediction]} genome-predictions]
    (str Serogroup_prediction ":" H1_prediction ":" H2_prediction)
    ))

(defn serovar-predictions-at-different-cgmlst-levels-table []
  (let [cgmlst-levels [:cgMLST_cluster_100%
                       :cgMLST_cluster_99%
                       :cgMLST_cluster_95%
                       :cgMLST_cluster_80%
                       :cgMLST_cluster_70%]
        cgmlst-levels-str (map #(second (re-find #"^.+_(\d+\%)" (utils/keyword->str %))) cgmlst-levels)
        serovar-predicted (:serovar_predicted @selected-genome-md)
        predicted-antigenic-formula (antigenic-formula @data/serovar-prediction-results (:genome @selected-genome-md))
        freqs-pred-at-levels (map #(freq-md-cgmlst (vals @data/genome-name-metadata)
                                                   (get @selected-genome-md %)
                                                   %)
                                  cgmlst-levels)
        ]
    [:div
     [:h5 "Genome name: " [:strong (first @selected-genome)]]
     [:h5 "Predicted serovar: " [:strong serovar-predicted] " " [:em "(" predicted-antigenic-formula ")"]]
     [:table.table.table-striped
      [:thead
       [:tr
        [:th "cgMLST cluster similarity"]
        [:th "Top Prediction"]
        [:th "# / Total"]
        [:th "Other predictions"]]]
      [:tbody
       (doall
         (map-indexed
           (fn [i [xs lvl]]
             (let [sum (->> xs vals (reduce +))]
               ^{:key i}
               [:tr
                [:td lvl]
                [:td (ffirst xs)]
                [:td (str (-> xs first second) " / " sum)]
                [:td (apply str (->> (rest xs)
                                     (map (fn [[k v]] (str k " (" v "/" sum ")")))
                                     (interpose " | ")))]]))
           (partition 2 (interleave freqs-pred-at-levels cgmlst-levels-str))))
       ]]]))

(defn select-curation-code []
  (deref selected-genome-md)
  (let [genome-curation (reagent/cursor selected-genome-md [:curation_info])
        curation-codes (atom (->> @data/serovar-curation-info
                                  (map :code)))]
    [:div
     [:h4 "Genome Serovar Curation"]
     [:div.row
      [:div.col-md-6
       [single-selectize "genome-curation-code" "Serovar curation code"
        curation-codes
        genome-curation
        #(reset! genome-curation %)]]
      [:div.col-md-6
       (let [matching-curation-info (->> @data/serovar-curation-info
                                         (filter #(= @genome-curation (:code %)))
                                         first)]
         [:div
          [:strong (:code matching-curation-info)]
          [:p (:description matching-curation-info)]
          [:p "Trusted? " (if (:is_trusted matching-curation-info)
                            [:i.fa.fa-check.text-success]
                            [:i.fa.fa-ban.text-danger])]]
         )]]]))

(defn edit-form []
  (fn []
    [:div.container-fluid
     [:div
      (when (= @data/sistr-user data/public-sistr-user)
        [select-curation-code])
      [serovar-predictions-at-different-cgmlst-levels-table]]
     [bind-fields form-template selected-genome-md
      parse-collection-date-month
      parse-collection-date-year
      host-common-to-latin-name]
     [map-refresh-btn]
     [leaflet-map]
     ]))

(defn put->chan [uri data & {:keys [headers] :or {headers {}}}]
  (let [c (chan)]
    (PUT uri {:params          data
              :handler         #(put! c %)
              :error-handler   #(put! c {:error %})
              :format          :json
              :response-format :json
              :keywords?       true
              :headers         headers})
    c))

(defn save-genome-metadata-to-server! [genome-name md]
  (go
    (reset! saving-to-server-info
            [:div.alert.alert-info
             [:span [:i.fa.fa-spinner.fa-spin] " Saving updated metadata to server..."]])
    (let [put-uri (str data/api-uri
                       "user/" @data/sistr-user
                       "/genome/" genome-name
                       "/metadata")
          auth-headers (<! (data/auth->chan!))
          prepped-md (->> md
                          (utils/remove-forbidden-keys uneditable-fields)
                          (utils/remove-forbidden-keys [:host_common :host_latin])
                          (utils/remove-keys-by-regex #"^cgMLST_.*")
                          (utils/regex-rename-keys #"^source_(lat|lng|region|country)")
                          (utils/regex-rename-keys #"^(host_\w+)_name")
                          (into {}))
          resp (<! (put->chan put-uri {:metadata      prepped-md
                                       :curation_code (:curation_info md)} :headers auth-headers))
          error-resp (:error resp)]
      (js/console.info "PUT request data" (clj->js prepped-md))
      (if error-resp
        (reset! saving-to-server-info
                [:div.alert.alert-danger [:span [:i.fa.fa-exclamation-triangle] " Could not save metadata for genome '" genome-name "' to server! HTTP code " (:status error-resp)]])
        (reset! saving-to-server-info
                [:div.alert.alert-success [:span [:i.fa.fa-floppy-o] " Updated metadata for genome '" genome-name "' saved to server!"]]))
      (prn resp)
      (prn error-resp)
      )))

(defn save-changes-btn []
  (let [args (if (= (get @genome-metadata (first @selected-genome)) @selected-genome-md)
               {:disabled "disabled"}
               {})]
    [:button.btn.btn-primary
     (merge args
            {:on-click
             (fn []
               (swap! genome-metadata-history conj @genome-metadata)
               (swap! genome-metadata assoc (first @selected-genome) @selected-genome-md)
               (reset! genome-metadata-forward-history [])
               (save-genome-metadata-to-server! (first @selected-genome) @selected-genome-md))})
     [:i.fa.fa-floppy-o] " Save Changes"]))

(defn undo-changes-btn []
  (let [args (if (> (count @genome-metadata-history) 1)
               {}
               {:disabled "disabled"})]
    [:button.btn.btn-danger
     (merge args
            {:on-click
             (fn []
               (swap! genome-metadata-forward-history conj @genome-metadata)
               (reset! genome-metadata (peek @genome-metadata-history))
               (reset! genome-metadata-history (pop @genome-metadata-history))
               (save-genome-metadata-to-server! (first @selected-genome) @selected-genome-md))})
     [:i.fa.fa-undo] " Undo " [:span.badge (dec (count @genome-metadata-history))]]))

(defn redo-changes-btn []
  (let [args (if (empty? @genome-metadata-forward-history)
               {:disabled "disabled"}
               {})]
    [:button.btn.btn-warning
     (merge args {:on-click
                  (fn []
                    (swap! genome-metadata-history conj @genome-metadata)
                    (reset! genome-metadata (peek @genome-metadata-forward-history))
                    (reset! genome-metadata-forward-history (pop @genome-metadata-forward-history))
                    (save-genome-metadata-to-server! (first @selected-genome) @selected-genome-md))})
     [:i.fa.fa-repeat] " Redo " [:span.badge (count @genome-metadata-forward-history)]]))

(defn edit-panel-genome-metadata []
  (when-not (empty? (first @selected-genome))
    (do
      (reset! selected-genome-md (get @genome-metadata (first @selected-genome)))
      [panel
       "edit-metadata-panel"
       [:h4 [:i.fa.fa-edit] (str " Edit Genome '" (first @selected-genome) "' Metadata")]
       "default"
       false
       [:div
        [save-changes-btn]
        [undo-changes-btn]
        [redo-changes-btn]
        [edit-form]
        [:br]
        [save-changes-btn]
        [undo-changes-btn]
        [redo-changes-btn]]
       ])
    ))

(defn diff-map-values [[x y]]
  (let [allkeys (distinct (mapcat keys [x y]))
        diff-keys (filter #(not= (get x %) (get y %)) allkeys)]
    (map (fn [k]
           {:old (get x k) :new (get y k) :key k})
         diff-keys)))

(defn diff-array-maps [x y]
  (let [x-keys (keys x)
        diff-keys (filter #(not= (get x %) (get y %)) x-keys)
        diffs (map (fn [v] [(get x v) (get y v)]) diff-keys)]
    (zipmap diff-keys (map diff-map-values diffs))))

(defn diff-view []
  (let [orig-md (first @genome-metadata-history)
        curr-md @genome-metadata
        diff-mds (diff-array-maps orig-md curr-md)]
    (when-not (empty? diff-mds)
      [panel
       "user-changes-diff-view-panel"
       [:h4 [:i.fa.fa-eye] " View Genome Metadata Changes"]
       "default"
       false
       [:div
        [alerts/alert-dismissable
         "info"
         ^{:key 0}
         [:div
          [:p "All genome metadata changes are shown in this panel."]
          [:p "Changes will be committed to rest of app on page change."]]]
        [:ul
         (map-indexed
           (fn [i [genome vs]]
             ^{:key i}
             [:li [:strong genome]
              [:button.btn.btn-danger.pull-right
               {:on-click (fn [_]
                            (let [old-md (get orig-md genome)]
                              (swap! genome-metadata-history conj curr-md)
                              (reset! genome-metadata-forward-history [])
                              (swap! genome-metadata assoc genome old-md)
                              (save-genome-metadata-to-server! genome old-md)))}
               [:i.fa.fa-ban] " Revert to original values"]
              [:table.table.table-condensed.table-striped
               [:thead
                [:tr
                 [:th "Field"]
                 [:th "Original value"]
                 [:th "Current value"]]]
               [:tbody
                (map-indexed
                  (fn [j {:keys [old new key]}]
                    ^{:key j}
                    [:tr [:td (utils/keyword->str key)]
                     [:td old]
                     [:td new]])
                  vs)]]])
           diff-mds)]]])))

(defn update-table-selection! [genomes]
  (slick-grid/update-selection @table-grid @table-dataview genomes))

(defn refresh-table! []
  (when-not (nil? @table-grid)
    (.resizeCanvas @table-grid)
    (update-table-selection! @selected-genome)))


(defn user-genomes-page-help []
  [well-collapsible "user-genomes-page-help"
   [:span [:i.fa.fa-info-circle] " Help"]
   false
   [:div
    [:h5 [:i.fa.fa-edit] " Edit the metadata information of your genomes"]
    [:ul
     [:li "Select a genome in the table in the main panel to edit its metadata"]
     [:li "Most metadata fields have typeahead suggestions to aid in filling out the metadata info for your genomes"]
     [:li "Click a point on the map or search for a location to perform automatic geocoding!"]
     ]]])

(defn curation-code-info-list []
  [panel
   "user-curation-info-list-panel"
   [:h4 [:i.fa.fa-table] " Serovar Curation Codes Table"]
   "default"
   true
   [:div
    [:table.table.table-condensed.table-striped
     [:thead>tr
      [:th "Trusted?"]
      [:th "Code"]
      [:th "Description"]]
     [:tbody
      (map-indexed
        (fn [i {:keys [code description is_trusted]}]
          ^{:key i}
          [:tr
           [:td (if is_trusted
                  [:i.fa.fa-check.text-success]
                  [:i.fa.fa-ban.text-danger])]
           [:td code]
           [:td description]])
        @data/serovar-curation-info)]]
    [:div.alert.alert-warning
     [:em "Serovar curation codes only apply to public genomes belonging to user 'sistr'"]]
    [:div.alert.alert-info
     [:p "The curation code of a genome and whether it is trusted determines if it will be used for cgMLST based serovar prediction. The 'Curated Serovar' metadata field is used to determine the cgMLST based serovar prediction along with the serovar curation code. "]]]])


(defn user-genomes []
  (when (data/logged-in? @data/sistr-user @data/sistr-user-token)
    [:div {:style {:padding-left 300}}
     [:div.sidebar
      [:div.well.well-sm
       [user-genomes-page-help]
       [:div
        @saving-to-server-info]]
      ]
     [:div.container-fluid
      [:div.row
       [:div.col-md-10
        [loading-genome-data]
        (if (and (empty? @data/genome-name-metadata) (empty? @genome-metadata))
          [:div.alert.alert-warning
           [:h3 "No genomes have been uploaded and analyzed!"]
           [:p "All genomes uploaded by the current user will be shown here"]]
          [:div
           [curation-code-info-list]
           [edit-panel-genome-metadata]
           [diff-view]
           [user-genome-table-panel]])]]]]))

(ns ^:figwheel-load sistr-app.pages.results
  (:require-macros [cljs.core.async.macros :refer [go go-loop]])
  (:require
    [cljs.core.async :refer [put! chan <! timeout]]
    [reagent.core :refer [atom]]
    [sistr-data.data :as data]
    [sistr-utils.utils :as utils]
    [sistr-components.bootstrap :refer [panel well-collapsible]]
    [sistr-components.slickgrid :as slick-grid :refer [slick-grid-table]]
    [sistr-components.filtered-selection :refer [dynamite-filtered-selection]]
    [sistr-components.dimple-charts :as dimple]
    [sistr-components.selection-manager :as selection]
    [sistr-app.pages.about :refer [serovar-variant-warning-info]]))

(defonce show-table? (atom false))
(defonce serovar-prediction-results (atom []))
(defonce serovar-prediction-results-map-lists (atom {}))
(defonce table-grid (atom nil))
(defonce table-dataview (atom nil))

(defonce filtered-genomes (atom []))
(defonce dynamite-obj (atom nil))

(defn loading-prediction-results []
  (if (empty? @data/serovar-prediction-results)
    [:div.alert.alert-warning
     [:h1 "Loading serovar prediction results... " [:i.fa.fa-spinner.fa-spin.fa-lg]]]
    (let [results @data/serovar-prediction-results]
      (reset! serovar-prediction-results-map-lists (utils/list-maps->map-lists results))
      (reset! serovar-prediction-results results)
      nil)
    ))

(defn get-filtered-genomes!
  ([]
   (let [gs (map :genome @data/serovar-prediction-results)]
     (reset! filtered-genomes gs)
     (reset! serovar-prediction-results @data/serovar-prediction-results)))
  ([gs]
   (let [genome-results (zipmap (map :genome @data/serovar-prediction-results)
                                @data/serovar-prediction-results)]
     (reset! filtered-genomes gs)
     (reset! serovar-prediction-results (map #(get genome-results %) gs)))))

(defn filter-panel []
  (when-not (empty? @serovar-prediction-results)
    [panel
     "results-filter-panel"
     [:h4 [:i.fa.fa-filter] " Filter Serovar Prediction Results Table"]
     "default"
     true
     (let [d (atom (utils/list-maps->map-lists @serovar-prediction-results))]
       [dynamite-filtered-selection "dynamite-results"
        d
        ;serovar-prediction-results-map-lists
        :genome
        dynamite-obj
        get-filtered-genomes!])]))

(defn download-results-table []
  (when-not (nil? @serovar-prediction-results)
    (let [table-fields data/serovar-prediction-fields
          table-data (->> @serovar-prediction-results
                          (group-by :genome)
                          vals
                          (map last))
          csv-data (utils/map->csv-text table-data table-fields)
          url-obj (utils/text->blob->obj-url csv-data)]
      ;; It appears necessary to deref selected-tree-genomes in order to properly reactively update
      ;; this component with the proper CSV data
      (deref serovar-prediction-results)
      [:a.btn.btn-default {:href     url-obj
                           :download (str "SISTR-" (count table-data) "_genome-serovar-prediction-results-table.csv") }
       [:i.fa.fa-download.fa-lg.pull-left]
       " Download Table (CSV)"
       [:br]
       ])))

(defn dataview-filter [item args]
  (let [search-string (aget args "searchString")]
    (if (empty? search-string)
      true
      (try
        (let [search-pattern (re-pattern search-string)
              search-fields (map utils/keyword->str [:genome
                                                     :serovar_prediction
                                                     :curated_serovar
                                                     :serovar_antigen_prediction])]
          (->> search-fields
               (map #(aget item %))
               (remove nil?)
               (map #(re-find search-pattern %))
               (remove nil?)
               empty?
               not))
        (catch js/Object _
          true)))))

(defn serovar-prediction-results-table []
  [slick-grid-table
   "serovar-prediction-results-table"
   serovar-prediction-results
   :genome
   (map utils/keyword->str data/serovar-prediction-fields)
   data/genome-selection
   table-grid
   table-dataview
   :filter-fn dataview-filter])

(defn input-box [label val]
  [:div.form-horizontal
   [:label.control-label label]
   [:input.form-control {:value     @val
                         :on-change #(reset! val (-> % .-target .-value))}]
   ])

(defonce search-string (atom ""))

(add-watch search-string :update
           (fn [_ _ _ v]
             (try
               (re-pattern v)
               (slick-grid/update-filter! table-grid table-dataview v)
               (catch js/Object e
                 (js/console.error "Search string invalid" e)))))

(defn filter-genomes []
  [input-box "Filter table by genome name and/or serovar (regex allowed)" search-string])

(defn serovar-prediction-results-table-panel []
  [panel
   "serovar-prediction-results-table-panel"
   [:h4 [:i.fa.fa-table] " Serovar Prediction Results"]
   "default"
   false
   [:div
    [filter-genomes]
    [serovar-prediction-results-table]
    [download-results-table]]])

(defn show-number-selected []
  (let [n-selected (count @data/genome-selection)
        n-genomes (count @data/genome-name-metadata)
        n-filtered (count @serovar-prediction-results)]
    [:div.alert.alert-info
     (str n-selected " "
          (utils/pluralize "genome" n-selected)
          " selected of "
          n-genomes (utils/pluralize "genome" n-genomes)
          " (" n-filtered " filtered)")]))

(defn update-table-selection! [genomes]
  (slick-grid/update-selection @table-grid @table-dataview genomes))

(defn refresh-table! []
  (when-not (nil? @table-grid)
    (.resizeCanvas @table-grid)
    (update-table-selection! @data/genome-selection)))

(defn on-chart-item-click! [y x category-key series-key]
  (let [genomes (->> @data/serovar-prediction-results
                     (filter
                       (fn [m]
                         (= [y x]
                            [(get m (keyword category-key))
                             (get m (keyword series-key))])))
                     (map :genome))]
    (reset! data/genome-selection
            (distinct (concat genomes @data/genome-selection)))
    (update-table-selection! @data/genome-selection)))

(defn results-table-help-info []
  [well-collapsible "serovar-predictions-table-info-well"
   [:span [:i.fa.fa-info-circle] " Help"]
   true
   [:ul
    [:li "You can find the SISTR serovar prediction results in the table in the main panel"]
    [:li "If you have uploaded any genomes, results for those genomes should appear in the table"]
    [:li "You can download the full table as a CSV"]
    [:li "Ctrl/Shift-click to select genomes within table to display in visualizations"]
    [:li "Click and drag table columns to reorder"]
    [:li "Left-click column to sort by column"]
    [:li "Right-click column to select which columns to display in table"]]
   ])

(defn in-silico-results-page []
  [:div {:style {:padding-left 300}}
   [:div.sidebar
    [:div.well.well-sm
     [results-table-help-info]
     [well-collapsible "serovar-variant-info-well"
      [:span [:i.fa.fa-exclamation-triangle] " Serovar variant prediction"]
      true
      [serovar-variant-warning-info]]
     [show-number-selected]
     [selection/genome-selection-manager
      "results-table-selection-manager"
      #(update-table-selection! %)]
     ]]
   [:div.container-fluid
    [:div.row
     [:div.col-md-12
      [loading-prediction-results]
      (when (and @show-table?
                 (not (empty? @serovar-prediction-results)))
        [:div
         [filter-panel]
         [serovar-prediction-results-table-panel]
         [dimple/horiz-bar-chart-panel
          "serovar-predictions-overview"
          [:h4 [:i.fa.fa-bar-chart] " Serovar Predictions Overview"]
          serovar-prediction-results
          (atom data/serovar-prediction-fields)
          :category-key :serovar_prediction
          :series-key :MLST_ST_in_silico
          :on-click-handler! on-chart-item-click!]])]]]])

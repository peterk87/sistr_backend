(ns ^:figwheel-load sistr-components.slickgrid
  (:require
    [reagent.core :as reagent :refer [atom]]
    [sistr-utils.utils :as utils]))

(defn default-dataview-filter [item args]
  (let [search-string (aget args "searchString")
        item-id (aget item "id")]
    (if (empty? search-string)
      true
      (try
        (not (nil? (re-find (re-pattern search-string) item-id)))
        (catch js/Object _
          true)))))

(defn update-filter! [grid dataview search-string]
  (.setFilterArgs @dataview (clj->js {"searchString" search-string}))
  (.refresh @dataview)
  (.resizeCanvas @grid)
  (.invalidate @grid))

(defn update-selection
  [grid data-view selected-ids]
  (.setSelectedRows grid (clj->js
                           (map
                             #(.getIdxById data-view %)
                             selected-ids)))
  (.syncGridSelection data-view grid true)
  (.invalidate grid)
  )

(defn slick-grid-table [id data-atom id-key columns selected-ids grid data-view
                        & {:keys [filter-fn
                                  filter-args]
                           :or   {filter-fn   default-dataview-filter
                                  filter-args {"searchString" ""}}}]
  (reagent/create-class
    (letfn [(render! []
                     (deref data-atom)
                     [:div.slick-grid-container {:id id
                                                 :style {:height 600}}])
            (updater! []
              (let [d1 (js/Date.now)
                    tbl-data-id (->> (if (map? @data-atom)
                                        (vals @data-atom)
                                        @data-atom)
                                      (map #(assoc % :id (get % id-key))))
                    slick-grid-data (clj->js tbl-data-id)]
                (js/console.info (str id ":: regenerated table data in " (- (js/Date.now) d1) "ms"))
                (aset @data-view "cljs_data" tbl-data-id)
                (.beginUpdate @data-view)
                (.setItems @data-view slick-grid-data)
                (when-not (nil? filter-args)
                  (prn filter-args)
                  (.setFilterArgs @data-view (clj->js filter-args)))
                (when-not (nil? filter-fn)
                  (prn filter-fn)
                  (.setFilter @data-view filter-fn))
                (.endUpdate @data-view)
                (.syncGridSelection @data-view @grid true)
                (update-selection @grid @data-view @selected-ids)
                (js/console.info (str id ":: Updated table data in " (- (js/Date.now) d1) "ms"))
                ))
            (mounter! []
                      (js/console.log (js/Date.) "Mounting SlickGrid table" id)
                      (set! (.-innerHTML (utils/by-id id)))
                      (let [opts {:enableCellNavigation true
                                  :enableColumnReorder  true
                                  :topPanelHeight       25}
                            checkbox-selector (new js/Slick.CheckboxSelectColumn
                                                   #_(clj->js {:cssClass "slick-cell-checkboxsel"}))
                            auto-tooltip (new js/Slick.AutoTooltips
                                              (clj->js {:enableForHeaderCells true}))
                            columns (concat [(.getColumnDefinition checkbox-selector)]
                                            (map (fn [x] {:id       x
                                                          :field    x
                                                          :sortable true
                                                          :name     (clojure.string/replace x  #"_" " ")}) columns))
                            ]
                        (if (nil? @data-view)
                          (reset! data-view (new js/Slick.Data.DataView)))
                        (reset! grid (new js/Slick.Grid
                                          (str "#" id)
                                          @data-view
                                          (clj->js columns)
                                          (clj->js opts)))
                        (new js/Slick.Controls.ColumnPicker
                             (clj->js columns)
                             @grid
                             (clj->js opts))
                        (.registerPlugin @grid checkbox-selector)
                        (.registerPlugin @grid auto-tooltip)
                        (.. @grid -onSort (subscribe
                                            (fn [_ args]
                                              (let [field (.. args -sortCol -field)
                                                    kw-field (keyword field)
                                                    sort-asc (.. args -sortAsc)
                                                    tbl-data-id (aget @data-view "cljs_data")
                                                    tbl-data-id-zipmap (zipmap (map :id tbl-data-id)
                                                                               #_(map kw-field tbl-data-id)
                                                                               (map (fn [x]
                                                                                      (let [v (kw-field x)]
                                                                                        (if (nil? v)
                                                                                          ""
                                                                                          v)))
                                                                                    tbl-data-id))]
                                                (js/console.info field kw-field sort-asc)
                                                (.. @data-view (sort (fn [a b]
                                                                       (let [a-id (aget a "id")
                                                                             b-id (aget b "id")
                                                                             a-val (get tbl-data-id-zipmap a-id)
                                                                             b-val (get tbl-data-id-zipmap b-id)]
                                                                         (cond
                                                                           (> a-val b-val) 1
                                                                           (< a-val b-val) -1
                                                                           :else 0)))
                                                                     sort-asc)))
                                              (.invalidate @grid)
                                              )))
                        (.. @grid
                            -onSelectedRowsChanged
                            (subscribe
                              (fn [_ args]
                                (let [row-idxs (.-rows args)
                                      row-items (map #(.getDataItem @grid %) row-idxs)]
                                  (reset! selected-ids (map #(aget % "id") row-items))
                                  )
                                )))
                        (.setSelectionModel @grid (new js/Slick.RowSelectionModel
                                                       #_(clj->js {:selectActiveRow false})))
                        (js/console.log (js/Date.) "Mounted SlickGrid table" id)
                        (updater!)
                        ))
            ]
      {:render               render!
       :component-did-mount  mounter!
       :component-did-update updater!})))

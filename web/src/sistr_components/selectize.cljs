(ns ^:figwheel-load sistr-components.selectize
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require
    [reagent.core :as reagent]
    [sistr-utils.utils :as utils]))



(defn single-selectize [id label options selected-option on-change-handler!]
  (reagent/create-class
    (letfn [(render []
                    (deref options)
                    [:div {:id (str id "-container")}
                     [:label {:for id} label]
                     [:select {:id    id
                               :name  id
                               :style {:width "100%"}}]])
            (mounter-updater! []
                              (set! (.-innerHTML (utils/by-id (str id "-container")))
                                    (str "<label for=\"" id "\">" label "</label>
                                        <select id=\"" id "\" name=\"" id "\" style=\"width:100%\"></select>"))
                              (let [el (js/jQuery (str "select#" id))
                                    settings {:options      (->> @options (map #(let [x {:id %}] x)))
                                              :valueField   "id"
                                              :labelField   "id"
                                              :searchField  "id"
                                              :sortField    "id"
                                              :create       false
                                              :maxOptions   nil
                                              :hideSelected true
                                              :plugins      ["remove_button"]
                                              :onChange     on-change-handler!}
                                    select-jquery (.selectize el (clj->js settings))
                                    selectize-select (.-selectize (aget select-jquery 0))]
                                (.setValue selectize-select (if (keyword? @selected-option)
                                                              (utils/keyword->str @selected-option)
                                                              @selected-option))))]
      {:render               render
       :component-did-mount  mounter-updater!
       :component-did-update mounter-updater!})))

(defn multi-selectize [id label options selected-options on-change-handler!]
  (reagent/create-class
    (letfn [(render []
                    (deref options)
                    [:div {:id (str id "-container")}
                     [:label {:for id} label]
                     [:select {:id       id
                               :name     id
                               :multiple true
                               :style    {:width "100%"}}]])
            (mounter-updater! []
                              (set! (.-innerHTML (utils/by-id (str id "-container")))
                                    (str "<label for=\"" id "\">" label "</label>
                                        <select id=\"" id "\" name=\"" id "\" style=\"width:100%\" multiple></select>"))
                              (let [el (js/jQuery (str "select#" id))
                                    opts-grouped-sorted (->> @options
                                                             utils/sorted-freqs
                                                             (remove (fn [[k _] _] (nil? k))))
                                    opts (vec (map (fn [[k v] _] {:id    k
                                                                  :label (str k " [n=" v "]")
                                                                  :count (+ v (* v -100))})
                                                   opts-grouped-sorted))
                                    settings {:options      opts
                                              :valueField   "id"
                                              :labelField   "label"
                                              :searchField  "id"
                                              :sortField    "count"
                                              :create       false
                                              :maxOptions   nil
                                              :hideSelected true
                                              :plugins      ["remove_button"]
                                              :onChange     on-change-handler!}
                                    default-opts (take 5 (keys opts-grouped-sorted))
                                    select-jquery (.selectize el (clj->js settings))
                                    selectize-select (.-selectize (aget select-jquery 0))]
                                (reset! selected-options default-opts)
                                (.setValue selectize-select (clj->js default-opts))))]
      {:render               render
       :component-did-mount  mounter-updater!
       :component-did-update mounter-updater!})))
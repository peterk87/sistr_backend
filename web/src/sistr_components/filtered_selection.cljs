(ns sistr-components.filtered-selection
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require
    [reagent.core :as r :refer [atom]]
    [cljs.core.async :refer [chan <!]]
    [sistr-utils.utils :as utils]))

(defn clear-filter-btn [changed? clear-filter change-handler!]
  (when @changed?
    [:button.btn.btn-danger
     {:on-click #(do
                  (swap! clear-filter inc)
                  (reset! changed? false)
                  (change-handler!))}
     [:i.fa.fa-ban] " Clear filter"]))

(defn dynamite-component [id dynamite-data key dynamite change-handler! clear-filter changed?]
  (r/create-class
    (letfn [(render! []
                     (deref dynamite-data)
                     (deref clear-filter)
                     [:div.dynamite-container {:id id}])
            (change-watcher! []
                             (let [changed (utils/listen (utils/by-id id) "change")]
                               (go (while true
                                     (<! changed)
                                     (reset! changed? true)
                                     (let [gs (js->clj (.get_final_selection @dynamite))]
                                       (change-handler! gs))))))
            (mounter-updater! []
                              (set! (.-innerHTML (utils/by-id id)) "")
                              (reset! dynamite (new js/Dynamite
                                                    id
                                                    (clj->js @dynamite-data)
                                                    (utils/keyword->str key)))
                              (change-watcher!))]
      {:render               render!
       :component-did-mount  mounter-updater!
       :component-did-update mounter-updater!})
    ))

(defn dynamite-filtered-selection [id dynamite-data key dynamite change-handler!]
  (when-not (empty? @dynamite-data)
    (let [clear-filter (atom 0)
          changed? (atom false)]
      (fn []
        [:div
         [clear-filter-btn changed? clear-filter change-handler!]
         [dynamite-component id dynamite-data key dynamite change-handler! clear-filter changed?]
         ])))
  )
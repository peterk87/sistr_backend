(ns ^:figwheel-load sistr-components.bootstrap
  (:require
    [reagent.core :refer [atom]]
    [sistr-utils.utils :as utils]))

(defn progress-bar
  "Bootstrap progress bar component"
  [progress pb-class pb-active]
  (let [progressbar-class (str "progress-bar-" pb-class)
        progress-bar-active (if pb-active "active" "")]
    [:div.progress {:style {:margin-bottom "0px"}}
     [:div {:role "progressbar"
            :class (apply str (interpose " " ["progress-bar"
                                              "progress-bar-striped"
                                              progressbar-class
                                              progress-bar-active]))
            :aria-valuenow (str progress)
            :aria-valuemin "0"
            :aria-valuemax "100"
            :style {:width (str progress "%")}}]]))


(defn panel [id title bs-class collapsed? components]
  [:div.panel {:class (str "panel-" bs-class)}
   [:div.panel-heading
    [:a {:data-toggle "collapse"
         :href        (str "#" id)}
     (conj title
           [:i.fa.fa-chevron-circle-down.pull-right])]]
   [:div.panel-collapse.collapse {:id id :class (if collapsed? "" "in")}
    [:div.panel-body
     components
     ]]
   ])

(defn well-collapsible [id title collapsed? components]
  (let [c? (atom collapsed?)]
    (fn []
      [:div.well.well-sm
       [:a {:data-toggle "collapse"
            :on-click #(swap! c? not)
            :href (str "#" id)}
        title
        [:i.fa.fa-chevron-circle-down.pull-right {:class (if @c? "" "fa-rotate-90")}]]
       [:div.collapse {:id id
                       :class (if @c? "" "in")}
        [:br]
        components]]))
  )


(defn slider [& {:keys [value min max change-handler!] :or {min 0 max 100}}]
  [:input {:type      "range" :value @value :min min :max max
           :style     {:width "100%"}
           :on-change change-handler!}])


(defn download-text-button [filename label content]
  [:a.btn.btn-default {:href     (utils/text->blob->obj-url content :type "text/plain-text;charset=utf-8")
                       :download filename}
   [:i.fa.fa-download]
   (str " " label)])

(defn download-svg-button [label el-id svg-data]
  [:a.btn.btn-default {:on-mouse-over #(reset! svg-data (-> (utils/by-id el-id) .-innerHTML))
                       :href          (utils/text->blob->obj-url (str @svg-data) :type "image/svg+xml;charset=utf-8")
                       :download      (str el-id ".svg")}
   [:i.fa.fa-download]
   (str " " label)])
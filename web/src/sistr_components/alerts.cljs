(ns sistr-components.alerts
  (:require
    [reagent.core]
    [sistr-data.data :as data]
    ))

(defn alert-dismissable
  "A Bootstrap dismissable alert component that has an `alert-class` like 'warning' or 'info'.
  Any number of sub-components can be nested within the dismissable alert."
  [alert-class & components]
  [:div.alert.alert-dismissable {:class (str "alert-" alert-class)
                                 :style {:padding-left 5}}
   [:button.close {:type "button"
                   :data-dismiss "alert"
                   :aria-hidden true}
    [:i.fa.fa-close.text-danger.fa-lg]]
   components
   ]
  )

(defn show-genomes-in-qviz
  "Create button to show genome assembly stats in Qviz R Shiny app in another browser tab.
  e.g. button with the following href
  https://lfz.corefacility.ca/shiny/qviz/?qviz_data_uri=https://lfz.corefacility.ca/sistr-wtf/api/user/10c6W03oNmLy6IDZqiOw/genomes/assembly_stats"
  []
  (let [qviz-url "https://lfz.corefacility.ca/shiny/qviz/"
        ]
    [alert-dismissable "success"
     ^{:key 0}
     [:a
      {:target "_blank"
       :href (str qviz-url "?qviz_data_uri=" data/api-uri "user/" @data/sistr-user "/genomes/assembly_stats")}
      [:i.fa.fa-search.fa-2x] " Compare genome assembly quality of your genomes against SISTR public genomes in Qviz"]]
    ))

(defn upload-errors-alerts
  "Alert the user of any upload errors that occur with dismissable alerts"
  [errors]
  [:div
   (map-indexed
     (fn [i error]
       (let [{:keys [time msg status]} error]
         ^{:key i}
         [alert-dismissable "danger"
          [:p [:span.badge "HTTP Code " status] " " msg]
          [:p [:em time]]
          ]))
     @errors)])

(ns sistr-components.selection-manager
  (:require
    [reagent.core :refer [atom]]
    [sistr-data.data :as data]
    [sistr-components.bootstrap :as bootstrap]))


(defn text-input [id label value]
  [:div {:id (str id "-container")}
   [:label {:for id} label]
   [:input.form-control {:id        id
                         :name      id
                         :type      "text"
                         :value     @value
                         :on-change #(reset! value (.-target.value %))}]])

(defn add-selection-btn [new-selection-name selection]
  [:button.btn.btn-success
   {:on-click #(swap! data/genome-selection-collection assoc @new-selection-name @data/genome-selection)}
   [:i.fa.fa-plus] " Add new selection " [:span.badge (count @selection)]]
  )

(defn add-new-genome-selection [id]
  (when-not (or (empty? @data/genome-selection)
                (some #(= @data/genome-selection %)
                      (vals @data/genome-selection-collection)))
    (let [new-selection-name (atom (str "selection-" (count @data/genome-selection-collection)))]
      [:div
       [text-input (str id "-add-new-selection-input") "Selection Name:" new-selection-name]
       [add-selection-btn new-selection-name data/genome-selection]])))

(defn genome-selection-history-list-item [name genomes on-change-handler!]
  [:tr
   {:style {:background-color (if (= @data/genome-selection genomes)
                                "rgba(0,100,255,0.2)"
                                "transparent")
            :cursor           "pointer"}}
   [:td
    {:on-click #(do
                 (reset! data/genome-selection genomes)
                 (on-change-handler! genomes))}
    (str name " [n=" (count genomes) "]")]
   [:td.pull-right
    [:button.destroy {:type        "button"
                      :aria-hidden true
                      :on-click    #(swap! data/genome-selection-collection dissoc name)}
     [:i.fa.fa-close.text-danger.fa-lg]]]])

(defn genome-selection-history-list [on-change-handler!]
  [:table.table.table-condensed
   [:tbody
    (map-indexed
      (fn [i [name genomes]]
        ^{:key i}
        [genome-selection-history-list-item name genomes on-change-handler!])
      @data/genome-selection-collection)]])

(defn clear-genome-selection-btn [on-change-handler!]
  (when-not (empty? @data/genome-selection)
    [:button.btn.btn-danger
     {:on-click #(do
                  (reset! data/genome-selection [])
                  (on-change-handler! []))}
     [:i.fa.fa-ban] " Clear selection " [:span.badge (count @data/genome-selection)]]))

(defn genome-selection-manager [id on-change-handler!]
  (when-not (and (empty? @data/genome-selection)
                 (empty? @data/genome-selection-collection))
    [bootstrap/well-collapsible
     id
     [:span "Genome Selections"]
     false
     [:div
      [add-new-genome-selection id]
      [genome-selection-history-list on-change-handler!]
      [clear-genome-selection-btn on-change-handler!]]]))

(ns ^:figwheel-load sistr-components.table
  (:require-macros [cljs.core.async.macros :refer [go go-loop]])
  (:require
    [reagent.core :refer [atom]]
    [cljs.core.async :as async]
    [sistr-utils.utils :as utils]))

(defonce idx (atom 0))

(defn header-sort-icon [header header-sort-by header-sort-asc?]
  (if (= header @header-sort-by) (if @header-sort-asc?
                                   [:i.fa.fa-sort-asc.pull-right]
                                   [:i.fa.fa-sort-desc.pull-right])
                                 nil))

(defn Table-Header-Item [header header-sort-by header-sort-asc?]
  [:div.z-column-header.z-sortable.z-unselectable.z-cell
   {:on-click #(do
                (if (= header @header-sort-by)
                  (swap! header-sort-asc? not)
                  (reset! header-sort-asc? false))
                (reset! header-sort-by header))
    :style    {:cursor        "pointer"
               :width         100
               :text-overflow "ellipsis"
               :white-space   "nowrap"
               :overflow      "hidden"
               :left          0
               }}
   (let [text (str " " (utils/keyword->str header))]
     [:div.z-content
      [header-sort-icon header header-sort-by header-sort-asc?]
      [:span {:title text} text]
      ])
   ]
  )


(defn Table-Head [headers header-sort-by header-sort-asc?]
  [:div.z-header-wrapper
   [:div.z-header {:style {:padding-right 20
                           :transform     (str "transform3d(" -100 "px, 0px, 0px)")}}
    (map-indexed (fn [i header]
                   ^{:key i}
                   [Table-Header-Item header header-sort-by header-sort-asc?])
                 @headers)]
   ])

(defn Table-Row [headers x &
                 {:keys [cell-height
                         selection
                         ]}]
  [:div.z-row {:on-click #(do
                           (if (utils/in? @selection (:genome x))
                             (swap! selection disj (:genome x))
                             (swap! selection conj (:genome x))))
               :style    {:background-color (if (utils/in? @selection (:genome x))
                                              "red"
                                              "transparent")
                          :height           cell-height}}
   (map-indexed
     (fn [i k]
       ^{:key i}
       [:div.z-cell
        {:style {:width 100}}
        (let [text (get x k)]
          [:div.z-content
           {:title text}
           text])])
     @headers)])

(defn table-listeners [id]
    (let [el (utils/by-id id)
          c (utils/listen js/window "keydown")]
      (go-loop []
               (let [x (async/<! c)
                     keycode (.-keyCode x)]
                 #_(js/console.info x)
                 (condp = keycode
                   34 (reset! idx (utils/clamp 0 (- @idx 10) 4329))
                   33 (reset! idx (utils/clamp 0 (+ @idx 10) 4329))
                   36 (reset! idx 0)
                   35 (reset! idx 4329)
                   :else #(prn "KEYCODE " x keycode))
                 )
               (recur))))

(defn Table-Body [table-data headers &
                  {:keys [header-sort-by
                          sort-asc?
                          cell-height
                          n-cells
                          selection]}]
  (let [d (if (map? @table-data) (vals @table-data) @table-data)
        sorted-table-data (if (nil? @header-sort-by)
                            d
                            (if @sort-asc?
                              (sort-by @header-sort-by d)
                              (reverse (sort-by @header-sort-by d))))
        derp (->> sorted-table-data
                  (drop @idx)
                  (take n-cells))]
    #_(table-listeners "tbl1")
    [:div.z-table
     {
      ;:id "tbl1"
      :style {:width (* (count @headers) 100)}}
     (map-indexed
       (fn [i x]
         ^{:key i}
         [Table-Row headers x
          :cell-height cell-height
          :selection selection])
       derp)]))

(def event-bus (async/chan))
(def event-bus-pub (async/pub event-bus first))

(let [ch (async/chan)]
  (async/sub event-bus-pub :wheel ch)
  (go-loop []
           (let [[_ delta-y] (async/<! ch)
                 delta-y (if (pos? delta-y) 1 -1)
                 idx-delta-y (+ @idx delta-y)
                 n-max 4330]
             (cond
               (>= idx-delta-y n-max) (reset! idx (- n-max 20))
               (<= idx-delta-y 0) (reset! idx 0)
               :else (swap! idx + delta-y))
             (recur))))

(let [ch (async/chan)]
  (async/sub event-bus-pub :mouse-over ch)
  (go-loop []
           (let [[x y] (async/<! ch)]
             (recur))))


(defn Vert-Scroller [n-rows row-height]
  [:div.z-vertical-scrollbar {:style {:width 20}}
   [:div.ref-verticalScrollbar
    {
     :id    "vs"
     :style {:overflow  "auto"
             :width     "100%"
             :height    "100%"
             :scrollTop (* @idx row-height)}}
    [:div.z-vertical-scroller
     {:style {:height (* n-rows row-height)}}]]])

(defn Scroller [table-data row-height width content]
  [:div.z-scroller
   [:div.z-content-wrapper {:style {:width    width
                                    :overflow "hidden"}}
    [Vert-Scroller (count @table-data) row-height]
    [:div.z-content-wrapper-fix {:style {:max-width (str "calc(100% - 20px)")}}
     content]]])

(defn linear-scale [domain range v]
  (let [[min-d max-d] domain
        d-len (- max-d min-d)
        [min-r max-r] range
        r-len (- max-r min-r)
        prop-d (/ (- v min-d) d-len)
        x (* r-len prop-d)]
    (+ x min-r)))


(defn Table [table-data table-headers &
             {:keys [height
                     width
                     header-sort-by
                     sort-asc?
                     cell-height
                     selection]
              :or   {height         800
                     width          1500
                     cell-height    20
                     header-sort-by nil
                     sort-asc?      false
                     selection      (atom #{})}}]
  (let [n-cells (int (/ (- height cell-height) cell-height))
        h-sort-by (atom header-sort-by)
        sort-asc? (atom sort-asc?)
        ctrl? (atom false)
        shift? (atom false)]
    [:div.react-datagrid.z-cell-ellipsis.z-style-alternate.z-with-column-menu
     {
      :on-wheel      #(do
                       (.preventDefault %)
                       (let [delta-y (.-deltaY %)]
                         (go (async/put! event-bus [:wheel delta-y]))))
      :on-mouse-over #(do
                       (go (async/put! event-bus [:mouse-over 1])))
      :on-scroll     #(do
                       (.preventDefault %)
                       (let [scroll-top (-> % (.-target) (.-scrollTop))]
                         (reset! idx (int (js/Math.floor (/ scroll-top cell-height))))
                         ))
      :on-key-down   #(do
                       (let [keycode (.-keyCode %)]
                         (.persist %)
                         (js/console.warn "KEYDOWN" %)
                         (condp = keycode
                           34 (reset! idx (utils/clamp 0 (- @idx 10) 4329))
                           33 (reset! idx (utils/clamp 0 (+ @idx 10) 4329))
                           36 (reset! idx 0)
                           35 (reset! idx 4329)
                           :else (fn [] (prn "KEYCODE " % keycode)))
                         ))
      :style         {:height   (* n-cells cell-height)
                      :width    width
                      :overflow "hidden"
                      }}
     [:div.z-inner {:on-drag-over #(.preventDefault %)

                    :style        {:width (* (count @table-headers) 100)}}
      [Table-Head table-headers h-sort-by sort-asc?]
      [Scroller table-data cell-height width
       [Table-Body table-data table-headers
        :header-sort-by h-sort-by
        :sort-asc? sort-asc?
        :cell-height cell-height
        :n-cells n-cells
        :selection selection]]
      ]]))

"""added new tables

Revision ID: a3ed75ed2b67
Revises: 1e118df16bca
Create Date: 2016-05-05 11:12:44.022681

"""

# revision identifiers, used by Alembic.
revision = 'a3ed75ed2b67'
down_revision = '1e118df16bca'

from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

def upgrade():
### commands auto generated by Alembic - please adjust! ###
    op.create_table('mash_refseq',
    sa.Column('created_at', sa.DateTime(), nullable=True),
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('genome_id', sa.Integer(), nullable=False),
    sa.Column('user_id', sa.Integer(), nullable=False),
    sa.Column('match_id', sa.String(), nullable=True),
    sa.Column('distance', sa.Numeric(), nullable=True),
    sa.Column('pvalue', sa.Numeric(), nullable=True),
    sa.Column('matching', sa.String(), nullable=True),
    sa.Column('top_100_results', postgresql.JSON(), nullable=True),
    sa.Column('taxid', sa.Integer(), nullable=True),
    sa.Column('bioproject', sa.String(), nullable=True),
    sa.Column('biosample', sa.String(), nullable=True),
    sa.Column('assembly_accession', sa.String(), nullable=True),
    sa.Column('plasmid', sa.String(), nullable=True),
    sa.Column('strain', sa.String(), nullable=True),
    sa.Column('subspecies', sa.String(), nullable=True),
    sa.Column('serovar', sa.String(), nullable=True),
    sa.ForeignKeyConstraint(['genome_id'], ['genome.id'], ),
    sa.ForeignKeyConstraint(['user_id'], ['user.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('rfb_cluster_prediction',
    sa.Column('created_at', sa.DateTime(), nullable=True),
    sa.Column('blast_results', postgresql.JSON(), nullable=True),
    sa.Column('top_result', postgresql.JSON(), nullable=True),
    sa.Column('is_trunc', sa.Boolean(), nullable=True),
    sa.Column('is_missing', sa.Boolean(), nullable=True),
    sa.Column('is_perfect_match', sa.Boolean(), nullable=True),
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('serogroup', sa.String(), nullable=True),
    sa.Column('genome_id', sa.Integer(), nullable=False),
    sa.Column('user_id', sa.Integer(), nullable=False),
    sa.ForeignKeyConstraint(['genome_id'], ['genome.id'], ),
    sa.ForeignKeyConstraint(['user_id'], ['user.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    ### end Alembic commands ###


def downgrade():
### commands auto generated by Alembic - please adjust! ###
    op.drop_table('rfb_cluster_prediction')
    op.drop_table('mash_refseq')
    ### end Alembic commands ###

#!/usr/bin/env sh
#
# Usage:
# $ cd /path/to/sistr_backend
# $ . sistr.sh [f|flaskrun] [c|celery]
# 
# Example:
# Activate virtualenv and export necessary envars for sistr_backend
# $ . sistr.sh
# 
# Start Python Flask server
# $ . sistr.sh f
# 
# Start Leiningen Figwheel for Clojurescript dev
# $ . sistr.sh l
# 
# Start Celery task queue
# $ . sistr.sh c
# 
# Start Celery beat periodic task queue
# $ . sistr.sh b
# 
# Run tests
# $ . sistr.sh t
# 

. .venv/bin/activate
echo "Virtualenv activated!"
export PYTHONPATH=$(pwd)
export SISTR_APP_SETTINGS=$(pwd)/sistr-config.py
echo "Env vars PYTHONPATH and SISTR_APP_SETTINGS exported!"

for arg in "$@"; do
	case "$arg" in
	i|ipython)
		echo "Starting IPython session"
		ipython --config $(pwd)/init_ipy.py
		;;
	f|flaskrun)
		echo "Starting Flask dev server"
		export FLASK_APP=$(pwd)/run.py
		export FLASK_DEBUG=1
		flask run 
		;;
	c|celery) 
		echo "Starting Celery"
		celery worker -A app.tasks 
		;;
	b|beat) 
		echo "Starting Celery Beat - Periodic Task Scheduler"
		celery beat -A app.tasks 
		;;
	l|figwheel)
		echo "Starting lein figwheel for frontend dev"
		echo "NOTE: Start Flask app server at localhost:5000"
		echo "Open browser at localhost:3449 and localhost:5000"
		echo "figwheel will be active in both sessions"
		cd web/
		lein figwheel
		;;
	t|pytest)
		echo "Running all backend tests"
		pytest -v tests/
	esac
done

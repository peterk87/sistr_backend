#!.venv/bin/python
__author__ = 'peter'

import argparse
import os
from app.mist.parser import MistJSONParser
from sqlalchemy.exc import IntegrityError
from app.models import User
from app import session
from app import logger

prog_desc = '''
    Load genome metadata and MIST results into the DB
    -------------------------------------------------

    - load in the following from MIST JSON output
      - marker results (e.g. allele call, binary absence/presence)
      - test metadata (e.g. MLST ST, CC)
      - test info
        - markers
        - alleles (only applicable for sequence-typing tests; set -A flag when using MIST)
    '''

parser = argparse.ArgumentParser(prog='load_db',
                                 formatter_class=argparse.RawDescriptionHelpFormatter,
                                 description=prog_desc)
parser.add_argument('-t',
                    '--load_tests',
                    action='append',
                    help='Specify one or more MIST JSON output files to load in silico test info from')
parser.add_argument('-i',
                    '--mist_input',
                    action='append',
                    help='Specify one or more MIST JSON output files to load results from')
parser.add_argument('-u',
                    '--user',
                    default='sistr',
                    help='User')
parser.add_argument('-v',
                    '--verbose',
                    action='count',
                    default=0)


def get_user(user_name):
    return session.query(User).filter_by(name=user_name).first()


def parse_marker_results(user, mist_output_file):
    assert os.path.exists(mist_output_file)
    parser = MistJSONParser(session,
                            user=user,
                            json_path=mist_output_file)
    try:
        parser.parse_all_marker_results()
        logger.info('Parsed MIST results from {0}'.format(mist_output_file))
    except Exception as exception:
        logger.error('Could not parse MIST results from {0}'.format(mist_output_file))
        logger.exception(exception.message)
        logger.error('Rolling back DB session')
        session.rollback()
    except IntegrityError as exception:
        logger.error('Could not parse MIST results from {0}'.format(mist_output_file))
        logger.exception('IntegrityError: {0}'.format(exception.message))
        logger.error('Rolling back DB session')
        session.rollback()


def parse_metadata_results(user, mist_output_file):
    assert os.path.exists(mist_output_file)
    parser = MistJSONParser(session,
                            user=user,
                            json_path=mist_output_file)
    try:
        parser.parse_all_test_metadata()
        logger.info('Parsed MIST metadata from {0}'.format(mist_output_file))
    except Exception as exception:
        logger.error('Could not parse MIST metadata from {0}'.format(mist_output_file))
        logger.exception(exception.message)
        logger.error('Rolling back DB session')
        session.rollback()
    except IntegrityError as exception:
        logger.error('Could not parse MIST metadata from {0}'.format(mist_output_file))
        logger.exception('IntegrityError: {0}'.format(exception.message))
        logger.error('Rolling back DB session')
        session.rollback()


def parse_test_info(user, mist_output_file):
    assert os.path.exists(mist_output_file)
    parser = MistJSONParser(session,
                            user=user,
                            json_path=mist_output_file)
    try:
        parser.parse_mist_tests()
        logger.info('Parsed MIST test info from {0}'.format(mist_output_file))
    except Exception as exception:
        logger.exception(exception.message)
        logger.error('Rolling back DB session')
        session.rollback()
    except IntegrityError as exception:
        logger.error('Could not parse MIST test info from {0}'.format(mist_output_file))
        logger.exception('IntegrityError: {0}'.format(exception.message))
        logger.error('Rolling back DB session')
        session.rollback()


import logging
from app import logging_format


def init_console_logger():
    global logging_verbosity, console_logger
    logging_verbosity = args.verbose
    console_logger = logging.StreamHandler()
    console_logger.setFormatter(logging_format)
    if logging_verbosity == 0:
        console_logger.setLevel(logging.ERROR)
    elif logging_verbosity == 1:
        console_logger.setLevel(logging.WARN)
    elif logging_verbosity == 2:
        console_logger.setLevel(logging.INFO)
    elif logging_verbosity == 3:
        console_logger.setLevel(logging.DEBUG)
    logger.addHandler(console_logger)


if __name__ == '__main__':
    args = parser.parse_args()

    init_console_logger()

    if args.load_tests is None and args.mist_input is None:
        print '''
        No input files specified.
        Please specify one or more files to load MIST test info or results from.
        '''
        parser.print_help()
        exit(1)
    from app import create_app

    app = create_app()
    user = get_user(args.user)
    assert user is not None
    if args.load_tests is not None:
        for test_file in args.load_tests:
            parse_test_info(user, test_file)

    if args.mist_input is not None:
        for results_file in args.mist_input:
            parse_marker_results(user, results_file)
            parse_metadata_results(user, results_file)



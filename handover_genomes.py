#!.venv/bin/python

import os, sys, re
import argparse
import logging
from app import logger

from app.models import \
    User, \
    Genome, \
    SerovarPrediction, \
    SerogroupPrediction, \
    WzxPrediction, \
    WzyPrediction, \
    H1FliCPrediction, \
    H2FljBPrediction, \
    MistMarkerResult, \
    MashRefSeq

console_logger = None

def init_console_logger(logging_verbosity):
    global console_logger
    from app import logging_format
    console_logger = logging.StreamHandler()
    console_logger.setFormatter(logging_format)
    if logging_verbosity == 0:
        console_logger.setLevel(logging.ERROR)
    elif logging_verbosity == 1:
        console_logger.setLevel(logging.WARN)
    elif logging_verbosity == 2:
        console_logger.setLevel(logging.INFO)
    elif logging_verbosity == 3:
        console_logger.setLevel(logging.DEBUG)
    logger.addHandler(console_logger)


def init_args():
    prog_desc = '''
        Handover all genomes belonging to a user to SISTR public user
        -------------------------------------------------------------
        '''
    parser = argparse.ArgumentParser(prog='handover_genomes',
                                     formatter_class=argparse.RawDescriptionHelpFormatter,
                                     description=prog_desc)
    parser.add_argument('-u',
                        '--user',
                        required=True,
                        help='User whose genomes will be handed over')
    parser.add_argument('--public-user',
                        default='sistr',
                        help='Public SISTR user')
    parser.add_argument('-v',
                        '--verbose',
                        action='count',
                        default=0)
    return parser


def get_user(session, user_name):
    return session.query(User).filter_by(name=user_name).first()


def reassign(session, user_sistr_id, user_id):
    model_classes = [Genome,
                     SerovarPrediction,
                     SerogroupPrediction,
                     WzyPrediction,
                     WzxPrediction,
                     H1FliCPrediction,
                     H2FljBPrediction,
                     MashRefSeq,
                     MistMarkerResult]
    logging.debug('model classes %s', model_classes)
    for cls in model_classes:
        logging.debug('model class %s', cls)
        for x in session.query(cls).filter(cls.user_id == user_id).all():
            assert isinstance(x, cls)
            x.user_id = user_sistr_id
            session.add(x)
            logging.info('%s %s from user %s to user %s', cls, x, user_id, user_sistr_id)
        logging.info('Committing changes to %s', cls)
        session.commit()



if __name__ == '__main__':
    parser = init_args()
    args = parser.parse_args()

    init_console_logger(args.verbose)
    logging.error('FUCK %s', args)
    from app import create_app
    _app = create_app()
    from app import session


    public_user = get_user(session, args.public_user)
    assert isinstance(public_user, User)
    logging.info('Retrieved public SISTR user %s', public_user)
    user = get_user(session, args.user)
    assert isinstance(user, User)
    logging.info('Retrieved user %s', user)

    logging.info('Reassigning all from public SISTR user "%s" to user "%s"', public_user, user)
    reassign(session, public_user.id, user.id)

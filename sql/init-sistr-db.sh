#!/usr/bin/env sh
cat << EOF
================================================================================
Initialize sistr_db and sistr PostgreSQL user
================================================================================
EOF
cat sql/create-dbs.sql | sudo -u postgres psql

cat << EOF

================================================================================
Bootstrap sistr_db with minimum required data
================================================================================
EOF
cat sql/sistr_db-min.sql | sudo -u postgres psql

# Create and initialize a minimum `sistr_db`

Required:
- Postgresql 9.5+
- `psql`

Run the following 
```bash
cd sistr_backend/sql
./init-sistr-db.sh
```

May require `sudo` to run `psql`.
 
Default password for `sistr` user in DB is `sistr_password`.
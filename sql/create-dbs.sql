CREATE USER sistr WITH PASSWORD 'sistr_password';
CREATE DATABASE sistr_db;
GRANT ALL PRIVILEGES ON DATABASE "sistr_db" to sistr;
CREATE DATABASE sistr_test_db;
GRANT ALL PRIVILEGES ON DATABASE "sistr_test_db" to sistr;
\c sistr_db
CREATE EXTENSION hstore;
\c sistr_test_db
CREATE EXTENSION hstore;
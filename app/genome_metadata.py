# -*- coding: utf-8 -*-
from __future__ import absolute_import

from collections import defaultdict

from sqlalchemy import desc
from typing import Dict, Optional, Union

from app.util import count_dict_to_str
from . import cache, session
from .models import Genome, MistTest, MistMetadataResult, User, GeographicLocation, SerovarPrediction, CgMLSTPrediction, \
    CgMLSTProfile, SistrCmdQC


def listdict_to_dictlist(ld):
    assert isinstance(ld, list)
    ks = set([k for d in ld for k, v in d.iteritems()])
    dl = defaultdict(list)
    for d in ld:
        for k in ks:
            dl[k].append(d[k] if k in d else None)
    return dict(dl)


@cache.memoize(timeout=int(1e7))
def get_genome_metadata_dict(genome_id):
    """
    Get a dictionary of genome metadata for a specified Genome_

    Args:
        genome (int): Genome_ DB id

    Returns:
        dict: Metadata attributes for a Genome_
    """
    genome = session.query(Genome).filter(Genome.id == genome_id).first()
    date_str = None
    date_year = None
    date_month = None
    if genome.collection_date is not None:
        dt = genome.collection_date
        date_year = dt.year
        date_month = dt.month
        date_str = dt.strftime('%Y/%m/%d')

    mlst_test = session.query(MistTest).filter(MistTest.name == 'MLST').scalar()
    mlst_result = genome.mist_metadata_results.filter(MistMetadataResult.test == mlst_test).first()
    mlst_st = ''
    if mlst_result.attrs:
        mlst_st = mlst_result.attrs['ST']

    serovar_prediction = genome.serovar_prediction.order_by(desc(SerovarPrediction.created_at)).first()
    predicted_serovar = ''
    if serovar_prediction:
        predicted_serovar = serovar_prediction.serovar

    geo_loc = genome.geographic_location
    host = genome.host
    cgmlst_clusters = genome.cgmlst_clusters
    curation_info = None if genome.curation_info is None else genome.curation_info.code
    source_lat = float(geo_loc.lat) if geo_loc and geo_loc.lat else None
    source_lng = float(geo_loc.lng) if geo_loc and geo_loc.lng else None
    genome_metadata_dict = {
        'genome': genome.name,
        'user_uploader': genome.user.name,
        'subspecies': genome.subspecies.name if genome.subspecies else None,
        'serovar': genome.serovar,
        'serovar_predicted': predicted_serovar,
        'host_common_name': host.common_name if host else None,
        'host_latin_name': host.latin_name if host else None,
        'collection_date': date_str,
        'collection_year': date_year,
        'collection_month': date_month,
        'source_type': genome.source_type,
        'source_info': genome.source_info,
        'source_country': geo_loc.country if geo_loc else None,
        'source_region': geo_loc.region if geo_loc else None,
        'source_lat': source_lat,
        'source_lng': source_lng,
        'MLST_ST_in_silico': mlst_st,
        'curation_info': curation_info,
    }

    if cgmlst_clusters and isinstance(cgmlst_clusters, dict):
        cgmlst_key_fmt = 'cgmlst_clusters_{}%'
        levels = ['0.99',
                  '0.9',
                  '0.3',
                  '0.25',
                  '0.2',
                  '0.15',
                  '0.1',
                  '0.05',
                  '0.01',
                  '0', ]
        for level in levels:
            if level not in cgmlst_clusters:
                continue
            d = float(level)
            similarity = 100 - (100 * d)
            genome_metadata_dict[cgmlst_key_fmt.format(similarity)] = cgmlst_clusters[level]

    misc_metadata_ignore = ['Nominatim_geocode_json',
                            'strain',
                            'sub_species_reported',
                            'location',
                            'NGS_read_length',
                            'NGS_total_bases',
                            # 'pfge_primaryenzyme_pattern',
                            # 'pfge_secondaryenzyme_pattern',
                            # 'outbreak',
                            ]
    if genome.misc_metadata is not None:
        for k, v in genome.misc_metadata.iteritems():
            if k in misc_metadata_ignore:
                continue
            genome_metadata_dict[k] = v
    return genome_metadata_dict


def user_genomes_metadata(user_name):
    genome_query = session.query(Genome.id) \
        .join(Genome.user) \
        .filter(User.name == user_name, Genome.is_analyzed)
    md = [get_genome_metadata_dict(genome_id) for genome_id, in genome_query.all()]
    return md


@cache.memoize(timeout=int(1e7))
def get_genome_quality_stats(genome_id):
    genome = session.query(Genome).filter(Genome.id == genome_id).first()
    return genome.quality_stats


@cache.memoize(timeout=int(1e7))
def genome_quality_stats_plus_metadata_dict(genome_id):
    d = get_genome_quality_stats(genome_id)
    # add all QUAST assembly stats to dict
    for k, v in d.iteritems():
        d[k] = float(v)
        # add all genome metadata to dict
    genome_md = get_genome_metadata_dict(genome_id)
    for k, v in genome_md.iteritems():
        d[k] = v
    d['Assembly'] = d['genome']
    # add all serovar prediction data to dict
    # serovar_prediction = genome.serovar_prediction.first()
    serovar_prediction_md = get_basic_serovar_prediction_info(genome_id)
    for k, v in serovar_prediction_md.iteritems():
        d[k] = v
    return d


def genome_assembly_stats(user_name):
    genome_query = session.query(Genome.id) \
        .join(Genome.user) \
        .filter(User.name == user_name,
                Genome.is_analyzed == True)

    genome_stats_md = []
    for genome_id, in genome_query.all():
        genome_stats_md.append(genome_quality_stats_plus_metadata_dict(genome_id))
    return genome_stats_md


@cache.memoize(timeout=int(1e7))
def get_basic_serovar_prediction_info(genome_id):
    # type: (int) -> Dict[str, Optional[Union[str, int, float]]]
    genome = session.query(Genome) \
        .filter(Genome.id == genome_id) \
        .first()
    serovar_prediction = genome.serovar_prediction.order_by(desc(SerovarPrediction.created_at)).first()
    assert isinstance(serovar_prediction, SerovarPrediction)

    curated_serovar = genome.serovar
    reported_serovar = 'reported_serovar'
    reported_serovar = genome.misc_metadata[
        reported_serovar] if genome.misc_metadata is not None and reported_serovar in genome.misc_metadata else None
    curation_info = None if genome.curation_info is None else genome.curation_info.code

    mlst_test = session.query(MistTest).filter(MistTest.name == 'MLST').scalar()
    mlst_result = genome.mist_metadata_results.filter(MistMetadataResult.test == mlst_test).first()
    mlst_st = ''
    if mlst_result.attrs:
        mlst_st = mlst_result.attrs['ST']

    cgmlst_prediction = session.query(CgMLSTPrediction) \
        .filter(CgMLSTPrediction.serovar_prediction == serovar_prediction) \
        .first()
    assert isinstance(cgmlst_prediction, CgMLSTPrediction)
    profile = cgmlst_prediction.profile
    assert isinstance(profile, CgMLSTProfile)
    n_complete_alleles = len(profile.profile) - profile.missing_alleles - profile.partial_alleles
    qc = session.query(SistrCmdQC).filter(SistrCmdQC.genome == genome).first()
    assert isinstance(qc, SistrCmdQC)
    return {'genome': genome.name,
            'user_uploader': genome.user.name,
            'subspecies': cgmlst_prediction.subspecies,
            'serovar_prediction': serovar_prediction.serovar,
            'serovar_antigen_prediction': serovar_prediction.serovar_antigen,
            'serogroup_prediction': serovar_prediction.serogroup,
            'O_antigen': serovar_prediction.o_antigen,
            'H1_prediction': serovar_prediction.h1,
            'H2_prediction': serovar_prediction.h2,
            'cgMLST_serovar_prediction': serovar_prediction.serovar_cgmlst,
            'cgMLST_match_distance': cgmlst_prediction.distance,
            'cgMLST_allele_matches': cgmlst_prediction.allele_matches,
            'cgMLST_match_genome': cgmlst_prediction.genome_match,
            'cgMLST_ST': profile.sequence_type,
            'cgMLST_complete_alleles': n_complete_alleles,
            'cgMLST_missing_alleles': profile.missing_alleles,
            'cgMLST_partial_alleles': profile.partial_alleles,
            'MLST_ST_in_silico': mlst_st,
            'MLST_serovar_prediction': serovar_prediction.serovar_mlst,
            'MLST_serovar_count_predictions': count_dict_to_str(serovar_prediction.mlst_serovar_counts),
            'qc_status': qc.status,
            'qc_message': qc.message,
            'reported_serovar': reported_serovar,
            'curation_info': curation_info,
            'curated_serovar': curated_serovar, }


def genome_locations(user_name):
    query_stmt = session.query(Genome.name,
                               GeographicLocation.country,
                               GeographicLocation.region,
                               GeographicLocation.lat,
                               GeographicLocation.lng) \
        .join(GeographicLocation) \
        .join(User) \
        .filter(User.name == user_name, Genome.is_analyzed)
    return [{"genome": name,
             "country": country,
             "region": region,
             "lat": float(lat) if lat is not None else None,
             "lng": float(lng) if lng is not None else None,
             } for name, country, region, lat, lng in query_stmt.all()]

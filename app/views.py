import os

from flask import Blueprint, send_from_directory

PRJ_PATH = os.path.abspath(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
WEB_PATH = os.path.join(PRJ_PATH, 'web', 'resources', 'public')


def web_static_path(*args):
    return os.path.join(WEB_PATH, *args)


views_bp = Blueprint('views', __name__)


@views_bp.route('/css/<path:path>', methods=['GET'])
def send_css(path):
    return send_from_directory(web_static_path('css'), path)


@views_bp.route('/js/<path:path>', methods=['GET'])
def send_js(path):
    return send_from_directory(web_static_path('js'), path)


@views_bp.route('/favicon.ico', methods=['GET'])
def favicon():
    return send_from_directory(WEB_PATH, 'favicon.ico')


@views_bp.route('/', methods=['GET'])
def root():
    return send_from_directory(WEB_PATH, 'index.html')

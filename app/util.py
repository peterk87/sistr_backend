import cStringIO
import logging
import os
from cStringIO import StringIO
import decimal
import json

import pandas as pd
from typing import Optional, Tuple, Union, List, Dict
from werkzeug.datastructures import FileStorage

from .models import Contig

#: set: valid IUPAC nucleotide characters for checking FASTA format
VALID_NUCLEOTIDES = {'A', 'a',
                     'C', 'c',
                     'G', 'g',
                     'T', 't',
                     'R', 'r',
                     'Y', 'y',
                     'S', 's',
                     'W', 'w',
                     'K', 'k',
                     'M', 'm',
                     'B', 'b',
                     'D', 'd',
                     'H', 'h',
                     'V', 'v',
                     'N', 'n',
                     'X', 'x', }  # X for masked nucleotides


class DecimalEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, decimal.Decimal):
            return float(o)
        return super(DecimalEncoder, self).default(o)


def fasta_format_check(fasta_str):
    # type: (str) -> Tuple[bool, Optional[str]]
    """
    FASTA format check
    ==================

    Check that a FASTA file contents string is valid FASTA format.

     - First non-blank line needs to begin with a '>' header character.
     - Sequence can only contain valid IUPAC nucleotide characters

    Args:
        fasta_str (str): FASTA file contents string

    Returns:
        bool: Valid FASTA?
        str: Error message
    """
    sio = StringIO(fasta_str)
    header_count = 0
    line_count = 1
    nt_count = 0
    for l in sio.readlines():
        l = l.strip()
        if l == '':
            continue
        if l[0] == '>':
            header_count += 1
            continue
        if header_count == 0 and l[0] != '>':
            error_msg = 'First non-blank line (L:{}) does not contain FASTA header. Line beginning with ">" expected.' \
                .format(line_count)
            return False, error_msg
        non_nucleotide_chars_in_line = set(l) - VALID_NUCLEOTIDES

        if len(non_nucleotide_chars_in_line) > 0:
            error_msg = 'Line {} contains the following non-nucleotide characters: {}'.format(
                line_count,
                non_nucleotide_chars_in_line)
            return False, error_msg
        nt_count += len(l)
        line_count += 1

    if nt_count == 0:
        error_msg = 'FASTA does not contain any nucleotide sequence.'
        return False, error_msg

    return True, None


def write_temp_file(input):
    # type: (Union[StringIO, FileStorage]) -> str
    """
    Write an input File-like object to a temporary file and return the filename

    Args:
        input_file (StringIO|werkzeug.datastructures.FileStorage): Input File-like object

    Returns:
        (str): temporary file full file path
    """
    import tempfile, os
    fd, filename = tempfile.mkstemp()
    tfile = os.fdopen(fd, 'w')
    tfile.write(input.read())
    tfile.close()
    return filename


def contigs_to_fasta_stringio(contigs):
    # type: (List[Contig]) -> StringIO
    """
    Write list of app.models.Contig to StringIO buffer.

    Args:
        contigs (list of app.models.Contig): Contig objects

    Returns:
        StringIO: StringIO buffer with FASTA format string written and seek returned to first position.
    """
    sio = cStringIO.StringIO()
    for contig in contigs:
        assert isinstance(contig, Contig)
        sio.write('>{}\n{}\n'.format(contig.name, contig.seq))
    sio.seek(0)
    return sio


def parse_fasta(filepath):
    # type: (str) -> Tuple[str, str]
    '''
    Parse a fasta file returning a generator yielding tuples of fasta headers to sequences.

    Note:
        This function should give equivalent results to SeqIO from BioPython

        .. code-block:: python

            from Bio import SeqIO
            # biopython to dict of header-seq
            hseqs_bio = {r.description:str(r.seq) for r in SeqIO.parse(fasta_path, 'fasta')}
            # this func to dict of header-seq
            hseqs = {header:seq for header, seq in parse_fasta(fasta_path)}
            # both methods should return the same dict
            assert hseqs == hseqs_bio

    Args:
        filepath (str): Fasta file path

    Returns:
        generator: yields tuples of (<fasta header>, <fasta sequence>)
    '''
    with open(filepath, 'r') as f:
        seqs = []
        header = ''
        for line in f:
            line = line.strip()
            if line[0] == '>':
                if header == '':
                    header = line.replace('>','')
                else:
                    yield header, ''.join(seqs)
                    seqs = []
                    header = line.replace('>','')
            else:
                seqs.append(line)
        yield header, ''.join(seqs)


def first_row(df):
    # type: (pd.DataFrame) -> (pd.Series)
    for i,r in df.iterrows():
        return r


def write_temp_genome_fasta(output_dir, filename, fasta_str):
    # type: (str, str, str) -> str
    """Write temp FASTA file

    Args:
        output_dir (str): directory to write FASTA file to
        filename (str): base filename
        fasta_str (str): FASTA file contents

    Returns:
        str: File path to the temporary FASTA file that was written
    """
    temp_genome_fasta_path = os.path.join(output_dir, filename + '.fasta')
    with open(temp_genome_fasta_path, 'w') as f:
        f.write(fasta_str)
    return temp_genome_fasta_path


def make_tmp_dir(dir_path, retries=5):
    # type: (str, int) -> str
    """Create temp directory

    If dir cannot be created at original path, try appending an incremented number to the dir path ("{base temp dirname}-{#retry}")

    Args:
        dir_path (str): temporary directory path
        retries (int): Number of retries with incremented dirname

    Returns:
        str: Temporary directory path

    Raises:
        IOError: if temp analysis directory cannot be created
    """
    retry = 0
    orig_dir_path = dir_path
    while retry < retries:
        try:
            if retry > 0:
                dir_path = '{}-{}'.format(orig_dir_path, retry)
            os.makedirs(dir_path)
            assert os.path.exists(dir_path)
            return dir_path
        except Exception as ex:
            logging.error('Could not create tmp dir at "{}" because "{}"'.format(
                dir_path,
                ex))
        finally:
            retry += 1
    raise IOError(
        'Could not create tmp dir at "{}" after {} retries with incremented dirnames'.format(
            orig_dir_path,
            retries
        ))


def count_dict_to_str(counts_dict):
    # type: (Dict[str, int]) -> Optional[str]
    if counts_dict is None:
        return None
    tuples = [(k, v) for k, v in counts_dict.iteritems()]
    tuples.sort(key=lambda x: x[1], reverse=True)
    total = sum([count for x, count in tuples])
    return ', '.join(['{} ({}/{})'.format(x, count, total) for x, count in tuples])

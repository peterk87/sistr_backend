from typing import Dict, List, Optional
import logging
from cStringIO import StringIO
from collections import defaultdict
from datetime import datetime

from Bio import SeqIO
from sqlalchemy.orm import Session

from app.models import Genome, Contig, MistTest, MistMarker, MistMarkerResult, User, SerovarPrediction


def get_mist_marker_results(session, user_name):
    # type: (Session, str) -> Dict[str, Dict[str, List[Dict[str, str]]]]
    query_stmt = session.query(Genome.name, MistTest.name, MistMarker.name, MistMarkerResult.result) \
        .join(Genome.mist_marker_results) \
        .join(MistMarkerResult.test) \
        .join(MistMarkerResult.marker) \
        .join(Genome.user) \
        .filter(User.name == user_name)

    d = defaultdict(dict)

    for g, t, m, r in query_stmt.all():
        if t in d[g]:
            d[g][t].append({m: r})
        else:
            d[g][t] = [{m: r}]

    return d


def get_user(session, username):
    # type: (Session, str) -> Optional[User]
    return session.query(User).filter(User.name == username).first()


def get_genome(session, user_id, genome_name):
    # type: (Session, int, str) -> Optional[Genome]
    return session.query(Genome) \
        .filter(Genome.user_id == user_id,
                Genome.name == genome_name) \
        .first()


def add_genome_to_db(session, user, genome_name, fasta_str, reanalyze=False):
    # type: (Session, User, str, str, bool) -> Genome
    """Add Genome_ and all Contig_ to DB

    Add User_ Genome_ and all Contig_ for Genome_ to database.

    Notes:
        BioPython FASTA parser used to parse FASTA string.

    Args:
        fasta_str (str): Genome FASTA file contents
        user (app.models.User): User object
        genome_name (str): A unique Genome name

    Returns:
        app.models.Genome: User Genome object that was just added to the database

    Raises:
        Exception: if Genome_ or Genome_ Contig_ cannot be added to the DB
    """
    if reanalyze:
        genome = session.query(Genome).filter(Genome.name == genome_name,
                                              Genome.user_id == user.id).first()
        genome.time_uploaded = datetime.utcnow()
    else:
        genome_name = Genome.make_unique_name(genome_name, session)
        assert user is not None, 'User {0} not found in DB'.format(user)
        genome = Genome(name=genome_name,
                        user=user,
                        time_uploaded=datetime.utcnow())

    session.add(genome)
    try:
        session.commit()
        logging.info('Added user {0} genome {1} to DB'.format(user.name, genome_name))
    except:
        session.rollback()
        error_msg = 'Could not add genome {0} of user {1} to DB. Rolling back DB transaction'.format(
            genome_name,
            user.name)
        logging.error(error_msg)
        raise Exception(error_msg)

    if not reanalyze:
        contigs = [Contig(name=rec.description,
                          seq=str(rec.seq),
                          genome=genome)
                   for rec in SeqIO.parse(StringIO(fasta_str), 'fasta')]

        session.add_all(contigs)
        try:
            session.commit()
            logging.info('Added {0} contigs for genome {1} of user {2} to DB.'.format(
                len(contigs),
                genome_name,
                user.name
            ))
        except:
            session.rollback()
            error_msg = 'Could not add contigs for genome {0} of user {1} to DB. Rolling back DB transaction.'.format(
                genome_name,
                user.name)
            logging.error(error_msg)
            raise Exception(error_msg)

    return genome


def set_genome_analyzed(session, username, genome_name):
    # type: (Session, str, str) -> Genome
    """Update Genome as analyzed in DB

    .. code-block:: python

        genome.is_analyzed = True

    Args:
        session (Session): DB session
        username (str): username
        genome_name (str): genome name

    Returns:
        Genome: Genome DB object that was set as finalized
    """
    user = get_user(session, username)
    genome = get_genome(session, user.id, genome_name)
    genome.is_analyzed = True
    session.add(genome)
    session.commit()
    return genome


def delete_old_serovar_predictions(session, user_name, genome_name):
    # type: (Session, str, str) -> None
    user = get_user(session, user_name)
    if user is None:
        return
    genome = get_genome(session, user.id, genome_name)
    if genome is None:
        return
    if genome.serovar_prediction.count() > 0:
        sps = genome.serovar_prediction.order_by(SerovarPrediction.id).all()
        for sp in sps[:-1]:
            logging.warning('User %s | Genome %s | Deleting old SerovarPrediction id=%s %s',
                            user.name,
                            genome.name,
                            sp.id,
                            sp)
            session.delete(sp)
        logging.info('User %s | Genome %s | Keeping SerovarPrediction id=%s %s',
                     user.name,
                     genome.name,
                     sps[-1].id,
                     sps[-1])
        session.commit()

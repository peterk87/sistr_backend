from typing import Dict, List, Optional, Union

import pandas as pd
from sqlalchemy.orm import Session

from ..util import first_row
from ..models import MashRefSeq, User, Genome

MASH_OUTPUT_COLUMNS = [
    'match_id',
    'query_id',
    'distance',
    'pvalue',
    'matching',
]


def to_db(session, user, genome, df_results):
    # type: (Session, User, Genome, pd.DataFrame) -> MashRefSeq
    """Parse sorted Mash output vs RefSeq Sketch into DB

    The Mash output is parsed and added into the SISTR DB.
    The ID string (`match_id`) of the top result (result with the least distance) is parsed for various
     bits of data such as the species, NCBI accession, taxonomic ID, etc.
    If the match is from the genus Salmonella then an attempt is made to parse the serovar information from
    the `match_id`.

    Args:
        session (sqlalchemy.orm.session.Session): SQLAlchemy scoped :class:`Session` instance
        user (app.models.User): User object
        genome (app.models.Genome): Genome object
        df_results (pandas.DataFrame): Mash output in Pandas DataFrame

    Returns:
        app.models.MashRefSeq: MashRefSeq object containing parsed results
    """
    assert isinstance(user, User)
    assert isinstance(genome, Genome)
    assert isinstance(df_results, pd.DataFrame)

    assert validate_mash_dist_df(df_results)

    mashrefseq = MashRefSeq()
    mashrefseq.top_100_results = top_n(df_results)
    mashrefseq.genome = genome
    mashrefseq.user = user
    mashrefseq = parse_top_result(df_results, mashrefseq)

    session.add(mashrefseq)
    session.commit()

    return mashrefseq


def validate_mash_dist_df(df):
    # type: (pd.DataFrame) -> bool
    """Validate Mash dist DataFrame

    Mash dist output DataFrame must:

    - contain MASH_OUTPUT_COLUMNS
    - have more than 0 rows
    - pvalue and distance columns must be numeric (float64)

    Args:
        df (pandas.DataFrame): Mash dist DataFrame

    Returns:
        (bool): Valid Mash dist output DataFrame?
    """
    return (sorted(list(df.columns)) == sorted(MASH_OUTPUT_COLUMNS)) and \
           (df.shape[0] > 0) and \
           (df.pvalue.dtype == 'float64') and \
           (df.distance.dtype == 'float64')


def top_n(df, n=100):
    # type: (pd.DataFrame, int) -> List[Dict[str, Optional[Union[str, int, float]]]]
    """Top n entries in DataFrame to list of dict

    Args:
        df (pandas.DataFrame): DataFrame
        n (int): Number of entries to transform into dict

    Returns:
        (list of dict): List of top n rows as dict of column name to value
    """
    assert isinstance(df, pd.DataFrame)
    return df.head(n).transpose().to_dict().values()


def parse_top_result(df, mashrefseq):
    # type: (pd.DataFrame, MashRefSeq) -> MashRefSeq
    """Parse top Mash dist result

    From the top result to the Mash results DataFrame, parse Mash distance, p-value, raw matching sketches
    and Mash RefSeq DB specific match_id info.

    Args:
        df (pandas.DataFrame): DataFrame of Mash dist results
        mashrefseq (app.models.MashRefSeq): MashRefSeq object to parse top Mash dist result into

    Returns:
        (app.models.MashRefSeq): MashRefSeq with parsed info

    Notes:
        Assuming that the input DataFrame of Mash results is sorted by distance in ascending order.
    """
    if mashrefseq is None:
        mashrefseq = MashRefSeq()
    row = first_row(df)
    mashrefseq = parse_refseq_info(row.match_id, mashrefseq)
    mashrefseq.distance = row.distance
    mashrefseq.matching = row.matching
    mashrefseq.pvalue = row.pvalue

    return mashrefseq


def parse_refseq_info(match_id, mashrefseq):
    # type: (str, MashRefSeq) -> MashRefSeq
    """Parse a Mash dist match_id

    If "Salmonella" is found in the full strain name, then a serovar, strain name and subspecies will
    be attempted to be parsed as well.
    Values with periods ('.') will be treated as None (null).

    Args:
        match_id (str): Mash RefSeq match_id with taxid, bioproject, full strain name, etc delimited by '-'
        mashrefseq (app.models.MashRefSeq): MashRefSeq object to parse information into

    Returns:
        (app.models.MashRefSeq): MashRefSeq object with parsed info
    """

    def no_periods(s):
        # type: (str) -> Optional[str]
        return s if s != '.' else None

    assert isinstance(mashrefseq, MashRefSeq)

    sp = match_id.split('-')
    _, prefix, taxid_str, bioproject, biosample, assembly_acc, plasmid, fullname = sp
    taxid = int(taxid_str)
    fullname = fullname.replace('.fna', '')
    serovar = None
    strain = None
    subsp = None
    if 'Salmonella' in fullname:
        if 'serovar' in fullname:
            serovar = fullname.split('_serovar_')[-1].split('_str.')[0]
        if '_str._' in fullname:
            strain = fullname.split('_str._')[-1]
        if '_subsp._' in fullname:
            subsp = fullname.split('_subsp._')[-1].split('_')[0]

    mashrefseq.match_id = match_id
    mashrefseq.taxid = taxid
    mashrefseq.biosample = no_periods(biosample)
    mashrefseq.bioproject = no_periods(bioproject)
    mashrefseq.assembly_accession = no_periods(assembly_acc)
    mashrefseq.plasmid = no_periods(plasmid)
    mashrefseq.serovar = serovar
    mashrefseq.subspecies = subsp
    mashrefseq.strain = strain

    return mashrefseq

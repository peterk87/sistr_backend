import logging
from subprocess import Popen, PIPE
import os
import pandas as pd
from sqlalchemy.orm import Session

from ..models import User, Genome, MashRefSeq

#: (list of str): Expected `mash dist` output columns in expected order
MASH_OUTPUT_COLUMNS = [
    'match_id',
    'query_id',
    'distance',
    'pvalue',
    'matching',
]

MASH_REFSEQ = os.path.abspath('app/mash/data/RefSeqSketches.msh')


def sketch(mash_bin, user_temp_dir, genome_name, fasta_path):
    # type: (str, str, str, str) -> str
    """
    Create Mash sketch for RefSeq sketch DB with kmer size of 16 (-k 16)

    Args:
        mash_bin (str): Mash binary path
        user_temp_dir (str): Temp user directory
        genome_name (str): Genome name
        fasta_path (str): Genome fasta file path

    Returns:
        str: Mash sketch file path for genome fasta file
    """
    msh_path = os.path.join(user_temp_dir, genome_name + '.msh')
    args = [mash_bin,
            'sketch',
            '-k', '16',
            '-o', msh_path,
            fasta_path]
    p = Popen(args, stdout=PIPE, stderr=PIPE)
    (stdout, stderr) = p.communicate()
    retcode = p.returncode
    if retcode != 0:
        raise Exception('Could not create Mash sketch. RETCODE={} STDERR="{}"'.format(retcode, stderr))

    return msh_path


def query_refseq(mash_bin, input_path):
    # type: (str, str) -> str
    """
    Compute Mash distances of sketch file of genome fasta to RefSeq sketch DB.

    Args:
        mash_bin (str): Mash binary path
        input_path (str): Mash sketch file path or genome fasta file path

    Returns:
        (str): Mash STDOUT string
    """
    assert os.path.exists(input_path)
    args = [mash_bin,
            'dist',
            MASH_REFSEQ,
            input_path]
    p = Popen(args, stderr=PIPE, stdout=PIPE)
    (stdout, stderr) = p.communicate()
    retcode = p.returncode
    if retcode != 0:
        raise Exception('[error {}]Could not run Mash dist {}'.format(retcode, stderr))

    return stdout


def to_dataframe(mash_out):
    # type: (str) -> pd.DataFrame
    """Mash stdout to Pandas DataFrame

    Args:
        mash_out (str): Mash stdout

    Returns:
        (pandas.DataFrame): DataFrame with named columns sorted by distance in ascending order (match_id, query_id, distance, pvalue, matching)
    """
    from StringIO import StringIO
    mash_header_str = '\t'.join(MASH_OUTPUT_COLUMNS)
    sio_o = StringIO('\n'.join([mash_header_str, mash_out]))
    df = pd.read_table(sio_o)
    df.sort_values(by='distance', ascending=True, inplace=True)
    return df


def mash_against_refseq(session, mash_bin, fasta_filepath, user, genome):
    # type: (Session, str, str, User, Genome) -> MashRefSeq
    logging.warning('Running Mash on {} with fasta at {}'.format(genome.name, fasta_filepath))
    mashout = query_refseq(mash_bin, fasta_filepath)
    logging.warning('Ran Mash on {}. output length={}'.format(fasta_filepath, len(mashout)))
    df_mash = to_dataframe(mashout)

    from parser import to_db
    mashrefseq = to_db(session, user, genome, df_mash)
    logging.info('Genome {}: User {}: Mash RefSeq={} at {} ({})'.format(genome.name,
                                                                        user.name,
                                                                        mashrefseq.match_id,
                                                                        mashrefseq.distance,
                                                                        mashrefseq.matching))
    return mashrefseq
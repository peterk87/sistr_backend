# -*- coding: utf-8 -*-

from typing import Optional, Dict, Tuple

import os
import logging
from collections import Counter
import pandas as pd
from sqlalchemy.orm import Session

from ..models import  User, Genome, MistTest, MistMetadataResult, SerovarPrediction

MLST_TO_SEROVAR_MATRIX_PATH = os.path.abspath('app/mlst/data/MLST_to_serovar_matrix.csv')

def mlst_to_serovar_matrix_table():
    # type: () -> pd.DataFrame
    df = pd.read_csv(MLST_TO_SEROVAR_MATRIX_PATH, index_col=0)
    return df


def query_mlst_result(session, user, genome):
    # type: (Session, User, Genome) -> Optional[MistMetadataResult]
    mlst_test_id = session.query(MistTest.id).filter(MistTest.name == 'MLST').scalar()
    mlst_mist_result = genome.mist_metadata_results \
        .filter(MistMetadataResult.test_id == mlst_test_id) \
        .first()
    return mlst_mist_result


def mlst_st_to_serovar(mlst_st):
    df_mlst_to_serovar = mlst_to_serovar_matrix_table()
    if mlst_st not in df_mlst_to_serovar.columns:
        return
    series_mlst_serovars = df_mlst_to_serovar[mlst_st]
    series_mlst_serovars = series_mlst_serovars[~pd.isnull(series_mlst_serovars)]
    mlst_serovar_counts = {i: int(v) for i, v in series_mlst_serovars.iteritems()}
    counter_mlst_serovar_counts = Counter(mlst_serovar_counts)
    top_serovar_by_mlst = counter_mlst_serovar_counts.most_common(1)[0][0]
    return top_serovar_by_mlst, mlst_serovar_counts


def update_serovar_prediction(session, serovar_pred, top_serovar_by_mlst, mlst_serovar_counts):
    # type: (Session, SerovarPrediction, str, Dict[str, int]) -> None
    try:
        serovar_pred.mlst_serovar_counts = mlst_serovar_counts
        serovar_pred.serovar_mlst = top_serovar_by_mlst
        session.add(serovar_pred)
        session.commit()
    except Exception as ex:
        logging.error('Could not update SerovarPrediction {}. Exception {}'.format(
            serovar_pred,
            ex))
        logging.warning('Rolling back DB session')
        session.rollback()


def add_mlst_serovar_prediction(session, user, genome, serovar_pred):
    # type: (Session, User, Genome, SerovarPrediction) -> Optional[Tuple[str, str, Dict[str, int]]]
    """
    Determine the serovar prediction based off the MLST ST if the genome has an MLST ST assigned and save prediction to database.

    Notes:
        The MLST ST to serovar table was generated from a supplementary table from
        Achtmann et al 2012 paper

        http://journals.plos.org/plospathogens/article?id=10.1371/journal.ppat.1002776

        Updated from Enterobase 2016-05-17.

    Args:
        session (Session): DB session
        user (User):
        genome (Genome): genome to update
        serovar_pred (SerovarPrediction): serovar prediction to update with MLST serovar prediction

    Returns:
        (str, str, Dict[str,int]): MLST ST, MLST serovar prediction, MLST to serovar frequency counts
    """
    mlst_mist_result = query_mlst_result(session, user, genome)
    if mlst_mist_result is not None and 'ST' in mlst_mist_result.attrs:
        mlst_st = mlst_mist_result.attrs['ST']
        if mlst_st != '':
            top_serovar_by_mlst, mlst_serovar_counts = mlst_st_to_serovar(mlst_st)
            update_serovar_prediction(session, serovar_pred, top_serovar_by_mlst, mlst_serovar_counts)
            return mlst_st, top_serovar_by_mlst, mlst_serovar_counts
    logging.warning('Could not determine MLST serovar prediction for {} ({})'.format(
        genome,
        serovar_pred
    ))
    return None

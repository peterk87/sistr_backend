from __future__ import absolute_import

from flask_restful import Resource, reqparse
from redis import Redis

from app import logger


class CeleryTaskAPI(Resource):
    """API for getting the status of one or more Celery tasks.
    """

    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('task_id', type=str, action='append', required=True)
        super(CeleryTaskAPI, self).__init__()

    def get(self):
        """Get the status of one or more Celery tasks.
        The ``task_id`` request argument should contain one or more task IDs.

        :query string task_id: One or more Celery task IDs to retrieve statuses and info for.
        :>jsonarr string task_id: Task ID
        :>jsonarr string status: Status of task (`PENDING`, `SUCCESS`, `FAILURE`, etc)
        :>jsonarr dict info: Other info/results about task (e.g. Mash genome search against 55k RefSeq genomes)
        :>jsonarr string desc: Description of current task state.
        """
        args = self.reqparse.parse_args()
        task_ids = args['task_id']
        rtn = []
        from ...tasks import CELERY

        for task_id in task_ids:
            task = CELERY.AsyncResult(task_id)
            logger.info('task {} {}'.format(task, task.__dict__))
            status = task.status
            if not isinstance(task.info, dict) and status == 'FAILURE':
                desc = '{}'.format(task.info)
                info = {'desc': desc, 'progress': 100, }
            else:
                info = task.info

            d = {'task_id': task.task_id,
                 'status': status,
                 'info': info,
                 }
            logger.info('task dict %s', d)
            rtn.append(d)
        return rtn

class CeleryQueueMonAPI(Resource):

    def get(self):
        """
        Basic Celery task queue information about genome analysis task timings and how many tasks are currently queued.

        :>json dict runtime_stats: `mean`, `median` and `std` deviation of genome analysis runtimes.
        :>json int tasks_queued: Number of genome tasks queued for analysis
        """
        r = Redis()
        celery_queue_name = 'celery'
        tasks_queued = r.llen(celery_queue_name)

        resp_dict = {'tasks_queued': tasks_queued, }

        try:
            timings_key = 'genome_analysis_task_timings'
            timings = [float(x) for x in r.lrange(timings_key, 0, r.llen(timings_key))]

            import numpy

            mean_runtime = numpy.mean(timings)
            median_runtime = numpy.median(timings)
            std_runtime = numpy.std(timings)
            resp_dict['runtime_stats'] = {'mean': mean_runtime,
                                          'median': median_runtime,
                                          'std': std_runtime,}
        except:
            pass

        return resp_dict
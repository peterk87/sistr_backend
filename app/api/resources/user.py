from __future__ import absolute_import

from datetime import datetime
from typing import Dict, List, Optional, Union
from flask import g, current_app
from flask_restful import reqparse, Resource
from sqlalchemy.orm import Session

from app.db_util import get_user
from ...genome_metadata import get_genome_metadata_dict, get_basic_serovar_prediction_info, get_genome_quality_stats
from ... import session, logger, cache
from ...api import authorized, auth
from ...models import User, UserRole, Genome


def add_commit_user(dbsession, user):
    # type: (Session, User) -> None
    try:
        dbsession.add(user)
        dbsession.commit()
    except Exception as ex:
        err_msg = 'Could not add/update user "{}". Exception: {}'.format(user.name, ex)
        logger.error(err_msg)
        logger.error('Rolling back DB session')
        dbsession.rollback()
        raise ex


def user_info(user):
    # type: (User) -> Dict[str, object]
    genome_name_query = session.query(Genome.name)\
        .filter(Genome.user_id == user.id,
                Genome.is_analyzed)
    genome_names = [x for x, in genome_name_query.all()]
    resp = {'name': user.name,
            'last_seen': user.last_seen.isoformat(),
            'genomes': genome_names,
            'role': user.role.value,
            'email': user.email}
    if user.role == UserRole.registered:
        resp['token'] = user.generate_auth_token(current_app.config['SECRET_KEY'])
    return resp


def invalidate_genome_info_cache(user):
    # type: (User) -> None
    #: Invalidate cached metadata, serovar prediction results and quality stats info for all User genomes
    for genome in user.genomes.all():
        cache.delete_memoized(get_genome_metadata_dict, genome.id)
        cache.delete_memoized(get_basic_serovar_prediction_info, genome.id)
        cache.delete_memoized(get_genome_quality_stats, genome.id)

class UserCheckAPI(Resource):
    def get(self, user_name):
        """
        Check if user already exists in DB
        :param string user_name: username

        :status 200: user exists
        :status 404: user does not exist
        """
        user = get_user(session, user_name)
        if user:
            return {}, 200
        else:
            return {}, 404


class UserTokenAPI(Resource):
    @auth.login_required
    def get(self):
        """Get an auth token for a SISTR user.

        Note:
            Token expires in 24 hours.

        :>json string token: Auth token
        :>json int duration: Auth token expiration (default=24hr)

        :reqheader Authorization: Basic HTTP Auth for user
        """
        sk = current_app.config['SECRET_KEY']
        token = g.user.generate_auth_token(sk)
        logger.info('Get Auth token for user "{}"'.format(g.user))
        return {'token': token.decode('ascii'),
                'duration': 86400}


class UserAPI(Resource):
    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('password', type=str, location='json')
        self.reqparse.add_argument('email', type=str, location='json')
        self.reqparse.add_argument('new_username', type=str, location='json')
        super(UserAPI, self).__init__()

    @authorized(public_auth_required=False)
    def get(self, user_name):
        # type: (str) -> (Dict[str, object], int)
        """Get user information.

        Note:
            Authentication required for accessing Registered user information.

        :param string user_name: SISTR username

        :>json string name: username
        :>json string last_seen: UTC Date/Time in ISO format when user was last seen
        :>json array genomes: List of user genome names
        :>json string role: User's role (admin, registered or temporary)
        :>json string email: User's email address

        :reqheader Authorization: Basic HTTP Auth for registered user info access

        :status 403: unauthenticated access
        :status 404: user not found
        """
        user = session.query(User).filter(User.name == user_name).first()
        if user is None:
            logger.error('GET User "{}" not found in DB'.format(user_name))
            return {'error': 'Specified user does not exist'}, 404
        if user != g.user:
            logger.error('GET Auth user {} does not match specified user {}'.format(user, g.user))
            return {'error': 'Authenticated user does not match specified user'}, 403
        assert isinstance(user, User)
        logger.info('GET User {}; username {}'.format(user, user_name))
        # Update when User last seen
        user.last_seen = datetime.utcnow()
        add_commit_user(session, user)
        return user_info(user), 200

    def post(self, user_name):
        # type: (str) -> (Dict[str, str], int)
        """Create new SISTR User.
        If the user makes a POST request with a new user name then try to fetch
        that user.
        If a user by that name does not exist then create one.
        If the user is POSTing with the public SISTR user name then create a
        new randomly named user.

        :param string user_name: User name of new user to create/fetch. If public SISTR user name then generate a new random user name.I hear that you and Ruth are all set up with a nice pad - congrats on buying a house!

        :>json string name: username
        :>json string last_seen: UTC Date/Time in ISO format when user was last seen
        :>json array genomes: List of user genome names
        :>json string role: User's role (admin, registered or temporary)
        :>json string email: User's email address

        :status 200: retrieving existing user information; updating when user was last seen
        :status 201: created new user
        :status 403: unauthenticated access
        """
        public_username = current_app.config['PUBLIC_SISTR_USER_NAME']
        if user_name == public_username:
            user_name = User.create_new_user_id(session)

        logger.info('Creating new user "{}"'.format(user_name))
        user = get_user(session, user_name)
        logger.info('Query for user "{}"={}'.format(user_name, user))
        if user:
            # registered users cannot be retrieved by POST; neeed auth to GET registered user info
            if user.role == UserRole.registered:
                return {'error': 'User already exists. Login required.'}, 403

            # temporary users can be retrieved by POST; no auth needed to retrieve info
            if user.role == UserRole.temporary:
                # update user's last seen
                user.last_seen = datetime.utcnow()
                try:
                    session.add(user)
                    session.commit()
                    return user_info(user), 200
                except Exception as ex:
                    err_msg = 'Could not update user info for user "{}". Exception: {}'.format(user_name, ex)
                    session.rollback()
                    return {'error': err_msg}, 500

        user = User(name=user_name, last_seen=datetime.utcnow())
        args = self.reqparse.parse_args()
        password = args['password']
        email = args['email']
        # if new user provides password, then hash password and set UserRole to registered
        if password:
            logger.info('New user {} has password; setting UserRole to Registered user'.format(user_name))
            user.hash_password(password)
            user.role = UserRole.registered
            if email:

                user.email = email

        add_commit_user(session, user)

        resp = user_info(user)
        return resp, 201

    @authorized(public_auth_required=True)
    def put(self, user_name):
        """Update User_ information.

        Update SISTR User information such as the ``new_username``, ``password`` and recovery ``email`` address
        which can be included in a JSON body of a PUT request.

        :<json string new_username: New username for User
        :<json string password: Password for User
        :<json string email: Email address to set for User

        :param string user_name: SISTR username

        :>json string name: username
        :>json string last_seen: UTC Date/Time in ISO format when user was last seen
        :>json array genomes: List of user genome names
        :>json string role: User's role (admin, registered or temporary)
        :>json string email: User's email address
        :>json string token: User auth token if registered user
        """
        user = get_user(session, user_name)
        if user is None:
            err_msg = 'User "{}" does not exist'.format(user_name)
            logger.error(err_msg)
            return {'error': err_msg}, 404

        if user != g.user:
            err_msg = 'Specified user does not match authenticated user'
            logger.error(err_msg)
            return {'error': err_msg}, 403

        public_username = current_app.config['PUBLIC_SISTR_USER_NAME']
        args = self.reqparse.parse_args()
        password = args['password']
        email = args['email']
        new_username = args['new_username']
        logger.info('PUT user_name="{}"; new password?={}; email="{}"; new_username="{}"'.format(
            user_name,
            password is not None,
            email,
            new_username))

        if new_username:
            if public_username == user_name:
                return {'error': 'Renaming this user not permitted'}, 403
            if session.query(User).filter(User.name == new_username).count() > 0:
                return {'error': 'Name "{}" already taken. Please try another name'.format(new_username)}, 403
            user.name = new_username
            invalidate_genome_info_cache(user)

        if email:
            if public_username == user_name:
                return {'error': 'Cannot change email address for this user'}, 403
            if not User.validate_email(email):
                return {'error': 'Invalid email address "{}"'.format(email)}, 403
            user.email = email

        if password:
            if public_username == user_name:
                return {'error': 'Cannot change password for this user'}, 403
            if len(password) < 6:
                return {'error': 'Passwords must be at least 6 characters in length'}, 403
            user.hash_password(password)
            user.role = UserRole.registered

        add_commit_user(session, user)
        resp = user_info(user)
        return resp, 200


class UserSelectionsAPI(Resource):
    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('selections', type=dict, location='json')

    @authorized(public_auth_required=False)
    def get(self, user_name):
        # type: (str) -> Union[(Dict[str, str], int), (Optional[Dict[str, List[str]]], int)]
        """
        Get a user's genome selections.

        **Example user genome selections:**

        .. code-block:: json

            {
                'selection name 1': ['genome1', 'genome2', ...],
                'selection name 4': ['genome3', 'genome4', ...],
            }

        :param string user_name: SISTR username

        :status 404: user not found
        :status 403: unauthenticated access
        """
        user = get_user(session, user_name)
        if user is None:
            logger.error('GET User "{}" not found in DB'.format(user_name))
            return {'error': 'Specified user does not exist'}, 404
        if user != g.user:
            logger.error('GET Auth user {} does not match specified user {}'.format(user, g.user))
            return {'error': 'Authenticated user does not match specified user'}, 403
        return user.selections, 200

    @authorized(public_auth_required=True)
    def put(self, user_name):
        # type: (str) -> Union[(Dict[str, str], int), (Optional[Dict[str, List[str]]], int)]
        """
        Update user selections of genomes.

        :param string user_name: Username

        :<json dict selections: dict of lists of user genome names
        :>json dict selections: dict of lists of user genome names

        :status 403: unauthorized access
        :status 404: user not found
        """
        user = session.query(User).filter(User.name == user_name).first()  # type: User
        if user is None:
            logger.error('GET User "{}" not found in DB'.format(user_name))
            return {'error': 'Specified user does not exist'}, 404
        if user != g.user:
            logger.error('GET Auth user {} does not match specified user {}'.format(user, g.user))
            return {'error': 'Authenticated user does not match specified user'}, 403
        assert isinstance(user, User)

        args = self.reqparse.parse_args()
        selections = args['selections']
        if selections is None:
            # return User selections
            return user.selections, 200
        user.selections = selections
        add_commit_user(session, user)
        return user.selections, 200


class UserPasswordResetAPI(Resource):
    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('email')
        self.reqparse.add_argument('username')
        super(UserPasswordResetAPI, self).__init__()

    @staticmethod
    def reset_password_send_email(user):
        # type: (User) -> None
        email_subject_fmt = '[SISTR] Password Reset for user "{user}"'
        email_fmt = (
            'SISTR - Salmonella In Silico Typing Resource\n'
            '\n'
            'Password for user "{user}" has been reset!\n'
            '\n'
            'Username: {user}\n'
            'Temporary Password: {pw}\n'
            '\n'
            'Do not reply to this email.'
        )
        temp_pw = User.create_new_user_id(session)
        user.hash_password(temp_pw)
        add_commit_user(session, user)

        from ...tasks import send_email

        t = send_email.s([user.email],
                         email_subject_fmt.format(user=user.name),
                         email_fmt.format(user=user.name,
                                          pw=temp_pw))
        r = t.apply_async()
        logger.info('Added task ({}) to send email to {} for user {}'.format(r, user.email, user))

    def get(self):
        # type: () -> (Dict[str, str], int)
        """
        Send a new temporary randomly generated password to a user's email.

        :query string email: If an email is provided, attempt to send a new temp password to that email if there exists a user with that email address.
        :query string username: Username

        :status 200: if email with new password is queued for sending
        """
        args = self.reqparse.parse_args()
        email = args['email']
        public_username = current_app.config['PUBLIC_SISTR_USER_NAME']
        if email:
            users = session.query(User).filter(User.email == email).all()
            for user in users:
                if user.name == public_username:
                    logger.warning('Attempt to reset password for public SISTR user!')
                    continue
                UserPasswordResetAPI.reset_password_send_email(user)

        username = args['username']
        if username:
            if username == public_username:
                logger.warning('Attempt to reset password for public SISTR user!')
                return {'error': 'Forbidden request'}, 403
            else:
                user = get_user(session, username)
                if user and user.email:
                    UserPasswordResetAPI.reset_password_send_email(user)

        return {}, 200

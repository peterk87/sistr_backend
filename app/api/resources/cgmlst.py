# -*- coding: utf-8 -*-
from __future__ import absolute_import

from typing import Dict, Optional, List
from flask import g, current_app, request
from flask_restful import Resource, reqparse
import numpy as np
from scipy.spatial.distance import squareform
from scipy.cluster.hierarchy import to_tree, ClusterNode
from scipy.sparse import find
from scipy.sparse.csgraph import minimum_spanning_tree
from fastcluster import linkage
from numpy import array

from app.db_util import get_user
from .. import authorized
from ... import cache, session, logger
from ...cgmlst import condensed_dist_matrix, cgmlst_profiles, profile_dist
from ...models import User, Genome, CurationInfo


def complete_linkage(dm):
    # type: (array) -> (array)
    """
    Perform complete linkage hierarchical clustering on a distance matrix.

    Args:
        dm (numpy.array): Distance matrix

    Returns:
        (object): fastcluster complete linkage hierarchical clustering object
    """
    return linkage(dm, 'complete')


@cache.memoize(timeout=int(1e7))
def mst_adjacency_list(genome_names):
    # type: (List[str]) -> (Dict[str, object])
    """MST adjency list for list of genomes

    Args:
        genome_names (list of string): List of genomes to generate MST for

    Returns:
        (dict): `['genomes']` contains list of lists with grouped genome names that have 0 distance between them.
                `['mst']['links']` contains the source/target/weight adjacency list

    """
    dm, gs_collapse = condensed_dist_matrix(genome_names)
    logger.debug('Condensed distance matrix of size {0}'.format(dm.shape))
    # scipy MST func uses Kruskal's algo and is much faster than NetworkX
    mst = minimum_spanning_tree(squareform(dm))
    logger.debug('MST compressed row format object {}'.format(mst))
    # scipy.sparse.find returns a tuple with 3 vectors - targets, sources and weights - when given an MST as input
    sources, targets, weights = find(mst)
    # tolist is necessary to convert Numpy Int64/Float64 to int/float for correct JSON serialization
    sources = sources.tolist()
    targets = targets.tolist()
    weights = weights.tolist()
    logger.debug('sources %s; targets %s; weights %', len(sources), len(targets), len(weights))
    # return a similar data structure as NetworkX json_graph - list of dicts
    links = []
    for s,t,w in zip(sources, targets, weights):
        # undirected graph so order of source to target doesn't matter, however
        # swap source and target ids if source id is larger than target id so
        # that it can be rendered similarly as NetworkX MST graphs
        if s > t:
            s, t = (t, s)
        links.append({'source': s,
                      'target': t,
                      'weight': w, })
    return {'genomes': gs_collapse,
            'mst': {'links': links}}


def get_user_accessible_genomes(user_name):
    # type: (str) -> (List[str])
    """Get a list of Genome_ names accessible to a specified User.

    Args:
        user_name (str): User_ name

    Returns:
        list: Genome_ names
    """
    public_username = current_app.config['PUBLIC_SISTR_USER_NAME']
    public_genomes = [name for name, in session.query(Genome.name) \
        .join(Genome.user) \
        .filter(User.name == public_username).all()]
    if user_name == public_username:
        genomes = public_genomes
    else:
        private_genomes = [name for name, in session.query(Genome.name) \
            .join(Genome.user) \
            .filter(User.name == user_name).all()]
        genomes = private_genomes + public_genomes
    return genomes


def _scipy_tree_to_newick_list(node, newick, parentdist, leaf_names):
    # type: (ClusterNode, List[str], float, List[str]) -> (List[str])
    """List of Newick tree output string from SciPy hierarchical clustering tree

    This is a recursive function to help build a Newick output string from a scipy.cluster.hierarchy.to_tree input with
    user specified leaf node names in `leaf_names`.

    Notes:
        This function is meant to be used with `to_newick`

    Args:
        node (scipy.cluster.hierarchy.ClusterNode): Root node is output of scipy.cluster.hierarchy.to_tree from hierarchical clustering linkage matrix
        parentdist (float): Distance of parent node of `node`
        newick (list of string): Newick string output accumulator list which needs to be reversed and concatenated (i.e. `''.join(newick)`) for final output
        leaf_names (list of string): Leaf node names

    Returns:
        (list of string): Returns `newick` list of Newick output strings
    """
    if node.is_leaf():
        newick.append("%s:%.6f" % (leaf_names[node.id], parentdist - node.dist))
        return newick
    else:
        if len(newick) > 0:
            newick.append("):%.6f" % (parentdist - node.dist))
        else:
            newick.append(");")
        newick = _scipy_tree_to_newick_list(node.get_left(), newick, node.dist, leaf_names)
        newick.append(',')
        newick = _scipy_tree_to_newick_list(node.get_right(), newick, node.dist, leaf_names)
        newick.append("(")
        return newick


def to_newick(tree, leaf_names):
    # type: (ClusterNode, List[str]) -> (str)
    """Newick tree output string from SciPy hierarchical clustering tree

    Convert a SciPy ClusterNode tree to a Newick format string.
    Use scipy.cluster.hierarchy.to_tree on a hierarchical clustering linkage matrix to create the root ClusterNode for the `tree` input of this function.

    Args:
        tree (scipy.cluster.hierarchy.ClusterNode): Output of scipy.cluster.hierarchy.to_tree from hierarchical clustering linkage matrix
        leaf_names (list of string): Leaf node names

    Returns:
        (string): Newick output string
    """
    newick_list = _scipy_tree_to_newick_list(tree, [], tree.dist, leaf_names)
    return ''.join(newick_list[::-1])


@cache.memoize(timeout=int(1e7))
def get_newick_tree(genomes):
    # type: (List[str]) -> (Dict[str, object])
    """
    Construct a complete linkage hierarchical clustering tree for some specified Genomes_ and return
    the Newick string for that tree along with a list of the list of Genomes_ at each leaf node.

    Args:
        genomes (list): List of Genome_ names

    Notes:
        cgMLST_330 profiles are expected to stay static after initial analysis of a Genome_ so the profiles, trees, distance matrices, etc derived from the cgMLST results can be cached essentially forever.

    Returns:
        dict['tree']: Newick string of collapsed tree of specified `genomes`
        dict['genomes']: 2D list of genomes grouped based on 100% cgMLST profile similarity
    """
    dm, gs_collapse = condensed_dist_matrix(genomes)
    logger.info('Distance matrix has %s nodes from %s genomes; shape %s', len(gs_collapse), len(genomes), dm.shape)
    Z = complete_linkage(dm)
    logger.info('Complete linkage %s; dist matrix shape %s', Z.shape, dm.shape)
    T = to_tree(Z, False)
    nwk_str = to_newick(T, [names[0] for names in gs_collapse])
    rtn_obj = {'tree': nwk_str, 'genomes': gs_collapse}
    return rtn_obj


class CgMLSTProfilesAPI(Resource):
    @authorized(public_auth_required=False)
    def get(self, user_name):
        """Get cgMLST profiles for all user genomes.

        :param string user_name: user name

        :>json array genomes: List of lists of genomes grouped at 100% cgMLST profile similarity.
        :>json array results: cgMLST profiles as 1D array of length ``#genomes x #markers``
        :>json array markers: List of cgMLST markers

        :status 403: unauthorized access
        :status 404: User not found
        """
        user = get_user(session, user_name)
        if user is None:
            logger.error('GET User "{}" not found in DB'.format(user_name))
            return {'error': 'Specified user does not exist'}, 404
        if user != g.user:
            logger.error('GET Auth user {} does not match specified user {}'.format(user, g.user))
            return {'error': 'Authenticated user does not match specified user'}, 403
        genomes = get_user_accessible_genomes(user_name)
        d = cgmlst_profiles(genomes)
        rs = [int(x) if not np.isnan(x) else None for x in d['results']]
        d['results'] = rs
        return d

class CgMLSTProfilesCSVAPI(Resource):
    @authorized(public_auth_required=False)
    def get(self, user_name):
        """Get cgMLST profiles for all user genomes as CSV.

        :param string user_name: user name

        :>json array genomes: List of lists of genomes grouped at 100% cgMLST profile similarity.
        :>json array results: cgMLST profiles as 1D array of length ``#genomes x #markers``
        :>json array markers: List of cgMLST markers

        :status 403: unauthorized access
        :status 404: User not found
        """
        user = session.query(User).filter(User.name == user_name).first()
        if user is None:
            logger.error('GET User "{}" not found in DB'.format(user_name))
            return {'error': 'Specified user does not exist'}, 404
        if user != g.user:
            logger.error('GET Auth user {} does not match specified user {}'.format(user, g.user))
            return {'error': 'Authenticated user does not match specified user'}, 403
        genomes = get_user_accessible_genomes(user_name)
        d = cgmlst_profiles(genomes)
        rs = [int(x) if not np.isnan(x) else None for x in d['results']]
        d['results'] = rs
        import pandas as pd
        arr = np.array(rs)
        arr.shape = (len(d['genomes']), len(d['markers']))
        df = pd.DataFrame(arr)
        df.index = d['genomes']
        df.columns = d['markers']
        return df.to_csv()


class CgMLSTDistanceMatrixAPI(Resource):
    @authorized(public_auth_required=False)
    def get(self, user_name):
        """Get cgMLST distance matrix

        Get the pairwise non-redundant distances between wgMLST_330 *in silico* derived profiles for genomes accessible
        to a user. The lower triangular distance matrix values are returned as a 1-D list. This requires that the client
        rearrange the values into the appropriate configuration.

        :param string user_name: user name

        :>json array genomes: List of lists of genomes grouped at 100% cgMLST profile similarity.
        :>json array distances: List of floating point decimal non-redundant pairwise cgMLST distances between genomes.

        Notes:
            Genomes with 0 distance between each other will be grouped together under ``genomes`` and will only have one
            distance in the distance matrix. This along with only returning the distances for the lower triangular
            matrix are done to save on bandwidth.

        :status 403: unauthorized access
        :status 404: User not found
        """
        user = session.query(User).filter(User.name == user_name).first()
        if user is None:
            logger.error('GET User "{}" not found in DB'.format(user_name))
            return {'error': 'Specified user does not exist'}, 404
        if user != g.user:
            logger.error('GET Auth user {} does not match specified user {}'.format(user, g.user))
            return {'error': 'Authenticated user does not match specified user'}, 403
        public_username = current_app.config['PUBLIC_SISTR_USER_NAME']
        if user_name is None:
            user_name = public_username
        genomes = get_user_accessible_genomes(user_name)
        dm, gs_collapse = condensed_dist_matrix(genomes)
        return {'genomes': gs_collapse, 'distances': list(dm)}


class CgMLSTAdjacencyList(Resource):
    @authorized(public_auth_required=False)
    def post(self, user_name):
        """
        Get MST adjacency list of ``source`` to ``target`` with ``weight``.

        :param string user_name: Username
        :<json list genome: List of genomes to generate MST for

        :>json array genomes: List of list of grouped genomes based on 100% cgMLST similarity.
        :>json dict mst: ``links`` with value of list of ``source`` genome group to ``target`` genome group with numeric cgMLST distance ``weight``.
        """
        genomes = None
        req_json = request.get_json()
        if req_json:
            try:
                genomes = req_json['genome']
                assert isinstance(genomes, list)
                for x in genomes:
                    assert isinstance(x, (str, unicode,))
            except KeyError:
                genomes = get_user_accessible_genomes(user_name)
            except AssertionError:
                return {'error': 'Expected list of genome names in POST JSON under key "genome"'}
            public_username = current_app.config['PUBLIC_SISTR_USER_NAME']
            if user_name is None:
                user_name = public_username
            logger.info('Getting MST adjacency list for genomes accessible to user {}'.format(user_name))

        user_accessible_genomes = get_user_accessible_genomes(user_name)
        if genomes is None:
            genomes = user_accessible_genomes
        else:
            genomes_names_set = set(genomes)
            user_accessible_genomes_set = set(user_accessible_genomes)
            forbidden_genomes = genomes_names_set - user_accessible_genomes_set
            if len(forbidden_genomes) > 0:
                logger.warning('User {} tried to access n={} genomes for which they did not have access to'.format(user_name, len(forbidden_genomes)))
            genomes = list(genomes_names_set & user_accessible_genomes_set)

        return mst_adjacency_list(sorted(genomes))

    @authorized(public_auth_required=False)
    def get(self, user_name):
        return self.post(user_name)


class CgMLSTNewickTree(Resource):
    @authorized(public_auth_required=False)
    def post(self, user_name):
        """
        Get complete linkage tree in Newick format for a list of genomes from pairwise Hamming distances between cgMLST
        profiles.

        :param string user_name: Username
        :<json list genome: List of genomes to generate tree for

        :>json array genomes: List of list of grouped genomes based on 100% cgMLST similarity.
        :>json string tree: Complete linkage tree as Newick format string
        """
        genomes = None
        req_json = request.get_json()
        if req_json:
            try:
                genomes = req_json['genome']
                assert isinstance(genomes, list)
                for x in genomes:
                    assert isinstance(x, (str, unicode, ))
            except KeyError:
                genomes = get_user_accessible_genomes(user_name)
            except AssertionError:
                return {'error': 'Expected list of genome names in POST JSON under key "genome"'}
        user_accessible_genomes = get_user_accessible_genomes(user_name)
        if genomes is None:
            genomes = user_accessible_genomes
        else:
            genomes_names_set = set(genomes)
            user_accessible_genomes_set = set(user_accessible_genomes)
            forbidden_genomes = genomes_names_set - user_accessible_genomes_set
            if len(forbidden_genomes) > 0:
                logger.warning(
                    'User {} tried to access n={} genomes for which they did not have access to'.format(user_name, len(
                        forbidden_genomes)))
            genomes = list(genomes_names_set & user_accessible_genomes_set)
        if genomes is None or len(genomes) <= 1:
            return {'error': 'No genomes specified for cgMLST tree'}, 400
        logger.info('Newick tree of {} genomes'.format(len(genomes)))
        rtn_obj = get_newick_tree(genomes)
        return rtn_obj

    @authorized(public_auth_required=False)
    def get(self, user_name):
        return self.post(user_name)


class ClosestRelative(Resource):
    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('curated_only', type=bool, default=False)
        super(ClosestRelative, self).__init__()

    @authorized(public_auth_required=False)
    def get(self, user_name, genome_name):
        """
        Find closest public genome by cgMLST distance.

        :param string user_name: Username
        :param string genome_name: Genome name

        :query boolean curated_only: Only return relatives that are have trusted serovar curations.

        :>json dict closest_relative: Info for closest related genome by cgMLST distance
        :>json array relatives: List of dict of info for top 5 closest related genomes by cgMLST distance
        :>json string genome: Genome name
        """
        args = self.reqparse.parse_args()
        curated_only = args['curated_only']

        logger.info('Retrieving closest relative (curated={}) for genome {} of user {}'.format(
            curated_only,
            genome_name,
            user_name))
        user = g.user
        if user is None:
            return {'error': 'user {0} not found'.format(user_name)}, 404
        genome = user.genomes.filter(Genome.name == genome_name).first()
        if genome is None:
            return {'error': 'genome {0} not found'.format(genome)}, 404
        if not genome.is_analyzed:
            return {'error': 'genome {} not analyzed'.format(genome)}, 403

        public_username = current_app.config['PUBLIC_SISTR_USER_NAME']

        query = session.query(Genome.name) \
            .join(Genome.user) \
            .join(CurationInfo) \
            .filter(User.name == public_username,
                    Genome.is_analyzed)

        if curated_only:
            query = query.filter(CurationInfo.is_trusted)

        public_genome_names = [name for name, in query.order_by(Genome.id).all()]
        public_genome_profiles = cgmlst_profiles(public_genome_names)
        genome_profile_array = cgmlst_profiles([genome.name])['results']
        closest_relative, genome_matching_alleles = profile_dist(genome_name=genome_name,
                                                                 genome_profile=genome_profile_array,
                                                                 other_genome_profiles=public_genome_profiles)
        logger.info(
            'GET - The closest public genome relative for genome {0} of user {1} is {2} with {3} matching alleles.'.format(
                genome_name,
                user_name,
                closest_relative['genome'],
                closest_relative['n_matching_alleles']))
        return {'closest_relative': closest_relative,
                'relatives': genome_matching_alleles,
                'genome': genome_name,
                'user': user_name}

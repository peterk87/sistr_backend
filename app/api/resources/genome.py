# -*- coding: utf-8 -*-
from __future__ import absolute_import

from datetime import datetime

import pandas as pd
from flask import g, current_app
from flask_restful import Resource, reqparse, abort
from sqlalchemy import or_
from werkzeug.datastructures import FileStorage

from .. import authorized
from ... import logger, session, cache
from ...db_loaders import DefaultDataLoader
from ...genome_metadata import get_genome_metadata_dict, listdict_to_dictlist, genome_assembly_stats, \
    get_genome_quality_stats, get_basic_serovar_prediction_info, user_genomes_metadata, genome_locations
from ...models import Genome, User, SerovarPrediction, CurationInfo, Contig
from ...tasks import analyse_genome
from ...util import write_temp_file, contigs_to_fasta_stringio
from app.db_util import get_user, get_genome


class GenomeAPI(Resource):
    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('fasta', type=FileStorage, location='files')
        self.reqparse.add_argument('metadata', type=dict, location='json')
        self.reqparse.add_argument('run_rmlst', type=bool, default=False)
        self.reqparse.add_argument('reanalyze', type=bool, default=False)

    @authorized(public_auth_required=False)
    def get(self, user_name, genome_name):
        """
        Get Genome metadata.

        :param string user_name: Username
        :param string genome_name: Genome name

        :>json string user: Username
        :>json string genome: Genome name
        :>json dict metadata: Genome metadata

        :reqheader Authorization: Basic HTTP Auth for registered user info access

        :status 403: unauthorized access
        :status 404: User/genome not found
        """
        user = session.query(User).filter(User.name == user_name).first()
        if user is None:
            logger.error('GET User "{}" not found in DB'.format(user_name))
            return {'error': 'Specified user does not exist'}, 404
        if user != g.user:
            logger.error('GET Auth user {} does not match specified user {}'.format(user, g.user))
            return {'error': 'Authenticated user does not match specified user'}, 403

        genome = user.genomes.filter(Genome.name == genome_name).first()

        if genome is None:
            return abort(404)
        if not genome.is_analyzed:
            return {'error': 'genome {} not analyzed'.format(genome_name)}, 403

        return {'metadata': get_genome_metadata_dict(genome.id),
                'user': user_name,
                'genome': genome_name}

    @authorized(public_auth_required=True)
    def post(self, user_name, genome_name):
        """
        Perform SISTR analysis of User Genome.

        - Serovar prediction (cgMLST, antigen gene search, MLST)
        - cgMLST330
        - MLST
        - Mash RefSeq search

        :param string user_name: Username
        :param string genome_name: Genome name

        :<json boolean reanalyze: Reanalyze genome
        :<json boolean run_rmlst: Perform in silico rMLST (long runtime; does not affect serovar predictions)
        :form fasta: FASTA format file


        :>json string user: Username
        :>json string genome: Genome name
        :>json string task: Celery task ID for checking progress on genome analysis

        :reqheader Authorization: Basic HTTP Auth for registered user info access

        :status 400: Analysis could not proceed without FASTA file
        :status 403: unauthorized access
        :status 404: User/genome not found
        """
        user = session.query(User).filter(User.name == user_name).first()
        if user is None:
            logger.error('GET User "{}" not found in DB'.format(user_name))
            return {'error': 'Specified user does not exist'}, 404
        if user != g.user:
            logger.error('GET Auth user {} does not match specified user {}'.format(user, g.user))
            return {'error': 'Authenticated user does not match specified user'}, 403

        args = self.reqparse.parse_args()
        reanalyze = args['reanalyze']
        fasta = args['fasta']
        genome = session.query(Genome) \
            .filter(Genome.name == genome_name,
                    Genome.user_id == user.id) \
            .first()
        filename = None
        #: Not possible to re-analyze genome without genome already existing in the DB
        if reanalyze and genome is None:
            return {'error': 'Cannot reanalyze genome without genome assembly FASTA.'}, 400

        if fasta is None and not reanalyze:
            return {'error': 'No FASTA file provided. Analysis cannot proceed'}, 400
        elif fasta is None and reanalyze:
            genome_contigs = session.query(Contig).filter(Contig.genome_id == genome.id).all()
            if len(genome_contigs) == 0:
                return {'error': 'FASTA file must be provided to reanalyze genome'}, 400
            sio_fasta = contigs_to_fasta_stringio(genome_contigs)
            genome_name = genome.name
            filename = write_temp_file(sio_fasta)
        elif fasta is not None and reanalyze:
            genome_name = genome.name
            filename = write_temp_file(fasta)
        elif fasta is not None and not reanalyze:
            import re
            #: Replace non-word characters with underscores to reduce conflicts later on in analysis
            genome_name = re.sub(r'[\W]+', '_', genome_name)
            #: Ensure name is unique
            genome_name = Genome.make_unique_name(genome_name, session)
            #: Write FASTA to a temporary file
            filename = write_temp_file(fasta)
        if filename is None:
            return {'error': 'FASTA file could not be written to temporary location'}, 500
        task = analyse_genome(app=current_app,
                              fasta_temp_file=filename,
                              user_name=user_name,
                              genome_name=genome_name,
                              reanalyze=reanalyze)

        if genome:
            #: Invalidate cached metadata, serovar prediction results and quality stats info for Genome
            logger.warning(
                'Invalidating cached metadata, serovar prediction results and quality stats info for Genome %s', genome)
            cache.delete_memoized(get_genome_metadata_dict, genome.id)
            cache.delete_memoized(get_basic_serovar_prediction_info, genome.id)
            cache.delete_memoized(get_genome_quality_stats, genome.id)

        return {'task': task.id,
                'user': user_name,
                'genome': genome_name}, 201

    @authorized(public_auth_required=True)
    def delete(self, user_name, genome_name):
        user = get_user(session, user_name)
        if user is None:
            logger.error('Genome {} DELETE User "{}" not found in DB'.format(
                genome_name,
                user_name))
            return {'error': 'Specified user does not exist'}, 404
        if user != g.user:
            logger.error('Genome {} DELETE Auth user {} does not match specified user {}'.format(
                genome_name,
                user,
                g.user))
            return {'error': 'Authenticated user does not match specified user'}, 403

        genome = get_genome(session, user.id, genome_name)
        if genome is None:
            logger.error('User {} genome {} not found in DB'.format(user, genome_name))
            return {'error': 'Genome not found'}, 404

        try:
            session.delete(genome)
            session.commit()
            logger.warning('Genome {} (User {}) deleted from DB!'.format(genome_name, user))
        except Exception as ex:
            logger.error('Could not delete genome {} of user {} from DB. Exception {}'.format(
                genome,
                user,
                ex
            ))
            logger.warning('Rolling back DB session')
            session.rollback()


class GenomesAssemblyStatsAPI(Resource):
    @authorized(public_auth_required=False)
    def get(self, user_name):
        """
        Get QUAST genome assembly metrics and genome metadata for all user genomes.

        :param string user_name: Username
        """
        user = session.query(User).filter(User.name == user_name).first()
        if user is None:
            logger.error('GET User "{}" not found in DB'.format(user_name))
            return {'error': 'Specified user does not exist'}, 404
        if user != g.user:
            logger.error('GET Auth user {} does not match specified user {}'.format(user, g.user))
            return {'error': 'Authenticated user does not match specified user'}, 403
        public_username = current_app.config['PUBLIC_SISTR_USER_NAME']
        public_stats = genome_assembly_stats(public_username)
        if user_name == public_username:
            return listdict_to_dictlist(public_stats)

        private_stats = genome_assembly_stats(user_name)

        return listdict_to_dictlist(private_stats + public_stats)


class GenomeMetadataAPI(Resource):
    def __init__(self):
        self.reqparse = reqparse.RequestParser()
        self.reqparse.add_argument('metadata', type=dict, location='json')
        self.reqparse.add_argument('curation_code', type=str, location='json')
        super(GenomeMetadataAPI, self).__init__()

    @authorized(public_auth_required=False)
    def get(self, user_name, genome_name):
        """
        Get Genome metadata.

        :param string user_name: Username
        :param string genome_name: Genome name

        :reqheader Authorization: Basic HTTP Auth for registered user info access

        :status 403: unauthorized access
        :status 404: User/genome not found
        """
        user = session.query(User).filter(User.name == user_name).first()
        if user is None:
            logger.error('GET User "{}" not found in DB'.format(user_name))
            return {'error': 'Specified user does not exist'}, 404
        if user != g.user:
            logger.error('GET Auth user {} does not match specified user {}'.format(user, g.user))
            return {'error': 'Authenticated user does not match specified user'}, 403
        public_username = current_app.config['PUBLIC_SISTR_USER_NAME']
        genome_query = session.query(Genome) \
            .join(Genome.user) \
            .filter(or_(User.name == public_username, User.name == user_name)) \
            .filter(Genome.name == genome_name)

        genome = genome_query.first()
        if genome is None:
            return None
        if not genome.is_analyzed:
            return {'error': 'genome {} not analyzed'.format(genome_name)}, 403
        genome_metadata_dict = get_genome_metadata_dict(genome.id)
        return genome_metadata_dict

    @authorized(public_auth_required=True)
    def put(self, user_name, genome_name):
        """
        Change Genome metadata (and/or curation status)

        :param string user_name: Username
        :param string genome_name: Genome name

        :<json string curation_code: Curation code of genome (basically whether to trust serovar designation or not)
        :<json dict metadata: New genome metadata (trusted serovar designation, host, source, geographic location, date collected, etc)

        :reqheader Authorization: Basic HTTP Auth for registered user info access

        :status 403: unauthorized access
        :status 404: User/genome not found
        """
        user = session.query(User).filter(User.name == user_name).first()
        if user is None:
            logger.error('GET User "{}" not found in DB'.format(user_name))
            return {'error': 'Specified user does not exist'}, 404
        if user != g.user:
            logger.error('GET Auth user {} does not match specified user {}'.format(user, g.user))
            return {'error': 'Authenticated user does not match specified user'}, 403

        genome = session.query(Genome).join(Genome.user) \
            .filter(User.name == user_name) \
            .filter(Genome.name == genome_name) \
            .first()

        if genome is None:
            return abort(404)
        if not genome.is_analyzed:
            return {'error': 'genome {} not analyzed'.format(genome_name)}, 403

        args = self.reqparse.parse_args()
        logger.info('PUT args {}'.format(args))
        metadata = args['metadata']
        curation_code = args['curation_code']
        if metadata is None and curation_code is None:
            logger.info('PUT user {} genome {} no metadata or curation code'.format(user, genome))
            return {'metadata': get_genome_metadata_dict(genome.id),
                    'user': user_name,
                    'genome': genome_name}, 204

        if metadata:
            logger.info('PUT user {} genome {} metadata {}'.format(user, genome, metadata))
            del_keys = ['serovar_predicted', 'collection_year', 'collection_month', 'host_latin', 'host_common']
            logger.warning('Deleting metadata keys with following values %s', del_keys)
            for k in del_keys:
                metadata.pop(k, None)
            logger.info('PUT user {} genome {} metadata {}'.format(user, genome, metadata))
            df = pd.DataFrame({genome_name: metadata}).transpose()
            if 'collection_date' in df.columns:
                df.collection_date = pd.to_datetime(df.collection_date).astype(datetime)
            df.rename(columns=DefaultDataLoader.HOST_FIELD_DICT, inplace=True)
            ddl = DefaultDataLoader(session)
            ddl.parse_genome_metadata(df, user)

        if curation_code:
            logger.info('PUT user {} genome {} curation code {}'.format(user, genome, curation_code))
            ci = session.query(CurationInfo).filter(CurationInfo.code == curation_code).first()
            if ci:
                assert isinstance(ci, CurationInfo)
                genome.curation_info_id = ci.id
                session.add(genome)
                session.commit()

        genome = session.query(Genome).join(Genome.user) \
            .filter(User.name == user_name) \
            .filter(Genome.name == genome_name) \
            .first()

        #: Invalidate cached metadata, serovar prediction results and quality stats info for Genome
        cache.delete_memoized(get_genome_metadata_dict, genome.id)
        cache.delete_memoized(get_basic_serovar_prediction_info, genome.id)
        cache.delete_memoized(get_genome_quality_stats, genome.id)

        return {'metadata': get_genome_metadata_dict(genome.id),
                'user': user_name,
                'genome': genome_name}, 200


class GenomeMetadataListAPI(Resource):
    @authorized(public_auth_required=False)
    def get(self, user_name):
        """
        Get all metadata for all user genomes.

        Returns dict of ordered lists.

        **Example output:**

        .. code-block:: json

            {
                'genome': ['g1', 'g2', 'g3', ... ],
                'serovar': ['X', 'Y', 'Z', ... ],
            }

        Where genome `g1` is serovar `X` and genome `g2` is serovar `Y`.

        :param string user_name: Username

        :reqheader Authorization: Basic HTTP Auth for registered user info access

        :status 403: unauthorized access
        :status 404: User/genome not found
        """
        user = session.query(User).filter(User.name == user_name).first()
        if user is None:
            logger.error('GET User "{}" not found in DB'.format(user_name))
            return {'error': 'Specified user does not exist'}, 404
        if user != g.user:
            logger.error('GET Auth user {} does not match specified user {}'.format(user, g.user))
            return {'error': 'Authenticated user does not match specified user'}, 403
        public_username = current_app.config['PUBLIC_SISTR_USER_NAME']
        public_md = user_genomes_metadata(public_username)
        if user_name == public_username:
            return listdict_to_dictlist(public_md)
        if user.genomes.count() == 0:
            return listdict_to_dictlist(public_md)
        private_md = user_genomes_metadata(user_name)
        public_private_md = public_md + private_md
        return listdict_to_dictlist(public_private_md)


class GenomeMetadataListObjAPI(Resource):
    @authorized(public_auth_required=False)
    def get(self, user_name):
        """
        Get all metadata for all user genomes.

        Returns list of dicts for each genome.

        :param string user_name: Username

        :reqheader Authorization: Basic HTTP Auth for registered user info access

        :status 403: unauthorized access
        :status 404: User/genome not found
        """
        user = session.query(User).filter(User.name == user_name).first()
        if user is None:
            logger.error('GET User "{}" not found in DB'.format(user_name))
            return {'error': 'Specified user does not exist'}, 404
        if user != g.user:
            logger.error('GET Auth user {} does not match specified user {}'.format(user, g.user))
            return {'error': 'Authenticated user does not match specified user'}, 403
        public_username = current_app.config['PUBLIC_SISTR_USER_NAME']
        public_md = user_genomes_metadata(public_username)
        if user_name == public_username:
            return public_md
        if user.genomes.count() == 0:
            return public_md
        private_md = user_genomes_metadata(user_name)
        return public_md + private_md


class GenomesLocationsAPI(Resource):
    @authorized(public_auth_required=False)
    def get(self, user_name):
        """
        Get all location information for all user genomes.

        Returns list of dicts for each genome.

        **Example output:**

        .. code-block:: json

            [{"genome": "g1",
             "country": "Canada",
             "region": "Alberta",
             "lat": 	55,
             "lng": -114.9999998,
             },]

        :param string user_name: Username

        :reqheader Authorization: Basic HTTP Auth for registered user info access

        :status 403: unauthorized access
        :status 404: User/genome not found
        """
        user = session.query(User).filter(User.name == user_name).first()
        if user is None:
            logger.error('GET User "{}" not found in DB'.format(user_name))
            return {'error': 'Specified user does not exist'}, 404
        if user != g.user:
            logger.error('GET Auth user {} does not match specified user {}'.format(user, g.user))
            return {'error': 'Authenticated user does not match specified user'}, 403
        public_username = current_app.config['PUBLIC_SISTR_USER_NAME']
        public_locations = genome_locations(public_username)
        if user_name == public_username:
            return public_locations

        private_locations = genome_locations(user_name)
        return private_locations + public_locations


class GenomeSerovarPredictionAPI(Resource):
    @authorized(public_auth_required=False)
    def get(self, user_name, genome_name):
        """
        Get serovar prediction results for a genome.

        :param string user_name: Username
        :param string genome_name: Genome name

        :reqheader Authorization: Basic HTTP Auth for registered user info access

        :status 403: unauthorized access
        :status 404: User/genome not found
        """
        user = session.query(User).filter(User.name == user_name).first()
        if user is None:
            logger.error('GET User "{}" not found in DB'.format(user_name))
            return {'error': 'Specified user does not exist'}, 404
        if user != g.user:
            logger.error('GET Auth user {} does not match specified user {}'.format(user, g.user))
            return {'error': 'Authenticated user does not match specified user'}, 403
        genome = session.query(Genome).filter(Genome.user == user, Genome.name == genome_name).first()
        if genome is None:
            return {'error': 'No such genome "{}" for user "{}"'.format(genome_name, user_name)}
        return get_basic_serovar_prediction_info(genome.id)


class GenomeSerovarPredictionsListAPI(Resource):
    @authorized(public_auth_required=False)
    def get(self, user_name):
        """
        Get all serovar prediction results for all user genomes.

        Returns dict of ordered lists.

        **Example output:**

        .. code-block:: json

            {
                'genome': ['g1', 'g2', 'g3', ... ],
                'serovar': ['X', 'Y', 'Z', ... ],
            }

        Where genome `g1` is serovar `X` and genome `g2` is serovar `Y`.

        :param string user_name: Username

        :reqheader Authorization: Basic HTTP Auth for registered user info access

        :status 403: unauthorized access
        :status 404: User/genome not found
        """
        user = session.query(User).filter(User.name == user_name).first()
        if user is None:
            logger.error('GET User "{}" not found in DB'.format(user_name))
            return {'error': 'Specified user does not exist'}, 404
        if user != g.user:
            logger.error('GET Auth user {} does not match specified user {}'.format(user, g.user))
            return {'error': 'Authenticated user does not match specified user'}, 403
        if user.genomes.count() == 0:
            logger.info('User {} has no genomes'.format(user))
            return {}
        genome_query = session.query(Genome.id) \
            .filter(Genome.user_id == user.id, Genome.is_analyzed)
        d = [get_basic_serovar_prediction_info(genome_id) for genome_id, in genome_query.all()]
        return listdict_to_dictlist(d)


class SerovarCurationInfoAPI(Resource):
    @cache.memoize(int(1e7))
    def get(self):
        """
        Serovar designation curation codes.

        See http://journals.plos.org/plosone/article?id=10.1371/journal.pone.0147101#sec012 for more info.

        :>jsonarr boolean is_trusted: Serovar designation trusted?
        :>jsonarr string code: Curation code
        :>jsonarr string description: Description of serovar curation status code.
        """
        curation_infos = session.query(CurationInfo).all()

        d = []
        for curation_info in curation_infos:
            d.append({'is_trusted': curation_info.is_trusted,
                      'code': curation_info.code,
                      'description': curation_info.description})

        return listdict_to_dictlist(d)


class GenomeCurationInfoListAPI(Resource):
    @cache.memoize(int(1e7))
    def get(self):
        """
        Public user genome serovar designation curation information.

        See http://journals.plos.org/plosone/article?id=10.1371/journal.pone.0147101#sec012 for more info.

        :>jsonarr string genome: Genome name
        :>jsonarr string user_uploader: Username
        :>jsonarr boolean is_trusted: Serovar designation trusted?
        :>jsonarr string code: Curation code
        :>jsonarr string reported_serovar: Serovar as reported on NCBI or other public genome repository
        :>jsonarr string curated_serovar: Curated serovar based on predicted antigens and relations to other genomes
        :>jsonarr string predicted_serovar: Serovar as predicted by SISTR analysis
        """
        public_username = current_app.config['PUBLIC_SISTR_USER_NAME']
        user = session.query(User).filter(User.name == public_username).scalar()
        d = []
        for genome in user.genomes.all():
            assert isinstance(genome, Genome)
            curation_info = genome.curation_info
            assert isinstance(curation_info, CurationInfo)

            reported_serovar = 'reported_serovar'
            reported_serovar = genome.misc_metadata[reported_serovar] \
                if genome.misc_metadata is not None \
                   and reported_serovar in genome.misc_metadata \
                else ''
            serovar_prediction = genome.serovar_prediction.first()
            assert isinstance(serovar_prediction, SerovarPrediction)
            d.append({'genome': genome.name,
                      'is_trusted': curation_info.is_trusted,
                      'code': curation_info.code,
                      'reported_serovar': reported_serovar,
                      'curated_serovar': genome.serovar,
                      'predicted_serovar': serovar_prediction.serovar})
        return listdict_to_dictlist(d)

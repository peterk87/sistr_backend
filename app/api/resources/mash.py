from __future__ import absolute_import

from flask import g
from flask_restful import Resource, reqparse, abort

from ... import session, logger
from .. import authorized
from ...models import Genome, MashRefSeq, User


class MashRefSeqAPI(Resource):
    def __init__(self):
        self.reqparse = reqparse.RequestParser()

    @authorized(public_auth_required=False)
    def get(self, user_name, genome_name):
        """
        Get top Mash genomic distance from search against 55k NCBI RefSeq genomes.

        :param string user_name: Username
        :param string genome_name: Genome name

        :>json string genome: Genome name
        :>json string user: Username
        :>json dict mash: Mash results

        :reqheader Authorization: Basic HTTP Auth for registered user info access

        :status 403: unauthorized access
        :status 404: User/genome not found
        """
        user = session.query(User).filter(User.name == user_name).first()
        if user is None:
            logger.error('GET User "{}" not found in DB'.format(user_name))
            return {'error': 'Specified user does not exist'}, 404
        if user != g.user:
            logger.error('GET Auth user {} does not match specified user {}'.format(user, g.user))
            return {'error': 'Authenticated user does not match specified user'}, 403

        genome = user.genomes.filter(Genome.name == genome_name).first()

        if genome is None:
            return abort(404)
        if not genome.is_analyzed:
            return {'error': 'genome {} not analyzed'.format(genome_name)}, 403

        mashrefseq = session.query(MashRefSeq)\
            .filter(MashRefSeq.user_id == user.id,
                    MashRefSeq.genome_id == genome.id)\
            .first()

        if mashrefseq is None:
            return abort(404)

        mash_results = {'distance': float(mashrefseq.distance),
                        'matching': mashrefseq.matching,
                        'bioproject': mashrefseq.bioproject,
                        'taxid': mashrefseq.taxid,
                        'biosample': mashrefseq.biosample,
                        'strain': mashrefseq.strain,
                        'serovar': mashrefseq.serovar,
                        'subspecies': mashrefseq.subspecies,
                        'assembly_accession': mashrefseq.assembly_accession,
                        'plasmid': mashrefseq.plasmid,
                        'match_id': mashrefseq.match_id,
                        'is_salmonella': 'Salmonella' in mashrefseq.match_id,
                        'timestamp': mashrefseq.created_at.isoformat(),
                        'pvalue': float(mashrefseq.pvalue),
                        }

        return {'mash': mash_results,
                'user': user_name,
                'genome': genome_name,
                }
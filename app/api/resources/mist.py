from __future__ import absolute_import

from collections import defaultdict

from flask import current_app, g
from flask_restful import fields, Resource, marshal, marshal_with, abort
from sqlalchemy import or_

from ... import session, cache, logger
from app.db_util import get_mist_marker_results
from ...api import authorized
from ...models import MistTest, User, Genome, MistMarkerResult, MistMetadataResult, MistMarker


class DBEnumTypeItem(fields.Raw):
    def format(self, value):
        return value.value


class MarkerNameItems(fields.Raw):
    def format(self, value):
        return [m.name for m in value.all()]


mist_test_fields = {
    'name': fields.String,
    'type': DBEnumTypeItem,
    'markers': MarkerNameItems
}


class MistTestListAPI(Resource):
    def get(self):
        mist_tests = session.query(MistTest).all()
        map_res = map(lambda t: marshal(t, mist_test_fields), mist_tests)
        return {'tests': map_res}


class MistTestAPI(Resource):
    @cache.memoize(30)
    @marshal_with(mist_test_fields)
    def get(self, mist_test_name):
        """
        Get information about an in silico typing test.

        :query string mist_test_name: Name of MIST in silico typing test

        :>json string name: Name of typing test
        :>json string type: Type of typing test (allelic, probe, PCR)
        :>json array markers: List of markers in typing test

        :status 404: typing test does not exist
        """
        mist_test = session.query(MistTest).filter(MistTest.name == mist_test_name).first()
        if mist_test is None:
            return abort(404)
        return mist_test


class MistTestResultItems(fields.Raw):
    def format(self, value):
        d = {}
        for m in value.all():
            d[m.test.name] = m.attrs

        return d


mist_marker_result_fields = {
    'result': fields.String,
    'is_missing': fields.Boolean,
    'is_contig_truncated': fields.Boolean,
    'has_potential_homopolymer_errors': fields.Boolean,
    'time_added': fields.DateTime(attribute='timestamp')
}


class MistMarkerResultItem(fields.Raw):
    def format(self, value):
        d = defaultdict(defaultdict)
        for m in value.all():
            d[m.test.name][m.marker.name] = marshal(m, mist_marker_result_fields)
        return d


genome_mist_results_fields = {
    'genome': fields.String(attribute='name'),
    'test_results': MistTestResultItems(attribute='mist_metadata_results'),
    'marker_results': MistMarkerResultItem(attribute='mist_marker_results')
}


class GenomeMistResultsAPI(Resource):
    @authorized(public_auth_required=False)
    @marshal_with(genome_mist_results_fields)
    def get(self, user_name, genome_name):
        """
        Get all MIST in silico typing test results for a genome.

        :param string user_name: Username
        :param string genome_name: Genome name

        :>json string genome: Genome name
        :>json dict test_results: Test results such as MLST ST or SGSA predictions
        :>json dict marker_results: Results of each marker within each typing test

        :reqheader Authorization: Basic HTTP Auth for registered user info access

        :status 403: unauthorized access
        :status 404: User/genome not found
        """
        user = session.query(User).filter(User.name == user_name).first()
        if user is None:
            logger.error('GET User "{}" not found in DB'.format(user_name))
            return {'error': 'Specified user does not exist'}, 404
        if user != g.user:
            logger.error('GET Auth user {} does not match specified user {}'.format(user, g.user))
            return {'error': 'Authenticated user does not match specified user'}, 403
        public_username = current_app.config['PUBLIC_SISTR_USER_NAME']
        genome = session.query(Genome).join(User) \
            .filter(or_(User.name == public_username, User.name == user_name)) \
            .filter(Genome.name == genome_name) \
            .first()

        if genome is None:
            return abort(404)

        return genome


class GenomesMistResultsAPI(Resource):
    @authorized(public_auth_required=False)
    def get(self, user_name):
        """
        Get all MIST in silico typing test results for all genomes of a user.

        :param string user_name: Username

        :>jsonarr string genome: Genome name
        :>jsonarr dict test_results: Test results such as MLST ST or SGSA predictions
        :>jsonarr dict marker_results: Results of each marker within each typing test

        :reqheader Authorization: Basic HTTP Auth for registered user info access

        :status 403: unauthorized access
        :status 404: User/genome not found
        """
        user = session.query(User).filter(User.name == user_name).first()
        if user is None:
            logger.error('GET User "{}" not found in DB'.format(user_name))
            return {'error': 'Specified user does not exist'}, 404
        if user != g.user:
            logger.error('GET Auth user {} does not match specified user {}'.format(user, g.user))
            return {'error': 'Authenticated user does not match specified user'}, 403
        public_username = current_app.config['PUBLIC_SISTR_USER_NAME']
        if user_name == public_username:
            return get_mist_marker_results(session, public_username)
        else:
            return get_mist_marker_results(session, user_name)


class GenomeMistRawResultsAPI(Resource):
    @authorized(public_auth_required=False)
    def get(self, user_name, mist_test, genome_name):
        """
        Get the raw MIST results for a typing test of a genome.

        :param string user_name: Username
        :param string mist_test: MIST typing test name
        :param string genome_name: Genome name

        :>json string genome: Genome name
        :>json string user: Username
        :>json string mist_test: MIST typing test name
        :>json dict mist_results: Raw MIST results for `mist_test`

        :reqheader Authorization: Basic HTTP Auth for registered user info access

        :status 403: unauthorized access
        :status 404: User/genome not found
        """
        user = session.query(User).filter(User.name == user_name).first()
        if user is None:
            logger.error('GET User "{}" not found in DB'.format(user_name))
            return {'error': 'Specified user does not exist'}, 404
        if user != g.user:
            logger.error('GET Auth user {} does not match specified user {}'.format(user, g.user))
            return {'error': 'Authenticated user does not match specified user'}, 403

        mist_test_obj = session.query(MistTest).filter(MistTest.name == mist_test).first()
        if mist_test_obj is None:
            return abort(404)

        genome = session.query(Genome).filter(Genome.name == genome_name).first()
        if genome is None:
            return abort(404)

        mist_results = session.query(MistMarkerResult) \
            .filter(MistMarkerResult.user_id == user.id) \
            .filter(MistMarkerResult.test_id == mist_test_obj.id) \
            .filter(MistMarkerResult.genome_id == genome.id) \
            .all()

        res = defaultdict(list)

        for mist_result in mist_results:
            mist_json = mist_result.mist_json
            for k, v in mist_json.iteritems():
                res[k].append(v)

        return {
            'user': user_name,
            'mist_test': mist_test,
            'genome': genome_name,
            'mist_results': res
        }


class UserMistRawResultsListAPI(Resource):
    @authorized(public_auth_required=True)
    def get(self, user_name, mist_test):
        """
        Get the raw MIST results for a typing test for all genomes of a user.

        :param string user_name: Username
        :param string mist_test: MIST typing test name

        :>json array genomes: Genome name
        :>json string user: Username
        :>json string mist_test: MIST typing test name
        :>json dict mist_results: Raw MIST results for `mist_test`

        :reqheader Authorization: Basic HTTP Auth for registered user info access

        :status 403: unauthorized access
        :status 404: User/genome/test not found
        """
        user = session.query(User).filter(User.name == user_name).first()
        if user is None:
            logger.error('GET User "{}" not found in DB'.format(user_name))
            return {'error': 'Specified user does not exist'}, 404
        if user != g.user:
            logger.error('GET Auth user {} does not match specified user {}'.format(user, g.user))
            return {'error': 'Authenticated user does not match specified user'}, 403

        mist_test_obj = session.query(MistTest).filter(MistTest.name == mist_test).first()
        if mist_test_obj is None:
            return abort(404)

        mist_results = session.query(MistMarkerResult) \
            .filter(MistMarkerResult.user_id == user.id) \
            .filter(MistMarkerResult.test_id == mist_test_obj.id).all()

        res = defaultdict(list)
        genome_names = []
        for mist_result in mist_results:
            genome_name = mist_result.genome.name
            genome_names.append(genome_name)
            mist_json = mist_result.mist_json
            for k, v in mist_json.iteritems():
                res[k].append(v)

        return {
            'user': user_name,
            'mist_test': mist_test,
            'genomes': genome_names,
            'mist_results': res
        }


class UserGenomesMistTestMetadataListAPI(Resource):
    @authorized(public_auth_required=False)
    def get(self, user_name):
        """
        Get all MIST test results for all genomes of a user.

        :param string user_name: Username

        :>jsonarr array genome: Genome name
        :>jsonarr string mist_test: MIST typing test name
        :>jsonarr dict test_results: MIST test results for `mist_test` (e.g. MLST ST)

        :reqheader Authorization: Basic HTTP Auth for registered user info access

        :status 403: unauthorized access
        :status 404: User not found
        """
        user = session.query(User).filter(User.name == user_name).first()
        if user is None:
            logger.error('GET User "{}" not found in DB'.format(user_name))
            return {'error': 'Specified user does not exist'}, 404
        if user != g.user:
            logger.error('GET Auth user {} does not match specified user {}'.format(user, g.user))
            return {'error': 'Authenticated user does not match specified user'}, 403
        public_username = current_app.config['PUBLIC_SISTR_USER_NAME']

        genome_mist_test_md = session.query(Genome.name, MistTest.name, MistMetadataResult.attrs) \
            .join(MistMetadataResult) \
            .join(MistTest) \
            .join(User) \
            .filter(or_(User.name == public_username, User.name == user_name)) \
            .all()

        test_md_list = [{'genome': genome, 'mist_test': test, 'test_results': results}
                        for genome, test, results in genome_mist_test_md]

        return test_md_list


class UserGenomesMistMarkerResultsAPI(Resource):
    @authorized(public_auth_required=True)
    def get(self, user_name, test_name):
        """
        Get the MIST marker results for a typing test for all genomes of a user.

        :param string user_name: Username
        :param string mist_test: MIST typing test name

        :>jsonarr array genome: Genome name
        :>jsonarr string marker: MIST marker name
        :>jsonarr string result: MIST marker result

        :reqheader Authorization: Basic HTTP Auth for registered user info access

        :status 403: unauthorized access
        :status 404: User/test not found
        """
        user = session.query(User).filter(User.name == user_name).first()
        if user is None:
            logger.error('GET User "{}" not found in DB'.format(user_name))
            return {'error': 'Specified user does not exist'}, 404
        if user != g.user:
            logger.error('GET Auth user {} does not match specified user {}'.format(user, g.user))
            return {'error': 'Authenticated user does not match specified user'}, 403

        test = session.query(MistTest).filter(MistTest.name == test_name).first()
        if test is None:
            return {'error': 'MIST test {0} not found'.format(test_name)}, 404

        query_stmt = session.query(MistMarkerResult.result, Genome.name, MistMarker.name) \
            .join(MistMarkerResult.genome) \
            .join(MistMarkerResult.test) \
            .join(MistMarkerResult.marker) \
            .join(MistMarkerResult.user) \
            .filter(User.name == user_name) \
            .filter(MistTest.name == test_name)

        marker_results = [{'genome': genome, 'marker': marker, 'result': result}
                          for result, genome, marker in query_stmt.all()]

        return marker_results

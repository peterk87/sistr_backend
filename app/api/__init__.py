# -*- coding: utf-8 -*-
"""
Flask REST API implemented using Flask-RESTful using Flask Blueprint

API routes are specified here. API implementations are specified in app.api.resources.
Basic HTTP authentication is used for now. Auth tokens can be generated from valid username:password or valid tokens.
"""

from __future__ import absolute_import
from typing import Dict, List, Optional, Union, Callable, Any
from functools import wraps
from json import dumps
from io import BytesIO
from gzip import GzipFile

from redis import Redis
from flask import make_response, request, Blueprint, current_app, jsonify, g, Response
from flask_httpauth import HTTPBasicAuth

from .. import session, logger
from ..db_util import get_user
from ..models import User, UserRole
from ..util import DecimalEncoder


auth = HTTPBasicAuth()


def authorized(public_auth_required=False):
    # type: (bool) -> Callable
    def decorator(f):
        @wraps(f)
        def check_auth(*args, **kwargs):
            if 'user_name' in kwargs:
                user = get_authorized_user(kwargs['user_name'], public_auth_required)
                if not user:
                    return unauthorized()
                g.user = user

            return f(*args, **kwargs)

        return check_auth

    return decorator


def get_authorized_user(user_name, public_auth_required=False):
    # type: (str, bool) -> Optional[User]
    user = get_user(session, user_name)

    if not user:
        return None

    if user.role == UserRole.temporary:
        return user

    if user_name == current_app.config['PUBLIC_SISTR_USER_NAME']:
        if not public_auth_required:
            return user

    authorization = request.authorization

    if authorization:
        if not verify_password(authorization.username, authorization.password):
            return None
        g_user = g.user
        if not g_user == user:
            return None
        else:
            return user
    return None


@auth.verify_password
def verify_password(username_or_token, password):
    # type: (str, str) -> bool
    secret_key = current_app.config['SECRET_KEY']
    user = User.verify_auth_token(session, secret_key, username_or_token)
    logger.debug('Verify auth token result {}'.format(user))
    if not user:
        user = session.query(User).filter(User.name == username_or_token).first()
        logger.debug("Verify password for user {}".format(user))
        if not user or not user.verify_password(password):
            logger.debug('Password not verified for user {}'.format(username_or_token))
            return False
    logger.debug('password/token verified for user {} with token/username {}'.format(user, username_or_token))
    g.user = user
    return True


@auth.error_handler
def unauthorized():
    # type: () -> Response
    return make_response(jsonify({'error': 'Unauthorized access'}), 403)


from flask_restful import Api

api_bp = Blueprint('api', __name__, url_prefix='/api')

from .resources.genome import GenomeMetadataAPI, GenomeMetadataListAPI, GenomeAPI, GenomesAssemblyStatsAPI, \
    GenomeMetadataListObjAPI, GenomesLocationsAPI, GenomeSerovarPredictionAPI, GenomeSerovarPredictionsListAPI, \
    SerovarCurationInfoAPI, GenomeCurationInfoListAPI
from .resources.cgmlst import CgMLSTProfilesAPI, CgMLSTDistanceMatrixAPI, CgMLSTAdjacencyList, CgMLSTNewickTree, \
    ClosestRelative, CgMLSTProfilesCSVAPI
from .resources.celery_tasks import CeleryTaskAPI, CeleryQueueMonAPI
from .resources.mist import MistTestListAPI, MistTestAPI, GenomeMistResultsAPI, UserMistRawResultsListAPI, \
    GenomeMistRawResultsAPI, UserGenomesMistTestMetadataListAPI, GenomesMistResultsAPI, UserGenomesMistMarkerResultsAPI
from .resources.user import UserAPI, UserSelectionsAPI, UserTokenAPI, UserPasswordResetAPI, UserCheckAPI
from .resources.mash import MashRefSeqAPI


@api_bp.before_request
def option_autoreply():
    # type: () -> Optional[Response]
    """ Always reply 200 on OPTIONS request """

    if request.method == 'OPTIONS':
        resp = current_app.make_default_options_response()

        headers = None
        if 'ACCESS_CONTROL_REQUEST_HEADERS' in request.headers:
            headers = request.headers['ACCESS_CONTROL_REQUEST_HEADERS']

        h = resp.headers

        # Allow the origin which made the XHR
        h['Access-Control-Allow-Origin'] = request.headers['Origin']
        # Allow the actual method
        h['Access-Control-Allow-Methods'] = request.headers['Access-Control-Request-Method']
        # Allow for 10 seconds
        h['Access-Control-Max-Age'] = "10"

        # We also keep current headers
        if headers is not None:
            h['Access-Control-Allow-Headers'] = headers

        return resp


@api_bp.after_request
def set_allow_origin(resp):
    # type: (Response) -> Response
    """ Set origin for GET, POST, PUT, DELETE requests """

    h = resp.headers

    # Allow crossdomain for other HTTP Verbs
    if request.method != 'OPTIONS' and 'Origin' in request.headers:
        h['Access-Control-Allow-Origin'] = request.headers['Origin']

    return resp


from werkzeug.exceptions import NotAcceptable


class CustomApi(Api):
    FORMAT_MIMETYPE_MAP = {
        "csv": "text/csv",
        "json": "application/json"
        # Add other mimetypes as desired here
    }

    def mediatypes(self):
        """Allow all resources to have their representation
        overriden by the `format` URL argument"""

        preferred_response_type = []
        format = request.args.get("format")
        logger.error('CustomApi format %s', format)
        if format:
            mimetype = self.FORMAT_MIMETYPE_MAP.get(format)
            preferred_response_type.append(mimetype)
            if not mimetype:
                raise NotAcceptable()
        logger.error('CustomApi preferred resp type %s', preferred_response_type)
        return preferred_response_type + super(CustomApi, self).mediatypes()


api = CustomApi(api_bp, catch_all_404s=True)

api.add_resource(MistTestAPI,
                 '/mist_test/<string:mist_test_name>')

api.add_resource(MistTestListAPI,
                 '/mist_tests')

api.add_resource(UserAPI,
                 '/user/<string:user_name>')

api.add_resource(UserTokenAPI,
                 '/token')

api.add_resource(UserSelectionsAPI,
                 '/user/<string:user_name>/selections')

api.add_resource(UserPasswordResetAPI,
                 '/password_reset')

api.add_resource(UserCheckAPI,
                 '/check_user/<string:user_name>')

api.add_resource(GenomeAPI,
                 '/user/<string:user_name>/genome/<string:genome_name>')

api.add_resource(GenomesAssemblyStatsAPI,
                 '/user/<string:user_name>/genomes/assembly_stats')

api.add_resource(GenomeMetadataAPI,
                 '/user/<string:user_name>/genome/<string:genome_name>/metadata')
api.add_resource(GenomeMetadataListAPI,
                 '/user/<string:user_name>/genomes/metadata')

api.add_resource(GenomeMetadataListObjAPI,
                 '/user/<string:user_name>/genomes/metadata_obj_list')

api.add_resource(GenomesLocationsAPI,
                 '/user/<string:user_name>/genomes/locations')

api.add_resource(GenomeMistResultsAPI,
                 '/user/<string:user_name>/genome/<string:genome_name>/mist_results')

api.add_resource(UserGenomesMistTestMetadataListAPI,
                 '/user/<string:user_name>/genomes/mist_test_results')

api.add_resource(UserGenomesMistMarkerResultsAPI,
                 '/user/<string:user_name>/genomes/mist_test/<string:test_name>/marker_results')

api.add_resource(GenomesMistResultsAPI,
                 '/user/<string:user_name>/genomes/mist_marker_results')

api.add_resource(GenomeMistRawResultsAPI,
                 '/user/<string:user_name>/genome/<string:genome_name>/mist_test/<string:mist_test>/raw_mist_results')

api.add_resource(UserMistRawResultsListAPI,
                 '/user/<string:user_name>/genomes/mist_test/<string:mist_test>/raw_mist_results')

api.add_resource(CgMLSTProfilesAPI,
                 '/user/<string:user_name>/genomes/cgmlst')

api.add_resource(CgMLSTProfilesCSVAPI,
                 '/user/<string:user_name>/genomes/cgmlst/csv')

api.add_resource(ClosestRelative,
                 '/user/<string:user_name>/genome/<string:genome_name>/cgmlst/closest_relative')

api.add_resource(CgMLSTDistanceMatrixAPI,
                 '/user/<string:user_name>/genomes/cgmlst/distance_matrix')

api.add_resource(CgMLSTAdjacencyList,
                 '/user/<string:user_name>/genomes/cgmlst/mst')

api.add_resource(CgMLSTNewickTree,
                 '/user/<string:user_name>/genomes/cgmlst/newick')

api.add_resource(CeleryTaskAPI,
                 '/task')

api.add_resource(CeleryQueueMonAPI,
                 '/queue')

api.add_resource(GenomeSerovarPredictionAPI,
                 '/user/<string:user_name>/genome/<string:genome_name>/serovar_prediction')

api.add_resource(GenomeSerovarPredictionsListAPI,
                 '/user/<string:user_name>/genomes/serovar_prediction')

api.add_resource(SerovarCurationInfoAPI,
                 '/curation_info')

api.add_resource(GenomeCurationInfoListAPI,
                 '/user/sistr/genomes/curation_info')

api.add_resource(MashRefSeqAPI,
                 '/user/<string:user_name>/genome/<string:genome_name>/mashrefseq')


def compress(content):
    # type: (str) -> bytes
    """
    GZIP compress a string.

    Args:
        content (str): String to GZIP compress

    Returns:
        bytes: Gzipped string
    """
    gzip_buffer = BytesIO()
    with GzipFile(mode='wb',
                  compresslevel=6,
                  fileobj=gzip_buffer) as gzip_file:
        gzip_file.write(content)
    return gzip_buffer.getvalue()


def compress_key(data):
    # type: (str) -> str
    """
    MD5 hexdigest a string.

    Args:
        data (str): string to MD5 hexdigest

    Returns:
        str: hexdigest of ``data``
    """
    import hashlib
    md5 = hashlib.md5()
    md5.update(data.encode('utf-8'))
    return md5.hexdigest()






@api.representation('application/json')
def output_json(data, code, headers=None):
    # type: (Any, int, Optional[Dict[str, str]]) -> Response
    """
    Extending output_json from Flask-RESTful to add compatibility for JSONP requests and adding GZIP compression.
    These JSONP requests are expected to have a callback request argument and are returned as:
    <callback request value>(<JSON response data>);

    Args:
        data (obj): data to JSONify and send in response as content
        code (int): HTTP code
        headers (dict): Response headers

    Returns:
        flask.Response: Flask HTTP Response object with Gzipped JSON content
    """
    callback = request.args.get('callback', False)
    try:
        data = dumps(data, cls=DecimalEncoder)
    except TypeError:
        from sistr.src.writers import to_dict
        data = dumps(to_dict(data, 0), cls=DecimalEncoder)
    if callback:
        content = str(callback) + '(' + data + ')'
    else:
        content = data

    # gzip compress JSON response data and save gzipped resp to Redis
    key = 'sistr-compress-' + compress_key(content)
    redis = Redis()
    gzip_content = redis.get(key) or compress(content)
    redis.set(key, gzip_content)

    resp = make_response(gzip_content, code)
    resp.headers.extend(headers or {})

    resp.headers['Content-Encoding'] = 'gzip'
    resp.headers['Content-Length'] = resp.content_length

    return resp


@api.representation('text/csv')
def output_csv(data, code, headers=None):
    # type: (Any, int, Optional[Dict[str, str]]) -> Response
    """REST API CSV output

    Args:
        data (string): CSV string
        code (int): HTTP code
        headers (dict): Response headers

    Returns:
        flask.Response: Flask HTTP Response object with Gzipped JSON content
    """
    content = data

    # gzip compress JSON response data and save gzipped resp to Redis
    key = 'sistr-compress-csv-' + compress_key(content)
    redis = Redis()
    gzip_content = redis.get(key) or compress(content)
    redis.set(key, gzip_content)

    resp = make_response(gzip_content, code)
    resp.headers.extend(headers or {})

    resp.headers['Content-Encoding'] = 'gzip'
    resp.headers['Content-Length'] = resp.content_length

    return resp

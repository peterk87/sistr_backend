'''
Celery is used to execute various tasks ranging from writing updated allele FASTA files to
running analysis pipelines for user-uploaded genomes.

This file contains the functions necessary to handle

- user genome uploads (see genome_analysis_task)
- cgMLST cluster assignment
- sending emails (password recovery, internal server exceptions to admins)
- cleaning out abandoned anonymous user accounts (not seen for more than 2 weeks)
- writing novel cgMLST alleles to disk for use by MIST
'''
from __future__ import absolute_import, print_function

import logging
import os
import shutil
from datetime import datetime
from subprocess import PIPE, Popen

import flask
import numpy as np
import pandas as pd
from celery import Celery, Task
from celery.exceptions import Ignore
from flask import Flask
from psycopg2._psycopg import TransactionRollbackError
from redis import Redis
from sqlalchemy.orm import Session
from typing import Dict

from . import session
from .models import Genome, User, MistTest, MistAllele, UserRole, MashRefSeq, SerovarPrediction
from .db_loaders import DefaultDataLoader
from .db_util import add_genome_to_db, get_user, delete_old_serovar_predictions, set_genome_analyzed
from .genome_metadata import get_genome_metadata_dict, get_basic_serovar_prediction_info, get_genome_quality_stats
from .cgmlst import cgmlst_profiles, generate_temp_cgmlst_clusters, profile_dist, public_cgmlst_clusters, \
    generate_cgmlst_clusters
from .mash import mash_against_refseq
from .sistrcmd import run_sistr_cmd, parse_add_sistr_cmd_results, get_serovar_prediction_dict
from .mist import run_mist, parse_mist_results
from .mlst import add_mlst_serovar_prediction
from .util import fasta_format_check, write_temp_genome_fasta, make_tmp_dir, count_dict_to_str

#: cgMLST finalization lock expiration time (default 15 minutes)
LOCK_EXPIRE_CGMLST_CLUSTER_FINALIZE = 60 * 15
#: cgMLST writing lock expiration time (default 1 minute)
LOCK_EXPIRE_WRITE_CGMLST_ALLELES = 60


class CeleryFailure(Exception):
    def __init__(self, desc, exc_type, exc_message):
        super(self.__class__, self).__init__(exc_message)
        self.desc = desc
        self.exc_type = exc_type
        self.exc_message = exc_message

    def encode(self, encoding):
        d = {'progress': 100,
             'desc': self.desc,
             'exc_type': self.exc_type.__name__,
             'exc_message': self.exc_message, }
        import json
        return unicode(json.dumps(d))


def analyse_genome(app, fasta_temp_file, user_name, genome_name, reanalyze):
    # type: (Flask, str, str, str, bool) -> Task
    """
    Analyse a user uploaded genome
    ==============================

    Start a Celery chained task that does the following:

     - check that FASTA file string is valid FASTA format
     - add Genome_ and contigs to DB
     - create a temp analysis folder for the Genome_ and User_
     - write a temp FASTA file to the temp analysis folder
     - run MIST_
     - from MIST_ output, parse all MistMetadataResult_ and MistMarkerResult_ into DB
     - determine cgMLST cluster membership for Genome_
     - determine SerovarPrediction_ using antigen BLAST search and comparison to cgMLST clusters
     - clean up temp analysis folder

    Notes:
        A Celery task chain object is returned so that it can be iterated to show the current
        status of the tasks within the chain and if any subtasks have failed.

    Args:
        fasta_str (str): User uploaded genome FASTA file contents string
        user_name (str): User name
        genome_name (str): Genome name
        reanalyze (bool): Re-analyze genome?

    Returns:
        object: Celery task object
    """
    CELERY.app = app
    task = genome_analysis_task.delay(user_name,
                                      genome_name,
                                      fasta_temp_file,
                                      reanalyze)
    return task


def make_celery(flask_app=None, config_name='prod'):
    # type: (flask.Flask, str) -> Celery
    """Create Celery object with Flask app object and config

    Args:
        flask_app (flask app or None): Flask app object
        config_name (str): Config to use (default=`prod`)

    Returns:
        Celery: initialized Celery object
    """
    from . import create_app

    flask_app = flask_app or create_app(config_name)
    c = Celery(flask_app.import_name)
    c.app = flask_app
    logging.info('celery {} flask app {}'.format(c, c.app))
    c.config_from_object('celeryconf')
    TaskBase = c.Task

    class ContextTask(TaskBase):
        abstract = True

        def __call__(self, *args, **kwargs):
            logging.error('ContextTask {}'.format(flask_app))
            with flask_app.app_context():
                logging.error('ContextTask app context {}'.format(flask_app))
                return TaskBase.__call__(self, *args, **kwargs)

    c.Task = ContextTask
    return c

# TODO: add celery test with test DB session
CELERY = make_celery()


@CELERY.task()
def send_email(recipients, subject, body):
    """Send email async with Flask-Mail

    Args:
        recipients (list of str): List of recipient email addresses
        subject (str): Subject heading
        body (str): Body text content
    """
    from flask_mail import Message
    from . import mail
    logging.warning('Sending email to {} with subject {}'.format(recipients, subject))
    with CELERY.app.app_context():
        with mail.connect() as conn:
            msg = Message(subject=subject,
                          body=body,
                          recipients=recipients)
            conn.send(msg)


@CELERY.task()
def cache_cgmlst_clusters():
    pass


@CELERY.task()
def write_cgmlst_alleles():
    """Write updated cgMLST alleles

    Write updated cgMLST alleles to ``mist_assays/alleles``.
    First write the alleles to a temporary directory then ``shutil.copy`` the FASTA files
    to the ``mist_assays/alleles`` directory with Redis lock (if lock key exists, don't write; otherwise,
    safe to write new alleles).

    Todo:
        Check if it's even necessary to write the new allele FASTA files based on timestamp.
    """
    allele_dir = 'mist_assays/alleles-tmp'
    try:
        os.mkdir(allele_dir)
    except:
        if not os.path.exists(allele_dir):
            raise IOError('Temp cgMLST alleles dir could not be created at {}'.format(allele_dir))

    t = session \
        .query(MistTest) \
        .filter(MistTest.name == 'wgMLST_330') \
        .first()

    fasta_paths = []
    for m in t.markers.all():
        fasta_path = os.path.join(allele_dir, m.name + '.fasta')
        fasta_paths.append(fasta_path)
        with open(fasta_path, 'w') as fout:
            for a in m.alleles.order_by(MistAllele.timestamp).all():
                fout.write('>{}\n{}\n'.format(a.name, a.seq))
    lock_id = 'lock-write_mist_alleles'
    r = Redis()
    try:
        if r.exists(lock_id):
            logging.error('Lock already acquired for write_mist_alleles')
            return
        r.set(lock_id, 1, ex=LOCK_EXPIRE_WRITE_CGMLST_ALLELES)
        # get Redis lock for writing new cgMLST alleles FASTA
        for fasta_path in fasta_paths:
            shutil.copy(fasta_path, 'mist_assays/alleles/')
        shutil.rmtree(allele_dir)
    except Exception as ex:
        logging.error('Could not write cgMLST alleles. Retrying... Exception: {}'.format(ex))
        write_cgmlst_alleles()
    finally:
        r.delete(lock_id)


@CELERY.task()
def delete_old_tmp_users():
    """
    Delete temporary User_ accounts and associated Genome_ and analysis data for temporary
    User_ accounts that are over 2 weeks old.

    Todo:
        Write to logger.info rather than stdout
    """
    dt_2weeks_ago = datetime.fromordinal(datetime.now().toordinal() - 14)
    users = session.query(User).filter(User.role == UserRole.temporary,
                                       User.last_seen <= dt_2weeks_ago).all()
    logging.warning('Deleting temporary {} Users last seen on or before {}'.format(
        len(users),
        dt_2weeks_ago))
    for user in users:
        logging.warning('Deleting {} Genomes from User {}'.format(user.genomes.count(), user))
        for g in user.genomes.all():
            logging.warning('Deleting Genome {}'.format(g))
            try:
                session.delete(g)
                session.commit()
            except TransactionRollbackError as ex:
                logging.error(
                    'delete_old_tmp_users:: TransactionRollbackError while deleting genome {} {}'.format(g, ex))
                session.rollback()
        try:
            session.delete(user)
            session.commit()
        except TransactionRollbackError as ex:
            logging.error('delete_old_tmp_users:: TransactionRollbackError while deleting user {} {}'.format(user, ex))
            session.rollback()


@CELERY.task()
def finalize_cgmlst_clusters():
    lock_id = 'lock-finalize_cgmlst_clusters'
    r = Redis()
    logging.debug('finalize_cgmlst_clusters using Redis object {}'.format(r))
    if not r.exists(lock_id):
        try:
            x = r.set(lock_id, 1, LOCK_EXPIRE_CGMLST_CLUSTER_FINALIZE)
            logging.debug('finalize_cgmlst_clusters Set lock {}'.format(x))
            # query for all genomes for which cgMLST cluster assignments have not been finalized
            # and perform

            query_unfinalized_genomes = session.query(Genome) \
                .filter(Genome.is_cgmlst_clusters_final == False,
                        Genome.is_analyzed) \
                .order_by(Genome.user_id, Genome.id)
            n_unfinalized = query_unfinalized_genomes.count()
            if n_unfinalized == 0:
                x = r.delete(lock_id)
                return
            else:
                logging.warning('Finalizing cgMLST clusters for {} genomes'.format(n_unfinalized))

            d = {name: clusters for name, clusters in
                 session.query(Genome.name, Genome.cgmlst_clusters)
                     .filter(Genome.is_analyzed,
                             Genome.is_cgmlst_clusters_final)
                     .all()}
            df_clusters = pd.DataFrame(d).transpose()
            assert isinstance(df_clusters, pd.DataFrame)
            for genome in query_unfinalized_genomes.all():
                #: refresh lock expiration for each genome. cgMLST cluster assignment should not take very long...
                x = r.set(lock_id, 1, LOCK_EXPIRE_CGMLST_CLUSTER_FINALIZE)
                logging.debug('finalize_cgmlst_clusters Refresh lock {}'.format(x))
                logging.info('Finalizing cgMLST clusters for {}'.format(genome.name))
                logging.info('Using cgMLST clusters DataFrame with shape {}'.format(df_clusters.shape))
                with CELERY.app.app_context():
                    finalized_genome_profiles = cgmlst_profiles(list(df_clusters.index))
                    genome_profile_array = np.array(cgmlst_profiles([genome.name])['results'], dtype=np.float64)
                closest_match, _ = profile_dist(genome.name, genome_profile_array, finalized_genome_profiles)
                closest_distance = closest_match['distance']
                closest_genome_name = closest_match['genome']
                logging.info('Closest genome to {} by cgMLST is {}'.format(genome.name, closest_match))
                genome.cgmlst_clusters = generate_cgmlst_clusters(df_cluster=df_clusters,
                                                                  genome_name=closest_genome_name,
                                                                  distance=closest_distance)
                genome.is_cgmlst_clusters_final = True
                logging.info('Final cgMLST clusters set for {}'.format(genome.name))
                session.add(genome)
                session.commit()

                from app import cache
                with CELERY.app.app_context():
                    #: Invalidate cached metadata, serovar prediction results and quality stats info for Genome
                    cache.delete_memoized(get_genome_metadata_dict, genome.id)
                    cache.delete_memoized(get_basic_serovar_prediction_info, genome.id)
                    cache.delete_memoized(get_genome_quality_stats, genome.id)

                logging.info('Final cgMLST clusters added to DB for {}'.format(genome.name))

                df_novel = pd.DataFrame({genome.name: genome.cgmlst_clusters}).transpose()
                df_clusters = pd.concat([df_clusters, df_novel])

        finally:
            x = r.delete(lock_id)
            logging.debug('finalize_cgmlst_clusters Releasing lock... {}'.format(x))
        return
    logging.warning('cgMLST cluster finalization locked!')


@CELERY.task(bind=True)
def genome_analysis_task(self, user_name, genome_name, fasta_temp_file,
                         reanalyze):
    # type: (Celery, str, str, str, bool) -> bool
    """Main SISTR genome analysis task

    Note:
        Exceptions are caught, logged and can be retrieved by users using the task queue API to see
        what went wrong (e.g. incorrect format, some obscure error).

    Args:
        self (Celery): from bind=True
        app (Flask): Flask app object
        session (Session): DB session
        user_name (str): User name
        genome_name (str): User Genome name
        fasta_temp_file (str): FASTA file temporary path
        reanalyze (bool): Reanalyze a previously analyzed genome?

    Returns:
        bool: True if task successfully completes
    """
    app = CELERY.app
    print('app', app)
    temp_dir = app.config['TEMP_DIR']
    print('temp_dir', temp_dir)
    user_temp_dir = os.path.join(temp_dir, 'SISTR', '{0}-{1}'.format(user_name, genome_name))
    print('user_temp_dir', user_temp_dir)

    mist_bin = app.config['MIST_BIN_PATH']
    print('mist_bin', mist_bin)
    mist_alleles_dir = app.config['MIST_ALLELES_DIR']
    mist_markers_files = app.config['MIST_MARKERS_FILES']

    mash_bin = app.config['MASH_BIN_PATH']
    print('mash_bin', mash_bin)

    public_username = app.config['PUBLIC_SISTR_USER_NAME']
    print('public_username', public_username)
    with open(fasta_temp_file) as f:
        fasta_str = f.read()
    os.remove(fasta_temp_file)

    time_start = datetime.now()
    user = get_user(session, user_name)
    print('user', user)
    public_user = get_user(session, public_username)
    print('public_user', public_user)
    mash_results = None
    sistrcmd_results = None
    meta_task_info = {'progress': 0,
                      'desc': None}
    try:
        meta_task_info['desc'] = 'FASTA format check'
        self.update_state(state='PROGRESS', meta=meta_task_info)
        valid_fasta, fasta_format_check_err_msg = fasta_format_check(fasta_str)
        if not valid_fasta:
            raise Exception(fasta_format_check_err_msg)

        meta_task_info['progress'] = 4
        meta_task_info['desc'] = 'Adding genome to database'
        self.update_state(state='PROGRESS', meta=meta_task_info)
        genome = add_genome_to_db(session, user, genome_name, fasta_str, reanalyze)
        print('genome', genome)
        meta_task_info['progress'] = 8
        meta_task_info['desc'] = 'Create temp genome analysis directory'
        self.update_state(state='PROGRESS', meta=meta_task_info)
        user_temp_dir = make_tmp_dir(user_temp_dir)
        print('user_temp_dir', user_temp_dir)
        meta_task_info['progress'] = 12
        meta_task_info['desc'] = 'Write temp genome FASTA'
        self.update_state(state='PROGRESS', meta=meta_task_info)
        fasta_path = write_temp_genome_fasta(user_temp_dir, genome_name, fasta_str)
        print('os.path.exists(fasta_path)', os.path.exists(fasta_path))
        meta_task_info['progress'] = 16
        meta_task_info['desc'] = 'Mash search against RefSeq'
        self.update_state(state='PROGRESS', meta=meta_task_info)
        mashrefseq = mash_against_refseq(session=session,
                                         mash_bin=mash_bin,
                                         fasta_filepath=fasta_path,
                                         user=user,
                                         genome=genome)
        assert isinstance(mashrefseq, MashRefSeq)
        mash_results = {'distance': float(mashrefseq.distance),
                        'matching': mashrefseq.matching,
                        'bioproject': mashrefseq.bioproject,
                        'taxid': mashrefseq.taxid,
                        'biosample': mashrefseq.biosample,
                        'strain': mashrefseq.strain,
                        'serovar': mashrefseq.serovar,
                        'subspecies': mashrefseq.subspecies,
                        'assembly_accession': mashrefseq.assembly_accession,
                        'plasmid': mashrefseq.plasmid,
                        'match_id': mashrefseq.match_id,
                        'is_salmonella': 'Salmonella' in mashrefseq.match_id,
                        }
        meta_task_info['mash'] = mash_results
        print('User {} | Genome {} | Mash results {}'.format(user.name, genome.name, mash_results))
        meta_task_info['progress'] = 25
        from sistr.version import __version__
        msg = 'Running sistr-cmd v' + __version__
        print(msg)
        meta_task_info['desc'] = msg
        self.update_state(state='PROGRESS', meta=meta_task_info)
        sistrcmd_prediction, sistrcmd_cgmlst = run_sistr_cmd(input_fasta=fasta_path,
                                                             genome_name=genome.name,
                                                             tmp_dir=user_temp_dir)
        sistrcmd_results = get_serovar_prediction_dict(user, genome, sistrcmd_prediction, sistrcmd_cgmlst)

        meta_task_info['sistrcmd'] = sistrcmd_results
        logging.warning('User {} | Genome {} | sistr-cmd results {}'.format(user.name, genome.name, sistrcmd_results))

        meta_task_info['progress'] = 50
        meta_task_info['desc'] = 'Saving sistr-cmd v{} results to database'.format(__version__)
        self.update_state(state='PROGRESS', meta=meta_task_info)
        parse_add_sistr_cmd_results(session=session,
                                    user_name=user.name,
                                    genome_name=genome.name,
                                    sistr_cmd_pred=sistrcmd_prediction,
                                    allele_results=sistrcmd_cgmlst)

        meta_task_info['progress'] = 66
        meta_task_info['desc'] = 'Run MIST in silico typing assays'
        self.update_state(state='PROGRESS', meta=meta_task_info)
        mist_out_path = run_mist(genome_name=genome_name,
                                 user_name=user_name,
                                 temp_output_dir=user_temp_dir,
                                 mist_bin=mist_bin,
                                 mist_alleles_dir=mist_alleles_dir,
                                 mist_markers_files=mist_markers_files)

        meta_task_info['progress'] = 75
        meta_task_info['desc'] = 'Parse MIST in silico typing results'
        self.update_state(state='PROGRESS', meta=meta_task_info)
        logging.warning('Parsing MIST results from %s for genome %s', mist_out_path, genome)
        parse_mist_results(session, mist_out_path, user_name, genome_name)
        logging.warning('Parsed MIST results for genome %s', genome)

        meta_task_info['progress'] = 80
        meta_task_info['desc'] = 'MLST serovar prediction'
        self.update_state(state='PROGRESS', meta=meta_task_info)
        serovar_prediction = session.query(SerovarPrediction) \
            .filter(SerovarPrediction.user == user,
                    SerovarPrediction.genome == genome) \
            .first()
        mlst_serovar_tuple = add_mlst_serovar_prediction(session, user, genome, serovar_prediction)
        if mlst_serovar_tuple:
            mlst_st, mlst_serovar, mlst_serovar_counts = mlst_serovar_tuple
            mlst_results = {'MLST_ST_in_silico': mlst_st,
                            'MLST_serovar_prediction': mlst_serovar,
                            'MLST_serovar_count_predictions': count_dict_to_str(mlst_serovar_counts), }
            sistrcmd_results_with_mlst = sistrcmd_results.copy()
            sistrcmd_results_with_mlst.update(mlst_results)
            meta_task_info['sistrcmd'] = sistrcmd_results_with_mlst




        # meta_task_info['progress'] = 85
        # meta_task_info['desc'] = 'Assign cgMLST clusters'
        # self.update_state(state='PROGRESS', meta=meta_task_info)
        # assign_cgmlst_clusters(app=CELERY.app,
        #                        user=user,
        #                        genome=genome,
        #                        public_user=public_user)
        # logging.warning('cgMLST clusters assigned to {}. Finalized? {}'.format(
        #     genome.name,
        #     genome.is_cgmlst_clusters_final
        # ))
        meta_task_info['progress'] = 95
        meta_task_info['desc'] = 'Cleanup temp analysis files'
        self.update_state(state='PROGRESS', meta=meta_task_info)
        if reanalyze:
            delete_old_serovar_predictions(session, user_name, genome_name)
        set_genome_analyzed(session, user_name, genome_name)
    except Exception as ex:
        template = "Genome '{genome}' of user '{user}' analysis exception {exc}"
        message = template.format(genome=genome_name,
                                  user=user_name,
                                  exc=ex)
        logging.error(message)
        logging.error(ex, exc_info=True)
        self.update_state(state='FAILURE',
                          meta=CeleryFailure(ex.message, type(ex), ex.message))
        # TODO: delete genomes that fail analysis?
        # user = get_user(session, user_name)
        # genome = get_genome(session, user.id, genome_name)
        # if genome:
        #     session.delete(genome)
        #     session.commit()
        raise Ignore()
    finally:
        try:
            logging.warning('Cleaning up temporary analysis directory {}'.format(user_temp_dir))
            shutil.rmtree(user_temp_dir)
        except Exception as ex:
            logging.error('Could not cleanup temp user directory "{}"!'.format(user_temp_dir))

    elapsed = datetime.now() - time_start

    try:
        r = Redis()
        key_name = 'genome_analysis_task_timings'
        r.rpush(key_name, elapsed.total_seconds())
    except:
        pass

    return True


def run_quast(quast_bin_path, fasta_filepath):
    # type: (str, str) -> str
    """Run QUAST to generate assembly quality stats a genome

    Args:
        quast_bin_path (str): QUAST binary path
        fasta_filepath (str): Genome FASTA file path

    Returns:
        str: File path of QUAST_ ``transposed_report.tsv``
    """
    temp_output_dir = os.path.dirname(fasta_filepath)
    quast_output_dir = os.path.join(temp_output_dir, 'quast')
    p = Popen([quast_bin_path,
               '--no-plot',  # disable plotting since it's unneeded
               '-f',  # gene finding
               '-o', os.path.abspath(quast_output_dir),
               os.path.abspath(fasta_filepath)],
              stdout=PIPE,
              stderr=PIPE)
    stdout, stderr = p.communicate()

    quast_report_path = os.path.join(quast_output_dir, 'transposed_report.tsv')

    if os.path.exists(quast_report_path):
        return quast_report_path
    else:
        logging.error('QUAST, what did you do? stderr=%s | stdout=%s', stderr, stdout)
        raise IOError('QUAST did not produce the expected transposed_report.tsv file')


def parse_quast_results(quast_output_path, user, genome):
    """Parse QUAST_ output

    Parse the QUAST_ assembly stats for a user uploaded Genome_ into the DB.

    Args:
        quast_output_path (str): QUAST output file path
        user_name (str): User name
        genome_name (str): Genome name
    """
    ddl = DefaultDataLoader(session)
    ddl.load_assembly_stats(quast_output_path)

    assert genome.quality_stats is not None, 'Genome {0} of user {1} does not have QUAST generated quality stats'.format(
        genome.name,
        user.name
    )


def assign_cgmlst_clusters(session, app, user, genome, public_user):
    # type: (Session, Flask, User, Genome, User) -> None
    """Assign cgMLST clusters from public SISTR genomes

    Find the closest related fully analysed public genome to the user uploaded genome
    and assign clusters to the user genome based off the public genome up to the level
    of distance that is shared between the user uploaded genome and the public genome.
    If needed, at a later time, using a scheduled periodic task, assign the finalized
    cluster numbers.

    Args:
        session (Session): DB session
        app (Flask): Flask app object
        user (User): User_ object
        genome (Genome): Genome_ object
        public_user (User): Public SISTR User object
    """
    assert isinstance(genome, Genome)
    assert isinstance(user, User)

    df_clusters = public_cgmlst_clusters(public_user.id)
    logging.warning('df_clusters for public cgMLST clusters {}'.format(df_clusters.shape))
    public_genomes = [name for name, in session.query(Genome.name) \
        .filter(Genome.user_id == public_user.id,
                Genome.is_analyzed,
                Genome.is_cgmlst_clusters_final) \
        .order_by(Genome.id) \
        .all()]
    logging.warning('Public genomes length={}'.format(len(public_genomes)))
    genome_name = genome.name
    #: Public genome cgMLST profiles may be cached so they can be retrieved and checked very quickly
    with app.app_context():
        #: Note: need to use following functions with app.app_context() since Flask-Cache can only be accessed
        #: within app_context(), cgmlst_profiles needs
        public_profiles_dict = cgmlst_profiles(public_genomes)
        logging.warning('Public profiles results len={}'.format(len(public_profiles_dict['results'])))
        genome_profile_array = cgmlst_profiles([genome_name])['results']
        logging.debug('Genome {} profile {}'.format(genome_name, genome_profile_array))
    closest_genome, other_related_genomes = profile_dist(genome_name=genome_name,
                                                         genome_profile=genome_profile_array,
                                                         other_genome_profiles=public_profiles_dict)
    logging.warning('Closest genome to {} is {}'.format(genome_name, closest_genome))
    public_distance = closest_genome['distance']
    public_genome_name = closest_genome['genome']

    if public_distance == 0.0:
        logging.warning(
            'User.id {} | Genome {} | Perfect cgMLST profile match found; using cgMLST clusters of public genome {}'.format(
                genome.user_id,
                genome.name,
                public_genome_name,
            ))
        public_genome = session.query(Genome).filter(Genome.name == public_genome_name).first()
        assert isinstance(public_genome, Genome)
        genome.cgmlst_clusters = public_genome.cgmlst_clusters
        genome.is_cgmlst_clusters_final = True
    else:
        logging.warning(
            'User.id {} | Genome {} | No perfect cgMLST profile match found; final cgMLST clusters will be assigned later'.format(
                genome.user_id,
                genome.name,
            ))
        #: If temporary cgMLST clusters are assigned then they will be finalized (fully assigned)
        #: at a later date via the periodic sheduled task
        genome.cgmlst_clusters = generate_temp_cgmlst_clusters(df_clusters, public_genome_name, public_distance)
        genome.is_cgmlst_clusters_final = False
    session.add(genome)
    session.commit()

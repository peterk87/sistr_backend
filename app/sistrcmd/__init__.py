from __future__ import absolute_import

from typing import Dict, List, Optional, Any, Union
import logging
from sqlalchemy.orm import Session
import numpy as np

import sistr.src.serovar_prediction as sistr_cmd
from sistr.sistr_cmd import sistr_predict

from app.db_util import get_user, get_genome
from ..models import User, \
    Genome, \
    SerogroupPrediction, \
    SerovarPrediction, \
    H1FliCPrediction, \
    H2FljBPrediction, \
    CgMLSTProfile, \
    CgMLSTPrediction, \
    CgMLSTAlleleMatch, \
    WzxPrediction, \
    WzyPrediction, \
    MashSalmonella, \
    SistrCmdQC


def version():
    # type: () -> str
    """sistr-cmd version"""
    from sistr.version import __version__
    return __version__


class SistrCmdMockArgs:
    """Mock arguments for running sistr-cmd through Python rather than commandline"""
    run_mash = True
    no_cgmlst = False
    qc = True
    use_full_cgmlst_db = False


SISTR_CMD_ARGS = SistrCmdMockArgs()


def run_sistr_cmd(input_fasta, genome_name, tmp_dir):
    # type: (str, str, str) -> (sistr_cmd.SerovarPrediction, Dict[str, Union[str, int, float, Dict[str, Union[str, int, float]]]])
    sistr_cmd_pred, allele_results = sistr_predict(input_fasta=input_fasta,
                                                   genome_name=genome_name,
                                                   tmp_dir=tmp_dir,
                                                   keep_tmp=False,
                                                   args=SISTR_CMD_ARGS)
    return sistr_cmd_pred, allele_results


def parse_add_sistr_cmd_results(session, user_name, genome_name, sistr_cmd_pred, allele_results):
    # type: (Session, str, str, sistr_cmd.SerovarPrediction, Dict[str, Union[str, int, float, Dict[str, Union[str, int, float]]]]) -> None
    assert isinstance(sistr_cmd_pred, sistr_cmd.SerovarPrediction)
    assert isinstance(allele_results, dict)
    user = get_user(session, user_name)
    genome = get_genome(session, user.id, genome_name)

    parse_add_mash_prediction(session, user, genome, sistr_cmd_pred)
    parse_add_cgmlst_alleles(session, user, genome, allele_results)

    serovar_pred = parse_serovar_prediction(user, genome, sistr_cmd_pred)
    serovar_pred.sistrcmd_version = version()
    cgmlst_profile = find_cgmlst_profile(session, sistr_cmd_pred, allele_results)
    if cgmlst_profile is None:
        cgmlst_profile = parse_cgmlst_profile(sistr_cmd_pred, allele_results)
    genome.cgmlst_profile = cgmlst_profile
    cgmlst_pred = parse_cgmlst_prediction(user, genome, serovar_pred, cgmlst_profile, sistr_cmd_pred)
    qc = parse_qc_info(user, genome, sistr_cmd_pred)
    try:
        session.add(qc)
        session.add(genome)
        session.add(serovar_pred)
        session.add(cgmlst_pred)
        session.commit()
    except Exception as ex:
        logging.error(ex)
        logging.error('Could not add sistr-cmd serovar prediction to DB for user {}, genome {}'.format(
            user.name,
            genome.name,
        ))
        session.rollback()


def parse_qc_info(user, genome, serovar_pred):
    # type: (User, Genome, sistr_cmd.SerovarPrediction) -> SistrCmdQC
    qc = SistrCmdQC()
    qc.user = user
    qc.genome = genome
    qc.status = serovar_pred.qc_status
    qc.message = serovar_pred.qc_messages
    return qc


def parse_cgmlst_prediction(user, genome, serovar_pred, cgmlst_profile, sistr_cmd_pred):
    # type: (User, Genome, SerovarPrediction, CgMLSTProfile, sistr_cmd.SerovarPrediction) -> CgMLSTPrediction
    cgmlst_pred = CgMLSTPrediction()
    cgmlst_pred.user = user
    cgmlst_pred.genome = genome
    cgmlst_pred.serovar_prediction = serovar_pred
    cgmlst_pred.profile = cgmlst_profile
    cgmlst_pred.serovar = sistr_cmd_pred.serovar_cgmlst
    cgmlst_pred.allele_matches = sistr_cmd_pred.cgmlst_matching_alleles
    cgmlst_pred.genome_match = sistr_cmd_pred.cgmlst_genome_match
    cgmlst_pred.subspecies = sistr_cmd_pred.cgmlst_subspecies
    cgmlst_pred.distance = sistr_cmd_pred.cgmlst_distance
    return cgmlst_pred


def find_cgmlst_profile(session, sistr_cmd_pred, allele_results):
    # type: (Session, sistr_cmd.SerovarPrediction, Dict[str, Union[str, int, float, Dict[str, Union[str, int, float]]]]) -> (Optional[CgMLSTProfile])
    if sistr_cmd_pred.cgmlst_ST:
        return session.query(CgMLSTProfile).filter(CgMLSTProfile.sequence_type == str(sistr_cmd_pred.cgmlst_ST)).first()
    arr = get_profile_array(marker_to_allele(allele_results))
    return session.query(CgMLSTProfile).filter(CgMLSTProfile.numpy_array == arr.tostring()).first()


def parse_cgmlst_profile(sistr_cmd_pred, allele_results):
    # type: (sistr_cmd.SerovarPrediction, Dict[str, Union[str, int, float, Dict[str, Union[str, int, float]]]]) -> (Optional[CgMLSTProfile])
    cgmlst_profile = CgMLSTProfile()
    n_missing_alleles = len([1 for x in allele_results.values() if x['blast_result'] is None])
    n_complete_alleles = len([1 for x in allele_results.values() if x['name'] is not None])
    n_partial_alleles = len(allele_results) - n_complete_alleles - n_missing_alleles
    cgmlst_profile.missing_alleles = n_missing_alleles
    cgmlst_profile.partial_alleles = n_partial_alleles
    cgmlst_profile.sequence_type = str(sistr_cmd_pred.cgmlst_ST) if sistr_cmd_pred.cgmlst_ST else None
    marker_alleles = marker_to_allele(allele_results)
    cgmlst_profile.profile = marker_alleles
    arr = get_profile_array(marker_alleles)
    cgmlst_profile.numpy_array = arr.tostring()
    return cgmlst_profile


def marker_to_allele(allele_results):
    # type: (Dict[str, Any]) -> (Dict[str, int])
    return {k: v['name'] for k, v in allele_results.items()}


def get_profile_array(marker_alleles):
    # type: (Dict[str, Any], Dict[str, int]) -> np.array
    markers = marker_alleles.keys()
    markers.sort()
    alleles = [marker_alleles[x] for x in markers]
    arr = np.array(alleles, dtype=np.float64)
    return arr


def parse_serovar_prediction(user, genome, sistr_cmd_pred):
    # type: (User, Genome, sistr_cmd.SerovarPrediction) -> SerovarPrediction
    serovar_pred = SerovarPrediction()
    serovar_pred.user = user
    serovar_pred.genome = genome
    serovar_pred.serovar = sistr_cmd_pred.serovar
    serovar_pred.serovar_antigen = sistr_cmd_pred.serovar_antigen
    serovar_pred.serogroup = sistr_cmd_pred.serogroup
    serovar_pred.h1 = sistr_cmd_pred.h1
    serovar_pred.h2 = sistr_cmd_pred.h2
    serovar_pred.cgmlst_cluster_level = sistr_cmd_pred.cgmlst_distance
    serovar_pred.serovar_cgmlst = sistr_cmd_pred.serovar_cgmlst
    h1 = parse_h1_prediction(user, genome, sistr_cmd_pred)
    serovar_pred.h1_flic_prediction = h1
    h2 = parse_h2_prediction(user, genome, sistr_cmd_pred)
    serovar_pred.h2_fljb_prediction = h2
    sg_cmd = sistr_cmd_pred.serogroup_prediction
    assert isinstance(sg_cmd, sistr_cmd.SerogroupPrediction)
    wzx = parse_wzx_prediction(user, genome, sg_cmd)
    wzy = parse_wzy_prediction(user, genome, sg_cmd)
    sg = parse_serogroup_prediction(user, genome, sg_cmd, wzx, wzy)
    serovar_pred.serogroup_prediction = sg
    return serovar_pred


def parse_add_mash_prediction(session, user, genome, sistr_cmd_pred):
    # type: (Session, User, Genome, sistr_cmd.SerovarPrediction) -> None
    mash = MashSalmonella()
    mash.user = user
    mash.genome = genome
    mash.sketches = sistr_cmd_pred.mash_match
    mash.distance = sistr_cmd_pred.mash_distance
    mash.genome_match = sistr_cmd_pred.mash_genome
    mash.serovar = sistr_cmd_pred.mash_serovar
    mash.subspecies = sistr_cmd_pred.mash_subspecies
    mash.top5 = sistr_cmd_pred.mash_top_5
    try:
        session.add(mash)
        session.commit()
    except Exception as ex:
        logging.error('Error adding Mash sistr-cmd prediction to database for user {}, genome {}'.format(
            user,
            genome,
        ))
        logging.error(ex)
        session.rollback()


def parse_serogroup_prediction(user, genome, sg_cmd, wzx, wzy):
    # type: (User, Genome, sistr_cmd.SerogroupPrediction, WzxPrediction, WzyPrediction) -> SerogroupPrediction
    sg = SerogroupPrediction()
    sg.user = user
    sg.genome = genome
    sg.wzx_prediction = wzx
    sg.wzy_prediction = wzy
    sg.serogroup = sg_cmd.serogroup
    return sg


def parse_wzy_prediction(user, genome, sg_cmd):
    # type: (User, Genome, sistr_cmd.SerogroupPrediction) -> WzyPrediction
    wzy_cmd = sg_cmd.wzy_prediction
    assert isinstance(wzy_cmd, sistr_cmd.WzyPrediction)
    wzy = WzyPrediction()
    wzy.user = user
    wzy.genome = genome
    wzy.serogroup = wzy_cmd.serogroup
    wzy.top_result = wzy_cmd.top_result
    # wzy.blast_results = wzy_cmd.blast_results
    wzy.is_perfect_match = wzy_cmd.is_perfect_match
    wzy.is_trunc = wzy_cmd.is_trunc
    wzy.is_missing = wzy_cmd.is_missing
    return wzy


def parse_wzx_prediction(user, genome, sg_cmd):
    # type: (User, Genome, sistr_cmd.SerogroupPrediction) -> WzxPrediction
    wzx_cmd = sg_cmd.wzx_prediction
    assert isinstance(wzx_cmd, sistr_cmd.WzxPrediction)
    wzx = WzxPrediction()
    wzx.user = user
    wzx.genome = genome
    wzx.serogroup = wzx_cmd.serogroup
    wzx.top_result = wzx_cmd.top_result
    # wzx.blast_results = wzx_cmd.blast_results
    wzx.is_perfect_match = wzx_cmd.is_perfect_match
    wzx.is_trunc = wzx_cmd.is_trunc
    wzx.is_missing = wzx_cmd.is_missing
    return wzx


def parse_h2_prediction(user, genome, sistr_cmd_pred):
    # type: (User, Genome, sistr_cmd.SerovarPrediction) -> H2FljBPrediction
    h2_cmd = sistr_cmd_pred.h2_fljb_prediction
    assert isinstance(h2_cmd, sistr_cmd.H2FljBPrediction)
    h2 = H2FljBPrediction()
    h2.user = user
    h2.genome = genome
    h2.h2 = h2_cmd.h2
    h2.top_result = h2_cmd.top_result
    # h2.blast_results = h2_cmd.blast_results
    h2.is_perfect_match = h2_cmd.is_perfect_match
    h2.is_trunc = h2_cmd.is_trunc
    h2.is_missing = h2_cmd.is_missing
    return h2


def parse_h1_prediction(user, genome, sistr_cmd_pred):
    # type: (User, Genome, sistr_cmd.SerovarPrediction) -> H1FliCPrediction
    h1_cmd = sistr_cmd_pred.h1_flic_prediction
    assert isinstance(h1_cmd, sistr_cmd.H1FliCPrediction)
    h1 = H1FliCPrediction()
    h1.user = user
    h1.genome = genome
    h1.h1 = h1_cmd.h1
    h1.top_result = h1_cmd.top_result
    # df = pd.DataFrame(h1_cmd.blast_results)
    # h1.blast_results = df.transpose().to_dict()
    h1.is_perfect_match = h1_cmd.is_perfect_match
    h1.is_trunc = h1_cmd.is_trunc
    h1.is_missing = h1_cmd.is_missing
    return h1


def parse_add_cgmlst_alleles(session, user, genome, allele_results):
    # type: (Session, User, Genome, Dict[str, Union[str, int, float, Dict[str, Union[str, int, float]]]]) -> None
    cgmlst_allele_matches = []
    for marker, result in allele_results.items():
        cgmlst_allele_match = CgMLSTAlleleMatch()
        cgmlst_allele_match.user = user
        cgmlst_allele_match.genome = genome
        cgmlst_allele_match.seq = result['seq']
        cgmlst_allele_match.name = result['name']
        cgmlst_allele_match.locus = marker
        cgmlst_allele_match.top_result = result['blast_result']
        cgmlst_allele_matches.append(cgmlst_allele_match)
    try:
        session.add_all(cgmlst_allele_matches)
        session.commit()
        logging.info('Added {} cgMLST allele results for user {}, genome {}'.format(
            len(cgmlst_allele_matches),
            user.name,
            genome.name,
        ))
    except Exception as ex:
        logging.error(ex)
        logging.error(
            'Could not add sistr-cmd cgMLST allele results to DB for user {}, genome {}. Rolling back DB session'.format(
                user.name,
                genome.name,
            ))
        session.rollback()
        raise ex


def get_serovar_prediction_dict(user, genome, pred, cgmlst):
    # type: (User, Genome, sistr_cmd.SerovarPrediction, Dict[str, Union[str, int, float, Dict[str, Union[str, int, float]]]]) -> Dict[str, Union[str, int, float]]
    n_missing_alleles = len([1 for x in cgmlst.values() if x['blast_result'] is None])
    n_complete_alleles = len([1 for x in cgmlst.values() if x['name'] is not None])
    n_partial_alleles = len(cgmlst) - n_complete_alleles - n_missing_alleles
    return {'genome': genome.name,
            'user_uploader': genome.user.name,
            'subspecies': pred.cgmlst_subspecies,
            'serovar_prediction': pred.serovar,
            'serovar_antigen_prediction': pred.serovar_antigen,
            'serogroup_prediction': pred.serogroup,
            'O_antigen': pred.o_antigen,
            'H1_prediction': pred.h1,
            'H2_prediction': pred.h2,
            'cgMLST_serovar_prediction': pred.serovar_cgmlst,
            'cgMLST_match_distance': pred.cgmlst_distance,
            'cgMLST_allele_matches': pred.cgmlst_matching_alleles,
            'cgMLST_match_genome': pred.cgmlst_genome_match,
            'cgMLST_ST': pred.cgmlst_ST,
            'cgMLST_complete_alleles': n_complete_alleles,
            'cgMLST_missing_alleles': n_missing_alleles,
            'cgMLST_partial_alleles': n_partial_alleles,
            'qc_status': pred.qc_status,
            'qc_message': pred.qc_messages, }

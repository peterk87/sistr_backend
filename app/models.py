"""
SQLAlchemy Database models

- one public user (e.g. 'sistr') and many temporary users (random string user name; no password; no auth)
- each user will have one or more genomes associated
- each temporary user will be able to view public data and their own "private" data
- each genome will have in silico typing data and assembly stat info associated with it
- certain genomes will have more metadata info than others (e.g. some of the public genomes)
- no metadata will be stored with "private" genomes other than in silico typing results and QUAST stats

"""
from __future__ import absolute_import

import string
from typing import Dict, List, Optional, Union
from sqlalchemy.dialects.postgresql import HSTORE, JSON, BYTEA
from sqlalchemy.ext.mutable import MutableDict
from sqlalchemy import Integer, String, Boolean, DateTime, Numeric, func, UniqueConstraint
from sqlalchemy import ForeignKey, Column, Index
from sqlalchemy.orm import relationship, backref, Session
from Crypto.Random import random
from itsdangerous import (TimedJSONWebSignatureSerializer as Serializer, BadSignature, SignatureExpired)
from passlib.apps import custom_app_context as pwd_context

from .decl_enum import DeclEnum
from . import Base

USER_NAME_LENGTH = 20
LETTERS_NUMBERS_RANDOM_SAMPLING_SET = (string.ascii_letters * 2) + (string.digits * 5)


class UserRole(DeclEnum):
    #: Admin user role
    admin = 'admin', 'Admin'
    #: Temporary user. Default user type for most users of SISTR.
    temporary = 'temporary', 'Temporary User'
    #: Registered user (unimplemented)
    registered = 'registered', 'Registered User'


class User(Base):
    __tablename__ = 'user'
    #: Integer: primary_key of User
    id = Column(Integer, primary_key=True)
    #: String(64): Unique user name
    name = Column(String(64), index=True, unique=True, nullable=False)
    #: UserRole_: User's role (e.g. admin, temporary, registered (unimplemented))
    role = Column(UserRole.db_type(), nullable=False, default=UserRole.temporary)
    #: String: Unique email address if not null
    email = Column(String, nullable=True)
    #: DateTime: Date and time when the user was last seen or when the user last accessed their data.
    last_seen = Column(DateTime)
    #: String(128): Cryptographically hashed User_ password
    password_hash = Column(String(128))
    #: JSON: User_ Genome_ selections for visualizations
    selections = Column(JSON)

    def hash_password(self, password):
        self.password_hash = pwd_context.encrypt(password)

    def verify_password(self, password):
        # type: (str) -> bool
        if self.role == UserRole.temporary:
            return True
        return pwd_context.verify(password, self.password_hash)

    def generate_auth_token(self, secret_key, expiration=(24 * 3600)):
        # type: (str, int) -> str
        s = Serializer(secret_key, expires_in=expiration)
        return s.dumps({'id': self.id})

    @staticmethod
    def verify_auth_token(session, secret_key, token):
        # type: (Session, str, str) -> Optional[User]
        s = Serializer(secret_key)
        try:
            data = s.loads(token)
        except SignatureExpired:
            return None
        except BadSignature:
            return None
        user = session.query(User).get(data['id'])
        return user

    @staticmethod
    def create_new_user_id(session):
        # type: (Session) -> str
        """
        Create a new random user name by randomly sampling from a list of lowercase
        and uppercase letters and numbers in equal proportions using PyCrypto.
        A new user name will be unique and will not already exist in the DB.

        Args:
            session (sqlalchemy.session.Session): DB session

        Returns:
            str: Randomly generated user name that does not exist in the DB.
        """
        while True:
            new_user_name = ''.join(random.sample(LETTERS_NUMBERS_RANDOM_SAMPLING_SET, USER_NAME_LENGTH))
            if session.query(User).filter(User.name == new_user_name).count() == 0:
                return new_user_name

    @staticmethod
    def validate_email(email):
        # type: (str) -> bool
        import re
        m = re.match(r"^[A-Za-z0-9\.\+_-]+@[A-Za-z0-9\._-]+\.[a-zA-Z]*$", email)
        return m is not None

    def __repr__(self):
        return '<User({id}, {name}, {role}, {last_seen})>'.format(
            id=self.id,
            name=self.name,
            role=self.role,
            last_seen=self.last_seen)


class Host(Base):
    """
    The Host_ organism that the Genome_ was isolated from.
    """
    __tablename__ = 'host'
    id = Column(Integer, primary_key=True)
    #: String(100): Common name of host from which genome was isolated (e.g. "human")
    common_name = Column(String(100), nullable=False)
    #: String(100): Latin or scientific name of host from which genome was isolated (e.g. "Homo sapiens")
    latin_name = Column(String(100), nullable=False)

    __table_args__ = (UniqueConstraint('common_name',
                                       'latin_name',
                                       name='host_uc'),)

    def __repr__(self):
        return '<Host({id}, {common}, {latin})>'.format(
            id=self.id,
            common=self.common_name,
            latin=self.latin_name
        )


class GeographicLocation(Base):
    """
    The geographic location where the Genome_ was isolated.
    """
    __tablename__ = 'geographic_location'

    id = Column(Integer, primary_key=True)
    #: Numeric: GPS Latitude
    lat = Column(Numeric)
    #: Numeric: GPS Longitude
    lng = Column(Numeric)
    #: String(100): Country where genome was isolated
    country = Column(String(100))
    #: String(100): Region within country where genome was isolated
    region = Column(String(1000))

    __table_args__ = (UniqueConstraint('lat',
                                       'lng',
                                       'country',
                                       'region',
                                       name='geographic_location_uc'),)

    def __repr__(self):
        return '<GeographicLocation({id}, {country}, {region}, [{lat},{lng}])>'.format(
            id=self.id,
            country=self.country,
            region=self.region,
            lat=self.lat,
            lng=self.lng
        )


class SalmonellaSubspecies(DeclEnum):
    """
    The *Salmonella* subspecies of the Genome_
    """
    #: *Salmonella enterica* subsp. arizonae
    arizonae = 'arizonae', 'Salmonella enterica subsp. arizonae'
    #: *Salmonella enterica* subsp. diarizonae
    diarizonae = 'diarizonae', 'Salmonella enterica subsp. diarizonae'
    #: *Salmonella enterica* subsp. enterica
    enterica = 'enterica', 'Salmonella enterica subsp. enterica'
    #: *Salmonella enterica* subsp. houtenae
    houtenae = 'houtenae', 'Salmonella enterica subsp. houtenae'
    #: *Salmonella enterica* subsp. indica
    indica = 'indica', 'Salmonella enterica subsp. indica'
    #: *Salmonella enterica* subsp. salamae
    salamae = 'salamae', 'Salmonella enterica subsp. salamae'
    #: Technically bongori is no longer a subspecies of *S. enterica* but rather a different species.
    #: It is included in this list for simplicity.
    bongori = 'bongori', 'Salmonella bongori'


class CurationInfo(Base):
    """
    Genome_ serovar curation info and whether the reported serovar makes sense according to
    cgMLST and antigen-based serovar prediction.
    """
    __tablename__ = 'curation_info'

    id = Column(Integer, primary_key=True)
    #: Boolean: Can the Genome_ serovar be trusted?
    is_trusted = Column(Boolean, default=False)
    #: String(100): A short code labelling whether a Genome_ metadata serovar matches the predicted
    #:   and if not the type of the discrepancy.
    code = Column(String(100), index=True)
    #: String: A longer description of the Genome_ metadata serovar curation code.
    description = Column(String)


class GenomeNGSInfo(Base):
    __tablename__ = 'genome_ngs_info'
    id = Column(Integer, primary_key=True)
    #: String(100): Sequencing platform (e.g. Illumina MiSeq)
    platform = Column(String(100))
    #: Numeric: Average sequencing fold coverage
    coverage = Column(Numeric)
    #: String(100): *De novo* assembler (e.g. SPAdes)
    assembler = Column(String(100))
    #: String(100): *De novo* assembler version
    assembler_version = Column(String(100))

    __table_args__ = (UniqueConstraint('platform',
                                       'coverage',
                                       'assembler',
                                       'assembler_version',
                                       name='genome_ngs_info_uc'),)

    def __repr__(self):
        return '<GenomeNGSInfo({platform}, {coverage}, {assembler} {version})>'.format(
            platform=self.platform,
            coverage=self.coverage,
            assembler=self.assembler,
            version=self.assembler_version)


class Genome(Base):
    """
    Genome_ metadata and derived *in silico* data including QUAST_
    quality statistics and MIST_ generated *in silico* typing results.
    """
    __tablename__ = 'genome'
    #: Integer: primary_key of Genome_
    id = Column(Integer, primary_key=True)
    #: String(100): Unique Genome_ name
    name = Column(String(100), index=True, nullable=False)
    #: Integer: ID of User_ owner of Genome_
    user_id = Column(Integer, ForeignKey('user.id'), nullable=False)
    #: sqlalchemy.orm.relationship: User_ model object owner of Genome
    user = relationship('User', backref=backref('genomes', lazy='dynamic', cascade='all,delete'))
    #: String(100): Metadata serovar of Genome
    serovar = Column(String(100))
    #: SalmonellaSubspecies_: Metadata subspecies of Genome_
    subspecies = Column(SalmonellaSubspecies.db_type())
    #: String(100): Metadata source type (e.g. food, clinical, animal, etc)
    source_type = Column(String(100))
    #: String(200): Specific metadata source (e.g. "chicken breast", "cow feces", etc)
    source_info = Column(String(200))
    #: DateTime: Date when isolate was collected
    collection_date = Column(DateTime)
    #: Integer: ID to GeographicLocation_ of Genome_
    geographic_location_id = Column(Integer, ForeignKey('geographic_location.id'))
    #: sqlalchemy.orm.relationship: GeographicLocation_ model object of Genome_
    geographic_location = relationship('GeographicLocation', backref=backref('genomes', lazy='dynamic'))
    #: Integer: ID of Host_ from which the genome was isolated if applicable
    host_id = Column(Integer, ForeignKey('host.id'))
    #: Host_: Host_ from which the genome was isolated if applicable
    host = relationship('Host', backref=backref('genomes', lazy='dynamic'))
    #: Integer: ID of GenomeNGSInfo_ of the Genome_
    ngs_info_id = Column(Integer, ForeignKey('genome_ngs_info.id'))
    #: sqlalchemy.orm.relationship: GenomeNGSInfo_ of the Genome_
    ngs_info = relationship('GenomeNGSInfo', backref=backref('genomes', lazy='dynamic'))
    #: HSTORE: key-value store of misc. metadata that does not fall into any other columns
    misc_metadata = Column(HSTORE)
    #: HSTORE: QUAST_ determined assembly quality statistics
    quality_stats = Column(HSTORE)
    #: DateTime: Date and time when Genome_ was uploaded to the database
    time_uploaded = Column(DateTime)
    #: String: FASTA file path (Unused)
    fasta_filepath = Column(String)

    #: JSON: cgMLST_330 clusters at 0-99% distance
    cgmlst_clusters = Column(JSON)

    #: Boolean: cgMLST clusters are finalized.
    is_cgmlst_clusters_final = Column(Boolean, default=False)

    #: Boolean: Flag for whether genome has been analyzed by the genome analysis pipeline and
    #:   if all the results are stored within the database.
    is_analyzed = Column(Boolean, default=False, nullable=False)

    #: Integer: ID of CurationInfo_ of the Genome_ (applicable to public genomes)
    curation_info_id = Column(Integer, ForeignKey('curation_info.id'))
    #: sqlalchemy.orm.relationship: CurationInfo_ of the Genome_
    curation_info = relationship('CurationInfo', backref=backref('genomes', lazy='dynamic'))

    cgmlst_profile_id = Column(Integer, ForeignKey('cgmlst_profile.id'))
    cgmlst_profile = relationship('CgMLSTProfile', backref=backref('genomes', lazy='dynamic'))

    @staticmethod
    def make_unique_name(name, session):
        """
        Ensure that the Genome_ has a unique name by appending
        ``_X`` where ``X`` is an incremental number to original
        genome name.

        Args:
            name (str): original Genome_ name
            session (sqlalchemy.session.Session): DB connection session

        Returns:
            str: unique Genome_ name
        """
        if session.query(Genome).filter_by(name=name).first() is None:
            return name
        version = 2
        while True:
            new_name = '{0}_{1}'.format(name, version)
            if session.query(Genome).filter_by(name=new_name).first() is None:
                break
            version += 1
        return new_name

    def __repr__(self):
        return '<Genome({id}, {name}, {user}, {serovar}, {n_contigs}, {subspecies}, analyzed? {is_analyzed})>'.format(
            id=self.id,
            name=self.name,
            user=self.user.name,
            serovar=self.serovar,
            n_contigs=self.contigs.count(),
            subspecies=self.subspecies,
            is_analyzed=self.is_analyzed)


class Contig(Base):
    __tablename__ = 'contig'
    #: Integer: primary_key of Contig_
    id = Column(Integer, primary_key=True)
    #: String(200): Contig_ name or FASTA entry header text
    name = Column(String(200), index=True, nullable=False)
    #: String: Nucleotide sequence
    seq = Column(String, nullable=False)
    #: Integer: ID of Genome_ that the Contig belongs to
    genome_id = Column(Integer, ForeignKey('genome.id'), nullable=False)
    #: sqlalchemy.orm.relationship: Genome_ model object that Contig belongs to
    genome = relationship('Genome', backref=backref('contigs', lazy='dynamic', cascade='all,delete'))

    def __repr__(self):
        return '<Contig({0}, {1}, {2})>'.format(self.name, len(self.seq), self.genome.name)


class MistMetadataResult(Base):
    __tablename__ = 'mist_metadata_result'
    id = Column(Integer, primary_key=True)
    #: MutableDict/HSTORE: Matching MistTest_ metadata for a Genome_
    attrs = Column(MutableDict.as_mutable(HSTORE))
    #: DateTime: Time MistMetadataResult_ was added to the DB
    timestamp = Column(DateTime)
    #: Integer: MistTest_ ForeignKey ID
    test_id = Column(Integer, ForeignKey('mist_test.id'), nullable=False)
    #: sqlalchemy.orm.relationship: MistTest_ object that MistMetadataResult_ belongs to
    test = relationship('MistTest', backref=backref('metadata_results', lazy='dynamic'))
    #: Integer: Genome_ ForeignKey ID
    genome_id = Column(Integer, ForeignKey('genome.id'), nullable=False)
    #: sqlalchemy.orm.relationship: Genome_ object that MistMetadataResult_ belongs to
    genome = relationship('Genome', backref=backref('mist_metadata_results', lazy='dynamic', cascade='all,delete'))

    def __repr__(self):
        return '<MistMetadataResult({id}, {test}, {genome}, {timestamp}, {attrs})>'.format(
            id=self.id,
            test=self.test.name,
            genome=self.genome.name,
            timestamp=self.timestamp,
            attrs=self.attrs)


class MistMarkerResult(Base):
    __tablename__ = 'mist_marker_result'
    id = Column(Integer, primary_key=True)
    #: String: MistMarkerResult_ *in silico* result for MistMarker_
    result = Column(String)
    #: Boolean: Is the *in silico* typing marker truncated due to being found at the end of a contig?
    is_contig_truncated = Column(Boolean, default=False)
    #: Boolean: Is the *in silico* typing marker missing?
    is_missing = Column(Boolean, default=False)
    #: Boolean: Does the *in silico* typing marker result contain potential homopolymer errors? *Only applies to allelic marker results*
    has_potential_homopolymer_errors = Column(Boolean, default=False)
    #: DateTime: Time when MistMarkerResult_ added to DB
    timestamp = Column(DateTime)
    #: JSON: Raw MIST_ JSON output for MistMarkerResult_
    mist_json = Column(JSON)
    #: Integer: MistMarker_ ForeignKey ID
    marker_id = Column(Integer, ForeignKey('mist_marker.id'))
    #: sqlalchemy.orm.relationship: MistMarker_ object that the MistMarkerResult_ belongs to
    marker = relationship('MistMarker', backref=backref('result', lazy='dynamic'))
    #: Integer: MistTest_ ForeignKey ID
    test_id = Column(Integer, ForeignKey('mist_test.id'))
    #: sqlalchemy.orm.relationship: MistTest_ object that the MistMarkerResult_ belongs to
    test = relationship('MistTest', backref=backref('marker_results', lazy='dynamic'))
    #: Integer: Genome_ ForeignKey ID
    genome_id = Column(Integer, ForeignKey('genome.id'), nullable=False)
    #: sqlalchemy.orm.relationship: Genome_ object that the MistMarkerResult_ belongs to
    genome = relationship('Genome', backref=backref('mist_marker_results', lazy='dynamic', cascade='all,delete'))
    #: Integer: User_ ForeignKey ID
    user_id = Column(Integer, ForeignKey('user.id'), nullable=False)
    #: sqlalchemy.orm.relationship: User_ object that the MistMarkerResult_ belongs to
    user = relationship('User', backref=backref('mist_marker_results', lazy='dynamic', cascade='all,delete'))

    __table_args__ = (Index('result', 'marker_id', 'test_id', 'genome_id', 'user_id', unique=True),)

    def __repr__(self):
        return '<MistMarkerResult({0}, result={1}, trunc={2}, missing={3}, marker={4}, test={5}, genome={6})>'.format(
            self.id,
            self.result,
            self.is_contig_truncated,
            self.is_missing,
            self.marker.name,
            self.test.name,
            self.genome.name)


class MistTestType(DeclEnum):
    """
    An Enum for the type of MIST_ *in silico* typing test that a 
    MistTest_ is.
    """
    allelic = 'Allelic', 'Allelic'
    pcr = 'PCR', 'PCR'
    oligo_probe = 'OligoProbe', 'OligoProbe'
    repeat = 'Repeat', 'Repeat'
    snp = 'SNP', 'SNP'
    amplicon_probe = 'AmpliconProbe', 'AmpliconProbe'


class MistTest(Base):
    """
    A MIST_ typing test with a unique name. 
    All MistTest_ should have one or more MistMarker_.
    Each of those MistMarker_ may have one or more MistAllele_ if the MistTestType_ is allelic.
    MIST_ may produce MistMetadataResult_ for a MistTest_.
    Each of the MistMarker_ for this MistTest_ will produce MistMarkerResult_.
    """
    __tablename__ = 'mist_test'
    id = Column(Integer, primary_key=True)
    #: String: Unique name of *in silico* typing test
    name = Column(String, unique=True, index=True, nullable=False)
    #: MistTestType_: Type of typing test (e.g. PCR, allelic, probe, etc)
    type = Column(MistTestType.db_type(), nullable=False)

    def __repr__(self):
        return '<MistTest({id}, Type={type}, Name={name})>'.format(
            id=self.id,
            type=self.type,
            name=self.name)


class MistMarker(Base):
    """
    A MistMarker_ is an *in silico* marker that belongs to a MistTest_ *in silico* typing test.
    MistMarkerResult_ are produced that are associated with a MistMarker_, a Genome_ and a User_.
    """
    __tablename__ = 'mist_marker'
    id = Column(Integer, primary_key=True)
    #: String: Name of MistMarker_
    name = Column(String, index=True, nullable=False)
    #: Integer: ID of MistTest_ that the MistMarker_ belongs to
    test_id = Column(Integer, ForeignKey('mist_test.id'))
    #: sqlalchemy.orm.relationship: MistTest_ object that the MistMarker_ belongs to
    test = relationship('MistTest', backref=backref('markers', lazy='dynamic', cascade='all'))

    __table_args__ = (UniqueConstraint('name',
                                       'test_id',
                                       name='mist_marker_uc'),)

    def __repr__(self):
        return '<MistMarker({id}, Name={name}, Test={test_name})>'.format(
            id=self.id,
            name=self.name,
            test_name=self.test.name
        )


class MistAllele(Base):
    """
    A MistAllele_ is a unique nucleotide allele sequence associated with a allelic MistTest_ and MistMarker_ such as MLST.
    """
    __tablename__ = 'mist_allele'
    id = Column(Integer, primary_key=True)
    #: String: Name of MistAllele_
    name = Column(String, nullable=False)
    #: String: Nucleotide sequence of MistAllele_
    seq = Column(String, index=True, nullable=False)
    #: DateTime: Date and time when MistAllele_ was added to the database
    timestamp = Column(DateTime)
    #: Integer: ID of MistMarker_ that the MistAllele_ belongs to
    marker_id = Column(Integer, ForeignKey('mist_marker.id'), nullable=False)
    #: sqlalchemy.orm.relationship: MistMarker_ model object that the MistAllele_ belongs to
    marker = relationship('MistMarker', backref=backref('alleles', lazy='dynamic'))
    #: Integer: ID of MistTest_ that the MistAllele_ belongs to
    test_id = Column(Integer, ForeignKey('mist_test.id'), nullable=False)
    #: MistTest_: MistTest_ model object that the MistAllele_ belongs to
    test = relationship('MistTest', backref=backref('alleles', lazy='dynamic'))

    __table_args__ = (UniqueConstraint('seq',
                                       'marker_id',
                                       'test_id',
                                       name='allele_uc'),)

    def __repr__(self):
        return '<MistAllele({id}, Test={test}, Allele={allele}, Length={length})>'.format(
            id=self.id,
            test=self.test.name,
            allele=self.name,
            length=len(self.seq)
        )


class TimestampMixin(object):
    created_at = Column(DateTime, default=func.now())


class BlastResultMixin(object):
    blast_results = Column(JSON)
    top_result = Column(JSON)
    is_trunc = Column(Boolean, default=False)
    is_missing = Column(Boolean, default=False)
    is_perfect_match = Column(Boolean, default=False)


class WzxPrediction(BlastResultMixin, TimestampMixin, Base):
    __tablename__ = 'wzx_prediction'
    id = Column(Integer, primary_key=True)
    serogroup = Column(String)

    genome_id = Column(Integer, ForeignKey('genome.id'), nullable=False)
    genome = relationship('Genome',
                          backref=backref('wzx_prediction',
                                          lazy='dynamic',
                                          cascade='all,delete'))

    user_id = Column(Integer, ForeignKey('user.id'), nullable=False)
    user = relationship('User',
                        backref=backref('wzx_prediction',
                                        lazy='dynamic',
                                        cascade='all,delete'))


class WzyPrediction(BlastResultMixin, TimestampMixin, Base):
    __tablename__ = 'wzy_prediction'
    id = Column(Integer, primary_key=True)
    serogroup = Column(String)

    genome_id = Column(Integer, ForeignKey('genome.id'), nullable=False)
    genome = relationship('Genome',
                          backref=backref('wzy_prediction',
                                          lazy='dynamic',
                                          cascade='all,delete'))

    user_id = Column(Integer, ForeignKey('user.id'), nullable=False)
    user = relationship('User',
                        backref=backref('wzy_prediction',
                                        lazy='dynamic',
                                        cascade='all,delete'))


class RfbClusterPrediction(BlastResultMixin, TimestampMixin, Base):
    __tablename__ = 'rfb_cluster_prediction'
    id = Column(Integer, primary_key=True)

    serogroup = Column(String)

    genome_id = Column(Integer, ForeignKey('genome.id'), nullable=False)
    genome = relationship('Genome',
                          backref=backref('rfb_cluster_prediction',
                                          lazy='dynamic',
                                          cascade='all,delete'))

    user_id = Column(Integer, ForeignKey('user.id'), nullable=False)
    user = relationship('User',
                        backref=backref('rfb_cluster_prediction',
                                        lazy='dynamic',
                                        cascade='all,delete'))


class SerogroupPrediction(TimestampMixin, Base):
    __tablename__ = 'serogroup_prediction'
    id = Column(Integer, primary_key=True)

    serogroup = Column(String)

    wzx_prediction_id = Column(Integer, ForeignKey('wzx_prediction.id'), nullable=False)
    wzx_prediction = relationship('WzxPrediction',
                                  backref=backref('serogroup_prediction',
                                                  lazy='dynamic',
                                                  cascade='all,delete'))

    wzy_prediction_id = Column(Integer, ForeignKey('wzy_prediction.id'), nullable=False)
    wzy_prediction = relationship('WzyPrediction',
                                  backref=backref('serogroup_prediction',
                                                  lazy='dynamic',
                                                  cascade='all,delete'))

    genome_id = Column(Integer, ForeignKey('genome.id'), nullable=False)
    genome = relationship('Genome',
                          backref=backref('serogroup_prediction',
                                          lazy='dynamic',
                                          cascade='all,delete'))

    user_id = Column(Integer, ForeignKey('user.id'), nullable=False)
    user = relationship('User',
                        backref=backref('serogroup_prediction',
                                        lazy='dynamic',
                                        cascade='all,delete'))


class H1FliCPrediction(BlastResultMixin, TimestampMixin, Base):
    __tablename__ = 'h1_flic_prediction'
    id = Column(Integer, primary_key=True)

    h1 = Column(String)

    genome_id = Column(Integer, ForeignKey('genome.id'), nullable=False)
    genome = relationship('Genome',
                          backref=backref('h1_flic_prediction',
                                          lazy='dynamic',
                                          cascade='all,delete'))

    user_id = Column(Integer, ForeignKey('user.id'), nullable=False)
    user = relationship('User',
                        backref=backref('h1_flic_prediction',
                                        lazy='dynamic',
                                        cascade='all,delete'))


class H2FljBPrediction(BlastResultMixin, TimestampMixin, Base):
    __tablename__ = 'h2_fljb_prediction'
    id = Column(Integer, primary_key=True)

    h2 = Column(String)

    genome_id = Column(Integer, ForeignKey('genome.id'), nullable=False)
    genome = relationship('Genome',
                          backref=backref('h2_fljb_prediction',
                                          lazy='dynamic',
                                          cascade='all,delete'))

    user_id = Column(Integer, ForeignKey('user.id'), nullable=False)
    user = relationship('User',
                        backref=backref('h2_fljb_prediction',
                                        lazy='dynamic',
                                        cascade='all,delete'))


class SerovarPrediction(TimestampMixin, Base):
    __tablename__ = 'serovar_prediction'
    id = Column(Integer, primary_key=True)
    sistrcmd_version = Column(String(10))
    serovar = Column(String)
    serovar_mlst = Column(String)
    mlst_serovar_counts = Column(JSON)

    serovar_cgmlst = Column(String)
    cgmlst_cluster_level = Column(String)
    cgmlst_cluster_number = Column(Integer)
    cgmlst_serovar_counts = Column(JSON)

    serovar_antigen = Column(String)
    o_antigen = Column(String)
    serogroup = Column(String)
    serogroup_sgsa = Column(String)
    serogroup_prediction_id = Column(Integer,
                                     ForeignKey('serogroup_prediction.id'),
                                     nullable=False)
    serogroup_prediction = relationship('SerogroupPrediction',
                                        backref=backref('serovar_prediction',
                                                        lazy='dynamic',
                                                        cascade='all,delete'))

    h1 = Column(String)
    h1_sgsa = Column(String)
    h1_flic_prediction_id = Column(Integer, ForeignKey('h1_flic_prediction.id'), nullable=False)
    h1_flic_prediction = relationship('H1FliCPrediction',
                                      backref=backref('serovar_prediction',
                                                      lazy='dynamic',
                                                      cascade='all,delete'))

    h2 = Column(String)
    h2_sgsa = Column(String)
    h2_fljb_prediction_id = Column(Integer, ForeignKey('h2_fljb_prediction.id'), nullable=False)
    h2_fljb_prediction = relationship('H2FljBPrediction',
                                      backref=backref('serovar_prediction',
                                                      lazy='dynamic',
                                                      cascade='all,delete'))

    genome_id = Column(Integer, ForeignKey('genome.id'), nullable=False)
    genome = relationship('Genome',
                          backref=backref('serovar_prediction',
                                          lazy='dynamic',
                                          cascade='all,delete'))

    user_id = Column(Integer, ForeignKey('user.id'), nullable=False)
    user = relationship('User',
                        backref=backref('serovar_prediction',
                                        lazy='dynamic',
                                        cascade='all,delete'))

    def __repr__(self):
        return '<SerovarPrediction({}, antigen={}, cgmlst={}, "({}) {}:{}:{}", v{}, {}, {})>'.format(
            self.serovar,
            self.serovar_antigen,
            self.serovar_cgmlst,
            self.serogroup,
            self.o_antigen,
            self.h1,
            self.h2,
            self.sistrcmd_version,
            self.genome,
            self.user)


class MashRefSeq(TimestampMixin, Base):
    __tablename__ = 'mash_refseq'

    id = Column(Integer, primary_key=True)

    genome_id = Column(Integer, ForeignKey('genome.id'), nullable=False)
    genome = relationship('Genome', backref=backref('mash_refseq', lazy='dynamic', cascade='all,delete'))

    user_id = Column(Integer, ForeignKey('user.id'), nullable=False)
    user = relationship('User', backref=backref('mash_refseq', lazy='dynamic', cascade='all,delete'))

    match_id = Column(String)
    distance = Column(Numeric)
    pvalue = Column(Numeric)
    matching = Column(String)

    top_100_results = Column(JSON)

    taxid = Column(Integer)
    bioproject = Column(String)
    biosample = Column(String)
    assembly_accession = Column(String)
    plasmid = Column(String)
    strain = Column(String)
    subspecies = Column(String)
    serovar = Column(String)


class MashSalmonella(TimestampMixin, Base):
    __tablename__ = 'mash_salmonella'

    id = Column(Integer, primary_key=True)

    genome_id = Column(Integer, ForeignKey('genome.id'), nullable=False)
    genome = relationship('Genome', backref=backref('mash_salmonella', lazy='dynamic', cascade='all,delete'))

    user_id = Column(Integer, ForeignKey('user.id'), nullable=False)
    user = relationship('User', backref=backref('mash_salmonella', lazy='dynamic', cascade='all,delete'))

    genome_match = Column(String)
    sketches = Column(Integer)
    serovar = Column(String)
    subspecies = Column(String)
    distance = Column(Numeric)
    top5 = Column(JSON)

    def __repr__(self):
        return '<MashSalmonella(id={}, user={}, genome={}, serovar={}, subspecies={}, genome_match={}, distance={}, sketches={})>'.format(
            self.id,
            self.user,
            self.genome,
            self.serovar,
            self.subspecies,
            self.genome_match,
            self.distance,
            self.sketches,
        )


class CgMLSTPrediction(TimestampMixin, Base):
    __tablename__ = 'cgmlst_prediction'

    id = Column(Integer, primary_key=True)

    genome_id = Column(Integer, ForeignKey('genome.id'), nullable=False)
    genome = relationship('Genome', backref=backref('cgmlst_prediction', lazy='dynamic', cascade='all,delete'))

    user_id = Column(Integer, ForeignKey('user.id'), nullable=False)
    user = relationship('User', backref=backref('cgmlst_prediction', lazy='dynamic', cascade='all,delete'))

    serovar_prediction_id = Column(Integer, ForeignKey('serovar_prediction.id'), nullable=False)
    serovar_prediction = relationship('SerovarPrediction',
                                      backref=backref('cgmlst_prediction', lazy='dynamic', cascade='all,delete'))

    profile_id = Column(Integer, ForeignKey('cgmlst_profile.id'), nullable=False)
    profile = relationship('CgMLSTProfile', backref=backref('cgmlst_prediction', lazy='dynamic', cascade='all,delete'))

    serovar = Column(String)
    subspecies = Column(String)
    distance = Column(Numeric)
    allele_matches = Column(Integer)
    genome_match = Column(String)

    def __repr__(self):
        return '<CgMLSTPrediction(id={}, user={}, genome={}, serovar={}, subspecies={}, distance={}, allele_matches={})>'.format(
            self.id,
            self.user,
            self.genome,
            self.serovar,
            self.subspecies,
            self.distance,
            self.allele_matches,
        )


class CgMLSTMarker(TimestampMixin, Base):
    __tablename__ = 'cgmlst_marker'
    id = Column(Integer, primary_key=True)
    name = Column(String)


class CgMLSTProfile(TimestampMixin, Base):
    __tablename__ = 'cgmlst_profile'

    id = Column(Integer, primary_key=True)
    profile = Column(JSON)
    numpy_array = Column(BYTEA, index=True)
    sequence_type = Column(String, index=True)
    missing_alleles = Column(Integer, default=0)
    partial_alleles = Column(Integer, default=0)

    def __repr__(self):
        return '<CgMLSTProfile(id={}, sequence_type={}, missing_alleles={}, partial_alleles={})>'.format(
            self.id,
            self.sequence_type,
            self.missing_alleles,
            self.partial_alleles,
        )


class CgMLSTAlleleMatch(BlastResultMixin, TimestampMixin, Base):
    __tablename__ = 'cgmlst_allele_match'

    id = Column(Integer, primary_key=True)

    genome_id = Column(Integer, ForeignKey('genome.id'), nullable=False)
    genome = relationship('Genome', backref=backref('cgmlst_allele_match', lazy='dynamic', cascade='all,delete'))

    user_id = Column(Integer, ForeignKey('user.id'), nullable=False)
    user = relationship('User', backref=backref('cgmlst_allele_match', lazy='dynamic', cascade='all,delete'))

    name = Column(Numeric)
    seq = Column(String)
    locus = Column(String)

    def __repr__(self):
        return '<CgMLSTAlleleMatch(id={}, user={}, genome={}, locus={}, name={}, seq={})>'.format(
            self.id,
            self.user,
            self.genome,
            self.locus,
            self.name,
            self.seq,
        )


class SistrCmdQC(TimestampMixin, Base):
    __tablename__ = 'sistrcmd_qc'

    id = Column(Integer, primary_key=True)
    genome_id = Column(Integer, ForeignKey('genome.id'), nullable=False)
    genome = relationship('Genome', backref=backref('qc', lazy='dynamic', cascade='all,delete'))

    user_id = Column(Integer, ForeignKey('user.id'), nullable=False)
    user = relationship('User', backref=backref('qc', lazy='dynamic', cascade='all,delete'))

    status = Column(String)
    message = Column(String)

    def __repr__(self):
        return '<SistrCmdQC(id={}, user={}, genome={}, status={}, message={})>'.format(
            self.id,
            self.user,
            self.genome,
            self.status,
            self.message,
        )

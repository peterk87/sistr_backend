from __future__ import absolute_import

from typing import List, Dict, Any, Optional, Union
import logging
import numpy as np
import pandas as pd
from scipy.spatial.distance import pdist, cdist
from redis import Redis

from . import session, cache
from .models import Genome, CgMLSTProfile

CGMLST_N_MARKERS = 330
CGMLST_SUBSPECIATION_DISTANCE_THRESHOLD = 0.9
CGMLST_CLUSTERS_REDIS_KEY = 'SISTR-cgmlst-clusters'
CGMLST_CLUSTERS_COLUMNS_REDIS_KEY = 'SISTR-cgmlst-clusters-columns'
CGMLST_PROFILE_KEY_FMT = 'sistr-cgmlst-{}'
GENOME_TO_CGMLST_PROFILE_KEY_FMT = 'sistr-gid-{}'


def first_row(df):
    # type: (pd.DataFrame) -> (pd.Series)
    for i, r in df.iterrows():
        return r


def dec_to_str(n):
    # type: (float) -> str
    """Convert a decimal number into a string with only up to 2 decimal places.

    Args:
        n (float): Number to convert to string

    Returns:
        str: Decimal number string up to only 2 decimal places
    """
    x = int(100 * n) / 100.0
    if x == 0.0:
        return '0'
    return str(x)


def set_cached_cgmlst_clusters(rdb, df):
    assert isinstance(rdb, Redis)
    assert isinstance(df, pd.DataFrame)

    cols = list(df.columns)
    from cPickle import dumps
    rdb.set(CGMLST_CLUSTERS_COLUMNS_REDIS_KEY, dumps(cols))
    for idx, row in df.iterrows():
        arr = np.array(row)
        rdb.hset(CGMLST_CLUSTERS_REDIS_KEY,
                 idx,
                 arr.tostring())


def get_cached_cgmlst_clusters(rdb):
    assert isinstance(rdb, Redis)

    from cPickle import loads
    cols = loads(rdb.get(CGMLST_CLUSTERS_COLUMNS_REDIS_KEY))

    d = rdb.hgetall(CGMLST_CLUSTERS_REDIS_KEY)
    assert isinstance(d, dict)
    out = {}
    for k, v in d.iteritems():
        out[k] = np.fromstring(v, dtype=np.int64)

    df = pd.DataFrame(out).transpose()
    df.columns = cols

    return df


@cache.memoize(timeout=86400)
def public_cgmlst_clusters(public_user_id):
    d = {name: clusters for name, clusters in
         session.query(Genome.name, Genome.cgmlst_clusters)
             .filter(Genome.user_id == public_user_id,
                     Genome.is_analyzed,
                     Genome.is_cgmlst_clusters_final)
             .all()}
    df_clusters = pd.DataFrame(d).transpose()
    return df_clusters


def profile_dist(genome_name, genome_profile, other_genome_profiles):
    """
    Find the curated and trusted public Genome_ which is most closely related to
    a specified Genome_ based on cgMLST profile similarity.

    Notes:
        It is assumed that ``other_genome_profiles`` contains only profiles for curated and trusted
        public Genomes_.
        This function is mostly used for cgMLST-based serovar prediction.

    Args:
        genome_name (str): Genome_ name
        other_genome_profiles (dict): Genome_ profiles to look through to find closest related Genomes_

    Returns:
        (dict, list): Most closely related Genome and list of other related Genomes_ in order of relatedness
    """

    other_genomes = other_genome_profiles['genomes']
    n_genomes = len(other_genomes)
    n_markers = len(other_genome_profiles['markers'])
    profiles_matrix = other_genome_profiles['results']
    profiles_matrix.shape = (n_genomes, n_markers)
    not_nan_counts = np.apply_along_axis(lambda x: np.sum((~np.isnan(x)) & (~np.isnan(genome_profile))), 1,
                                         profiles_matrix)
    distances = cdist(np.array([genome_profile]), profiles_matrix, 'hamming')[0]
    similarity_counts = [int(x) for x in np.round((1.0 - distances) * n_markers)]
    df = pd.DataFrame({'genome': other_genomes,
                       'n_matching_alleles': similarity_counts,
                       'distance': distances,
                       'non_missing_count': not_nan_counts,
                       })
    df = df[df['genome'] != genome_name]
    df.sort_values(by='distance', ascending=True, inplace=True)
    closest_relative = first_row(df)
    return closest_relative.to_dict(), [r.to_dict() for i, r in df.iterrows()]


def condensed_dist_matrix(genome_names):
    # type: (List[str]) -> (np.array, List[List[str]])
    """
    Get a condensed cgMLST pairwise distance matrix for specified Genomes_
    where condensed means redundant cgMLST profiles are only represented once in the distance matrix.

    Args:
        user_name (list): List of Genome_ names to retrieve condensed distance matrix for

    Returns:
        (numpy.array, list): tuple of condensed cgMLST distance matrix and list of grouped Genomes_
    """
    wgmlst_profiles = cgmlst_profiles(genome_names)
    arr = wgmlst_profiles['results']
    genomes = wgmlst_profiles['genomes']
    n_genomes = len(genomes)
    n_markers = len(wgmlst_profiles['markers'])
    # give the Numpy array the correct dimensions
    arr.shape = (n_genomes, n_markers)
    gs_collapse = []
    genome_idx_dict = {}
    indices = []
    patt_dict = {}
    for i, g in enumerate(genomes):
        p = arr[i, :].tostring()
        if p in patt_dict:
            parent = patt_dict[p]
            idx = genome_idx_dict[parent]
            gs_collapse[idx].append(g)
        else:
            indices.append(i)
            patt_dict[p] = g
            genome_idx_dict[g] = len(gs_collapse)
            gs_collapse.append([g])
    arr = arr[indices, :]
    dm = pdist(arr, metric='hamming')
    return dm, gs_collapse


def get_new_cluster_levels(cluster_levels, cluster_threshold):
    # type: (List[str], Union[str, float, int]) -> List[str]
    """
    Determine the cluster levels at which new cluster designations are required given
    a cluster distance threshold.

    Args:
        cluster_levels (list): List of possible cluster distance levels
        cluster_threshold (float): If float then converted to decimal string, if decimal string
          then must be within ``cluster_levels``

    Returns:
        list: List of cluster levels for which new cluster designations are required
    """
    if type(cluster_threshold) is str:
        if cluster_threshold not in cluster_levels:
            raise Exception(
                'Cluster level threshold is not in acceptable cluster levels "{}"'.format(cluster_threshold))
    if type(cluster_threshold) is float or type(cluster_threshold) is int:
        if cluster_threshold > 1.0 or cluster_threshold < 0.0:
            raise Exception(
                'Cluster level threshold outside acceptable range (not between 0 and 100 inclusive; cluster threshold is {})'.format(
                    cluster_threshold))
        cluster_threshold = dec_to_str(cluster_threshold)
    novel_levels = []
    for x in cluster_levels:
        if x != cluster_threshold:
            novel_levels.append(x)
        else:
            novel_levels.append(x)
            break
    return novel_levels


def generate_temp_cgmlst_clusters(df_cluster, genome, distance):
    # type: (pd.DataFrame, str, float) -> Dict[str, int]
    """
    Generate temporary cgMLST clusters for a new genome based on distance to closest Genome with finalized
    cgMLST clusters. New clusters are temporarily assigned "-1" as a cluster number until later finalized.

    Args:
        df_cluster (pandas.DataFrame): DataFrame of cluster designations with index of Genome_ names and
            columns of cluster distance levels (0 - 0.99)
        genome (str): Closest related Genome name in DB with finalized cgMLST clusters
        distance (float): cgMLST profile distance to Genome_

    Returns:
        dict: cgMLST cluster assignment profile for new Genome_
    """
    cluster_levels = list(df_cluster.columns)
    df_genome_clusters = df_cluster.ix[genome]  # type: pd.Series
    genome_clusters_dict = {i: x for i, x in df_genome_clusters.iteritems()}
    if distance > 0.0:
        novel_cluster_levels = get_new_cluster_levels(cluster_levels, distance)
        for novel_cluster_level in novel_cluster_levels:
            genome_clusters_dict[novel_cluster_level] = -1
    return genome_clusters_dict


def generate_cgmlst_clusters(df_cluster, genome_name, distance):
    # type: (pd.DataFrame, str, float) -> Dict[str, int]
    """
    Get the cgMLST cluster designations to a new Genome_ given a public or private Genome_ in the DB that it matches to most closely and the distance at which the new Genome_ matches.
    New cluster designations at a cluster level will be +1 of the current max for that cluster level.
    If distance is 0.0 then the clusters for the closest matching Genome_ will be returned.

    Args:
        df_cluster (pandas.DataFrame): DataFrame of cluster designations with index of Genome_ names and
            columns of cluster distance levels (0 - 0.99)
        genome (str): Closest related Genome_ name in DB
        distance (float): cgMLST profile distance to Genome_

    Returns:
        dict: cgMLST cluster assignment profile for new Genome_
    """
    cluster_levels = list(df_cluster.columns)
    df_genome_clusters = df_cluster.ix[genome_name]  # type: pd.Series
    genome_clusters_dict = {i: x for i, x in df_genome_clusters.iteritems()}
    if distance > 0.0:
        novel_cluster_levels = get_new_cluster_levels(cluster_levels, distance)
        for novel_cluster_level in novel_cluster_levels:
            new_cl_num = df_cluster[novel_cluster_level].max() + 1
            genome_clusters_dict[novel_cluster_level] = new_cl_num
    return genome_clusters_dict


def cgmlst_profiles(genome_names):
    # type: (List[str]) -> Optional[Dict[str, Any]]
    """
    Get the cgMLST profiles for a list of specified Genomes_.
    The returned dictionary contains the results in an array which can be easily transformed into
    a Numpy/Pandas matrix for proportional Hamming distance computation.

    Args:
        genome_names (list): List of Genome_ names to retrieve cgMLST profiles for

    Returns:
        dict['genomes']: Genome_ names ordered input ``genome_names``
        dict['markers']: cgMLST marker names alphanumerically sorted
        dict['results']: ``float64`` ``np.array`` of cgMLST allele numbers results ordered by ``genome_names``, alphanumerically sorted cgMLST marker names
    """
    if len(genome_names) == 0:
        logging.warning('No genomes specified. Cannot return cgMLST profiles')
        return None
    # query DB for all genome names, ids and cgMLST profile ids
    genome_id_to_profile_id, genome_names_to_ids = query_genome_name_id_profile_id()
    markers_names = query_cgmlst_marker_names()
    redis_cache = create_redis_object()
    redis_set_all_genome_id_to_profile_id(redis_cache, genome_id_to_profile_id)
    # dict of genome name to cgMLST profile Numpy array
    results_dict = {}  # type: Dict[str, np.array]
    # genome cgMLST profiles to retrieve from DB
    results_dict, genome_profiles_to_get = redis_get_genome_cgmlst_profiles(redis_cache,
                                                                            genome_id_to_profile_id,
                                                                            genome_names_to_ids,
                                                                            genome_names,
                                                                            results_dict)

    if len(genome_profiles_to_get) > 0:
        # retrieve all profiles in DB (seems to be faster query) and cache profiles in Redis
        query_redis_set_all_cgmlst_profiles(redis_cache)
        # retrieve all needed profiles from Redis
        results_dict, could_not_retrieve = redis_get_genome_cgmlst_profiles(redis_cache,
                                                                            genome_id_to_profile_id,
                                                                            genome_names_to_ids,
                                                                            genome_profiles_to_get,
                                                                            results_dict)
        logging.warning('Could not retrieve cgMLST profiles for {} genomes ({})'.format(
            len(could_not_retrieve),
            could_not_retrieve))
    # collect all cgMLST profile np.array ordered by genome_names, marker name order; 2D
    tmp_genome_names = []
    tmp_results = []
    for genome_name in genome_names:
        if genome_name in results_dict:
            tmp_results.append(results_dict[genome_name])
            tmp_genome_names.append(genome_name)
    genome_names = tmp_genome_names
    results = np.array(tmp_results, dtype=np.float64)
    logging.info('Retrieved cgMLST profile matrix of shape {}. Flattening to 1D array'.format(
        results.shape))
    results = results.flatten()
    return {'genomes': genome_names, 'markers': markers_names, 'results': results}


def create_redis_object(host='localhost', port=6379, db=0):
    return Redis(host=host, port=port, db=db)


def redis_get_genome_cgmlst_profiles(redis_cache,
                                     genome_id_to_profile_id,
                                     genome_names_to_ids,
                                     genome_names,
                                     results_dict):
    # type: (Redis, Dict[int, int], Dict[str, int], List[str], Dict[str, np.array]) -> (Dict[str, np.array], List[str])
    genome_profiles_to_get = []
    for genome_name in genome_names:
        gid = genome_names_to_ids[genome_name]
        pid = redis_cache.get(GENOME_TO_CGMLST_PROFILE_KEY_FMT.format(gid))
        if pid is None:
            genome_profiles_to_get.append(genome_name)
            continue
        arr_str = redis_cache.get(CGMLST_PROFILE_KEY_FMT.format(pid))
        # if arr_str for existing pid does not exist, delete key, fetch profile from DB
        if arr_str is None:
            redis_cache.delete(CGMLST_PROFILE_KEY_FMT.format(pid))
            genome_profiles_to_get.append(genome_name)
            continue
        arr = np.fromstring(arr_str, dtype=np.float64)
        # if the array is not the expected length, delete key, fetch profile from DB
        if arr.size != CGMLST_N_MARKERS:
            logging.warning(
                'cgMLST profile (id={}) array size (n={}) for genome name="{}";id="{}" does not equal # of markers (n={})'.format(
                    pid,
                    arr.size,
                    genome_name,
                    gid,
                    CGMLST_N_MARKERS
                ))
            redis_cache.delete(CGMLST_PROFILE_KEY_FMT.format(pid))
            genome_profiles_to_get.append(genome_name)
            continue
        results_dict[genome_name] = arr
    return results_dict, genome_profiles_to_get


def redis_set_all_genome_id_to_profile_id(redis_cache, genome_id_to_profile_id):
    for gid, pid in genome_id_to_profile_id.iteritems():
        if pid:
            redis_cache.set(GENOME_TO_CGMLST_PROFILE_KEY_FMT.format(gid), pid)


def query_cgmlst_marker_names():
    # type: () -> List[str]
    profile = session.query(CgMLSTProfile).filter(CgMLSTProfile.profile is not None).first()
    assert profile is not None
    assert isinstance(profile, CgMLSTProfile)
    markers_names = profile.profile.keys()
    markers_names.sort()
    assert len(markers_names) > 0, 'Should be more than 0 markers in any non-null cgMLST profile'
    assert len(markers_names) == CGMLST_N_MARKERS
    return markers_names


def query_genome_name_id_profile_id():
    # type: () -> (Dict[int, int], Dict[str, int])
    genome_query = session.query(Genome.id, Genome.name, Genome.cgmlst_profile_id)
    ids_names = [(id, name, profile_id) for id, name, profile_id in genome_query.all()]
    genome_names_to_ids = {name: id for id, name, profile_id in ids_names}
    genome_id_to_profile_id = {id: profile_id for id, name, profile_id, in ids_names}
    return genome_id_to_profile_id, genome_names_to_ids


def query_redis_set_all_cgmlst_profiles(redis_cache):
    # type: (Redis) -> None
    q = session.query(CgMLSTProfile.id,
                      CgMLSTProfile.numpy_array) \
        .order_by(CgMLSTProfile.id)
    for pid, arr_str in q.all():
        redis_cache.set(CGMLST_PROFILE_KEY_FMT.format(pid), arr_str)

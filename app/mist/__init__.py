from typing import Dict
import logging
import os
from subprocess import Popen, PIPE

from sqlalchemy.orm import Session

from ..mist.parser import MistJSONParser
from ..models import Genome
from app.db_util import get_user, get_genome


def run_mist(genome_name, user_name, temp_output_dir, mist_bin, mist_alleles_dir, mist_markers_files):
    # type: (str, str, str, str, str, Dict[str, str]) -> str
    """Run MIST in silico typing tests

    Run MIST on the Genome_ with the following *in silico* assays:

    - MLST
    - wgMLST_330 (cgMLST with 330 markers)

    Args:
        fasta_filepath (str): File path to temporary FASTA file of User_ uploaded Genome_
        temp_output_dir (str): Temporary analysis directory
        user_name (str): User_ name
        genome_name (str): Genome_ name
        mist_bin (str): Path to MIST.exe
        mist_alleles_dir (str): Path to MIST sequence-typing alelles directory
        mist_markers_files (dict): Dict of MIST test names to MIST markers file paths

    Returns:
        str: MIST JSON output file path

    Raises:
        Exception: if MIST JSON output file does not exist
    """
    temp_genome_fasta_path = os.path.join(temp_output_dir, genome_name + '.fasta')
    mist_temp_dir = os.path.join(temp_output_dir, 'mist_tmp-{0}'.format(genome_name))
    mist_output_filepath = os.path.join(temp_output_dir, 'MIST-{0}.json'.format(genome_name))

    # check if SGSA exists, if so then run it along with the other tests
    sgsa_exists = False
    sgsa_markers_file_path = None
    if 'SGSA' in mist_markers_files:
        sgsa_markers_file_path = os.path.abspath(mist_markers_files['SGSA'])
        sgsa_exists = os.path.exists(sgsa_markers_file_path)

    # run MIST on all assays
    args = ['mono',
            os.path.abspath(mist_bin),
            '-c', '1',
            '-T', mist_temp_dir,
            '-j', mist_output_filepath,
            '-a', os.path.abspath(mist_alleles_dir),
            '-t', os.path.abspath(mist_markers_files['MLST']),
            '-t' if sgsa_exists else None,
            sgsa_markers_file_path if sgsa_exists else None,
            temp_genome_fasta_path, ]
    args = filter(None, args)
    p = Popen(args, stderr=PIPE, stdout=PIPE)
    stdout, stderr = p.communicate()
    retcode = p.returncode
    if retcode != 0:
        raise Exception('[error {}] Error encountered while running MIST. stderr={}'.format(
            retcode,
            stderr,
        ))
    assert os.path.exists(mist_output_filepath), 'MIST output for genome {0} of user {1} does not exist at {2}.'.format(
        genome_name,
        user_name,
        mist_output_filepath
    )
    return mist_output_filepath


def parse_mist_results(session, mist_output_path, username, genome_name):
    # type: (Session, str, str, str) -> None
    """
    Parse MIST output
    =================

    Parse MistMarkerResult_ and MistMetadataResult_ from MIST_ JSON output.

    Args:
        session (Session): DB session
        mist_output_path (str): MIST JSON output file path
        user_name: User name

    Returns:
        str: MIST JSON output file path
    """
    assert os.path.exists(mist_output_path)
    user = get_user(session, username)
    genome = get_genome(session, user.id, genome_name)
    assert genome is not None
    assert isinstance(genome, Genome)
    parser = MistJSONParser(session, user, mist_output_path, genome_name=genome_name)
    parser.parse_all_marker_results()
    logging.warning('Parsed all marker results for genome {} of user {}'.format(genome_name, username))
    parser.parse_all_test_metadata()
    logging.warning('Parsed all MIST test metadata for genome {} of user {}'.format(genome_name, username))
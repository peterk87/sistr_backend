from __future__ import absolute_import

import logging
import json
import zlib
import re
import os
from datetime import datetime
from sqlalchemy.exc import IntegrityError

from ..models import \
    User, \
    Genome, \
    MistTest, \
    MistMarker, \
    MistAllele, \
    MistTestType, \
    MistMarkerResult, \
    MistMetadataResult


class MistJSONParser:
    """
    This class helps parse MIST JSON output and load it into the SISTR database.

    It is assuming that:

    - each MIST JSON file has data for only a single strain
    - there may be multiple tests
    - the test should exist within the database
    - there may be metadata for the test

    """

    def __init__(self, session, user, json_path, genome_name=None):
        """
        Initialize a MistJSONParser object in order to parse MIST JSON output
        into the database.

        Args:
            session (sqlalchemy.orm.session.Session): SQLAlchemy scoped :class:`Session` instance
            user (app.models.User): :class:`User` instance that the parsed MIST results
                and metadata will be assigned to
            json_path (str): Absolute path to the MIST JSON output file
        """

        assert isinstance(user, User)

        assert os.path.exists(json_path)
        # assert that the user exists within the database
        assert user == session.query(User).filter_by(name=user.name).first()

        self.session = session
        self.json_path = json_path
        self.user = user
        self.mist_json = json.load(open(self.json_path))
        if genome_name is None:
            self.strain = self.mist_json['Results'][0]['Strain']
        else:
            self.strain = genome_name
        self.genome = self.session.query(Genome) \
            .filter(Genome.name==self.strain, Genome.user_id == self.user.id) \
            .first()
        if self.genome is None:
            raise Exception('Genome {} does not exist for user {}'.format(self.strain, self.user))


    def parse_all_marker_results(self):
        self.test_result_dict = self.mist_json['Results'][0]['TestResults']
        for test_name in self.test_result_dict.keys():
            mist_test = self.session.query(MistTest).filter_by(name=test_name).first()
            if mist_test is not None:
                test_results = self.test_result_dict[test_name]
                if mist_test.type == MistTestType.allelic:
                    self.__parse_allelic_marker_results(mist_test, test_results)
                else:
                    self.__parse_non_allelic_marker_results(mist_test, test_results)
            else:
                logging.warning('Test {0} does not exist in the database! User: {1} | MIST JSON {2}'.format(
                    test_name,
                    self.user.name,
                    self.json_path))


    def __get_corrected_allele_seq(self, query_aln, subject_aln):
        """
        Get the corrected allele sequence for a subject BLAST alignment with
        respect to a query BLAST alignment.
        If there are 3 or more contiguous gaps in either alignment then the
        gap is considered to be real and a novel allele sequence is returned.
        Otherwise, gaps in the query sequence are not included in the
        corrected allele sequence and gaps in the subject sequence are changed
        to whatever nucleotide the query alignment has at that position.

        Args:
            query_aln (str): Query allele BLAST sequence alignment
            subject_aln (str): Subject genome allele BLAST sequence alignment
        Returns:
            str: Corrected allele nucleotide sequence
        Note:
            This allele sequence correction method works best for 454 data.
            Illumina tends to be very clean unless the coverage is very low.
        """
        long_gaps_subj = set([y for x in re.finditer(r'-{3,}', subject_aln) for y in range(x.start(), x.end())])
        long_gaps_query = set([y for x in re.finditer(r'-{3,}', query_aln) for y in range(x.start(), x.end())])
        corrected_subject_seq = []
        for i, t in enumerate(zip(query_aln, subject_aln)):
            if i in long_gaps_subj:
                continue
            if i in long_gaps_query:
                corrected_subject_seq.append(t[1])
                continue
            if t[0] == '-':
                continue
            if t[1] != '-':
                corrected_subject_seq.append(t[1])
            else:
                corrected_subject_seq.append(t[0])
        return ''.join(corrected_subject_seq)

    def __add_novel_mist_allele(self, mist_marker, allele_seq):
        """Add a novel allele to database

        Args:
            mist_marker (app.models.MistMarker): :class:`MistMarker` object
                to which the novel allele will be assigned.
            allele_seq (str): Novel allele nucleotide sequence.
        Returns:
            app.models.MistAllele: novel allele object
        """
        assert isinstance(mist_marker, MistMarker)
        allele_seq = str(allele_seq)

        mist_allele = mist_marker.alleles.filter_by(seq=allele_seq).first()
        if mist_allele:
            logging.warning('Test id %s | Marker %s | Allele already exists! %s; returning existing allele',
                            mist_marker.test_id,
                            mist_marker.name,
                            mist_allele,
                            )
            return mist_allele
        allele_name = zlib.crc32(allele_seq) & 0xffffffff
        try:
            mist_allele = MistAllele(name=allele_name,
                                     seq=allele_seq,
                                     marker=mist_marker,
                                     test=mist_marker.test,
                                     timestamp=datetime.utcnow())

            self.session.add(mist_allele)
            self.session.commit()

            logging.warning('Test id %s | Marker %s | Allele id=%s name=%s | Added novel allele',
                            mist_marker.test_id,
                            mist_marker.name,
                            mist_allele.id,
                            mist_allele.name,)
        except IntegrityError as exc:
            self.session.rollback()
            logging.warning('MistAllele "%s" of marker %s and test %s already exists! %s',
                            allele_name,
                            mist_marker.id,
                            mist_marker.test_id,
                            exc)
            mist_allele = mist_marker.alleles.filter_by(seq=allele_seq).first()
            if mist_allele:
                logging.warning('Test id %s | Marker %s | Allele already exists! %s; returning existing allele',
                            mist_marker.test_id,
                            mist_marker.name,
                            mist_allele,
                            )
                return mist_allele
            else:
                raise Exception('No MistAllele retrieved')
        return mist_allele

    def __get_mist_allele(self, mist_marker, allele_seq):
        """Get a MistAllele

        Search the database for the allele with the same sequence first.
        If allele doesn't exist then create a MistAllele if novel allele sequence
        addition allowed (wgMLST/cgMLST only).
        If novel allele for MLST, return None.

        Args:
            mist_marker (app.models.MistMarker): :class:`MistMarker` object
            allele_seq (str): novel allele sequence
        Returns:
            app.model.MistAllele or None: MistAllele object or None
        """
        assert isinstance(mist_marker, MistMarker)
        mist_test = mist_marker.test
        assert isinstance(mist_test, MistTest)
        allele_seq = allele_seq.replace('-', '')

        mist_allele = mist_marker.alleles.filter_by(seq=allele_seq).first()

        # only add novel alleles to cgMLST/wgMLST
        if mist_allele is None and re.match(r'\wgMLST.*', mist_test.name):
            mist_allele = self.__add_novel_mist_allele(mist_marker, allele_seq)

        return mist_allele

    def __parse_allelic_marker_result(self, marker_results, mist_marker, old_mist_marker_result):
        """
        Parse allelic MIST marker match result.
        Add novel alleles to the database if they are found.

        Args:
            marker_results (dict): MIST marker result dictionary
            mist_marker (app.models.MistMarker): :class:`MistMarker` object
            old_mist_marker_result (app.models.MistMarkerResult or None): Old MistMarkerResult for current marker for current genome if it exists
        Returns:
            app.models.MistMarkerResult: MIST marker result object
        """
        assert isinstance(mist_marker, MistMarker)
        blast_results = marker_results['BlastResults']
        allele_match = marker_results['AlleleMatch']
        marker_call = marker_results['MarkerCall']
        is_truncated = marker_results['IsContigTruncation']
        is_missing = blast_results is None
        has_potential_homopolymer_errors = False

        # perfect match found
        if marker_call != '':
            logging.debug('Genome %s | Marker %s | Exact allele found; result "%s"',
                          self.genome.name,
                          mist_marker.name,
                          marker_call,
                          )
            result = marker_call
        # if truncated just return the closest match
        elif is_truncated:
            logging.warning('Genome %s | Marker %s | Allele truncated; assuming closest result "%s"',
                            self.genome.name,
                            mist_marker.name,
                            allele_match,
                            )
            result = allele_match
        # match not found
        elif is_missing:
            logging.warning('Genome %s | Marker %s | Allele missing',
                            self.genome.name,
                            mist_marker.name,
                            )
            result = None
        # potentially novel allele found
        else:
            mismatches = blast_results['Mismatches']
            gaps = blast_results['Gaps']
            qlen = blast_results['QueryLength']
            aln_len = blast_results['AlignmentLength']
            amplicon = marker_results['Amplicon']
            logging.warning('Genome %s | Marker %s | Novel allele found? | mismatches=%s | gaps=%s | qlen=%s | alnlen=%s | amplicon len=%s',
                            mist_marker.name,
                            self.genome.name,
                            mismatches,
                            gaps,
                            qlen,
                            aln_len,
                            len(amplicon)
            )

            atgc_set = set('ATGC-')
            amplicon_set = set(amplicon)
            non_atgc_nts = amplicon_set - atgc_set

            saln = blast_results['SubjAln']
            qaln = blast_results['QueryAln']
            long_gaps_subj = set([y for x in re.finditer(r'-{3,}', saln) for y in range(x.start(), x.end())])
            long_gaps_query = set([y for x in re.finditer(r'-{3,}', qaln) for y in range(x.start(), x.end())])
            if len(non_atgc_nts) > 0:
                logging.warning('Genome %s | Marker %s | Non-ATGC nucleotides found in retrieved sequence %s | Unexpected result; assuming closest allele match "%s" with potential errors',
                                self.genome.name,
                                mist_marker.name,
                                non_atgc_nts,
                                allele_match,
                                )
                # If the retrieved sequence contains non-ATCG characters, do not add to DB as a new allele
                # Report a closest match
                has_potential_homopolymer_errors = True
                result = allele_match

            elif (mismatches == 0) and (gaps > 0) and (aln_len >= qlen) \
                and ((len(long_gaps_subj) < 3) or (len(long_gaps_query) < 3)):
                # match found with gaps, but otherwise full length, not truncated, no mismatches
                # determine if there are contiguous gaps 3+ in length
                # if there aren't any long gaps then assume that the
                # closest matching allele is the real result
                logging.warning('Genome %s | Marker %s | Allele found with gaps and no mismatches; assuming homopolymer errors and closest matching allele is actual match; result "%s"',
                                self.genome.name,
                                mist_marker.name,
                                allele_match,
                                )
                has_potential_homopolymer_errors = True
                result = allele_match

            elif mismatches > 0 and gaps == 0 and len(amplicon) == qlen:
                mist_allele = self.__get_mist_allele(mist_marker, amplicon)
                result = mist_allele.name if mist_allele is not None else None
                logging.warning('Genome %s | Marker %s | Potentially novel allele sequence; no gaps and %s mismatches; amplicon length == query length; closest result "%s"; allele "%s"',
                                self.genome.name,
                                mist_marker.name,
                                mismatches,
                                allele_match,
                                result,
                                )
            elif mismatches >= 0 and gaps > 0 and aln_len >= qlen:
                corrected_sseq = self.__get_corrected_allele_seq(qaln, saln)
                mist_allele = self.__get_mist_allele(mist_marker, corrected_sseq)
                result = mist_allele.name if mist_allele is not None else None
                logging.warning('Genome %s | Marker %s | Potentially novel allele sequence; %s gaps and %s mismatches; alignment length (%s) >= query length (%s); closest result "%s"; allele "%s"',
                                self.genome.name,
                                mist_marker.name,
                                gaps,
                                mismatches,
                                aln_len,
                                qlen,
                                allele_match,
                                result,
                                )
            else:
                #TODO: add extra checks
                # - start/stop codon check (no trust)
                # - no internal stop codons (no trust)
                # - HMM/MSA frameshifts
                # at least 75-80% nt identity
                corrected_sseq = self.__get_corrected_allele_seq(qaln, saln)
                mist_allele = self.__get_mist_allele(mist_marker, corrected_sseq)
                result = mist_allele.name if mist_allele is not None else None
                logging.warning('Genome %s | Marker %s | Novel allele sequence? else condition; %s gaps; %s mismatches; alignment length %s; query length %s; closest result "%s"; allele result="%s"; name="%s"; id="%s"',
                                self.genome.name,
                                mist_marker.name,
                                gaps,
                                mismatches,
                                aln_len,
                                qlen,
                                allele_match,
                                result,
                                mist_allele.name,
                                mist_allele.id,
                                )

        if old_mist_marker_result is None:

            mmr = MistMarkerResult(result=result,
                                   marker=mist_marker,
                                   test=mist_marker.test,
                                   genome=self.genome,
                                   mist_json=marker_results,
                                   is_contig_truncated=is_truncated,
                                   is_missing=is_missing,
                                   user=self.user,
                                   has_potential_homopolymer_errors=has_potential_homopolymer_errors,
                                   timestamp=datetime.utcnow())
            logging.info('Genome %s | Marker %s | test_id %s | Creating new MistMarkerResult %s',
                         self.genome.name,
                         mist_marker.name,
                         mist_marker.test_id,
                         mmr)
            return mmr
        else:
            assert isinstance(old_mist_marker_result, MistMarkerResult)
            old_mist_marker_result.result = result
            old_mist_marker_result.mist_json = marker_results
            old_mist_marker_result.is_missing = is_missing
            old_mist_marker_result.is_contig_truncated = is_truncated
            old_mist_marker_result.has_potential_homopolymer_errors = has_potential_homopolymer_errors
            old_mist_marker_result.timestamp = datetime.utcnow()
            logging.info('Genome %s | Marker %s | test_id %s | Updating existing MistMarkerResult %s',
                         self.genome.name,
                         mist_marker.name,
                         mist_marker.test_id,
                         old_mist_marker_result)
            return old_mist_marker_result

    def __parse_allelic_marker_results(self, mist_test, test_results):
        '''
        Parse allelic MIST marker results from MIST JSON output.
        Return a list of MistMarkerResult objects.

        Args:
            mist_test (app.models.MistTest): :class:`MistTest` object to save
                the marker results under
            test_results (dict): MIST test marker results dict
        '''
        assert isinstance(mist_test, MistTest)

        mist_results = []

        for marker, marker_results in test_results.iteritems():
            mist_marker = self.session.query(MistMarker).filter(MistMarker.name == marker,
                                                                MistMarker.test_id == mist_test.id).first()
            if mist_marker is not None:
                old_marker_result = self.session.query(MistMarkerResult) \
                    .filter(MistMarkerResult.genome_id == self.genome.id,
                            MistMarkerResult.marker_id == mist_marker.id) \
                    .first()
                has_old_result =  old_marker_result is not None
                marker_result = self.__parse_allelic_marker_result(marker_results, mist_marker, old_marker_result)
                if has_old_result:
                    logging.info('Updating existing marker result %s', marker_result)
                    self.session.add(marker_result)
                    self.session.commit()
                else:
                    mist_results.append(marker_result)
            else:
                marker_not_found_error = 'MIST marker {0} for test {1} does not exist in the database.'.format(
                    marker,
                    mist_test)
                raise Exception(marker_not_found_error)

        if len(mist_results) > 0:
            logging.warning('{0} allelic marker results parsed for test {1} from MIST JSON results for genome {2}'.format(
                len(mist_results),
                mist_test.name,
                self.genome.name))
            try:
                self.session.add_all(mist_results)
                logging.info('self.session.add_all(mist_results) %s new marker results', len(mist_results))
                logging.debug(mist_results)
                self.session.commit()
            except:
                logging.error(
                    'Could not add MIST results for test {0} for genome {1} (user:{user}) from MIST output {2}. Rolling back session.'.format(
                        mist_test.name,
                        self.genome.name,
                        self.json_path,
                        user=self.user.name
                    ))
                self.session.rollback()

    def __parse_non_allelic_marker_result(self, marker_results, mist_marker, old_mist_marker_result):
        """
        Parse a single MIST non-allelic marker result.

        Args:
            marker_results (dict): MIST results dict for marker.
            mist_marker (app.models.MistMarker): MistMarker object

        Returns:
            app.models.MistMarkerResult: MistMarkerResult object
        """
        blast_results = marker_results['BlastResults']
        marker_call = marker_results['MarkerCall']
        is_truncated = marker_results['IsContigTruncation']
        is_missing = blast_results is None
        has_potential_homopolymer_errors = False

        if old_mist_marker_result is None:
            return MistMarkerResult(result=marker_call,
                                    marker=mist_marker,
                                    test=mist_marker.test,
                                    genome=self.genome,
                                    mist_json=marker_results,
                                    is_contig_truncated=is_truncated,
                                    is_missing=is_missing,
                                    user=self.user,
                                    has_potential_homopolymer_errors=has_potential_homopolymer_errors,
                                    timestamp=datetime.utcnow())
        else:
            assert isinstance(old_mist_marker_result, MistMarkerResult)
            old_mist_marker_result.result = marker_call
            old_mist_marker_result.is_contig_truncated = is_truncated
            old_mist_marker_result.mist_json = marker_results
            old_mist_marker_result.is_missing = is_missing
            old_mist_marker_result.has_potential_homopolymer_errors = has_potential_homopolymer_errors
            old_mist_marker_result.timestamp = datetime.utcnow()
            return old_mist_marker_result

    def __parse_non_allelic_marker_results(self, mist_test, test_results):
        """
        Parse all non-allelic MIST marker results (e.g. probe, PCR).

        Args:
            mist_test (app.models.MistTest): MistTest object
            test_results (dict): MIST results for an in silico assay
        """
        isinstance(mist_test, MistTest)
        isinstance(test_results, dict)

        mist_results = []

        for marker, marker_results in test_results.iteritems():
            mist_marker = self.session.query(MistMarker).filter_by(name=marker, test=mist_test).first()
            if mist_marker is not None:
                old_marker_result = self.session.query(MistMarkerResult) \
                    .filter(MistMarkerResult.genome_id == self.genome.id,
                            MistMarkerResult.marker_id == mist_marker.id) \
                    .first()
                has_old_result = old_marker_result is not None
                marker_result = self.__parse_non_allelic_marker_result(marker_results, mist_marker, old_marker_result)
                if has_old_result:
                    logging.info('Updating existing non-allelic marker result %s', marker_result)
                    self.session.add(marker_result)
                    self.session.commit()
                else:
                    mist_results.append(marker_result)
            else:
                marker_not_found_error = 'MIST marker {0} for test {1} does not exist in the database.'.format(
                    marker,
                    mist_test.name)
                raise Exception(marker_not_found_error)

        if len(mist_results) > 0:
            logging.info('{0} marker results parsed for test {1} from MIST JSON results for genome {2}'.format(
                len(mist_results),
                mist_test.name,
                self.genome.name))

            try:
                self.session.add_all(mist_results)
                self.session.commit()
                logging.error('Added %s MIST marker results to DB', len(mist_results))
            except Exception as ex:
                logging.error('Could not add MIST marker results to DB %s', ex)
                self.session.rollback()

    def __parse_mist_test_metadata(self, mist_test, old_result):
        test_metadata_list = self.metadata_dict[mist_test.name]
        md_dict = {}
        for test_metadata in test_metadata_list:
            for attr, value in test_metadata.iteritems():
                if value == '':
                    if attr not in md_dict:
                        md_dict[attr] = set()
                    continue
                if attr in md_dict:
                    md_dict[attr].add(value)
                else:
                    md_dict[attr] = set([value])
        test_metadata_attrs = {}
        for attr, values in md_dict.iteritems():
            test_metadata_attrs[attr] = '|'.join(values)

        if old_result is None:
            mist_metadata_results = MistMetadataResult(attrs=test_metadata_attrs,
                                                       timestamp=datetime.utcnow(),
                                                       test=mist_test,
                                                       genome=self.genome)
            return mist_metadata_results
        else:
            assert isinstance(old_result, MistMetadataResult)
            old_result.attrs = test_metadata_attrs
            old_result.timestamp = datetime.utcnow()
            return old_result

    def parse_all_test_metadata(self):
        self.metadata_dict = self.mist_json['Results'][0]['Metadata']
        for test_name in self.metadata_dict.keys():
            mist_test = self.session.query(MistTest).filter_by(name=test_name).first()
            assert isinstance(mist_test, MistTest)
            if mist_test is not None:
                old_mist_metadata_results = self.session.query(MistMetadataResult)\
                    .filter(MistMetadataResult.genome_id == self.genome.id,
                            MistMetadataResult.test_id == mist_test.id)\
                    .first()
                mist_metadata_results = self.__parse_mist_test_metadata(mist_test, old_mist_metadata_results)
                self.session.add(mist_metadata_results)
                self.session.commit()
            else:
                logging.warning('Test {0} does not exist in the database! User: {1} | MIST JSON {2}'.format(
                    test_name,
                    self.user.name,
                    self.json_path))

    def parse_mist_tests(self):
        tests_types = self.mist_json['TestTypes']
        mist_tests = {}
        for test_name, test_type_string in tests_types.iteritems():
            test_type = MistTestType.from_string(test_type_string)
            mist_test = self.session.query(MistTest) \
                .filter(MistTest.name == test_name,
                        MistTest.type == test_type) \
                .first()
            if mist_test:
                mist_tests[test_name] = mist_test
            else:
                mist_tests[test_name] = MistTest(name=test_name, type=test_type)
        try:
            self.session.add_all([mist_test for name, mist_test in mist_tests.iteritems()])
            self.session.commit()
        except IntegrityError as exception:
            logging.error('Test(s) exist already. Continuing despite IntegrityError: {}'.format(exception))

        test_markers = self.mist_json['TestMarkers']
        mist_alleles = []
        for test_name, markers in test_markers.iteritems():
            mist_test = mist_tests[test_name]
            for marker_dict in markers:
                marker_name = marker_dict['Name']
                mist_marker = self.session.query(MistMarker) \
                    .filter(MistMarker.name == marker_name,
                            MistMarker.test == mist_test) \
                    .first()
                if mist_marker is None:
                    mist_marker = MistMarker(name=marker_name, test=mist_test)
                    self.session.add(mist_marker)
                    self.session.commit()
                assert isinstance(mist_marker, MistMarker)

                if marker_dict['Alleles'] is not None:
                    for allele in marker_dict['Alleles']:
                        allele_name = allele['Header']
                        allele_seq = allele['Sequence']
                        ma_name = self.session.query(MistAllele) \
                            .filter(MistAllele.test_id == mist_test.id,
                                    MistAllele.marker_id == mist_marker.id,
                                    MistAllele.name == allele_name) \
                            .first()
                        if ma_name is not None:
                            assert isinstance(ma_name, MistAllele)
                            if allele_seq != ma_name.seq:
                                logging.warning('MistTest {}: MistMarker {}: MistAllele {} sequence replaced with new value!'.format(
                                    mist_test,
                                    mist_marker,
                                    allele_name))

                            ma_name.seq = allele_seq
                            ma_name.timestamp = datetime.utcnow()
                            self.session.add(ma_name)
                            self.session.commit()
                            continue

                        ma_seq = self.session.query(MistAllele) \
                            .filter(MistAllele.test_id == mist_test.id,
                                    MistAllele.marker_id == mist_marker.id,
                                    MistAllele.seq == allele_seq) \
                            .first()
                        if ma_seq is not None:
                            assert isinstance(ma_seq, MistAllele)
                            if allele_name != ma_seq.name:
                                logging.warning('MistTest {}: MistMarker {}: MistAllele renamed from {} to {}'.format(
                                    mist_test,
                                    mist_marker,
                                    ma_seq.name,
                                    allele_name))
                            ma_seq.name = allele_name
                            ma_seq.timestamp = datetime.utcnow()
                            self.session.add(ma_seq)
                            self.session.commit()
                            continue

                        mist_allele = {'name': allele_name,
                                       'seq': allele_seq,
                                       'timestamp': datetime.utcnow(),
                                       'marker_id': mist_marker.id,
                                       'test_id': mist_test.id}
                        logging.info('MistTest {}: MistMarker {}: Novel MistAllele {} len={}'.format(
                            mist_test,
                            mist_marker,
                            allele_name,
                            len(allele_seq)
                        ))
                        mist_alleles.append(mist_allele)

        if len(mist_alleles) > 0:
            self.session.execute(MistAllele.__table__.insert(), mist_alleles)
            self.session.commit()

#!/usr/bin/env python
# -*- coding: utf-8 -*-

c = get_config()

c.InteractiveShellApp.exec_lines = [
    'import numpy as np',
    'import scipy as sp',
    'import pandas as pd',
    'from app import create_app',
    '_app = create_app()',
    'from app import session',
    'from app.models import *',
    'print "session.query(User).count()", session.query(User).count()',
]

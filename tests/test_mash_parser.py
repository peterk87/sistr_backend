import os
import shutil
from app.mash import sketch, query_refseq, to_dataframe
from app.models import MashRefSeq, Genome, User
from app.mash.parser import validate_mash_dist_df, top_n, parse_top_result, to_db, parse_refseq_info
from app.util import first_row
import pytest

@pytest.fixture
def mash_bin():
    return 'mash'

@pytest.fixture
def base_temp_dir():
    return '/tmp'


@pytest.fixture
def genome_fasta_path():
    return 'test_data/00_0163.fasta'


@pytest.fixture
def user_name():
    return 'tester'


@pytest.fixture
def genome_name():
    return 'test'


@pytest.yield_fixture
def user_temp_dir(user_name, genome_name, base_temp_dir):
    user_temp_dir = os.path.join(base_temp_dir, 'SISTR', '{0}-{1}'.format(user_name, genome_name))
    try:
        os.makedirs(user_temp_dir)
    except:
        pass

    yield user_temp_dir

    shutil.rmtree(user_temp_dir)

@pytest.fixture
def msh(mash_bin, user_temp_dir, genome_name, genome_fasta_path):
    return sketch(mash_bin, user_temp_dir, genome_name, genome_fasta_path)

@pytest.fixture
def mashout(mash_bin, msh):
    return query_refseq(mash_bin, msh)


def test_mash(mash_bin, user_temp_dir, genome_name, genome_fasta_path):


    sketch_path = sketch(mash_bin, user_temp_dir, genome_name, genome_fasta_path)

    assert os.path.exists(sketch_path)

    mashout = query_refseq(mash_bin, sketch_path)

    assert len(mashout) > 0

    df = to_dataframe(mashout)


    top_result = first_row(df)
    mashrefseq = MashRefSeq()

    assert mashrefseq is not None
    mashrefseq = parse_refseq_info(top_result.match_id, mashrefseq)
    assert mashrefseq is not None
    assert mashrefseq.match_id == top_result.match_id
    assert mashrefseq.bioproject == 'PRJNA224116'
    assert mashrefseq.biosample == 'SAMN00990755'
    assert mashrefseq.subspecies == 'salamae'
    assert mashrefseq.plasmid is None
    assert mashrefseq.assembly_accession == 'GCF_000486145.1'
    assert mashrefseq.serovar == '58_l_z13_z28_z6'
    assert mashrefseq.taxid == 1173951

    mashrefseq.top_100_results = top_n(df)
    assert type(mashrefseq.top_100_results) is list
    assert type(mashrefseq.top_100_results[0]) is dict

    mrs = parse_top_result(df, None)
    assert mrs is not None
    assert isinstance(mrs, MashRefSeq)
    assert mrs.distance == 0.0
    assert mrs.pvalue == 0.0
    assert mrs.matching == '400/400'
    assert mrs.match_id == top_result.match_id

    # running `mash dist` directly on a FASTA should produce the same output as making a sketch first
    mashout_fasta = query_refseq(mash_bin, genome_fasta_path)

    assert len(mashout_fasta) >  0

    df_mash_fasta = to_dataframe(mashout_fasta)
    assert validate_mash_dist_df(df_mash_fasta)
    top_result_fasta = first_row(df_mash_fasta)
    mashrefseq_fasta = MashRefSeq()

    assert mashrefseq_fasta is not None
    mashrefseq_fasta = parse_refseq_info(top_result_fasta.match_id, mashrefseq_fasta)
    assert mashrefseq_fasta is not None
    assert mashrefseq_fasta.match_id == top_result.match_id
    assert mashrefseq_fasta.bioproject == 'PRJNA224116'
    assert mashrefseq_fasta.biosample == 'SAMN00990755'
    assert mashrefseq_fasta.subspecies == 'salamae'
    assert mashrefseq_fasta.plasmid is None
    assert mashrefseq_fasta.assembly_accession == 'GCF_000486145.1'
    assert mashrefseq_fasta.serovar == '58_l_z13_z28_z6'
    assert mashrefseq_fasta.taxid == 1173951


def test_parser(dbsession, genome_name, user_name, mashout):
    user = User(name=user_name)
    dbsession.add(user)

    genome = Genome(name=genome_name, user=user)
    dbsession.add(genome)

    dbsession.commit()

    df = to_dataframe(mashout)

    assert dbsession.query(MashRefSeq).first() is None

    mashrefseq = to_db(dbsession, user, genome, df)

    assert mashrefseq.match_id == first_row(df)['match_id']

    mrs = dbsession.query(MashRefSeq).first()
    assert mrs is not None
    assert mrs.match_id == first_row(df)['match_id']

'''
Tests for sistr-cmd integration
'''
from __future__ import print_function
from typing import Dict, List, Optional, Any
from sqlalchemy.orm import Session
import pandas as pd
import numpy as np
import sistr.src.serovar_prediction as sistr_prediction

from app.sistrcmd import run_sistr_cmd, version, parse_add_sistr_cmd_results
from app.models import SerovarPrediction, CgMLSTAlleleMatch, CgMLSTPrediction, CgMLSTProfile, User, Genome, \
    SerogroupPrediction, WzyPrediction, WzxPrediction, H2FljBPrediction, H1FliCPrediction, MashSalmonella, SistrCmdQC
from app.db_util import get_user


def test_run_sistrcmd_poor_quality_genome(dbsession):
    user_name = 'tester0'
    genome_name = 'test0'
    expected_serovar = 'Choleraesuis'
    expected_spp = 'enterica'
    fasta_path = 'test_data/0006.fasta'

    user = get_user(dbsession, user_name)
    assert user is None
    user = get_user(dbsession, 'sistr')
    assert user is None

    user = User(name=user_name)
    genome = Genome(name=genome_name, user=user)
    dbsession.add(user)
    dbsession.add(genome)
    dbsession.commit()

    print('SISTR version', version())
    predictions, cgmlst_results = run_sistr_cmd(input_fasta=fasta_path,
                                                genome_name=genome_name,
                                                tmp_dir='/tmp/sistrcmd-test')
    assert isinstance(predictions, sistr_prediction.SerovarPrediction)
    assert predictions.serovar == expected_serovar
    assert predictions.cgmlst_matching_alleles < 330
    assert predictions.genome == genome_name
    assert predictions.cgmlst_subspecies == expected_spp
    assert predictions.qc_status == 'PASS'
    assert predictions.mash_serovar == expected_serovar
    assert len(cgmlst_results) == 330
    assert isinstance(cgmlst_results, dict)

    parse_add_sistr_cmd_results(dbsession, user_name, genome_name, predictions, cgmlst_results)

    sero_pred = dbsession.query(SerovarPrediction) \
        .join(Genome) \
        .join(User) \
        .filter(User.name == user.name,
                Genome.name == genome.name) \
        .first()
    print(sero_pred)
    assert isinstance(sero_pred, SerovarPrediction)
    assert sero_pred.serovar == expected_serovar
    assert sero_pred.serovar_antigen == predictions.serovar_antigen
    assert sero_pred.serogroup == predictions.serogroup
    assert isinstance(sero_pred.serogroup_prediction, SerogroupPrediction)
    assert isinstance(sero_pred.h1_flic_prediction, H1FliCPrediction)
    assert isinstance(sero_pred.h2_fljb_prediction, H2FljBPrediction)
    assert isinstance(sero_pred.serogroup_prediction.wzx_prediction, WzxPrediction)
    assert isinstance(sero_pred.serogroup_prediction.wzy_prediction, WzyPrediction)

    cgmlst_pred = dbsession.query(CgMLSTPrediction) \
        .join(User) \
        .join(Genome) \
        .filter(User.name == user.name,
                Genome.name == genome.name) \
        .first()
    print(cgmlst_pred)
    assert isinstance(cgmlst_pred, CgMLSTPrediction)
    assert cgmlst_pred.distance > 0.0
    assert cgmlst_pred.allele_matches < 330
    assert cgmlst_pred.profile.missing_alleles == 0
    assert cgmlst_pred.profile.partial_alleles > 0
    assert len(cgmlst_pred.profile.profile) == 330

    mash_prediction = dbsession.query(MashSalmonella) \
        .join(Genome) \
        .join(User) \
        .filter(User.name == user.name,
                Genome.name == genome.name) \
        .first()

    print(mash_prediction)
    assert isinstance(mash_prediction, MashSalmonella)
    assert mash_prediction.subspecies == expected_spp
    assert mash_prediction.serovar == expected_serovar
    assert mash_prediction.sketches == 1000
    assert mash_prediction.distance == 0.0

    genome = dbsession.query(Genome).filter(Genome.name == genome_name).first()
    cgmlst_profile = genome.cgmlst_profile
    print(cgmlst_profile)
    assert isinstance(cgmlst_profile, CgMLSTProfile)
    assert cgmlst_profile.missing_alleles == 0
    assert cgmlst_profile.partial_alleles > 0
    assert cgmlst_profile.sequence_type is None
    arr = np.fromstring(cgmlst_profile.numpy_array, dtype=np.float64)
    assert arr.size == 330
    assert np.isnan(arr).sum() == cgmlst_profile.partial_alleles

    qc = dbsession.query(SistrCmdQC).filter(SistrCmdQC.genome == genome).first()
    print(qc)
    assert isinstance(qc, SistrCmdQC)
    assert qc.status == 'PASS'


def test_run_sistrcmd(dbsession):
    user_name = 'tester'
    genome_name = 'test'
    expected_serovar = 'II 58:l,z13,z28:z6'
    expected_spp = 'salamae'
    fasta_path = 'test_data/00_0163.fasta'

    user = get_user(dbsession, user_name)
    assert user is None
    user = get_user(dbsession, 'sistr')
    assert user is None

    user = User(name=user_name)
    genome = Genome(name=genome_name, user=user)
    dbsession.add(user)
    dbsession.add(genome)
    dbsession.commit()

    print('SISTR version', version())
    predictions, cgmlst_results = run_sistr_cmd(input_fasta=fasta_path,
                                                genome_name=genome_name,
                                                tmp_dir='/tmp/sistrcmd-test')
    assert isinstance(predictions, sistr_prediction.SerovarPrediction)
    assert predictions.serovar == expected_serovar
    assert predictions.cgmlst_matching_alleles == 330
    assert predictions.genome == genome_name
    assert predictions.cgmlst_subspecies == expected_spp
    assert predictions.qc_status == 'PASS'
    assert predictions.mash_serovar == expected_serovar
    assert len(cgmlst_results) == 330
    assert isinstance(cgmlst_results, dict)

    parse_add_sistr_cmd_results(dbsession, user_name, genome_name, predictions, cgmlst_results)

    sero_pred = dbsession.query(SerovarPrediction) \
        .join(Genome) \
        .join(User) \
        .filter(User.name == user.name,
                Genome.name == genome.name) \
        .first()

    assert isinstance(sero_pred, SerovarPrediction)
    assert sero_pred.serovar == expected_serovar
    assert sero_pred.serovar_antigen == predictions.serovar_antigen
    assert sero_pred.serogroup == predictions.serogroup
    assert isinstance(sero_pred.serogroup_prediction, SerogroupPrediction)
    assert isinstance(sero_pred.h1_flic_prediction, H1FliCPrediction)
    assert isinstance(sero_pred.h2_fljb_prediction, H2FljBPrediction)
    assert isinstance(sero_pred.serogroup_prediction.wzx_prediction, WzxPrediction)
    assert isinstance(sero_pred.serogroup_prediction.wzy_prediction, WzyPrediction)

    cgmlst_pred = dbsession.query(CgMLSTPrediction) \
        .join(User) \
        .join(Genome) \
        .filter(User.name == user.name,
                Genome.name == genome.name) \
        .first()
    print(cgmlst_pred)
    assert isinstance(cgmlst_pred, CgMLSTPrediction)
    assert cgmlst_pred.distance == 0.0
    assert cgmlst_pred.allele_matches == 330
    assert cgmlst_pred.profile.missing_alleles == 0
    assert cgmlst_pred.profile.partial_alleles == 0
    assert len(cgmlst_pred.profile.profile) == 330

    mash_prediction = dbsession.query(MashSalmonella) \
        .join(Genome) \
        .join(User) \
        .filter(User.name == user.name,
                Genome.name == genome.name) \
        .first()

    print(mash_prediction)
    assert isinstance(mash_prediction, MashSalmonella)
    assert mash_prediction.subspecies == expected_spp
    assert mash_prediction.serovar == expected_serovar
    assert mash_prediction.sketches == 1000
    assert mash_prediction.distance == 0.0

    genome = dbsession.query(Genome).filter(Genome.name == genome_name).first()
    cgmlst_profile = genome.cgmlst_profile
    print(cgmlst_profile)


def test_run_sistrcmd_again(dbsession):
    # type: (Session) -> None
    assert isinstance(dbsession, Session)
    user_name = 'tester2'
    genome_name = 'test2'
    expected_serovar = 'II 58:l,z13,z28:z6'
    expected_spp = 'salamae'
    fasta_path = 'test_data/00_0163.fasta'

    user = get_user(dbsession, user_name)
    assert user is None
    user = get_user(dbsession, 'sistr')
    assert user is None

    user = User(name=user_name)
    genome = Genome(name=genome_name, user=user)
    dbsession.add(user)
    dbsession.add(genome)
    dbsession.commit()

    print('SISTR version', version())
    predictions, cgmlst_results = run_sistr_cmd(input_fasta=fasta_path,
                                                genome_name=genome_name,
                                                tmp_dir='/tmp/sistrcmd-test')
    assert isinstance(predictions, sistr_prediction.SerovarPrediction)
    assert predictions.serovar == expected_serovar
    assert predictions.cgmlst_matching_alleles == 330
    assert predictions.genome == genome_name
    assert predictions.cgmlst_subspecies == expected_spp
    assert predictions.qc_status == 'PASS'
    assert predictions.mash_serovar == expected_serovar
    assert len(cgmlst_results) == 330
    assert isinstance(cgmlst_results, dict)

    parse_add_sistr_cmd_results(dbsession, user_name, genome_name, predictions, cgmlst_results)

    cgmlst_pred = dbsession.query(CgMLSTPrediction) \
        .join(User) \
        .join(Genome) \
        .filter(User.name == user.name,
                Genome.name == genome.name) \
        .first()
    print(cgmlst_pred)
    assert isinstance(cgmlst_pred, CgMLSTPrediction)
    assert cgmlst_pred.distance == 0.0
    assert cgmlst_pred.allele_matches == 330
    assert cgmlst_pred.profile.missing_alleles == 0
    assert cgmlst_pred.profile.partial_alleles == 0
    assert len(cgmlst_pred.profile.profile) == 330

    cgmlst_pred_test = dbsession.query(CgMLSTPrediction) \
        .join(User) \
        .join(Genome) \
        .filter(User.name == 'tester',
                Genome.name == 'test') \
        .first()

    assert cgmlst_pred.profile_id == cgmlst_pred_test.profile_id

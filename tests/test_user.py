import base64
import json

from app.models import User, UserRole


__author__ = 'peter'

user_api_uri = '/api/user/'


def auth_headers(username, pw):
    return {'Authorization': 'Basic {}'.format(
        base64.b64encode('{}:{}'.format(username, pw)))}


def test_public_user(dbsession, app):
    public_username = app.config['PUBLIC_SISTR_USER_NAME']
    public_user_uri = '{}{}'.format(user_api_uri, public_username)
    with app.test_client() as client:
        public_user = User(name=public_username)
        public_user_pw = 'password'
        public_user.role = UserRole.admin

        public_user.hash_password(public_user_pw)
        assert public_user.password_hash != public_user_pw
        assert public_user.verify_password(public_user_pw)

        public_user_auth = auth_headers(public_username, public_user_pw)

        assert public_user_auth['Authorization'] == 'Basic c2lzdHI6cGFzc3dvcmQ='

        res = client.get(public_user_uri)
        # User doesn't exist yet in the DB so 403 forbidden
        assert res._status_code == 403

        dbsession.add(public_user)
        dbsession.commit()
        # User should now exist in DB
        assert client.get('/api/user/{}'.format(public_user.name))._status_code == 200
        # Empty put request should return the same data as a get request
        put_resp = client.put(public_user_uri,
                              data={},
                              headers=public_user_auth)
        assert put_resp._status_code == 200

        put_resp = client.put(public_user_uri,
                              data=json.dumps({
                                  'email': 'email@email.com',
                                  'new_username': 'newname',
                              }),
                              headers=public_user_auth,
                              content_type='application/json')
        # Not allowed to change public username
        assert put_resp._status_code == 403


def test_user_creation(app, dbsession):
    public_username = app.config['PUBLIC_SISTR_USER_NAME']
    public_user_uri = '{}{}'.format(user_api_uri, public_username)
    with app.test_client() as client:
        temp_username = 'test'
        temp_user_uri = user_api_uri + temp_username
        new_temp_username = 'newname'
        # get non-existent user == 403
        assert client.get(temp_user_uri)._status_code == 403
        # create new user 'test'
        post_resp_1 = client.post(temp_user_uri)
        assert post_resp_1._status_code == 201
        post_resp_1_data = json.loads(post_resp_1.data)
        assert post_resp_1_data['role'] == UserRole.temporary.value
        # another post to a temporary user will update the last_seen time
        post_resp_2 = client.post(temp_user_uri)
        assert post_resp_2._status_code == 200
        post_resp_2_data = json.loads(post_resp_2.data)
        assert post_resp_1_data['name'] == post_resp_2_data['name']
        assert post_resp_2_data['role'] == UserRole.temporary.value
        assert post_resp_1_data['role'] == UserRole.temporary.value

        from dateutil.parser import parse

        dt1 = parse(post_resp_1_data['last_seen'])
        dt2 = parse(post_resp_2_data['last_seen'])
        # last_seen updated between requests
        assert dt2 > dt1

        # can now get user 'test'
        assert client.get(temp_user_uri)._status_code == 200
        put_resp = client.put(temp_user_uri,
                              data=json.dumps({
                                  'new_username': new_temp_username,
                              }),
                              content_type='application/json')
        assert put_resp._status_code == 200
        assert json.loads(put_resp.data)['name'] == new_temp_username
        assert client.get(user_api_uri + new_temp_username)._status_code == 200
        assert client.get(temp_user_uri)._status_code == 403

        temp_user_pw = 'password'
        temp_user_auth = auth_headers(new_temp_username, temp_user_pw)
        # user promoted to registered user after PUT with password
        put_resp = client.put(user_api_uri + new_temp_username,
                              data=json.dumps({
                                  'password': temp_user_pw,
                              }),
                              content_type='application/json')
        assert put_resp._status_code == 200
        put_resp_data = json.loads(put_resp.data)
        assert put_resp_data['role'] == UserRole.registered.value
        # can no longer access user info without auth
        assert client.get(user_api_uri + new_temp_username)._status_code == 403
        # basic auth headers required for registered users
        get_resp = client.get(user_api_uri + new_temp_username,
                              headers=temp_user_auth)
        assert get_resp._status_code == 200
        # cannot POST existing registered user even with auth headers; only GET/PUT allowed
        post_resp = client.post(user_api_uri + new_temp_username,
                                headers=temp_user_auth)
        assert post_resp._status_code == 403


def test_reg_user_creation(app, dbsession):
    reg_username = 'reg1'
    reg_user_pw = 'password'
    reg_user_auth = auth_headers(reg_username, reg_user_pw)
    with app.test_client() as client:
        post_resp = client.post(user_api_uri + reg_username,
                                data=json.dumps({
                                    'password': reg_user_pw
                                }),
                                content_type='application/json')
        assert post_resp._status_code == 201

        post_resp_data = json.loads(post_resp.data)
        assert 'token' in post_resp_data
        token = post_resp_data['token']
        assert post_resp_data['role'] == UserRole.registered.value

        # use token to retrieve reg. user info
        token_auth = auth_headers(token, 'x')
        get_resp = client.get(user_api_uri + reg_username,
                              headers=token_auth)
        assert get_resp._status_code == 200
        # an invalid token won't allow retrieval of reg. user info
        invalid_token_auth = auth_headers(token + 'a', 'x')
        get_resp = client.get(user_api_uri + reg_username,
                              headers=invalid_token_auth)
        assert get_resp._status_code == 403

        # cannot get user info without auth
        assert client.get(user_api_uri + reg_username)._status_code == 403
        get_resp = client.get(user_api_uri + reg_username,
                              headers=reg_user_auth)
        assert get_resp._status_code == 200
        get_resp_data = json.loads(get_resp.data)
        assert get_resp_data['name'] == reg_username
        assert get_resp_data['role'] == UserRole.registered.value
        # creating another user with the same name is not allowed
        post_resp = client.post(user_api_uri + reg_username,
                                data=json.dumps({
                                    'password': reg_user_pw
                                }),
                                content_type='application/json')
        assert post_resp._status_code == 403


def test_user_selections(dbsession, app):
    temp_username = 'temp_test_selections'

    reg_username = 'reg_test_selections'
    reg_user_pw = 'password'

    test_selection = {'sel1': ['a', 'b', 'c', 'd'],
                      'sel2': ['z', 'x', 'w'], }

    with app.test_client() as client:
        assert client.post(user_api_uri + temp_username)._status_code == 201
        temp_user_sel_uri = user_api_uri + temp_username + '/selections'
        get_resp = client.get(temp_user_sel_uri)
        assert get_resp._status_code == 200
        get_resp_data = json.loads(get_resp.data)

        temp_user = dbsession.query(User).filter(User.name == temp_username).first()
        assert isinstance(temp_user, User)
        assert temp_user.selections is None
        assert get_resp_data is None
        put_resp = client.put(temp_user_sel_uri,
                              data=json.dumps({
                                  'selections': test_selection,
                              }),
                              content_type='application/json')
        assert put_resp._status_code == 200
        put_resp_data = json.loads(put_resp.data)

        temp_user = dbsession.query(User).filter(User.name == temp_username).first()
        assert isinstance(temp_user, User)
        assert temp_user.selections == put_resp_data

        post_resp = client.post(user_api_uri + reg_username,
                                data=json.dumps({
                                    'password': reg_user_pw,
                                }),
                                content_type='application/json')
        assert post_resp._status_code == 201
        post_resp_data = json.loads(post_resp.data)
        assert post_resp_data['role'] == UserRole.registered.value
        reg_user_sel_uri = user_api_uri + reg_username + '/selections'
        reg_user_auth = auth_headers(reg_username, reg_user_pw)
        # not allowed to update user selection without auth
        put_resp = client.put(reg_user_sel_uri,
                              data=json.dumps({
                                  'selections': test_selection,
                              }),
                              content_type='application/json')
        assert put_resp._status_code == 403
        put_resp = client.put(reg_user_sel_uri,
                              data=json.dumps({
                                  'selections': test_selection,
                              }),
                              headers=reg_user_auth,
                              content_type='application/json')
        assert put_resp._status_code == 200
        put_resp_data = json.loads(put_resp.data)
        assert put_resp_data is not None
        assert put_resp_data == test_selection
        reg_user = dbsession.query(User).filter(User.name == reg_username).first()
        assert isinstance(reg_user, User)
        assert reg_user.selections == put_resp_data
        # cannot retrieve registered user selections without auth
        get_resp = client.get(reg_user_sel_uri)
        assert get_resp._status_code == 403
        get_resp = client.get(reg_user_sel_uri,
                              headers=reg_user_auth)
        assert get_resp._status_code == 200
        get_resp_data = json.loads(get_resp.data)
        assert get_resp_data is not None
        assert get_resp_data == reg_user.selections

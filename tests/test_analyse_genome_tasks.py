from __future__ import print_function

import os
import pytest
from cStringIO import StringIO

from Bio import SeqIO

from app.models import User, Genome
from app.util import fasta_format_check, parse_fasta
from app.db_util import add_genome_to_db


@pytest.fixture
def fasta_str():
    TEST_FASTA_PATH = os.path.abspath('test_data/00_0163.fasta')
    with open(TEST_FASTA_PATH) as f:
        x = f.read()
    return x


def test_fasta_format_check(fasta_str):
    is_valid, err_msg = fasta_format_check(fasta_str)
    print(err_msg)
    assert is_valid, 'invalid FASTA format'

    fasta_str = '''
    AGTCG
    >header 1
    GTAGGC
    '''
    is_valid, err_msg = fasta_format_check(fasta_str)
    print(err_msg)
    assert not is_valid, 'Missing header'

    fasta_str = '''
    >header 1

    >header 2

    >header 3
    >header 4
    '''

    is_valid, err_msg = fasta_format_check(fasta_str)
    print(err_msg)
    assert not is_valid, 'No sequences'

    fasta_str = '''
    >header 1
    AaCcGgTtRrYySsWwKkMmBbDdHhVvNn
    '''
    is_valid, err_msg = fasta_format_check(fasta_str)
    print(err_msg)
    assert is_valid, 'FASTA file contents string contains invalid nucleotide characters'

    fasta_str = '''
    >header 1
    AGTCQ
    '''
    is_valid, err_msg = fasta_format_check(fasta_str)
    print(err_msg)
    assert not is_valid, 'invalid nucleotide character'

    fasta_str = '''
    >header 1
    12345
    '''
    is_valid, err_msg = fasta_format_check(fasta_str)
    print(err_msg)
    assert not is_valid, 'invalid nucleotide character'


def test_add_genome_to_db(dbsession, fasta_str):
    user_name = 'tester'
    genome_name = 'test'
    user = User()
    user.name = user_name
    dbsession.add(user)
    dbsession.commit()
    genome = add_genome_to_db(dbsession, user, genome_name, fasta_str)
    assert isinstance(genome, Genome)
    assert genome.name == genome_name
    assert genome.user == user
    assert genome.contigs.count() > 0
    total_seq_len_in_db = sum([len(c.seq) for c in genome.contigs.all()])
    total_seq_len_in_fasta_str = sum([len(r.seq) for r in SeqIO.parse(StringIO(fasta_str), 'fasta')])
    assert total_seq_len_in_db == total_seq_len_in_fasta_str

'''Test saving of binary string cgMLST profile data to database

Should be getting back the same NumPy array that is saved to the database
'''

from __future__ import print_function

import numpy as np
import pandas as pd
from sistr.src.cgmlst import ref_cgmlst_profiles
from sqlalchemy.orm import Session

from app.models import CgMLSTProfile


def test_save_read_binary_string(dbsession):
    # type: (Session) -> None

    ref_profiles = ref_cgmlst_profiles()
    assert isinstance(ref_profiles, pd.DataFrame)
    row0006 = ref_profiles.ix['0006']
    assert isinstance(row0006, pd.Series)
    profile = CgMLSTProfile()
    marker_alleles = {marker: int(allele) if not pd.isnull(allele) else None for marker, allele in row0006.to_dict().items()}
    profile.profile = marker_alleles
    arr = np.array(row0006, dtype=np.float64)
    arr_str = arr.tostring()
    profile.numpy_array = arr_str
    dbsession.add(profile)
    dbsession.commit()

    print(dbsession.query(CgMLSTProfile).count())
    dbprofile = dbsession.query(CgMLSTProfile).first()
    assert isinstance(dbprofile, CgMLSTProfile)
    dbprofile_arr = np.fromstring(dbprofile.numpy_array, dtype=np.float64)
    dbprofile_arr_isnull = pd.isnull(dbprofile_arr)
    arr_isnull = pd.isnull(arr)
    assert np.all(dbprofile_arr_isnull == arr_isnull), 'NaN values should be preserved in DB retrieved binary string'
    assert np.allclose(dbprofile_arr[~dbprofile_arr_isnull], arr[~arr_isnull]), 'All float values should be the same (allclose for floats)'

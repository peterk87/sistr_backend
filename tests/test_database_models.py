from __future__ import print_function
from app.models import Genome, MistMarker, MistTest, MistMarkerResult, User
from app.models import MistTestType

from sqlalchemy.exc import IntegrityError, StatementError

import pytest


def test_mist_test(dbsession):
    test1 = MistTest(name='test1', type=MistTestType.allelic)
    dbsession.add(test1)
    dbsession.commit()

    assert dbsession.query(MistTest).first() is not None
    assert dbsession.query(MistTest).count() == 1

    test2 = MistTest(name='test2', type=MistTestType.pcr)

    dbsession.add(test2)
    dbsession.commit()
    assert dbsession.query(MistTest).count() == 2

    assert dbsession.query(MistTest).filter_by(name='notindb').first() is None
    assert dbsession.query(MistTest).filter_by(name='test1').first() is not None


def test_mist_test_query(dbsession):
    t1 = dbsession.query(MistTest).first()
    assert t1 is not None
    assert dbsession.query(MistTest).count() == 2

#
# def test_add_duplicate_mist_test(dbsession):
#     assert dbsession.query(MistTest).count() == 2
#     assert dbsession.query(MistTest).first() is not None
#     test1_dup = MistTest(name='test1', type=MistTestType.pcr)
#     assert dbsession.query(MistTest).first() is not None
#     with pytest.raises(IntegrityError):
#         assert dbsession.query(MistTest).first() is not None
#         dbsession.add(test1_dup)
#         assert dbsession.query(MistTest).first() is not None
#         dbsession.commit()
#     print(dbsession.query(MistTest).count())
#
#     dbsession.rollback()
#     assert dbsession.query(MistTest).first() is not None


# def test_try_add_madeup_test_type(dbsession):
#     with pytest.raises(StatementError):
#         t = MistTest(name='test2', type='madeuptest')
#         dbsession.add(t)
#         dbsession.commit()
#     dbsession.rollback()


def test_mist_marker_result(dbsession):
    t1 = dbsession.query(MistTest).first()
    assert t1 is not None

    m1 = MistMarker(name='marker1', test=t1)
    dbsession.add(m1)
    dbsession.commit()
    assert t1.markers.count() == 1
    assert t1.markers.first() == m1

    u1 = User(name='user1')
    dbsession.add(u1)
    dbsession.commit()
    g1 = Genome(name='genome1', user=u1)
    mr1 = MistMarkerResult(result='1', marker=m1, test=t1, genome=g1, user=u1)
    dbsession.add(mr1)
    dbsession.commit()

    assert g1.mist_marker_results.count() == 1
    assert g1.mist_marker_results.first() == mr1
    assert g1.id is not None


    m2 = MistMarker(name='marker2', test=t1)
    dbsession.add(m2)
    dbsession.commit()

    mr2 = MistMarkerResult(result='2', marker=m2, test=t1, genome=g1, user=u1)
    dbsession.add(mr2)
    dbsession.commit()

    assert g1.mist_marker_results.count() == 2
    assert dbsession.query(MistMarkerResult).filter_by(marker=m2).first() == mr2


def test_make_unique_genome_name(dbsession):
    existing_genome_name = 'genome1'
    assert dbsession.query(Genome).filter(Genome.name == existing_genome_name).first() is not None
    g_name = Genome.make_unique_name(existing_genome_name, session=dbsession)

    assert existing_genome_name != g_name


def test_mist_test_type():
    assert MistTestType.from_string('Allelic') == MistTestType.allelic
    assert MistTestType.from_string('PCR') == MistTestType.pcr
    assert MistTestType.from_string('OligoProbe') == MistTestType.oligo_probe
    assert MistTestType.from_string('SNP') == MistTestType.snp
    assert MistTestType.from_string('AmpliconProbe') == MistTestType.amplicon_probe

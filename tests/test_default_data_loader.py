'''
Test that genomes and genome metadata can be loaded into the database from
files.
Test that genome MIST results can be loaded into the DB from a MIST JSON
output file using the MIST JSON parser class.
'''
from app.db_util import get_user

__author__ = 'piotr'
from app.models import Host, GeographicLocation, GenomeNGSInfo, MistMarkerResult
from app.models import Genome, MistMarker, MistTest, User
from app.mist.parser import MistJSONParser
from app.db_loaders import DefaultDataLoader

import os


def test_load_data(dbsession):
    metadata_file_path = os.path.join('test_data', 'genome_metadata-5-genomes-test.tsv')
    assert os.path.exists(metadata_file_path)
    default_data_loader = DefaultDataLoader(dbsession)
    u = User(name='tester')
    default_data_loader.load_default_genome_metadata(user=u,
                                                     genome_metadata_file_path=metadata_file_path)

    assert dbsession.query(Genome).count() == 5
    assert dbsession.query(Host).count() == 1

    host_cow = dbsession.query(Host).filter(Host.common_name == 'cow').first()

    assert dbsession.query(GeographicLocation).count() == 1
    geo_loc = dbsession.query(GeographicLocation).first()
    assert geo_loc is not None
    assert geo_loc.country == 'USA'
    assert geo_loc.region == 'VT'

    genome_0084 = dbsession.query(Genome).filter(Genome.name == '0084').first()

    assert genome_0084 is not None
    assert genome_0084.serovar == 'Typhimurium var. Copenhagen'
    assert genome_0084.source_type == 'animal'
    assert genome_0084.host == host_cow
    # check that misc metadata exists and that for key 'plasmid' value is 'plasmid1'
    assert genome_0084.misc_metadata is not None
    assert genome_0084.misc_metadata['plasmid'] == 'plasmid1'

    host_0084 = genome_0084.host
    assert host_0084.common_name == 'cow'
    assert host_0084 == host_cow

    genome_0047 = dbsession.query(Genome).filter(Genome.name == '0047').first()
    assert genome_0047 is not None
    assert genome_0047.geographic_location is not None
    # genome 0047 should have misc metadata for strain
    assert genome_0047.misc_metadata['strain'] is not None

    geo_loc_0047 = genome_0047.geographic_location
    assert geo_loc == geo_loc_0047

    genome_0014 = dbsession.query(Genome).filter(Genome.name == '0014').first()
    assert genome_0014 is not None
    assert genome_0014.serovar == 'Abony'
    assert genome_0014.geographic_location is None
    assert genome_0014.host is None


def test_update_data(dbsession):
    default_data_loader = DefaultDataLoader(dbsession)
    u = get_user(dbsession, 'tester')

    # There should be 5 genomes in the test DB
    assert dbsession.query(Genome).count() == 5
    assert dbsession.query(Host).count() == 1

    updated_metadata_file_path = os.path.join('test_data', 'genome_metadata-5-genomes-test-modded.tsv')
    default_data_loader.load_default_genome_metadata(user=u,
                                                     genome_metadata_file_path=updated_metadata_file_path)
    # there should still be 5 genomes in the test DB after loading the updated metadata
    assert dbsession.query(Genome).count() == 5
    # after updating the data in the DB there should be 2 host entries
    assert dbsession.query(Host).count() == 2

    # the new host entry should be type human
    host_human = dbsession.query(Host).filter(Host.common_name == 'human').first()
    assert host_human is not None

    # genome 0014 should now be serovar Enteriditis, from Ireland and be from a human clinical host
    genome_0014 = dbsession.query(Genome).filter(Genome.name == '0014').first()
    assert genome_0014 is not None
    assert genome_0014.serovar == 'Enteriditis'
    assert genome_0014.geographic_location is not None
    assert genome_0014.host is not None
    assert genome_0014.host == host_human

    # changed serovar from 'Typhimurium var. Copenhagen' to 'Typhimurium'
    genome_0084 = dbsession.query(Genome).filter(Genome.name == '0084').first()
    assert genome_0084 is not None
    assert genome_0084.serovar == 'Typhimurium'


def test_remove_metadata(dbsession):
    ddl = DefaultDataLoader(dbsession)
    u = dbsession.query(User).filter(User.name == 'tester').first()

    # There should be 5 genomes in the test DB
    assert dbsession.query(Genome).count() == 5

    blanked_metadata_file_path = os.path.join('test_data', 'genome_metadata-5-genomes-test-blanked.tsv')
    ddl.load_default_genome_metadata(user=u, genome_metadata_file_path=blanked_metadata_file_path)

    genome_0014 = dbsession.query(Genome).filter(Genome.name == '0014').first()
    assert genome_0014 is not None
    assert genome_0014.serovar is None
    assert genome_0014.subspecies is None
    assert genome_0014.source_info is None
    assert genome_0014.source_type is None
    assert genome_0014.collection_date is None
    assert genome_0014.host is None
    assert genome_0014.geographic_location is None
    assert genome_0014.ngs_info is None

    genome_0084 = dbsession.query(Genome).filter(Genome.name == '0084').first()
    assert genome_0084 is not None
    assert genome_0084.serovar == 'Typhimurium'

    assert genome_0084.source_type is None
    assert genome_0084.source_info is None
    assert genome_0084.collection_date is None
    assert genome_0084.host is None
    assert genome_0084.geographic_location is None
    assert genome_0084.ngs_info is not None


def test_load_clusters(dbsession):
    clusters_file_path = os.path.join('test_data', 'cgMLST_clusters-6genomes-test_data.csv')
    u = dbsession.query(User).filter(User.name == 'tester').first()

    db_loader = DefaultDataLoader(dbsession)
    db_loader.load_genome_clusters(u, clusters_file_path)

    genome_0006 = dbsession.query(Genome).filter(Genome.name == '0006').first()
    assert genome_0006 is not None
    assert genome_0006.cgmlst_clusters is not None
    assert len(genome_0006.cgmlst_clusters) == 100

    assert dbsession.query(Genome).filter(Genome.name == '29N').first() is None

    db_loader.load_genome_clusters(u, clusters_file_path, create_new_genome=True)
    genome_29N = dbsession.query(Genome).filter(Genome.name == '29N').first()
    assert genome_29N is not None
    assert len(genome_29N.cgmlst_clusters) == 100


def test_load_more_data(dbsession):
    metadata_file_path = os.path.join('test_data', 'genome_metadata-584.tsv')
    assert os.path.exists(metadata_file_path)
    default_data_loader = DefaultDataLoader(dbsession)

    u = dbsession.query(User).filter(User.name == 'tester').first()

    default_data_loader.load_default_genome_metadata(user=u, genome_metadata_file_path=metadata_file_path)

    assert dbsession.query(GeographicLocation).count() > 0
    assert dbsession.query(GeographicLocation).filter(GeographicLocation.country == 'USA').count() > 0
    assert dbsession.query(GenomeNGSInfo).count() > 0
    assert dbsession.query(GenomeNGSInfo).filter(GenomeNGSInfo.platform == '454').count() > 0
    assert dbsession.query(Host).count() > 0

    host_cow = dbsession.query(Host).filter(Host.common_name == 'cow').first()
    assert host_cow is not None
    assert host_cow.latin_name == 'Bos taurus'

    assert dbsession.query(Genome).count() == 584

    assembly_stats_file_path = os.path.join('test_data', 'assembly_stats-wide.tsv')
    assert os.path.exists(assembly_stats_file_path)
    assert dbsession.query(Genome).filter(Genome.quality_stats != None).count() == 0
    default_data_loader.load_assembly_stats(assembly_stats_file_path)
    # each genome should have some assembly quality stats
    assert dbsession.query(Genome).filter(Genome.quality_stats == None).count() == 0


def test_MistJSONParser_construction(dbsession):
    """
    Populate the DB with MIST test and marker info
    @param dbsession: SQLAlchemy DB session
    """
    u = dbsession.query(User).filter_by(name='tester').first()

    mist_json_path = os.path.join('test_data/mist-full.json')
    assert os.path.exists(mist_json_path)

    parser = MistJSONParser(dbsession, u, mist_json_path)
    assert dbsession.query(Genome).first() is not None

    parser.parse_mist_tests()
    assert dbsession.query(MistMarker).first() is not None
    marker = dbsession.query(MistMarker).first()
    assert marker.alleles.count() > 0

    assert dbsession.query(MistTest).first() is not None
    assert dbsession.query(MistTest).count() == 1
    test = dbsession.query(MistTest).first()
    assert dbsession.query(MistMarker).count() == test.markers.count()


def test_load_genome_mist_wgMLST_data(dbsession):
    """
    Parse MIST JSON output and populate DB with MIST data
    @param dbsession: SQLAlchemy DB session
    """
    u = dbsession.query(User).filter_by(name='tester').first()
    # test some MIST JSON output files that have given issues before
    troublesome_mist_jsons = ['test_data/wgMLST_330-63_H_87.json']
    test = dbsession.query(MistTest).first()
    for mist_json_path in troublesome_mist_jsons:
        assert os.path.exists(mist_json_path)
        parser = MistJSONParser(dbsession, u, mist_json_path)
        parser.parse_all_marker_results()
        genome = parser.genome
        num_mist_marker_results = dbsession.query(MistMarkerResult) \
            .filter(MistMarkerResult.genome_id == genome.id,
                    MistMarkerResult.test_id == test.id) \
            .count()
        assert num_mist_marker_results == 330

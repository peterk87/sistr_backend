===================================================
Mash MinHash in SISTR
===================================================

When a user uploads a genome, Mash is used to quickly search 55k NCBI RefSeq genomes for the top matches to that genome.
The Mash results can be used to determine if there is an issue with their genome (not the organism you think it is) or what the serovar might be if the genome is a *Salmonella* genome. 


The `Mash documentation`_ contains a lot of useful links and info about using MinHash for genomic distance estimation.




.. _Mash documentation: https://mash.readthedocs.io/en/latest/
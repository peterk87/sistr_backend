============================
Serovar Prediction in SISTR
============================

SISTR predicts the serovar of isolates from their WGS data using homology searching to O- and H-antigen related genes and cgMLST derived phylogenetic relationships to curated public genomes in the SISTR database.

.. warning::

    SISTR does not identify or report serovar variants requiring biochemical or sub-speciation tests for full characterization. SISTR infers a serovar which may require additional phenotypic information for final characterization in the following circumstances: 

    1. When multiple serovars have the same antigenic formula in the White-Kauffmann-Le Minor scheme [WKLM]_.
    2. When a partial antigenic formula is identified.
    3. For exceptions (eg : Choleraesuis/Paratyphi C/Typhisuis/Chiredzi, Sendai/Miami, Paratyphi B/Paratyphi B var. Java)

Serovar prediction exceptions
=============================

Due to the general difficulties in making phenotype predictions based on genotyping, there are a number of cases where SISTR cannot make an accurate serovar prediction and the prediction will rely on inference based on the cgMLST results.
This is especially true for biovar/pathovar predictions such as predicting whether a genome is `Paratyphi B` or `Paratyphi B var. Java`.
At most SISTR will try to make the base serovar prediction (`Paratyphi B` in the above example). 
It is up to the user to determine if they need further phenotypic discrimination.


O-antigen prediction
--------------------

SISTR is limited to prediction of somatic serogroups, which are a defined group of individual O antigenic factors as detailed by [WKLM]_. 
based on the results from BLAST searching for the wzx/wzy genes.
Based on the sequences of these genes, it is only possible to make a serogroup prediction and not an O-antigen prediction due to the lack of correlation between wzx/wzy genes and particular O-antigen combinations. 
The O-antigens can be inferred after serovar prediction by looking up what the O-antigens are that correspond to the serovar prediction.


Antigen-based serovar predictions
=================================

Antigen prediction in SISTR is done by *in silico* serogenotyping [SGSA]_ and homology searching of O- and H-antigen genes using nucleotide BLAST. 
The wzx and wzy genes are used to predict the serogroup.
The fliC gene is used for prediction of the H1 antigen, and the fljB gene is used for prediction of the H2 antigen.
For each antigen gene, only curated alleles are used for the antigen gene-based predictions. 


Ambiguity in antigen predictions
--------------------------------

For some O- or H-antigens predictions, there may be ambiguity in what the actual antigen should be due to high nucleotide similarity between alleles, thereby potentially leading to multiple possible serovar predictions based on the antigen predictions.
For example, if "g,m" is predicted for the H1 antigen, the actual H1 antigen could be "g,m,s" or "g,p" or various other g-complex antigens due to the high molecular similarity between the sequences for these antigens.

These ambiguities in antigen predictions are present for all antigens and are taken into account as best as possible in the serovar prediction logic.

Serogroup is predicted rather than the O-antigens since serogroup is as precise as you can get and even then there are ambiguities. 
For example, you can't easily tell apart the following serogroups from one another using molecular methods or nucleotide sequences for wzx and wzy:

- E1, E4
- A, D1, D2 (see :ref:`fig_wzx_A_D1_D2`)
- C1, F
- S, O62


So telling apart a '1,40' from a '40' (or other minor differences between O-antigens from the same serogroup) would be even harder than telling the above serogroups from one another. 
If there is a close relative in the database, then the cgMLST can potentially inform what the full serovar/antigenic formula is.

.. _fig_wzx_A_D1_D2:

.. figure:: figures/wzx-A,D1,D2-cluster.png
    :align: center

    Heatmap of BLAST percent identity of wzx gene alleles from genomes identified as part of serogroups A, D1 or D2. Alleles from different serogroups are very similar to one another (>99% identity).


Antigen-based serovar prediction using antigen gene homology searching
============================================================

For serogroup predictions, there are 2 genes used wzx and wzy.
Whichever gene gives the better result (longer and higher % identity), determines the serogroup prediction.
If both give perfect matches, then the wzx gene prediction is preferred since there is more antigen coverage and more alleles available.

The thresholds for making antigen predictions were determined through manual adjustments to maximize concordance with expected antigens for 3977 curated genomes. 
The reported metadata serovar for these genomes was curated using cgMLST cluster analysis and antigen-based serovar predictions. 


Serogroup thresholds for top BLAST result (highest bitscore) for wzx and wzy
-----------------------------------------------------------

- wzx
    - % identity >= 88.0%
    - length >= 400 bp
- wzy
    - % identity >= 88.0%
    - length >= 600 bp

.. warning::

    If the wzx or wzy BLAST result does not meet the above thresholds, the gene is considered missing. 


H1 fliC thresholds
------------------

1. Perfect match found (100% identity; 100% coverage)?
    - If found, then H1 antigen prediction is based on matching fliC allele
    - If not found then continue
2. Filter for ``X`` BLAST results with <= 25 mismatches and length >= 700 bp
    a. ``X`` empty? Then H1 call cannot be made from fliC gene; not enough sequence available or the BLAST result is too divergent from allele within the database to make an accurate prediction.
3. Filter for ``Y`` BLAST results with <= 5 mismatches and length >= 1000 bp
    - ``Y`` empty? Then the H1 prediction is the ``X`` result with the highest bitscore.
    - Otherwise, get the ``Y`` BLAST result with the least mismatches


H2 fljB thresholds
-------------------

1. Perfect match found (100% identity; 100% coverage)?
    - If found, then H2 antigen prediction is based on matching fljB allele
    - If not found, then continue
2. If the top BLAST result length is < 700 bp and the percent identity is < 88.0%, then the H2 antigen is predicted to be either missing or '-', othewise continue
3. Filter for ``X`` BLAST results with <= 50 mismatches and length >= 700 bp
    - ``X`` empty? H2 is predicted to be either missing or '-'
    - ``X`` not empty? continue
4. Filter for ``Y`` BLAST results with <= 5 mismatches and length >= 1000 bp
    - ``Y`` empty? Then the H2 prediction is the ``X`` result with the highest bitscore
    - Otherwise, get the ``Y`` BLAST result with least mismatches


Using antigen predictions to make serovar predictions
=====================================================

Antigen-based serovar predictions are made based on the WHO 2007 serovar table while accounting for antigen ambiguity.

The first 4 rows of the [WKLM]_ table:

======= ========= ======= ========= ======= ========= =================
Serovar O_antigen H1      H2        H_other Serogroup can_h2_be_missing
======= ========= ======= ========= ======= ========= =================
Aachen  17        z35     1,6               J         False
Aarhus  18        z4,z23  z64               K         False
Aba     6,8       i       e,n,z15           C2-C3     False
Abadina 28        g,m     [e,n,z15]         M         True
======= ========= ======= ========= ======= ========= =================

The serovar lookup is done based on the serogroup, H1 and H2 antigen predictions.
If H2 is '-' then the 'can_h2_be_missing' field can be True like with serovar Abadina in the above table.
Mutiple serovars could match the same antigens especially when antigen ambiguity is taken into account (e.g. a H1 g-complex antigen is predicted).
If the predicted antigens don't match an existing serovar in the WHO serovar table, then the antigen predicted serovar is reported as {serogroup}:{H1}:{H2} (like for Cathy's genomes).





.. [SGSA] 
    Franklin K, Lingohr EJ, Yoshida C, Anjum M, Bodrossy L, Clark CG, Kropinski AM, Karmali MA.
    Rapid genoserotyping tool for classification of Salmonella serovars.
    J Clin Microbiol. 2011 Aug;49(8):2954-65. doi: 10.1128/JCM.02347-10. Epub 2011 Jun 22.
    http://www.ncbi.nlm.nih.gov/pubmed/21697324
    
.. [WKLM]
    White-Kauffmann-Le Minor Scheme. 
    Antigenic Formulae of the Salmonella Serovars.
    9th edition (2007). 
    Patrick A.D. Grimont & François-Xavier Weill.
    PDF: :download:`WKLM-serovar-WHO-2007.pdf <data/WKLM-serovar-WHO-2007.pdf>`
    CSV: :download:`WKLM-serovar-WHO-2007.csv <../serovar_prediction_data/Salmonella-serotype_serogroup_antigen_table-WHO_2007.csv>`

    
=====================
SISTR Database Schema
=====================



Schema Diagram
==============

.. figure:: figures/schema-diagram.png
    :align: center

:download:`Download the schema diagram <figures/schema-diagram.png>`


SQLAlchemy Models
=================


.. contents::


.. _User:

User
--------

.. autoclass:: app.models.User
    :members:
    :undoc-members:


.. _UserRole:

UserRole
----------

.. autoclass:: app.models.UserRole
    :members:
    :undoc-members:

.. _Genome:

Genome
--------

.. autoclass:: app.models.Genome
    :members:
    :undoc-members:

.. _MistTest:

MistTest
--------

.. autoclass:: app.models.MistTest
    :members:
    :undoc-members:

.. _MistTestType:

MistTestType
----------

.. autoclass:: app.models.MistTestType
    :members:
    :undoc-members:

.. _MistMarker:

MistMarker
-----------

.. autoclass:: app.models.MistMarker
    :members:
    :undoc-members:


.. _MistAllele:

MistAllele
---------------------

.. autoclass:: app.models.MistAllele
    :members:
    :undoc-members:

.. _Host:

Host
---------------------

.. autoclass:: app.models.Host
    :members:
    :undoc-members:



.. _GeographicLocation:

GeographicLocation
---------------------

.. autoclass:: app.models.GeographicLocation
    :members:
    :undoc-members:

.. _SalmonellaSubspecies:

SalmonellaSubspecies
---------------------

.. autoclass:: app.models.SalmonellaSubspecies
    :members:
    :undoc-members:

.. _Contig:

Contig
---------------------

.. autoclass:: app.models.Contig
    :members:
    :undoc-members:

.. _GenomeNGSInfo:

GenomeNGSInfo
---------------------

.. autoclass:: app.models.GenomeNGSInfo
    :members:
    :undoc-members:

.. _MistMetadataResult:

MistMetadataResult
---------------------

.. autoclass:: app.models.MistMetadataResult
    :members:
    :undoc-members:

.. _MistMarkerResult:

MistMarkerResult
---------------------

.. autoclass:: app.models.MistMarkerResult
    :members:
    :undoc-members:


.. _WzxPrediction:

WzxPrediction
---------------------

.. autoclass:: app.models.WzxPrediction
    :members:
    :undoc-members:

.. _WzyPrediction:

WzyPrediction
---------------------

.. autoclass:: app.models.WzyPrediction
    :members:
    :undoc-members:

.. _SerogroupPrediction:

SerogroupPrediction
---------------------

.. autoclass:: app.models.SerogroupPrediction
    :members:
    :undoc-members:

.. _H1FliCPrediction:

H1FliCPrediction
---------------------

.. autoclass:: app.models.H1FliCPrediction
    :members:
    :undoc-members:

.. _H2FljBPrediction:

H2FljBPrediction
---------------------

.. autoclass:: app.models.H2FljBPrediction
    :members:
    :undoc-members:

.. _SerovarPrediction:

SerovarPrediction
---------------------

.. autoclass:: app.models.SerovarPrediction
    :members:
    :undoc-members:

.. _MashRefSeq

MashRefSeq
---------------------

.. autoclass:: app.models.MashRefSeq
    :members:
    :undoc-members:


TimestampMixin
---------------------

.. autoclass:: app.models.TimestampMixin
    :members:
    :undoc-members:

BlastResultMixin
---------------------

.. autoclass:: app.models.BlastResultMixin
    :members:
    :undoc-members:
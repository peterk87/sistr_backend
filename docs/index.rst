===========================
SISTR-backend documentation
===========================

The `Salmonella In Silico Typing Resource (SISTR) <http://lfz.corefacility.ca/sistr-app/>`_ is a web application for the rapid *in silico* typing and *in silico* serovar prediction of *Salmonella enterica* isolates using whole-genome sequence (WGS) data.
In addition, it allows various metadata-driven comparative genomic and epidemiological analyses through the `SISTR web application <https://bitbucket.org/peterk87/sistr-app>`_.

SISTR consists of a `Python (v2.7.9) <https://www.python.org/>`_ server application and `PostgreSQL (v9.4.1) <http://www.postgresql.org/>`_ database and a `ClojureScript (v0.0-3269) <https://github.com/clojure/clojurescript>`_ web application. 
The server app is implemented in Python using the `Flask web micro web framework (v0.10.1) <http://flask.pocoo.org/>`_ and communicates with the PostgreSQL database using `SQLAlchemy (v1.0.2) <http://www.sqlalchemy.org/>`_.
The server app exposes a REST API through which the user facing `SISTR web application <https://bitbucket.org/peterk87/sistr-app>`_ sends and receives data. 
It is also possible for other applications to send data to and from the server app through the REST API.
A `Celery distributed task queue (v3.1.18) <https://celery.readthedocs.org/en/latest/index.html>`_ is used for asynchronously running tasks such as in silico analyses on user uploaded genomes.

The source code for SISTR-backend is available on Bitbucket at:
https://bitbucket.org/peterk87/sistr_backend

This documentation is for the SISTR backend server application.
The documentation for the SISTR web application can be found at 
https://bitbucket.org/peterk87/sistr-app

********
Citation
********

Please cite as:


The *Salmonella In Silico* Typing Resource (SISTR): an open web-accessible tool for rapidly typing and subtyping draft *Salmonella* genome assemblies. Catherine Yoshida, Peter Kruczkiewicz, Chad R. Laing, Erika J. Lingohr, Victor P.J. Gannon, John H.E. Nash, Eduardo N. Taboada. PLoS ONE 11(1): e0147101. doi: 10.1371/journal.pone.0147101


Paper Link: http://journals.plos.org/plosone/article?id=10.1371/journal.pone.0147101

BibTeX:

.. code-block:: latex

    @article{Yoshida2016,
      doi = {10.1371/journal.pone.0147101},
      url = {http://dx.doi.org/10.1371/journal.pone.0147101},
      year  = {2016},
      month = {jan},
      publisher = {Public Library of Science ({PLoS})},
      volume = {11},
      number = {1},
      pages = {e0147101},
      author = {Catherine E. Yoshida and Peter Kruczkiewicz and Chad R. Laing and Erika J. Lingohr and Victor P. J. Gannon and John H. E. Nash and Eduardo N. Taboada},
      editor = {Michael Hensel},
      title = {The Salmonella In Silico Typing Resource ({SISTR}): An Open Web-Accessible Tool for Rapidly Typing and Subtyping Draft Salmonella Genome Assemblies},
      journal = {{PLOS} {ONE}}
    } 



Contents:
=========

.. toctree::
	:maxdepth: 3

	getting_started
	serovar_prediction
	mash
	rmlst
	db_schema
	rest_api
	development


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


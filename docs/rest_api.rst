=====================================================
SISTR RESTful Application Programming Interface (API)
=====================================================

The SISTR server app exposes a REST API for allowing other applications to
send and receive data from SISTR.
The SISTR web application communicates with the SISTR server app through this REST API.

.. note::

    All routes return data in JSON format.


API Routes
==========

.. autoflask:: runp-gunicorn:flask_app
    :include-empty-docstring:
    :endpoints:
    :order: path

